#include "./ephheadr.h"

struct site_struc
  {
    double latitude;
    double longitude;
    double elevation;
    double temp;
    double pressure;
  };


double moon_dip(int yr, int mn, int dy, int hr, int m, int s, struct site_struc site_data, double *phase) 
  {
      
    /* Based on PoCa code V1.1 August 14, 1992  (C) T. S. Trondsen */ 
    /* This code integrated into Polaris code Februrary 1999       */ 
      
      double lam, bet, hp;
      double lsn, rsn;
      double el;
      double deps, dpsi;
      double ra, dec;
      double ha;
      double aha, adec;
      double alt, az;
      double mjd, mjd_day;
      double utc, gst;
      double lst;
      double longitude, latitude, height;
      double temp, pressure;
      
    // longitude =-94.64;         /* neg = West */
    // latitude  =56.380;         /* North   */
    // height = 145;              /* meters  */
    // pressure = 1000;           /* mBar    */
    // temp = 10;                 /* Celsius */
      
      longitude = site_data.longitude;
      latitude = site_data.latitude;
      height = site_data.elevation;
      temp = site_data.temp;
      pressure = site_data.pressure;
      
      sex_dec (hr, m, s, &utc);	/* convert UT to decimal repr. */
    
      cal_mjd (mn, dy, yr, &mjd_day);	/* return modified Julian date */
    
      mjd = mjd_day + (utc / 24);	/* add UT time in fractions of a day */
    
      moon (mjd, &lam, &bet, &hp);	/* return geocentric ecl long, lat, hor parallax */
    
      nutation (mjd, &deps, &dpsi);	/* deps: nutat in obl, dpsi: nutat in long */
    
      lam += dpsi;		/* correct longitude (lam) for nutation */
    
      range (&lam, 2 * PI);
    
      ecl_eq (mjd, bet, lam, &ra, &dec);	/* convert to equatorial coords */
    
      utc_gst (mjd_day, utc, &gst);	/* find greenwich mean sidereal time, *gst */
    
      lst = gst + deghr (longitude);	/* now find local sidereal time */
    
      range (&lst, 24.0);	/* keep it within 24 h */
    
      ha = hrrad (lst) - ra;	/* find hour angle */
    
      latitude = degrad (latitude);	/* convert latitude to radians */
    
      height /= 6378140.0;	/* heigh as a fraction of earth radius */
    
      ta_par (ha, dec, latitude, height, hp, &aha, &adec); /* correct for geoc parllx */
    
      hadec_aa (latitude, aha, adec, &alt, &az);	/* convert to horizon coords */
    
      refract (pressure, temp, alt, &alt);	/* correct for refraction. */
    
#ifdef DEBUG
      printf ("Lunar ephemeris:\n\r");
      printf ("-------------------------------------\n\r");
      printf ("julian date %f \n\r", mjd + 2415020.0);
      printf ("ecl long: %f, ecl lat: %f \n\r", lam, bet);
      printf ("right asc: %f, decl: %f \n\r", radhr (ra), raddeg (dec));
      printf ("local sidereal time: %f\n\r", lst);
      printf ("hour angle: %f\n\r", ha);
      printf ("moon angle - alt = %f,  az = %f\n\r", raddeg (alt), raddeg (az));
#endif    

      sunpos (mjd, &lsn, &rsn);	/* lsn: solar ecl long */
    
      range (&lsn, 2 * PI);
    
      elongation (lam, bet, lsn, &el);
    
      *phase = fabs (el) / PI * 100.0;
    
#ifdef DEBUG
      printf ("moon phase = %f percent.\n\r", *phase);
#endif    
      
      return (raddeg (alt));
      
  }
