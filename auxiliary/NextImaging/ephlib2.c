#include "./ephheadr.h"

#define	EQtoECL	1
#define	ECLtoEQ	(-1)

/* given the modified Julian date, mjd, and an equitorial ra and dec, each in
 * radians, find the corresponding geocentric ecliptic latitude, *lat, and
 * longititude, *lng, also each in radians.
 * correction for the effect on the angle of the obliquity due to nutation is
 * included.
 */

void eq_ecl ( double mjd, double ra, double dec, double *lat, double *lng )
{
	ecleq_aux (EQtoECL, mjd, ra, dec, lng, lat);
}

/* given the modified Julian date, mjd, and a geocentric ecliptic latitude,
 * *lat, and longititude, *lng, each in radians, find the corresponding
 * equitorial ra and dec, also each in radians.
 * correction for the effect on the angle of the obliquity due to nutation is
 * included.
 */

void ecl_eq ( double mjd, double lat, double lng, double *ra, double *dec )
{
	ecleq_aux (ECLtoEQ, mjd, lng, lat, ra, dec);
}

void static
ecleq_aux ( int sw, double mjd, double x, double y, double *p, double *q )
/* sw = +1 for eq to ecliptic, -1 for vv. */
/* sw==1: x==ra, y==dec.  sw==-1: x==lng, y==lat. */
/* sw==1: p==lng, q==lat. sw==-1: p==ra, q==dec. */
{
	static double lastmjd = -10000;	/* last mjd calculated */
	static double seps, ceps;	/* sin and cos of mean obliquity */
	double sx, cx, sy, cy, ty;

	if (mjd != lastmjd) {
	    double eps;
	    double deps, dpsi;
	    obliquity (mjd, &eps);		/* mean obliquity for date */
	    nutation (mjd, &deps, &dpsi);
	    eps += deps;
    	    seps = sin(eps);
	    ceps = cos(eps);
	    lastmjd = mjd;
	}

	sy = sin(y);
	cy = cos(y);				/* always non-negative */
        if (fabs(cy)<1e-20) cy = 1e-20;		/* insure > 0 */
        ty = sy/cy;
	cx = cos(x);
	sx = sin(x);
        *q = asin((sy*ceps)-(cy*seps*sx*sw));
        *p = atan(((sx*ceps)+(ty*seps*sw))/cx);
        if (cx<0) *p += PI;		/* account for atan quad ambiguity */
	range (p, 2*PI);
}

/* given latitude (n+, radians), lat, altitude (up+, radians), alt, and
 * azimuth (angle round to the east from north+, radians),
 * return hour angle (radians), ha, and declination (radians), dec.
 */

void aa_hadec ( double lat, double alt, double az, double *ha, double *dec )
{
	aaha_aux (lat, az, alt, ha, dec);
}

/* given latitude (n+, radians), lat, hour angle (radians), ha, and declination
 * (radians), dec,
 * return altitude (up+, radians), alt, and
 * azimuth (angle round to the east from north+, radians),
 */

void hadec_aa ( double lat, double ha, double dec, double *alt, double *az )
{
	aaha_aux (lat, ha, dec, az, alt);
}

/* the actual formula is the same for both transformation directions so
 * do it here once for each way.
 * N.B. all arguments are in radians.
 */

void static
aaha_aux ( double lat, double x, double y, double *p, double *q )
{
	static double lastlat = -1000.;
	static double sinlastlat, coslastlat;
	double sy, cy;
	double sx, cx;
	double sq, cq;
	double a;
	double cp;

	/* latitude doesn't change much, so try to reuse the sin and cos evals.
	 */
	if (lat != lastlat) {
	    sinlastlat = sin (lat);
	    coslastlat = cos (lat);
	    lastlat = lat;
	}

	sy = sin (y);
	cy = cos (y);
	sx = sin (x);
	cx = cos (x);

/* define GOODATAN2 if atan2 returns full range -PI through +PI.
 */
#ifdef GOODATAN2
	*q = asin ((sy*sinlastlat) + (cy*coslastlat*cx));
	*p = atan2 (-cy*sx, -cy*cx*sinlastlat + sy*coslastlat);
#else
#define	EPS	(1e-20)
	sq = (sy*sinlastlat) + (cy*coslastlat*cx);
	*q = asin (sq);
	cq = cos (*q);
	a = coslastlat*cq;
	if (a > -EPS && a < EPS)
	    a = a < 0 ? -EPS : EPS; /* avoid / 0 */
	cp = (sy - (sinlastlat*sq))/a;
	if (cp >= 1.0)	/* the /a can be slightly > 1 */
	    *p = 0.0;
	else if (cp <= -1.0)
	    *p = PI;
	else
	    *p = acos ((sy - (sinlastlat*sq))/a);
	if (sx>0) *p = 2.0*PI - *p;
#endif
}

/* given a modified julian date, mjd, and a universally coordinated time, utc,
 * return greenwich mean siderial time, *gst.
 */

void utc_gst ( double mjd, double utc, double *gst ) 
{
	double tnaught();
	static double lastmjd = -10000;
	static double t0;

	if (mjd != lastmjd) {
	    t0 = tnaught (mjd);
	    lastmjd = mjd;
	}
	*gst = (1.0/SIDRATE)*utc + t0;
	range (gst, 24.0);
}

/* given a modified julian date, mjd, and a greenwich mean siderial time, gst,
 * return universally coordinated time, *utc.
 */

void gst_utc ( double mjd, double gst, double *utc )
{
	double tnaught();
	static double lastmjd = -10000;
	static double t0;

	if (mjd != lastmjd) {
	    t0 = tnaught (mjd);
	    range (&t0, 24.0);
	    lastmjd = mjd;
	}
	*utc = gst - t0;
	range (utc, 24.0);
	*utc *= SIDRATE;
}

static double
tnaught ( double mjd )
/* mjd = julian days since 1900 jan 0.5 */
{
	double dmjd;
	int m, y;
	double d;
	double t, t0;

	mjd_cal (mjd, &m, &d, &y);
	cal_mjd (1, 0., y, &dmjd);
	t = dmjd/36525;
	t0 = 6.57098e-2 * (mjd - dmjd) - 
	     (24 - (6.6460656 + (5.1262e-2 + (t * 2.581e-5))*t) -
		   (2400 * (t - (((double)y - 1900)/100))));
	return (t0);
}

/* given true ha and dec, tha and tdec, the geographical latitude, phi, the
 * height above sea-level (as a fraction of the earths radius, 6378.16km),
 * ht, and the equatorial horizontal parallax, ehp, find the apparent
 * ha and dec, aha and adec allowing for parallax.
 * all angles in radians. ehp is the angle subtended at the body by the
 * earth's equator.
 */

void ta_par ( double tha, double tdec, double phi, double ht, double ehp, 
		double *aha, double *adec )
{
	static double last_phi, last_ht, rsp, rcp;
	double rp;	/* distance to object in Earth radii */
	double ctha;
	double stdec, ctdec;
	double tdtha, dtha;
	double caha;

	/* avoid calcs involving the same phi and ht */
	if (phi != last_phi || ht != last_ht) {
	    double cphi, sphi, u;
	    cphi = cos(phi);
	    sphi = sin(phi);
	    u = atan(9.96647e-1*sphi/cphi);
	    rsp = (9.96647e-1*sin(u))+(ht*sphi);
	    rcp = cos(u)+(ht*cphi);
	    last_phi  =  phi;
	    last_ht  =  ht;
	}

        rp = 1/sin(ehp);

        ctha = cos(tha);
	stdec = sin(tdec);
	ctdec = cos(tdec);
        tdtha = (rcp*sin(tha))/((rp*ctdec)-(rcp*ctha));
        dtha = atan(tdtha);
	*aha = tha+dtha;
	caha = cos(*aha);
	range (aha, 2*PI);
        *adec = atan(caha*(rp*stdec-rsp)/(rp*ctdec*ctha-rcp));
}

/* given the apparent ha and dec, aha and adec, the geographical latitude, phi,
 * the height above sea-level (as a fraction of the earths radius, 6378.16km),
 * ht, and the equatorial horizontal parallax, ehp, find the true ha and dec,
 * tha and tdec allowing for parallax.
 * all angles in radians. ehp is the angle subtended at the body by the
 * earth's equator.
 * uses ta_par() iteratively: find a set of true ha/dec that converts back
  *  to the given apparent ha/dec.
 */

void at_par ( double aha, double adec, double phi, double ht, double ehp, 
		double *tha, double *tdec )
{
	double nha, ndec;	/* ha/dec corres. to current true guesses */
	double eha, edec;	/* error in ha/dec */

	/* first guess for true is just the apparent */
	*tha = aha;
	*tdec = adec;

	while (1) {
	    ta_par (*tha, *tdec, phi, ht, ehp, &nha, &ndec);
	    eha = aha - nha;
	    edec = adec - ndec;
	    if (fabs(eha)<1e-6 && fabs(edec)<1e-6)
		break;
	    *tha += eha;
	    *tdec += edec;
	}
}


/* given hours (or degrees), hd, minutes, m, and seconds, s, 
 * return decimal hours (or degrees), *d.
 * in the case of hours (angles) < 0, only the first non-zero element should
 *   be negative.
 */

void sex_dec ( int hd, int m, int s, double *d )
{
	int sign = 1;

	if (hd < 0) {
	    sign = -1;
	    hd = -hd;
	} else if (m < 0) {
	    sign = -1;
	    m = -m;
	} else if (s < 0) {
	    sign = -1;
	    s = -s;
	}

	*d = (((double)s/60.0 + (double)m)/60.0 + (double)hd) * sign;
}

/* given decimal hours (or degrees), d.
 * return nearest hours (or degrees), *hd, minutes, *m, and seconds, *s, 
 * each always non-negative; *isneg is set to 1 if d is < 0, else to 0.
 */

void dec_sex ( double d, int *hd, int *m, int *s, int *isneg )
{
	double min;

	if (d < 0) {
	    *isneg = 1;
	    d = -d;
	} else
	    *isneg = 0;

	*hd = (int)d;
	min = (d - *hd)*60.;
	*m = (int)min;
	*s = (int)((min - *m)*60. + 0.5);

	if (*s == 60) {
	    if ((*m += 1) == 60) {
		*hd += 1;
		*m = 0;
	    }
	    *s = 0;
	}
	/* no  negative 0's */
	if (*hd == 0 && *m == 0 && *s == 0)
	    *isneg = 0;
}

/* insure 0 <= *v < r.
 */

void range ( double *v, double r )
{
	while (*v <  0) *v += r;
	while (*v >= r) *v -= r;
}

/* correct the true altitude, ta, for refraction to the apparent altitude, aa,
 * each in radians, given the local atmospheric pressure, pr, in mbars, and
 * the temperature, tr, in degrees C.
 */

void refract ( double pr, double tr, double ta, double *aa )
{
	double r;	/* refraction correction*/

        if (ta >= degrad(15.)) {
	    /* model for altitudes at least 15 degrees above horizon */
            r = 7.888888e-5*pr/((273+tr)*tan(ta));
	} else if (ta > degrad(-5.)) {
	    /* hairier model for altitudes at least -5 and below 15 degrees */
	    double a, b, tadeg = raddeg(ta);
	    a = ((2e-5*tadeg+1.96e-2)*tadeg+1.594e-1)*pr;
	    b = (273+tr)*((8.45e-2*tadeg+5.05e-1)*tadeg+1);
	    r = degrad(a/b);
	} else {
	    /* do nothing if more than 5 degrees below horizon.
	     */
	    r = 0;
	}

	*aa  =  ta + r;
}

/* correct the apparent altitude, aa, for refraction to the true altitude, ta,
 * each in radians, given the local atmospheric pressure, pr, in mbars, and
 * the temperature, tr, in degrees C.
 */

void unrefract ( double pr, double tr, double aa, double *ta )
{
	double err;
	double appar;
	double true;

	/* iterative solution: search for the true that refracts to the
	 *   given apparent.
	 * since refract() is discontinuous at -5 degrees, there is a range
	 *   of apparent altitudes between about -4.5 and -5 degrees that are
	 *   not invertable (the graph of ap vs. true has a vertical step at
	 *   true = -5). thus, the iteration just oscillates if it gets into
	 *   this region. if this happens the iteration is forced to abort.
	 *   of course, this makes unrefract() discontinuous too.
	 */
	true = aa;
	do {
	    refract (pr, tr, true, &appar);
	    err = appar - aa;
	    true -= err;
	} while (fabs(err) >= 1e-6 && true > degrad(-5));

	*ta = true;
}

/* given geocentric ecliptic longitude and latitude, lam and bet, of some object
 * and the longitude of the sun, lsn, find the elongation, el. this is the
 * actual angular separation of the object from the sun, not just the difference
 * in the longitude. the sign, however, IS set simply as a test on longitude
 * such that el will be >0 for an evening object <0 for a morning object.
 * to understand the test for el sign, draw a graph with lam going from 0-2*PI
 *   down the vertical axis, lsn going from 0-2*PI across the hor axis. then
 *   define the diagonal regions bounded by the lines lam=lsn+PI, lam=lsn and
 *   lam=lsn-PI. the "morning" regions are any values to the lower left of the
 *   first line and bounded within the second pair of lines.
 * all angles in radians.
 */

void elongation ( double lam, double bet, double lsn, double *el )
{
	*el = acos(cos(bet)*cos(lam-lsn));
	if (lam>lsn+PI || lam>lsn-PI && lam<lsn) *el = - *el;
}


/* given the modified JD, mjd, return the true geocentric ecliptic longitude
 *   of the sun for the mean equinox of the date, *lsn, in radians, and the
 *   sun-earth distance, *rsn, in AU. (the true ecliptic latitude is never more
 *   than 1.2 arc seconds and so may be taken to be a constant 0.)
 * if the APPARENT ecliptic longitude is required, correct the longitude for
 *   nutation to the true equinox of date and for aberration (light travel time,
 *   approximately  -9.27e7/186000/(3600*24*365)*2*pi = -9.93e-5 radians).
 */

void sunpos ( double mjd, double *lsn, double *rsn )
{
	double t, t2;
	double ls, ms;    /* mean longitude and mean anomoay */
	double s, nu, ea; /* eccentricity, true anomaly, eccentric anomaly */
	double a, b, a1, b1, c1, d1, e1, h1, dl, dr;

	t = mjd/36525.;
	t2 = t*t;
	a = 100.0021359*t;
	b = 360.*(a-(long)a);
	ls = 279.69668+.0003025*t2+b;
	a = 99.99736042000039*t;
	b = 360*(a-(long)a);
	ms = 358.47583-(.00015+.0000033*t)*t2+b;
	s = .016751-.0000418*t-1.26e-07*t2;
	anomaly (degrad(ms), s, &nu, &ea);
	a = 62.55209472000015*t;
	b = 360*(a-(long)a);
	a1 = degrad(153.23+b);
	a = 125.1041894*t;
	b = 360*(a-(long)a);
	b1 = degrad(216.57+b);
	a = 91.56766028*t;
	b = 360*(a-(long)a);
	c1 = degrad(312.69+b);
	a = 1236.853095*t;
	b = 360*(a-(long)a);
	d1 = degrad(350.74-.00144*t2+b);
	e1 = degrad(231.19+20.2*t);
	a = 183.1353208*t;
	b = 360*(a-(long)a);
	h1 = degrad(353.4+b);
	dl = .00134*cos(a1)+.00154*cos(b1)+.002*cos(c1)+.00179*sin(d1)+
								.00178*sin(e1);
	dr = 5.43e-06*sin(a1)+1.575e-05*sin(b1)+1.627e-05*sin(c1)+
					    3.076e-05*cos(d1)+9.27e-06*sin(h1);
	*lsn = nu+degrad(ls-ms+dl);
	*rsn = 1.0000002*(1-s*cos(ea))+dr;
	range (lsn, 2*PI);
}

/* corrects ra and dec, both in radians, for precession from epoch 1 to epoch 2.
 * the epochs are given by their modified JDs, mjd1 and mjd2, respectively.
 * N.B. ra and dec are modifed IN PLACE.
 * TODO: find a better algorithm; this one is not even symmetric.
 */

void precess (double mjd1, double mjd2, double *ra, double *dec)
/* initial and final epoch modified JDs */
/* ra/dec for mjd1 in, for mjd2 out */
{
	static double lastmjd1 = -10000, lastmjd2 = -10000;
	static double m, n, nyrs;
	double dra, ddec;	/* ra and dec corrections */

	if (mjd1 != lastmjd1 || mjd2 != lastmjd2) {
	    double t1, t2; /* Julian centuries of 36525 days since Jan .5 1900*/
	    double m1, n1; /* "constants" for t1 */
	    double m2, n2; /* "constants" for t2 */
	    t1 = mjd1/36525.;
	    t2 = mjd2/36525.;
	    m1 = 3.07234+(1.86e-3*t1);
	    n1 = 20.0468-(8.5e-3*t1);
	    m2 = 3.07234+(1.86e-3*t2);
	    n2 = 20.0468-(8.5e-3*t2);
	    m = (m1+m2)/2;	/* average m for the two epochs */
	    n = (n1+n2)/2;	/* average n for the two epochs */
	    nyrs = (mjd2-mjd1)/365.2425;
	    lastmjd1 = mjd1;
	    lastmjd2 = mjd2;
	}

	dra = (m+(n*sin(*ra)*tan(*dec)/15))*7.272205e-5*nyrs;
	ddec = n*cos(*ra)*4.848137e-6*nyrs;
	*ra += dra;
	*dec += ddec;
	/* added by ECD */
	if (*dec > PI/2) {
	    *dec = PI - *dec;
	    *ra += PI;
	} else if (*dec < -PI/2) {
	    *dec = -PI - *dec;
	    *ra += PI;
	}
	range (ra, 2*PI);
}
