#include <errno.h>
#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define TRUE            1
#define FALSE           0

//#define DEBUG

/* SITE PARAMETERS                      */

struct site_struc {
        double latitude;
        double longitude;
        double elevation; /* meters */
        double temp; /* C */
        double pressure; /* hPa */
};
struct site_struc site_data={51.81, 103.08, 1000.0, -30.00, 1010.00}; /* TORY */

/* Max angles at which imaging should be done   */
/*						*/
/*  FYI:					*/
/*   Civil Twilight: 6 deg			*/
/*   Nautical Twilight: 6-12 deg		*/
/*   Astronomical Twilight: 12-18 deg below hor	*/
/*						*/
#define MAX_SUN_DIP -12.0   	  /* -12.0 is reasonable, 90 to always operate */
#define MAX_MOON_DIP 90.0    /* 0.0 is reasonable, use 90 to always operate */     
#define MAX_MOON_PHASE 1.0    /* 0.1 is reasonable, use 1.0 to always operate */

/* -- EPHEMERIDES CALCULATIONS */

extern double sun_dip(int year, int month, int day, int hour, int minute, int second, struct site_struc site_data);
extern double moon_dip(int year, int month, int day, int hour, int minute, int second, struct site_struc site_data, double *);

int f_sun_present(struct tm *time_now){
  double sun_angle;

  /* Return TRUE if the sun angle is > maximum allowed sun dip */

  sun_angle =sun_dip(1900+time_now->tm_year,1+time_now->tm_mon,
		    time_now->tm_mday,time_now->tm_hour,time_now->tm_min,
  		    time_now->tm_sec, site_data);

#ifdef DEBUG
  fprintf(stderr," solar alt = %f\n",sun_angle);
  if (sun_angle>MAX_SUN_DIP)
     fprintf(stderr,"     => SUN PRESENT = TRUE\n");
  else   
     fprintf(stderr,"     => SUN PRESENT = FALSE\n");
#endif

  return((sun_angle>MAX_SUN_DIP) ? TRUE : FALSE); 

}

int f_moon_present(struct tm *time_now){
  double moon_angle, moon_phase;

  /* Return TRUE if the lunar depression angle is greater than MAX_MOON_DIP
     AND lunar phase fraction is greater than MAX_MOON_PHASE 	*/

  moon_angle =moon_dip(1900+time_now->tm_year,1+time_now->tm_mon,
		    time_now->tm_mday,time_now->tm_hour,time_now->tm_min,
  		    time_now->tm_sec, site_data, &moon_phase);

#ifdef DEBUG
  fprintf(stderr," lunar alt = %f\n",moon_angle);
  fprintf(stderr," lunar ph  = %f\n",moon_phase);
  if (moon_angle>MAX_MOON_DIP && moon_phase>MAX_MOON_PHASE)
     fprintf(stderr,"     => MOON PRESENT AND PHASE GT .1 = TRUE\n");
  else   
     fprintf(stderr,"     => MOON PRESENT AND PHASE GT .1 = FALSE\n");
#endif

  return(((moon_angle>MAX_MOON_DIP && moon_phase>MAX_MOON_PHASE)) ? TRUE : FALSE); 

}

int main(int argc, char *argv[])
{
  struct sigaction act,old_act; 
  struct tm *time_now;
  time_t mytimenow;    
  int    i, ret, sec, min;
  int    sun_present, moon_present;
  char   buffer[32]        ={0}; 


  mytimenow 		=time(NULL);
  time_now 		=gmtime(&mytimenow);

  while(1) { 

    ++mytimenow;
    time_now=gmtime(&mytimenow);
    min =time_now->tm_min;
    sec =time_now->tm_sec;


    /* Update status flags */

    sun_present  =f_sun_present(time_now);
    moon_present =f_moon_present(time_now);

    if(sun_present || moon_present) { 
          continue;
    }

	
    strftime(buffer,27,"%d-%m-%Y at %H:%M:%S UTC",time_now);
    printf("  Next imaging period for sun_dip of %f to commence on %s  \n",MAX_SUN_DIP,buffer);
    exit(0);  /* Zzzzz */

  }


  exit(0);
}
