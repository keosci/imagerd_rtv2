#include <errno.h>
#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define TRUE            1
#define FALSE           0

/* SITE PARAMETERS                      */
/*                                      */
/*   - GILL = Gillam                    */
/*   - RESO = Resolute Bay              */
/*   - RANK = Rankin Inlet              */
/*   - LYR  = Longyearbyen              */
/*   - NYA  = Ny Aalesund               */
/*   - ATHA = Athabasca, Alberta        */

struct site_struc {
        double latitude;
        double longitude;
        double elevation;
        double temp;
        double pressure;
};
// struct site_struc site_data ={74.69, -95.00, 67.00, -25.00, 1010.00};  /* RESU */
// struct site_struc site_data ={62.82, -92.11, 19.00, -25.00, 1010.00};  /* RANK */
// struct site_struc site_data ={56.38, -94.64, 145.00, -25.00, 1010.00};  /* GILL */
// struct site_struc site_data ={54.72, -113.28, 10.00, -25.00, 1010.00};  /* ATHA */
// struct site_struc site_data ={50.20, -96.04, 275.00, -25.00, 1010.00};  /* PINA */


/* Max angles at which imaging should be done   */
/*						*/
/*  FYI:					*/
/*   Civil Twilight: 6 deg			*/
/*   Nautical Twilight: 6-12 deg		*/
/*   Astronomical Twilight: 12-18 deg below hor	*/
/*						*/
#define MAX_SUN_DIP -12.0   	  /* -12.0 is reasonable, 90 to always operate */
#define MAX_MOON_DIP 90.0    /* 0.0 is reasonable, use 90 to always operate */     
#define MAX_MOON_PHASE 1.0    /* 0.1 is reasonable, use 1.0 to always operate */

/* -- EPHEMERIDES CALCULATIONS */

extern double sun_dip(int year, int month, int day, int hour, int minute, int second, struct site_struc site_data);
extern double moon_dip(int year, int month, int day, int hour, int minute, int second, struct site_struc site_data, double *);

int f_sun_present(struct tm *time_now){
  double sun_angle;

  /* Return TRUE if the sun angle is > maximum allowed sun dip */

  sun_angle =sun_dip(1900+time_now->tm_year,1+time_now->tm_mon,
		    time_now->tm_mday,time_now->tm_hour,time_now->tm_min,
  		    time_now->tm_sec, site_data);

  printf("Solar Elevation Angle = %4.2f ",sun_angle);
  if (sun_angle>MAX_SUN_DIP)
     printf("            =>  Sun Present = TRUE\n");
  else   
     printf("            =>  Sun Present = FALSE\n");

  return((sun_angle>MAX_SUN_DIP) ? TRUE : FALSE); 

}

int f_moon_present(struct tm *time_now){
  double moon_angle, moon_phase;

  /* Return TRUE if the lunar depression angle is greater than MAX_MOON_DIP
     AND lunar phase fraction is greater than MAX_MOON_PHASE 	*/

  moon_angle =moon_dip(1900+time_now->tm_year,1+time_now->tm_mon,
		    time_now->tm_mday,time_now->tm_hour,time_now->tm_min,
  		    time_now->tm_sec, site_data, &moon_phase);

  printf("Lunar Elevation = %4.2f and ",moon_angle);
  printf("Phase = %3.1f% ",moon_phase);
  if (moon_angle>MAX_MOON_DIP && moon_phase>MAX_MOON_PHASE)
     printf(" => Moon Present = TRUE\n");
  else   
     printf(" => Moon Present = FALSE\n");

  return(((moon_angle>MAX_MOON_DIP && moon_phase>MAX_MOON_PHASE)) ? TRUE : FALSE); 

}

int main(int argc, char *argv[])
{
  struct sigaction act,old_act; 
  struct tm *time_now;
  time_t mytimenow;    
  int    i, ret, sec, min;
  int    sun_present, moon_present;
  int    old_sun_status, old_moon_status;
  char   buffer[25]        ={0}; 


  mytimenow 		=time(NULL);
  time_now 		=gmtime(&mytimenow);

    min =time_now->tm_min;
    sec =time_now->tm_sec;
    strftime(buffer,24,"%d-%m-%Y %H:%M:%S",time_now);
    printf("\n\nCURRENT STATUS AT PINAWA [ %s UT ]\n",buffer);
    printf("-----------------------------------------------------------------\n");

  old_sun_status  	=f_sun_present(time_now);
  old_moon_status	=f_moon_present(time_now);


  if(old_sun_status || old_moon_status)  /* If sun or moon present */
    printf("\nOne or more of the above conditions are TRUE. Imaging is pending.\n\n\n"); 
  else {
    printf("\nBoth of the above conditions are FALSE. The camera is imaging.\n\n\n"); }



}
