#ifndef PI
#define	PI		3.141592653589793
#endif
#define	TWOPI	(2*PI)

#define	degrad(x)	((x)*PI/180.)
#define	raddeg(x)	((x)*180./PI)
#define	hrdeg(x)	((x)*15.)
#define	deghr(x)	((x)/15.)
#define	hrrad(x)	degrad(hrdeg(x))
#define	radhr(x)	deghr(raddeg(x))
#define	SIDRATE		.9972695677

#include <stdio.h>
#include <math.h>

/* Function prototypes for EPHLIB functions. */

void obliquity ( double, double * );

void nutation ( double, double *, double * );

void cal_mjd ( int, double, int, double * );

void mjd_cal ( double, int *, double *, int * );

void mjd_dow ( double, double * );

void mjd_dpm ( double, int * );

void mjd_year ( double, double * );

void year_mjd ( double, double * );

void anomaly ( double, double, double *, double * );

void moon ( double, double *, double *, double * );

void eq_ecl ( double, double, double, double *, double * );

void ecl_eq ( double, double, double, double *, double * );

void static
ecleq_aux ( int, double, double, double, double *, double * );

void aa_hadec ( double, double, double, double *, double * );

void hadec_aa ( double, double, double, double *, double * );

void static
aaha_aux ( double lat, double x, double y, double *p, double *q );

void utc_gst ( double, double, double * );

void gst_utc ( double, double, double * );

static double
tnaught ( double );

void ta_par ( double, double, double, double, double, 
		double *, double * );

void at_par ( double, double, double, double, double, 
		double *, double * );

void sex_dec ( int, int, int, double * );

void dec_sex ( double, int *, int *, int *, int * );

void range ( double *, double );

void refract ( double, double, double, double * );

void unrefract ( double, double, double, double * );

void elongation ( double, double, double, double * );

void sunpos ( double, double *, double * );

void precess (double mjd1, double mjd2, double *ra, double *dec);

void do_starpos( void );

typedef struct {
      char Name[13];
      double Magnitude;
      char PopName[11];
      double RA;
      double DEC;
      double Epoch;
      double AZ;
      double ZenithAng;
      } Starstruc;

#define NUMBER_OF_STARS 14

