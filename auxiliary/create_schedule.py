#!/usr/bin/env python
# -*- coding: utf-8 -*-
#Schedule Initialization Program
#Version 1.0
#Written by Jarrett Little


import sys
import re
import glob
import os
import time

# Global variables within this module
Debug = False

def find_all_devices():
	device = []
	file_list = sorted(glob.glob('../config/imager/*'))
	for filename in file_list:
		f = open(filename,'r')
		for line in f:
			if line[:12] == 'device ID = ':
				#print line[19:-1]
				device.append(line[12:-1])
		f.close()
	return device

def select_device(devices):
	local_flag = None
	device = None
	flag = raw_input("Use attached imager device? (yes | no): ")
	if flag == "yes":
		f = open('../config/imager/imager.conf','r')
		for line in f:
			if line[:12] == 'device ID = ':
				#print line[19:-1]
				device = line[12:-1]
				local_flag = 1
				print "\nYou have selected device = " + device + "\n"
				return(device,local_flag)
	elif flag == "no":
		print "Select the desired device from the list below using its corresponding number:\n"
		for i in xrange(len(devices)):
			print str(i) + " = " + devices[i]
		selected_device = raw_input("\nDevice: ")
		try:
			if int(selected_device) < 0 or int(selected_device) > len(devices)-1:
				print "This device does not exist: Please select again"
				return(select_device(devices),0)
			print "You have selected: " + devices[int(selected_device)]
			flag = raw_input("yes | no : ")
			if flag == "no":
				return(select_device(devices),0)
			elif flag == "yes":
				local_flag = 0
				return(devices[int(selected_device)],local_flag)
			else:
				print "Only use \"yes\" or \"no\""
				return(select_device(devices),0)
		except ValueError:
			print "Only use \"yes\" or \"no\""
			return(select_device(devices),0)
	else:
		print "Only use \"yes\" or \"no\""
		return(select_device(devices),0)



def select_mode():
	print "\nSelect the desired mode from the list below using its corresponding number:\n"
	print "0 = normal (scheduled mode)"
	selected_mode = raw_input("\nMode: ")
	try:
		if int(selected_mode) < 0 or int(selected_mode) > 1:
			print "This mode does not exist: Please select again"
			return(select_mode())
	except ValueError:
		print "Please enter either 0 for normal"
		return(select_mode())
	return(int(selected_mode))

def zenith_angle():
	print "\nEnter the desired starting Sun Zenith Angle [deg]:\n"
	angle = raw_input("\nStarting Sun Zenith Angle [deg]: ")
	try:
		float(angle)
	except ValueError:
		print "This value is not a number. Please input a proper sun zenith angle in degrees\n"
		return(zenith_angle())
	if float(angle) < -90 or float(angle) > 90:
		print "Angle is out of range. Enter value between -90 and 90 [deg]\n"
		return(zenith_angle()) 
	print "You have entered: " + angle + " [deg]"
	flag = raw_input("yes | no : ")
	if flag == "no":
		return(zenith_angle())
	elif flag == "yes":
		return(angle)
	else:
		print "Only use \"yes\" or \"no\""
		return(zenith_angle())

def enter_sched_len():
	print "\nEnter the desired schedule period in milliseconds:\n"
	print "Examples: [15000] or [30000] or [60000] or [120000]\n"
	sch_len = raw_input("\nSchedule Period [ms]: ")
	try:
		if int(sch_len) < 5000 or int(sch_len) > 240000:
			print "\nInvalid schedule period. Please enter another value\n"
			return(enter_sched_len())
	except ValueError:
		print "\nInvalid schedule period. Please enter another value\n"
		return(enter_sched_len())
	print "You have selected: " + sch_len + " [ms]"
	flag = raw_input("yes | no : ")
	if flag == "no":
		return(enter_sched_len())
	elif flag == "yes":
		return(sch_len)
	else:
		print "Only use \"yes\" or \"no\""
		return(enter_sched_len())


def enter_prep():
	print "\nEnter the desired imager overhead time in ms:"
	print "Overhead time is the amount of time that the imager needs to generate the captured image\nand move the filter wheel to the next location\n"
	print "Leave blank for default overhead time\n"
	prep_time = raw_input("\nImager Overhead Time [Default=2000 ms]: ")
	try:
		if int(prep_time) < 500:
			print "Imager Overhead time to short. Please enter a larger time\n"
			return(enter_prep())
		elif int(prep_time) >= 500:
			return(prep_time)
	except ValueError:
		if prep_time == '':
			prep_time = '2000'
			return(prep_time)
		else:
			print "Time not recognized. Please enter a new time in [ms]\n"
			return(enter_prep())


def conf_filename(device):
	file_list = sorted(glob.glob('../config/imager/*'))
	conf_file = None
	for filename in file_list:
		f = open(filename,'r')
		#print 'device unique ID = ' + device
		for line in f:
			if line[:12] == 'device ID = ':
				if device == line[12:-1]:
					conf_file = filename
		f.close()
	return(conf_file)
  

def fill_filter_info(conf_file):
	filter_info = {}
	filter_meta = []
	f = open(conf_file,'r')
	slot_num = None
	for line in f:
		if line[:21] == 'Filter Slot Number = ':
			slot_num = int(line[21:])
		elif line[:13] == 'Wavelength = ':
			filter_meta.append(line[13:-1])
		elif line[:14] == 'Description = ':
			filter_meta.append(line[14:-1])
			filter_info[slot_num]=filter_meta
			filter_meta = []
	f.close()
	return(filter_info)


def print_filter_info(filter_info):
	for f in filter_info:
		print "Filter: " + str(f) + "\n" "Wavelength: " + filter_info[f][0] + "\n" + "Description: " + filter_info[f][1] + "\n"


def insert_start_time(f,sched_lst,end_time):
	last_slot = 0
	end_time = end_time
	print "\nEnter the desired start time for this slot:"
	print "Valid range for start times " + str(end_time) + " -> " + str(len(sched_lst)) + ":"
	print "To have this slot start immediately after the last slot, leave blank\n" 
	start_time = raw_input("Start Time: ")
	try:
		if int(start_time) < end_time or int(start_time) > len(sched_lst):
			#print "Invalid slot start time. Please select a new start time."
			return(insert_start_time(f,sched_lst,end_time))
		else:
			f.write('#Slot start time\n')
			f.write('start_time=' + start_time + '\n')
			return(start_time)
	except ValueError:
		if start_time == '':
			f.write('#Slot start time\n')
			f.write('start_time=' + str(end_time) + '\n')
			return(end_time)
		else:
			print "Invalid slot start time. Please select a new start time."
			return(insert_start_time(f,sched_lst,end_time))


def insert_exposure(f,sched_lst,start_time,prep_time):
	print "\nEnter the desired exposure length in ms:"
	print "Example: \"2000\" or \"500\""
	exposure = raw_input("Exposure Length: ")
	try:
		if int(exposure) > len(sched_lst):
			print "The entered exposure time is larger the entire schedule period"
			return(insert_exposure(f,sched_lst,start_time,prep_time))
		end_time = start_time + int(exposure) + int(prep_time)
		if end_time > len(sched_lst):
			print "The combined exposure time and prep time is larger the entire schedule period"
			return(insert_exposure(f,sched_lst,start_time,prep_time))
		for i in range(start_time,end_time):
			if sched_lst[i] == 1:
				print "Schedule slot: " + str(sched_lst[i]) + " has already been used. Please select a shorter exposure value.\n"
				flag = raw_input("Enter new exposure time? (yes | no): ")
				if flag == 'no' | flag == None:
					return(False)
				elif flag == 'yes':
					return(insert_exposure(f,sched_lst,start_time,prep_time))
				else:
					return(False)
			elif sched_lst[i] == 0:
				sched_lst[i] = 1
		f.write("#Exposure time for this schedule slot\n")
		f.write("exposure_len=" + exposure + "\n")
		#print "Slots: " + str(start_time) + " -> " + str(end_time) + " have been successfully filled\n"	
		return(end_time)
	except ValueError:
		print "Value entered is not a valid exposure time. Please input a new exposure time in [ms]\n"
		return(insert_exposure(f,sched_lst,start_time,prep_time))


def insert_binning(f):
	print "\nEnter the desired binning value:"
	print "Accepted Values: 1 | 2 | 4"
	binning = raw_input("Binning: ")
	try:
		if int(binning) == 1 or int(binning) == 2 or int(binning) == 4:
			f.write('#binning value for this schedule slot\n')
			f.write('binning=' + binning + '\n')
		else:
			print "\nEntered unaccepted binning value. Please try again"
			return(insert_binning(f))
	except ValueError:
		print "\nEntered unaccepted binning value. Please try again"
		return(insert_binning(f))


def insert_ixon_pag(f):
	print "\nEnter the desired Pre-amp Gain value:"
	print "Accepted pre-amp gain values (0 | 1 | 2)\n"
	print "Leave blank for default value"
	pre_amp_gain = raw_input("Pre-amp Gain [Default = 0]: ")
	try:
		if int(pre_amp_gain) < 0 or int(pre_amp_gain) > 2:
			print "\nEntered unaccepted pre-amp gain value. Please try again"
			return(insert_ixon_pag(f))
	except ValueError:
		if pre_amp_gain == '':
			pre_amp_gain = '0'
		else:
			print "\nEntered unaccepted pre-amp gain value. Please try again"
			return(insert_ixon_pag(f))
	f.write('#pre-amp gain value for this schedule slot\n')
	f.write('ixon_pre-amp_gain=' + pre_amp_gain + '\n\n')
	return()
	

def insert_ixon_emccd_mode(f):
	print "\nEnter the desired EMCCD gain mode:"
	print "EMCCD Gain Mode Options\n"
	print "0 = EM_GAIN_DAC_255"
	print "1 = EM_GAIN_DAC_4095"
	print "2 = EM_GAIN_LINEAR"
	print "3 = EM_GAIN_REAL\n"
	emccd_mode = raw_input("EMCCD Mode:  [Default = EM_GAIN_REAL (3)]")
	try:
		if int(emccd_mode) < 0 or int(emccd_mode) > 3:
			print "\nEntered unaccepted EMCCD gain mode. Please try again"
			return(insert_ixon_emccd_mode(f))
	except ValueError:
		if emccd_mode == '':
			emccd_mode = '3'
		else:
			print "\nEntered unaccepted EMCCD gain mode. Please try again"
			return(insert_ixon_emccd_mode(f))
	f.write('#EMCCD gain mode for this schedule slot\n')
	f.write('ixon_emccd_gain_mode=' + emccd_mode + '\n\n')
	return(emccd_mode)


def insert_ixon_emccd_gain(f,emccd_mode):
	print "\nEnter the desired EMCCD gain:"
	print "EMCCD Gain Options:"
	if int(emccd_mode) == 0:
		print "Valid EMCCD gain range: (0 -> 255)\n"
		emccd_gain = raw_input("EMCCD Gain:  [Default = 55]")
		try:
			if int(emccd_gain) < 0 or int(emccd_gain) > 255:
				print "\nEntered unaccepted EMCCD gain. Please try again"
				return(insert_ixon_emccd_gain(f,emccd_mode))
		except ValueError:
			if emccd_gain == '':
				emccd_gain = '55'
			else:
				print "\nEntered unaccepted EMCCD gain. Please try again"
				return(insert_ixon_emccd_gain(f,emccd_mode))
	elif int(emccd_mode) == 1:
		print "Valid EMCCD gain range: (0 -> 4095)\n"
		emccd_gain = raw_input("EMCCD Gain:  [Default = 55]")
		try:
			if int(emccd_gain) < 0 or int(emccd_gain) > 4095:
				print "\nEntered unaccepted EMCCD gain. Please try again"
				return(insert_ixon_emccd_gain(f,emccd_mode))
		except ValueError:
			if emccd_gain == '':
				emccd_gain = '55'
			else:
				print "\nEntered unaccepted EMCCD gain. Please try again"
				return(insert_ixon_emccd_gain(f,emccd_mode))
	elif int(emccd_mode) == 2:
		print "Valid EMCCD gain range: (0 -> 255)\n"
		emccd_gain = raw_input("EMCCD Gain:  [Default = 55]")
		try:
			if int(emccd_gain) < 0 or int(emccd_gain) > 255:
				print "\nEntered unaccepted EMCCD gain. Please try again"
				return(insert_ixon_emccd_gain(f,emccd_mode))
		except ValueError:
			if emccd_gain == '':
				emccd_gain = '55'
			else:
				print "\nEntered unaccepted EMCCD gain. Please try again"
				return(insert_ixon_emccd_gain(f,emccd_mode))
	elif int(emccd_mode) == 3:
		print "Valid EMCCD gain range: (0 -> 255)\n"
		emccd_gain = raw_input("EMCCD Gain:  [Default = 55]")
		try:
			if int(emccd_gain) < 0 or int(emccd_gain) > 255:
				print "\nEntered unaccepted EMCCD gain. Please try again"
				return(insert_ixon_emccd_gain(f,emccd_mode))
		except ValueError:
			if emccd_gain == '':
				emccd_gain = '55'
			else:
				print "\nEntered unaccepted EMCCD gain. Please try again"
				return(insert_ixon_emccd_gain(f,emccd_mode))
	f.write('#EMCCD gain for this schedule slot\n')
	f.write('ixon_emccd_gain=' + emccd_gain + '\n\n')
	return()


def insert_pmax_ccd_gain(f):
	print "\nEnter the desired CCD Gain value:"
	print "Accepted CCD gain values (1 | 2 | 3)\n"
	pmax_ccd_gain = raw_input("CCD Gain:  [Default = 1]")
	try:
		if int(pmax_ccd_gain) < 1 or int(pmax_ccd_gain) > 3:
			print "\nEntered unaccepted CCD gain value. Please try again"
			return(insert_pmax_ccd_gain(f))
	except ValueError:
		if pmax_ccd_gain == '':
			pmax_ccd_gain = '1'
		else:
			print "\nEntered unaccepted CCD gain value. Please try again"
			return(insert_pmax_ccd_gain(f))
	f.write('#CCD gain value for this schedule slot\n')
	f.write('pmax_ccd_gain=' + pmax_ccd_gain + '\n\n')
	return()


def insert_pmax_emccd_gain(f):
	print "\nEnter the desired EMCCD gain:"
	print "Valid EMCCD gain range: (0 -> 4095)\n"
	pmax_emccd_gain = raw_input("EMCCD Gain:  [Default = 3072]")
	try:
		if int(pmax_emccd_gain) < 0 or int(pmax_emccd_gain) > 4095:
			print "\nEntered unaccepted EMCCD gain. Please try again"
			return(insert_pmax_emccd_gain(f))
	except ValueError:
		if pmax_emccd_gain == '':
			pmax_emccd_gain = '3072'
		else:
			print "\nEntered unaccepted EMCCD gain. Please try again"
			return(insert_pmax_emccd_gain(f))
	#Write gain settings to file
	f.write('#EMCCD gain for this schedule slot\n')
	f.write('pmax_emccd_gain=' + pmax_emccd_gain + '\n\n')  
	return()


def insert_iccd_igain(f):
	print "\nEnter the desired ICCD Gain value:"
	print "Accepted ICCD gain values (0 | 1 | 2)\n"
	iccd_gain = raw_input("ICCD Gain:  [Default = 0]")
	try:
		if int(iccd_gain) < 0 or int(iccd_gain) > 2:
			print "\nEntered unaccepted ICCD gain value. Please try again"
			return(insert_iccd_igain(f))
	except ValueError:
		if iccd_gain == '':
			iccd_gain = '0'
		else:
			print "\nEntered unaccepted ICCD gain value. Please try again"
			return(insert_iccd_igain(f))
	f.write('#ICCD gain value for this schedule slot\n')
	f.write('iccd_gain=' + iccd_gain + '\n\n')
	return()


def insert_iccd_cgain(f):
	print "\nEnter the desired CCD Gain value:"
	print "Accepted CCD gain values (1 | 2)\n"
	iccd_ccd_gain = raw_input("CCD Gain:  [Default = 1]")
	try:
		if int(iccd_ccd_gain) < 1 or int(iccd_ccd_gain) > 2:
			print "\nEntered unaccepted CCD gain value. Please try again"
			return(insert_iccd_cgain(f))
	except ValueError:
		if iccd_ccd_gain == '':
			iccd_ccd_gain = '1'
		else:
			print "\nEntered unaccepted CCD gain value. Please try again"
			return(insert_iccd_cgain(f))	
	f.write('#CCD gain for this schedule slot\n')
	f.write('iccd_ccd_gain=' + iccd_ccd_gain + '\n\n')
	return()

def insert_proem_ccd_gain(f):
	print "\nEnter the desired CCD Gain value:"
	print "Accepted CCD gain values (1 = Low | 2 = Medium | 3 = High)\n"
	ccd_gain = raw_input("CCD Gain:  [Default = 3]")
	try:
		if int(ccd_gain) < 1 or int(ccd_gain) > 3:
			print "\nEntered unaccepted CCD gain value. Please try again"
			return(insert_proem_ccd_gain(f))
	except ValueError:
		if ccd_gain == '':
			ccd_gain = '3'
		else:
			print "\nEntered unaccepted CCD gain value. Please try again"
			return(insert_proem_ccd_gain(f))	
	f.write('#CCD gain for this schedule slot\n')
	f.write('ccd_gain=' + ccd_gain + '\n\n')
	return()

def insert_proem_em_gain(f):
	print "\nEnter the desired EMCCD gain:"
	print "Valid EMCCD gain range: (1 -> 1000)\n"
	emccd_gain = raw_input("EMCCD Gain:  [Default = 50]")
	try:
		if int(emccd_gain) < 1 or int(emccd_gain) > 1001:
			print "\nEntered unaccepted EMCCD gain. Please try again"
			return(insert_proem_em_gain(f))
	except ValueError:
		if emccd_gain == '':
			emccd_gain = '50'
		else:
			print "\nEntered unaccepted EMCCD gain. Please try again"
			return(insert_proem_em_gain(f))
	#Write gain settings to file
	f.write('#EMCCD gain for this schedule slot\n')
	f.write('emccd_gain=' + emccd_gain + '\n\n')  
	return()

def insert_iKon_ccd_gain(f):
	print "\nEnter the desired CCD Gain value:"
	print "Accepted CCD gain values (1 = 1x | 2 = 2.4x | 3 = 4x)\n"
	ccd_gain = raw_input("CCD Gain:  [Default = 1]")
	try:
		if int(ccd_gain) < 1 or int(ccd_gain) > 4:
			print "\nEntered unaccepted CCD gain value. Please try again"
			return(insert_iKon_ccd_gain(f))
	except ValueError:
		if ccd_gain == '':
			ccd_gain = '1'
		else:
			print "\nEntered unaccepted CCD gain value. Please try again"
			return(insert_iKon_ccd_gain(f))	
	f.write('#CCD gain for this schedule slot\n')
	f.write('ccd_gain=' + ccd_gain + '\n\n')
	return()

def insert_gain_values(f,device):
	#the_regex = re.compile(r"-(\w*)\d\d")
	#match_iterator = the_regex.findall(device)
	#camera = match_iterator[0]
	#emccd_gain = None
	
	#Set gain value for the iKon Camera head.
	#insert_iKon_ccd_gain(f)

	#Set gain values for pro-Em camera. There is two values to be set ADC Gain (CCD Gain) and Emgain.
	insert_proem_ccd_gain(f)
	#insert_proem_em_gain(f)

	#Allow user to enter gain values depending on selected device
	"""
	if camera == 'ixon':
		#insert ixon pre-amp gain into conf file
		insert_ixon_pag(f)
	  #insert ixon emccd gain mode
		emccd_mode = insert_ixon_emccd_mode(f)
		#insert ixon emccd gain depending on selected mode
		insert_ixon_emccd_gain(f,emccd_mode)

	elif camera == 'pmax':
		#insert pmax ccd gain value
		insert_pmax_ccd_gain(f)
	  #insert pmax emccd gain value 
		insert_pmax_emccd_gain(f)
	
	elif camera == 'iccd':
		#insert iccd intensifier gain
		#insert_iccd_igain(f)
		#insert iccd ccd gain
		insert_iccd_cgain(f)
	"""

	return()

#Print visual representation of imaging schedule
def print_schedule(f,filter_info,device,sched_lst,prep_time,end_time):
	#move current position in file to the beginning
	f.seek(0,0)
	slot_num = 0
	for line in f:
		if line[:15] == 'device unique ID':
			print "Device = " + line[19:-1]
		elif line[:11] == 'schedule_len':
			print "Schedule Total Length = " +line[13:-1] + ' ms'
		
	
	


#This is not used anymore just the insert imaging slot function below
def insert_first_imaging_slot(f,filter_info,device,sched_lst,prep_time):
	exposure = 0
	filter_num = raw_input("Filter Number: ")
	try:
		if int(filter_num) < 0 or int(filter_num) > len(filter_info):
			print "Filter number entered is out of range. Please use a new filter number (1-" + str(len(filter_info)) + ")\n"
			return(insert_first_imaging_slot(f,filter_info,device,sched_lst,prep_time))
		else:
			print "You have selected Filter: " + filter_num 
			flag = raw_input("yes | no : ")
			if flag == "no":
				return(insert_first_imaging_slot(f,filter_info,device,sched_lst,prep_time))
			elif flag == "yes":
				f.write('#slot 1 values\n')
				f.write('<slot>\n\n')
				f.write('#Slot start time\n')
				f.write('start_time=0\n')
				end_time = insert_exposure(f,sched_lst,0,prep_time)
				insert_binning(f)
				insert_gain_values(f,device)
				f.write('</slot>\n')
				return(end_time)
			else:
				print "Only use \"yes\" or \"no\""
				return(insert_first_imaging_slot(f,filter_info,device,sched_lst,prep_time))
	except ValueError:
		print "This value is not a number. Please input a proper filter number\n"
		return(insert_first_imaging_slot(f,filter_info,device,sched_lst,prep_time))

def insert_imaging_slot(f,filter_info,device,sched_lst,prep_time,end_time):
	exposure = 0
	start_time = 0
	print "**********************************************************************************\n"
	print "\nSchedule Entry\n"
	print "Please select one of the above filters for the first schedule slot."  #This list is generated from the selected imager.conf file
	filter_num = raw_input("Filter Number: ")
	try:
		if int(filter_num) < 0 or int(filter_num) > len(filter_info):
			print "Filter number entered is out of range. Please use a new filter number (1-" + str(len(filter_info)) + ")\n"
			return(insert_imaging_slot(f,filter_info,device,sched_lst,prep_time,end_time))
		else:
			print "You have selected Filter: " + filter_num 
			flag = raw_input("yes | no : ")
			if flag == "no":
				return(insert_imaging_slot(f,filter_info,device,sched_lst,prep_time,end_time))
			elif flag == "yes":
				f.write('#slot values\n')
				f.write('<slot>\n\n')
				f.write('#Filter number for this slot\n')
				f.write('filter_num=' + filter_num + '\n')
				start_time = insert_start_time(f,sched_lst,end_time)
				prep_time =  enter_prep()
				end_time = insert_exposure(f,sched_lst,int(start_time),prep_time)
				if end_time == False:
					return(insert_imaging_slot(f,filter_info,device,sched_lst,prep_time,end_time))
				insert_binning(f)
				insert_gain_values(f,device)
				f.write('\n#ms it takes to move filter wheel and readout image\n')
				f.write('<prep>\n')
				f.write('prep_time=' + prep_time + '\n')
				f.write('</prep>\n')
				f.write('\n</slot>\n')
			else:
				print "Only use \"yes\" or \"no\""
				return(insert_imaging_slot(f,filter_info,device,sched_lst,prep_time,end_time))
	except ValueError:
		print "This value is not a number. Please input a proper filter number\n"
		return(insert_imaging_slot(f,filter_info,device,sched_lst,prep_time,end_time))
	flag = raw_input("Insert another schedule slot? (yes | no): ")
	if flag == 'yes':
		print_filter_info(filter_info)
		return(insert_imaging_slot(f,filter_info,device,sched_lst,prep_time,end_time))
	else:
		return(True)


#Main Routine
print "\n\n\n\n\n"
print "*******************************************************************************"
print "*******************************************************************************"
print "                       SCHEDULE INITIALIZATION PROGRAM                         "
print "*******************************************************************************"
print "*******************************************************************************"
print "                             Keo Scientific LTD.                               "
print "                                Version: 3.0                                   "
print "                              November 18 2018                                 "
print "*******************************************************************************"
print "\n\n\n\n\n"


#Create new schedule conf file to write users settings to 
#Need use default filename if no new filename is passed in via cli
#Add the cli arg in 
now_time = time.strftime("%H-%M-%S")
now_date = time.strftime("%d-%m-%Y")

filename = "../config/schedule/schedule.conf_" + now_date + "_" + now_time
#filename = '../config/schedule/_default_schedule.conf'

f = open(filename,'w+')
f.write('#******************************************************************************#\n')
f.write('#******************************************************************************#\n')
f.write('#                      SCHEDULE INITIALIZATION PROGRAM                         #\n')
f.write('#******************************************************************************#\n')
f.write('#******************************************************************************#\n')
f.write('#                            Keo Scientific LTD.                               #\n')
f.write('#                               Version: 3.0                                   #\n')
f.write('#                              November 18 2018                                #\n')
f.write('#******************************************************************************#\n')
f.write('\n')


#Find devices using the imager config files
device = find_all_devices()
#let user select device to use
selected_device,local_flag = select_device(device)
#Write the start of the schedule config file and input device unique id
f.write('#Schedule Configuration File\n\n')
f.write('#DO NOT MANUALLY EDIT THIS FILE\n\n')
#Write device unique id to schedule file
f.write('device ID = '  + selected_device + '\n\n')

#Select Operational Mode -> only one mode is avaible "schedule"
selected_mode = select_mode()
f.write('#0 = schedule; 1 = rapid\n')
if selected_mode == 0:
  f.write('image_mode=0\n\n')
elif selected_mode == 1:
  f.write('image_mode=1\n\n')

#User input for starting zenith angle
angle = zenith_angle()
f.write('#Starting Sun Angle\n')
f.write('zenith_angle_start=' + angle + '\n\n')

#User input for schedule length
sched_len = enter_sched_len()
f.write('#Length of schedule\n')
f.write('schedule_len=' + sched_len + '\n')
#Create list for each second of imaging schedule 
sched_lst = [0 for x in range(int(sched_len))]
#Dictionary to hold slot information [filter number, start time, end time, exposure length]
#sched_dict = {1:[1,0,2500,500]}

#Determine what imager.conf file is being used
if local_flag == 1:
	conf_file = '../config/imager/imager.conf'
	#Fill the dict object with filter info
	filter_info = fill_filter_info(conf_file)
elif local_flag == 0:
	conf_file = conf_filename(selected_device)
	#Fill the dict object with filter info
	filter_info = fill_filter_info(conf_file)


print "\nSchedule Entry\n"
print "Please select one of the below filters for the first schedule slot."
#print_filter_info(filter_info)
#end_time = insert_first_imaging_slot(f, filter_info, selected_device, sched_lst,prep_time)
end_time = 0
prep_time = 0
print_filter_info(filter_info)
#This is the main state that inserts schedule slots -> will repeat till the user selects no more slots
insert_imaging_slot(f,filter_info,selected_device,sched_lst,prep_time,end_time)

f.close()

print "\nSchedule complete, you can view configuration file in /usr/local/src/imagerd_rt/config/schedule."
print "\nREMINDER: Next, create a symbolic link - see User Manual pages 15 and 24."






