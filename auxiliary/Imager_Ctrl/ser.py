#!/usr/bin/env python
import serial
import time
import sys

COM_PORT1 = '/dev/ttyS2'  #Temp Controller
COM_PORT0 = '/dev/ttyS1'  #Shutter


COMMAND_I = '!0R'
COMMAND_O = '!0S1'
COMMAND_C = '!0S0'

CMD_Q = 'TX*'
CMD_QU = 'TU*'
CMD_I = 'VU1*'
CMD_IU = 'VU0*'
CMD_O = 'VX0*'
CMD_C = 'VX1*'

#fw_command_1 = 'g=2\rGOSUB4\r'
fw_command_1 = 'g=3 GOSUB4\r'
fw_command_home = 'GOSUB5\r'
fw_shut_open = 'd=1 GOSUB6\r'
fw_shut_close = 'd=0 GOSUB6\r'

omega_read = '*X01\r'
#Need to add the hex value still
omega_write = '*W012000CD\r'


#Only works for values that are greater then 0 and have one decimal place
#Example 20.5C = 2000CD
#def convert_to_omega_format(setpoint):
	


SERIAL_TIMEOUT = 10

def send_command(ser,cmd):
	if cmd != fw_command_1 and cmd != fw_command_home and cmd != fw_shut_open and cmd != fw_shut_close and cmd != omega_read:
		print 'Sending Command: ' + cmd
		try:
			ser.write(cmd)
		except:
			print 'Error: '+str(sys.exc_info()[0])+'\nCheck USB/serial connections; you may need to restart the application.'
			print 'serial error', sys.exc_info()[0]
			return False
	else:
		print 'Sending Command: ' + cmd
		try:
			ser.write(cmd)
		except:
			print 'Error: '+str(sys.exc_info()[0])+'\nCheck USB/serial connections; you may need to restart the application.'
			print 'serial error', sys.exc_info()[0]
			return False

		print 'Starting to listen for returned bytes'
		buf = ''
		t0 = time.clock()
		while len(buf)==0 and time.clock()-t0<SERIAL_TIMEOUT:
			buf += ser.read(ser.inWaiting())
			#if buf!='': print "got '%s' %ss," % (buf, round(time.clock()-t0,3)),
			time.sleep(.05)


		if time.clock()-t0>=SERIAL_TIMEOUT:
			return False
		else:
			print '\n'
			#print buf[14] + '\n\n'
			print buf
			print '\n'
			print str(len(buf)) + '\n'
			print buf[4]
			#for char in buf:
			#	print '-----------'
			#	print char
			return True



print 'Starting Serial Control Program'
print 'GMT time:', time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime())

try:
	#Serial Port for shutter
	ser0 = serial.Serial(COM_PORT0, baudrate=9600, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE,
		stopbits=serial.STOPBITS_ONE,timeout=0, xonxoff=0, rtscts=0, interCharTimeout=None)
	#Seial port for sunshade
	#ser1 = serial.Serial(COM_PORT1, baudrate=9600, bytesize=serial.SEVENBITS, parity=serial.PARITY_ODD,
    #    	stopbits=serial.STOPBITS_ONE,timeout=0, xonxoff=0, rtscts=0, interCharTimeout=None)

except serial.serialutil.SerialException as (strerror):
	print "\nSerial error {0}\n".format(strerror)
	sys.exit()
except:
	print "Unexpected Serial error:", sys.exc_info()[0]
	raise

print 'Serial initiated0:', ser0
#print 'Serial initiated1:', ser1

if len(sys.argv) > 1:

	print sys.argv[1]

	if sys.argv[1] == 'sun_inquire':
		print 'Performing Sunshade Inquire...'
		send_command(ser1,CMD_Q)
	elif sys.argv[1] == 'sun_open':
		print 'Performing Sunshade Open...'
		send_command(ser1,CMD_O)
	elif sys.argv[1] == 'sun_close':
		print 'Performing SunShade Close...'
		send_command(ser1,CMD_C)
	elif sys.argv[1] == 'sun_inquire_u':
		print 'Performing Sunshade Inquire... U'
                send_command(ser1,CMD_QU)
	elif sys.argv[1] == 'sun_auto':
		print 'Performing Sunshade Manual Change'
		send_command(ser1,CMD_IU)
	elif sys.argv[1] == 'sun_manual':
		print 'Performing Sunshade Manual Mode'
		send_command(ser1,CMD_I)
	elif sys.argv[1] == 'shutter_status':
		print 'Performing Shutter Status'
		send_command(ser0,COMMAND_I)
	elif sys.argv[1] == 'shutter_open':
		print 'Performing Shutter Open'
		send_command(ser0,COMMAND_O)
	elif sys.argv[1] == 'shutter_close':
		print 'Performing Shutter Close'
		send_command(ser0,COMMAND_C)
	elif sys.argv[1] == 'fw_filt_1':
		print 'Performing Filterwheel move to 1'
		send_command(ser0,fw_command_1)
	elif sys.argv[1] == 'fw_filt_home':
		print 'Performing Filterwheel move home position'
		send_command(ser0,fw_command_home)
	elif sys.argv[1] == 'fw_shut_open':
		print 'Performing Filterwheel shutter open\n'
		send_command(ser0,fw_shut_open)
	elif sys.argv[1] == 'fw_shut_close':
		print 'Performing Filterwheel shutter close\n'
		send_command(ser0,fw_shut_close)
	elif sys.argv[1] == 'omega_temp':
		print 'Reading Temperature from Omega temp controller'
		send_command(ser0,omega_read)
	elif sys.argv[1] == 'omega_set':
		print 'Change Omega Setpoint'
		send_command(ser0,omega_write)

else:
	print 'No input parameters.\n omega_temp \n omega_set \n fw_filt_1 \n fw_filt_home \n fw_shut_open \n fw_shut_close \n sun_inquire \n sun_open \n sun_close \n sun_inquire_u \n sun_auto \n sun_manual \n shutter_status \n shutter_open \n shutter_close \n'

ser0.close()
#ser1.close()
