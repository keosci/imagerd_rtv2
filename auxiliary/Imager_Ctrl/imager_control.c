#include <stdlib.h>
#include <stdio.h>
#include "imager_ctrl.h"
#include <stdint.h>

/*
 * Main
 */
int main(int argc, char *argv[]){
	int fw_num=0,igain=0;

	if( argc < 2 ){
		fprintf(stderr,"usage: imager_control <arg>\n \
						<arg> types:\n \
						open_shutter\n \
						close_shutter\n \
						shutter_status\n \
						go_home\n \
						filt_1\n \
						filt_2\n \
						filt_3\n \
						filt_4\n \
						filt_5\n \
						filt_6\n \
						get_temp\n \
						set_temp\n \
						Example ./imager_control open_shutter\n");
		exit(0);
	}

	if(strstr(argv[1], "open_shutter") != NULL){

		if(FW_Control(SHUTCTRL,1)!=TRUE){
			fprintf(stderr,"Error: Could not open shutter\n");
			exit(0);
		}

	}else if(strstr(argv[1], "close_shutter") != NULL){

		if(FW_Control(SHUTCTRL,CLOSE)!=TRUE){
			fprintf(stderr,"Error: Could not close shutter\n");
			exit(0);
		}

	}else if(strstr(argv[1], "shutter_status") != NULL){

		if(FW_Control(SHUTCTRL,INQUIRE) != TRUE){
			fprintf(stderr,"Error: Could not query shutter\n");
			exit(0);
		}

	} else if(strstr(argv[1], "go_home") != NULL){

		if(FW_Control(FWCTRL,HOME)!=TRUE){
			fprintf(stderr,"Error: Could not send filterwheel to home position\n");
			exit(0);
		}

	} else if(strstr(argv[1], "filt_1") != NULL){

		if(FW_Control(FWCTRL,1)!=TRUE){
			fprintf(stderr,"Error: Could not send FW to filt_1 Position\n");
			exit(0);
		}

	} else if(strstr(argv[1], "filt_2") != NULL){

		if(FW_Control(FWCTRL,2)!=TRUE){
			fprintf(stderr,"Error: Could not send FW to filt_2 Position\n");
			exit(0);
		}
	} else if(strstr(argv[1], "filt_3") != NULL){

		if(FW_Control(FWCTRL,3)!=TRUE){
			fprintf(stderr,"Error: Could not send FW to filt_3 Position\n");
			exit(0);
		}
	} else if(strstr(argv[1], "filt_4") != NULL){

		if(FW_Control(FWCTRL,4)!=TRUE){
			fprintf(stderr,"Error: Could not send FW to filt_4 Position\n");
			exit(0);
		}
	} else if(strstr(argv[1], "filt_5") != NULL){

		if(FW_Control(FWCTRL,5)!=TRUE){
			fprintf(stderr,"Error: Could not send FW to filt_5 Position\n");
			exit(0);
		}
	} else if(strstr(argv[1], "filt_6") != NULL){

		if(FW_Control(FWCTRL,6)!=TRUE){
			fprintf(stderr,"Error: Could not send FW to filt_6 Position\n");
			exit(0);
		}
	
	} else if(strstr(argv[1], "get_temp") != NULL){
		
		if(Omega_Control(RDTEMP,0)!=TRUE){
			fprintf(stderr,"Error: Could not retrieve the omega current temperature\n");
                        exit(0);
		}
	
	} else if(strstr(argv[1], "set_temp") != NULL){
		
		float set_point = strtof(argv[2],NULL);
		printf("Set Point = %f\n",set_point);
		if(Omega_Control(SETTEMP,set_point)!=TRUE){
			fprintf(stderr,"Error: Could not set the omega set point\n");
                        exit(0);
		}	
		
	}else{
		fprintf(stderr,"usage: imager_control <arg>\n \
										<arg> types:\n \
										open_shutter\n \
										close_shutter\n \
										shutter_status\n \
										go_home\n \
										filt_1\n \
										filt_2\n \
										filt_3\n \
										filt_4\n \
										filt_5\n \
										filt_6\n \
										get_temp\n \
										set_temp <float> setpoint\n \
										Example ./imager_control set_temp 21.0\n");
										exit(0);
	}
	return(0);
}
