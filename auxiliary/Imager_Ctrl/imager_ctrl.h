/*
 *
 * imager_ctrl.h
 *
 * 2011-04-08 Modified by Jarrett Little.
 * 2013-11-23 Modified by Jarrett Little.
 * 2018-10-27 Modified by Jarrett Little.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */


#ifndef _IMAGER_CTRL_H_
#define _IMAGER_CTRL_H_

#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/time.h>
#include <string.h>
#include <stdlib.h>

//#include "imagerd_rt.h"
#define FW_Shutter_Ctrl
#define Omega_Temp_Ctrl

#define SYSTEM_LOG(x) fprintf(stderr,"%s\n",x)
#define MAX_STRING_LEN 		255

//Serial port defines
#define AUX_PORT     			"/dev/ttyUSB3"   		/**< Imager serial port device node. */
#define SUNSHADE_PORT			"/dev/ttyUSB2"		/**< Sun Shade serial port device node */

#ifdef FW_Shutter_Ctrl
#define FW_PORT           "/dev/ttyUSB0"
#endif

#ifdef Omega_Temp_Ctrl
#define OMEGA_PORT        "/dev/ttyUSB1"
#endif


#define BAUDRATE    			B9600        		/**< Serial port boudrate. */
#define ASC_TIMEOUT 			5            		/**< Serial port timeout. */

//Logic
#define TRUE				1
#define FALSE				0
#define UNDEF				 -1

//Shutter Defines
#define CLOSE    			0   			/**< Shutter open flag. */
#define OPEN     			1   			/**< Shutter close flag. */
#define INQUIRE 		 -1   			/**< Shutter state inquire flag. */
#define IS_CLOSED		 -3 				//Returned value for required state if shutter is closed
#define IS_OPEN			 -4 				//Returned value for required state if shutter is open

//SunShade Defines
#define CLOSE_SHADE			0			//VX0*
#define OPEN_SHADE			1			//VX1*
#define INQUIRE_SHADE	 -1			//TX* Returns: SR 1000 or SR 0000
#define INQUIRE_CTRL	 -2			//TU* Returns: SR 1000 or SR 0000
#define MANUAL_MODE			3			//VU1*
#define AUTO_MODE			  4			//VU0*


#define SHUTCTRL        1
#define FWCTRL          0
#define HOME            0

#define FILTER_MIN 			1   			/**< Minimum allowed filter index. */
#define FILTER_MAX 			6   			/**< Maximum allowed filter index. */

#define RDTEMP          0
#define SETTEMP         1
#define RDSETPOINT     -1

volatile int aux_fd;             			/**< Shutter device handle. */
volatile int sun_fd;						      /**< Sun shade device handle. */
volatile int fw_fd;                   //filterwheel device handle
volatile int omega_fd;                //Omega Device handle


//Function declarations
//Connect to Serial Port
int Connect_AUX(void);
int Connect_SUN(void);
int Connect_FW(void);
int Connect_OMG(void);
//Set the port parameters
int Set_AUX_Port(void);
int Set_SUN_Port(void);
int Set_FW_Port(void);
int Set_OMG_Port(void);
//Disconnect from Serial port
int Release_AUX_Connection(void);
int Release_SUN_Connection(void);
int Release_FW_Connection (void);
int Release_OMG_Connection(void);
//Send Command Functions
int Send_AUX_Command(char *command);
int Send_SUN_Command(char *command);
int Send_FW_Command(char *command);
int Send_OMG_Command(char *command);


//Device Control Functions of single filter imagers
int Shutter_Control(int controlvalue);
int SunShade_Control(int controlvalue);
//Shutter Control Functions
int Open_Shutter(void);
int Close_Shutter(void);
int Get_Shutter(void);
//SunShade Control Functions
int Open_SunShade(void);
int Close_SunShade(void);
int Get_SunShade(void);

//Device Control Functions for Multi Filter imagers
//FW_Contro(): func         -> shutter_ctrl
//             controlvalue -> open || close || status
//             func         -> fw_ctrl
//             controlvalue -> filter_num || home || status
int FW_Control(int func,int controlvalue);
int Omega_Control(int func, float controlvalue);
//Sends filterwheel into home position
//int Go_Home(void);
//FW_Control(fw_ctrl,home);

//For Omega Temperature Controller
//int Set_FW_Temp(int setpoint);
//int Get_FW_Temp(void);
//Need to set a ifdef statement for imagers with filter wheels and shutters controlled
//by the smart motor interface.

//int Shutter_Control(int controlvalue);
//int ASC_Connect(void);
//int Init_ASC_Connection(void);
//int Release_ASC_Connection(void);
//int Go_Home(void);
//int Open_Shutter(void);
//int Close_Shutter(void);
//int Get_Shutter(void);
//Not Used Currently
//int ASC_Set_Filter(int controlvalue);
//int Set_Filter(int filter);


#endif /* _IMAGER_CTRL_H_ */
