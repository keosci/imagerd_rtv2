**First Read INSTALL in the root directory of the Andor Driver Folder.

Example Andor Driver Install:

[user@andor-x andor]$ ./install_andor 
You must have root priveleges to install the andor software.
[usert@andor-x andor]$ sudo ./install_andor 
[sudo] password for user: 
Select your CCD Type from the following list:
1. CCD
2. ICCD
3. iStar
4. iXon
5. All USB Cameras

5

Creating usb udev rules...
libstdc++.so.6: Exists
Platform: 64 bit
installing SDK library libandor-stdc++6-x86_64.so.2.90.30004.0 into /usr/local/lib...
Updating library cache, please wait...
installing SDK help file into /usr/local/doc...
cp: cannot stat `doc/*.doc': No such file or directory
installing SDK header file into /usr/local/include...
installing configuration files into /usr/local/etc/andor...
updating installed directory list /etc/andor/andor.install for uninstall...

Do you wish to Read the Release Notes Now(y/n)?
n

Andor Installation successful

Proceed to INSTALL_IMAGERD_RT_ANDOR

