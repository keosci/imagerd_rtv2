/*
 *  lib_andor.c
 * 
 *	Low-Level Camera Library for Andor 2.0 SDK Camera
 * 
 *  Copyright 2015 - Jarrett Little
 *
 */

 //Custom Library Includes
#include "lib_andor.h"


/*
 * Initialize the Andor System.
 */
int Initialize_CCD(char *directory) {
	unsigned int error;

	error = Initialize(directory);

  	switch (error) {

  		case DRV_SUCCESS:
    		return TRUE;
    		break;

  		case DRV_VXDNOTINSTALLED:
    		SYSTEM_LOG("Initialize_CCD(): Failed to initialize CCD. VxD not loaded.");
    		return FALSE;
    		break;

  		case DRV_INIERROR:
    		SYSTEM_LOG("Initialize_CCD(): Failed to initialize CCD. Unable to load \"DETECTOR.INI\".");
    		return FALSE;
    		break;

  		case DRV_COFERROR:
    		SYSTEM_LOG("Initialize_CCD(): Failed to initialize CCD. Unable to load \"*.COF\".");
    		return FALSE;
    		break;

  		case DRV_FLEXERROR:
    		SYSTEM_LOG("Initilaize_CCD(): Failed to initialize CCD. Unable to load \"*.RBF\".");
    		return FALSE;
    		break;
    
  		case DRV_ERROR_ACK:
    		SYSTEM_LOG("Initialize_CCD(): Failed to initialize CCD. Unable to communicate with card.");
    		return FALSE;
    		break;

  		case DRV_ERROR_FILELOAD:
    		SYSTEM_LOG("Initialize_CCD(): Failed to initialize CCD. Unable to load \"*.COF\" or \"*.RBF\" files.");
    		return FALSE;
    		break;

  		case DRV_ERROR_PAGELOCK:
    		SYSTEM_LOG("Initialize_CCD(): Failed to initialize CCD. Unable to acquire lock on requested memory.");
    		return FALSE;
    		break;

  		case DRV_USBERROR:
    		SYSTEM_LOG("Initialize_CCD(): Faield to initialize CCD. Unable to detect USB device or not USB2.0.");
    		return FALSE;
    		break;

  		case DRV_ERROR_NOCAMERA:
    		SYSTEM_LOG("Initialize_CCD(): Faield to initialize CCD. No camera found.");
   			return FALSE;
    		break;

  		default:

    		/* This should never happen */

    		SYSTEM_LOG("Initialize_CCD(): WHAT THE HELL!!!!");
    		return FALSE;
    		break;
    }

  return FALSE;
}

/*
 * Start the image acquisition.
 */
int Start_Acquisition() {
	unsigned int error;

	error = StartAcquisition();

  	switch (error) {

  		case DRV_SUCCESS:

    		return TRUE;
    		break;

  		case DRV_NOT_INITIALIZED:

    		SYSTEM_LOG("Start_Acquisition(): Failed to start acquisition. System not initalized.");
    		return FALSE;
    		break;

  		case DRV_ACQUIRING:
    
    		SYSTEM_LOG("Start_Acquisition(): Failed to start acquisition. Acquisition in progress.");
    		return FALSE;
    		break;

  		case DRV_VXDNOTINSTALLED:

    		SYSTEM_LOG("Start_Acquisition(): Failed to start acquisition. VxD not loaded.");
    		return FALSE;
    		break;

  		case DRV_ERROR_ACK:

    		SYSTEM_LOG("Start_Acquisition(): Failed to start acquisition. Unable to communicate with card.");
    		return FALSE;
    		break;

  		case DRV_INIERROR:
    
    		SYSTEM_LOG("Start_Acquisition(): Failed to start acquisition. Error reading \"DETECTOR.INI\".");
    		return FALSE;
    		break;

  		case DRV_ACQUISITION_ERRORS: 

    		/* DRV_ACQERROR not found???? */

    		SYSTEM_LOG("Start_Acquisition(): Failed to start acquisition. Acquisition settings inavlid.");
    		return FALSE;
    		break;

  		case DRV_ERROR_PAGELOCK:

    		SYSTEM_LOG("Start_Acquisition(): Failed to start acquisition. Unable to allocate memory.");
    		return FALSE;
    		break;

  		case DRV_INVALID_FILTER:

    		SYSTEM_LOG("Start_Acquisition(): Faield to start acquisition. Filter not available for current acquisition.");
    		return FALSE;
    		break;

  		default:

    		/* This should never happen */

    		SYSTEM_LOG("Start_Acquisition(): WHAT THE HELL!!!!");
    		return FALSE;
    		break;

  	}

  return TRUE;
}

/*
 * Set the horizontal and vertical binning and the start and stop points 
 * for the image. 
 */ 
int Set_Image(int hbin, int vbin, int hstart, int hend, int vstart, int vend) {
	unsigned int error;

	//SYSTEM_LOG("hbin %d vbin %d hstart %d hend %d vstart %d vend %d",hbin,vbin,hstart,hend,vstart,vend);

  	error = SetImage(hbin,vbin,hstart,hend,vstart,vend);

  	switch (error) {

  		case DRV_SUCCESS:
    		return TRUE;
    		break;

  		case DRV_NOT_INITIALIZED:
    		SYSTEM_LOG("Set_Image(): Failed to set image. System not initalized.");
    		return FALSE;
    		break;

  		case DRV_ACQUIRING:
   			SYSTEM_LOG("Set_Image(): Failed to set image. Acquisition in progress.");
    		return FALSE;
    		break;

  		case DRV_P1INVALID:
    		SYSTEM_LOG("Set_Image(): Failed to set image. Binning parameter invalid.");
    		return FALSE;
   			break;

  		case DRV_P2INVALID:
    		SYSTEM_LOG("Set_Image(): Failed to set image. Binning parameter invalid.");
    		return FALSE;
    		break;

  		case DRV_P3INVALID:
    		SYSTEM_LOG("Set_Image(): Failed to set image. Sub-area coordinate invalid.");
    		return FALSE;
    		break;

  		case DRV_P4INVALID:    
    		SYSTEM_LOG("Set_Image(): Failed to set image. Sub-area coordinate invalid.");
    		return FALSE;
    		break;

  		case DRV_P5INVALID:
    		SYSTEM_LOG("Set_Image(): Failed to set image. Sub-area coordinate invalid.");
    		return FALSE;
    		break;

  		case DRV_P6INVALID:
    		SYSTEM_LOG("Set_Image(): Failed to set image. Sub-area coordinate invalid.");
    		return FALSE;
    		break;

  		default:
    		SYSTEM_LOG("Set_Image(): WHAT THE HELL!!!!");
    		return FALSE;
  	}

	return FALSE;
}

/*
 * Save raw data array to TIFF file  
 */
int Save_To_TIFF(char *filename, char *pal, int pos, int imageBits){
	
	unsigned int error;

  	error = SaveAsTiff(filename,pal,pos,imageBits);

  	switch (error) {

  		case DRV_SUCCESS:
    		return TRUE;
    		break;

  		case DRV_NOT_INITIALIZED:
    		SYSTEM_LOG("Save_To_TIFF(): Failed to create image. System not initalized.");
    		return FALSE;
    		break;

  		case DRV_ACQUIRING:
   			SYSTEM_LOG("Save_To_TIFF(): Failed to create image. Acquisition in progress.");
    		return FALSE;
    		break;

  		case DRV_P1INVALID:
    		SYSTEM_LOG("Save_To_TIFF(): Failed to create image. Path invalid.");
    		return FALSE;
   			break;

  		case DRV_P2INVALID:
    		SYSTEM_LOG("Save_To_TIFF(): Failed to create image. Invalid palette file.");
    		return FALSE;
    		break;

  		case DRV_P3INVALID:
    		SYSTEM_LOG("Save_To_TIFF(): Failed to create image. Position out of range.");
    		return FALSE;
    		break;

  		case DRV_P4INVALID:    
    		SYSTEM_LOG("Save_To_TIFF(): Failed to create image. Type of TIFF not valid.");
    		return FALSE;
    		break;

  		case DRV_ERROR_ACK:
    		SYSTEM_LOG("Save_To_TIFF(): Failed to create image. Unable to communicate with card.");
    		return FALSE;
    		break;

  		case DRV_ERROR_PAGELOCK:
    		SYSTEM_LOG("Save_To_TIFF(): Failed to create image. File too large to be generated in memory.");
    		return FALSE;
    		break;

  		default:
    		SYSTEM_LOG("Save_To_TIFF(): WHAT THE HELL!!!!");
    		return FALSE;
  	}

	return FALSE;
}

/*
 * Save raw data array to BMP file  
 */
int Save_To_BMP(char* filename, char* pal, long ymin, long ymax){

	unsigned int error;

  	error = SaveAsBmp(filename,pal,ymin,ymax);

  	switch (error) {

  		case DRV_SUCCESS:
    		return TRUE;
    		break;

  		case DRV_NOT_INITIALIZED:
    		SYSTEM_LOG("Save_To_TIFF(): Failed to create image. System not initalized.");
    		return FALSE;
    		break;

  		case DRV_ACQUIRING:
   			SYSTEM_LOG("Save_To_TIFF(): Failed to create image. Acquisition in progress.");
    		return FALSE;
    		break;

  		case DRV_P1INVALID:
    		SYSTEM_LOG("Save_To_TIFF(): Failed to create image. Path invalid.");
    		return FALSE;
   			break;

  		case DRV_ERROR_ACK:
    		SYSTEM_LOG("Save_To_TIFF(): Failed to create image. Unable to communicate with card.");
    		return FALSE;
    		break;

  		case DRV_ERROR_PAGELOCK:
    		SYSTEM_LOG("Save_To_TIFF(): Failed to create image. File too large to be generated in memory.");
    		return FALSE;
    		break;

  		default:
    		SYSTEM_LOG("Save_To_TIFF(): WHAT THE HELL!!!!");
    		return FALSE;
  	}

	return FALSE;
}

/*
 * Check the validity of the temperature.
 */
int Check_Temperature(int temperature) {
  	char line_buffer[255] = {0};
  	int error;
  	int min_temp;
  	int max_temp;

  	error = Get_Temperature_Range(&min_temp,&max_temp);

  	if (error != TRUE) {
    	SYSTEM_LOG("Check_Temperature(): Failed to get the valid temperature range.");
    	return FALSE;
  	}

  	if (temperature < min_temp || temperature > max_temp) {
    	sprintf(line_buffer,"Check_Temperature(): Set temperature %d is out of range: %d - %d.",temperature,min_temp,max_temp);
    	SYSTEM_LOG(line_buffer);
    	return FALSE;
  	}
  
  	return TRUE;
}


/*
 * Check the validity of the AD channel.
 */
int Check_AD_Channel(int ad_channel) {
	char line_buffer[255] = {0};
  	int num_ad_channels;
  	int error; 

  	error = Get_Number_AD_Channels(&num_ad_channels);

  	if (error != TRUE) {
    	sprintf(line_buffer,"Check_AD_Channel(): Failed to get the number of available A/D channels. Error: %d.",error);
    	SYSTEM_LOG(line_buffer);
    	return FALSE;
  	}

  	if (ad_channel < 0 || ad_channel > (num_ad_channels - 1)) {
    	sprintf(line_buffer,"Check_AD_Channel(): Set AD channel %d is not available.",ad_channel);
    	SYSTEM_LOG(line_buffer);
    	return FALSE;
  	}
      
  	return TRUE;
}

/**
 * Check the validity of the output amplifier.
 */
int Check_Output_Amp(int amp) {
	char line_buffer[255] = {0};
  	int num_amps;
  	int error;

  	error = Get_Number_Amp(&num_amps);
  
  	if (error != TRUE) {
    	sprintf(line_buffer,"Check_output_Amp(): Failed to get the number of available output amplifiers. Error: %d.",error);
    	SYSTEM_LOG(line_buffer);
    	return FALSE;
  	}

  	if (amp < 0 || amp > (num_amps - 1)) {
    	sprintf(line_buffer,"Check_Output_Amp(): Output amplifier %d is not available.",amp);
    	SYSTEM_LOG(line_buffer);
    	return FALSE;
  	}

  	return TRUE;
}

/*
 * Check the validity of the horizontal shift speed.
 */
int Check_HS_Speed(int hs_speed,int ad_channel,int output_amp) {
	char line_buffer[255] = {0};
  	int num_hs_speeds;
  	int error;

  	error = Get_Number_HS_Speeds(ad_channel,output_amp,&num_hs_speeds);

  	if (error != TRUE) {
    	SYSTEM_LOG("Check_HS_Speed(): Failed to get the number of available horizontal shift speedsd.");
    	return FALSE;
  	}
   
  	if (hs_speed < 0 || hs_speed > (num_hs_speeds - 1)) {
    	sprintf(line_buffer,"Check_HS_Speed(): Horizontal shift speed %d is not available.",hs_speed);
    	SYSTEM_LOG(line_buffer);
    	return FALSE;
  	}

  	return TRUE;
}

/*
 * Check the validity of the vertical shift speed.
 */
int Check_VS_Speed(int vs_speed) {
	char line_buffer[255] = {0};
  	int num_vs_speeds;
  	int error;

  	error = Get_Number_VS_Speeds(&num_vs_speeds);

  	if (error != TRUE) {
    	SYSTEM_LOG("Check_VS_Speed(): Failed to get the number of available vertical shift speeds.");
    	return FALSE;
  	}

  	if (vs_speed < 0 || vs_speed > (num_vs_speeds - 1)) {
    	sprintf(line_buffer,"Check_VS_Speed(): Vertical shift speed %d is not available.",vs_speed);
    	SYSTEM_LOG(line_buffer);
    	return FALSE;
  	}
  
  	return TRUE;
}

/*
 * Check the validity of the vertical clock voltage amplitude.
 */ 
int Check_VS_Amplitude(int vs_amplitude,int vs_speed) {
	char line_buffer[255] = {0};
  	int error;
  	int num_vs_amplitudes;
  	int fastest_vs_speed_index;
  	float fastest_vs_speed;

  	error = Get_Number_VS_Amplitudes(&num_vs_amplitudes);

  	if (error != TRUE) {
    	SYSTEM_LOG("Check_VS_Amplitude(): Failed to get the number of available vertical clock voltage amplitudes.");
    	return FALSE;
  	}
  
  	if (vs_amplitude < 0 || vs_amplitude > (num_vs_amplitudes - 1)) {
    	sprintf(line_buffer,"Check_VS_Amplitude(): Vertical clock voltage amplitude %d is not available.",vs_amplitude);
    	SYSTEM_LOG(line_buffer);
    	return FALSE;
  	}
  
  	error = Get_Fastest_Recommended_VS_Speed(&fastest_vs_speed_index,&fastest_vs_speed);

  	if (error != TRUE) {
    	SYSTEM_LOG("Check_VS_Amplitude(): Failed to get the fastest recommended vertical shift speed.");
    	return FALSE;
  	}

  	if (vs_speed <= fastest_vs_speed_index && vs_amplitude > 0){
    	SYSTEM_LOG("WARNING! Vertical shift speed is not above the fastest recommended speed but the set vertical clock voltage amplitude is above the normal. THE VERTICAL CLOCK VOLTAGE AMPLITUDE SHOULD BE SET TO NORMAL!!!");
 	}

  	if (vs_speed > fastest_vs_speed_index && vs_amplitude == 0) {
    	SYSTEM_LOG("WARNING! Vertical shift speed is above the fastest recommended speed. THE VERTICAL CLOCK VOLTAGE SHOULD BE ADJUSTED!!!.");
  	}

 	return TRUE;
}

/*
 * Check the validity of the pre-amplifier gain.
 */
int Check_Pre_Amp_Gain(int pre_amp_gain,int ad_channel,int output_amp,int hs_speed,int *available_pre_amp_gain_index) {
  	char line_buffer[255] = {0};
  	int num_pre_amp_gains;
  	int pre_amp_gain_available = PRE_AMP_GAIN_AVAILABLE;
  	int error;

  	// Get the number of available pre-amp gains

  	error = Get_Number_Pre_Amp_Gains(&num_pre_amp_gains);

  	if(error != TRUE) {
    	sprintf(line_buffer,"Check_Pre_Amp_Gain(): Failed to get the number of available pre-amplifier gains. Error: %d.",error);
    	SYSTEM_LOG(line_buffer);
    	return FALSE;
  	}

  	if(pre_amp_gain < 0 || pre_amp_gain > (num_pre_amp_gains - 1)) {
    	pre_amp_gain_available = PRE_AMP_GAIN_NOT_AVAILABLE; 
  	} 
  
  	//Check that the A/D channel exists, andn that the amplifier, speed and gain are available 
    //for the A/D channel.
    
  	if(pre_amp_gain_available == PRE_AMP_GAIN_AVAILABLE) {
    
    	error = Is_Pre_Amp_Gain_Available(ad_channel,output_amp,hs_speed,pre_amp_gain,&pre_amp_gain_available);
    
    	if (error != TRUE) {
      		sprintf(line_buffer,"Check_Pre_Amp_Gain(): Failed to retrieve pre-amplifier gain availability information. Error: %d.",error);
      		SYSTEM_LOG(line_buffer);
      		return FALSE;
    	}
  	}
  
  	// If the set pre-amplifier gain is not available, try to find available one.

  	if(pre_amp_gain_available == PRE_AMP_GAIN_NOT_AVAILABLE) {
    	sprintf(line_buffer,"Check_Pre_Amp_Gain(): Pre-amplifier gain %d is not available. Trying to find available one.",pre_amp_gain);
    	SYSTEM_LOG(line_buffer);
    
    	if(Find_Lowest_Available_Pre_Amp_Gain(num_pre_amp_gains,ad_channel,output_amp,hs_speed,available_pre_amp_gain_index) != TRUE) {
      		SYSTEM_LOG("Check_Pre_Amp_Gain(): Failed to find available pre-amplifier gain.");
    	}
    	return FALSE;
  	} 

  	return TRUE;
}

/*
 * Find the lowest available pre-amplifier gain index.
 */
int Find_Lowest_Available_Pre_Amp_Gain(int num_pre_amp_gains,int ad_channel,int output_amp,int hs_speed,int *available_pre_amp_gain){
	char line_buffer[255] = {0};
  	int error;
  	int pre_amp_gain_available;
  	int i;

  	for (i=0; i<num_pre_amp_gains; i++) {
    	error = Is_Pre_Amp_Gain_Available(ad_channel,output_amp,hs_speed,i,&pre_amp_gain_available);
    
    	if (error != TRUE) {
      		sprintf(line_buffer,"Find_Lowest_Available_Pre_Amp_Gain(): Failed to retrieve pre-amplifier gain availability information. Error: %d.",error);
      		SYSTEM_LOG(line_buffer);
      		return FALSE;
    	}

    	if (pre_amp_gain_available == PRE_AMP_GAIN_AVAILABLE) {
      		sprintf(line_buffer,"Lowest available pre-amp gain index %d",i);
			SYSTEM_LOG(line_buffer);
			*available_pre_amp_gain = i;
      		return TRUE;;
    	}
  	}

  	SYSTEM_LOG("Find_Lowest_Available_Pre_Amp_Gain(): Couldn't find any available pre-amplifier gains???");
  	return FALSE;
}

/*
 * Check that the pre-amplifier gain is available with the given A/D channel and amplifier.
 */
int Is_Pre_Amp_Gain_Available(int channel, int amplifier, int index, int gain, int *status) {
  
  unsigned int error;

  error = IsPreAmpGainAvailable(channel,amplifier,index,gain,status);

  switch (error) {

  case DRV_SUCCESS:

    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Is_Pre_Amp_Gain_Available(): Failed to check the availability of the pre-amplifier gain. System not initalized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    SYSTEM_LOG("Is_Pre_Amp_Gain_Available(): Failed to check the availability of the pre-amplifier gain. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Is_Pre_Amp_Gain_Available(): Failed to check the availability of the pre-amplifier gain. Invalid channel.");
    return FALSE;
    break;

  case DRV_P2INVALID:
    SYSTEM_LOG("Is_Pre_Amp_Gain_Available(): Failed to check the availability of the pre-amplifier gain. Invalid amplifier.");
    return FALSE;
    break;

  case DRV_P3INVALID:
    SYSTEM_LOG("Is_Pre_Amp_Gain_Available(): Failed to check the availability of the pre-amplifier gain. Invalid speed index.");
    return FALSE;
    break;

  case DRV_P4INVALID:
    SYSTEM_LOG("Is_Pre_Amp_Gain_Available(): Failed to check the availability of the pre-amplifier gain. Invalid gain.");
    return FALSE;
    break;

  default:

    SYSTEM_LOG("Is_Pre_Amp_Gain_Available(): WHAT THE HELL!!!!");
    return FALSE;
    break;

  }

  return FALSE; 
}

/*
 * Print available A/D channels and their bit depths to the screen.
 */
int Print_AD_Channels() {

	char line_buffer[255] = {0};
  	int error;
  	int num_ad_channels;
  	int bit_depth;
  	int i;

  	// Get the number of available A/D channels 
  
  	error = Get_Number_AD_Channels(&num_ad_channels);

  	if (error != TRUE) {
    	sprintf(line_buffer,"Print_AD_Channels(): Failed to get the number of available A/D channels.");
    	SYSTEM_LOG(line_buffer);
    	return FALSE;
  	}

  	// Print available A/D channels and bit depths to the screen

  	for (i=0; i<num_ad_channels; i++) {   

    	if (Get_Bit_Depth(i,&bit_depth) != TRUE) {
      		sprintf(line_buffer,"Print_AD_Channels(): Failed to get the bit depth for the A/D channel %d.",i);
      		SYSTEM_LOG(line_buffer);
      		return FALSE;
    	}

    	sprintf(line_buffer,"\tchannel: %d, bit depth %d",i,bit_depth);
    	SYSTEM_LOG(line_buffer);

  	}

  	return TRUE;

}

/*
 * Print available vertical shift speeds to the screen.
 */
int Print_Available_VS_Speeds() {

	char line_buffer[255] = {0};
  	int num_vs_speeds;
  	int error;
  	int i;
  	float vs_speed_usp;

  	// Get the number of available vertical shift speeds

  	error = Get_Number_VS_Speeds(&num_vs_speeds);
  
  	if (error != TRUE) {
    	sprintf(line_buffer,"Print_Available_VS_Speeds(): Failed to get the number of available vertical shift speeds.");
    	SYSTEM_LOG(line_buffer);
    	return FALSE;
  	}
  
  	// Print the available vertical shift speeds and their actual speed 
    //in microseconds per pixel shift
  
  	sprintf(line_buffer,"Available vertical shift speeds: ");
  	SYSTEM_LOG(line_buffer);

  	for (i=0; i<num_vs_speeds; i++) {
    
    	if (Get_VS_Speed(i,&vs_speed_usp) != TRUE) {
      		sprintf(line_buffer,"Print_Available_VS_Speeds(): Failed to get the actual vertical shift speed for speed setting %d.",i);
      		
      		return FALSE;
    	}
      
    	sprintf(line_buffer,"\tvertical shift speed: %d, %f microseconds per pixels shift",i,vs_speed_usp);
    	SYSTEM_LOG(line_buffer);
  
  	}
  	return TRUE;
}

/*
 * Print available horizontal shift speeds to stdout.
 */
int Print_Available_HS_Speeds(int ad_channel, int output_amp) {

  	char line_buffer[255] = {0};
  	int error;
  	int num_hs_speeds;
  	int i;
  	float hs_speed_MHz;

  	// Get the number of available horizontal shift speeds

  	error = Get_Number_HS_Speeds(ad_channel,output_amp,&num_hs_speeds);

  	if (error != TRUE) {
    	SYSTEM_LOG("Print_Available_HS_Speeds(): Failed to get the number of available horizontal shift speeds.");
    	return FALSE;
  	}

  	// Print the available speed settings and their actual values to the screen

  	for (i=0; i<num_hs_speeds; i++) {

    	if (Get_HS_Speed(ad_channel,output_amp,i,&hs_speed_MHz) != TRUE) {
      		sprintf(line_buffer,"Print_Available_HS_Speeds(): Failed to get the actual horizontal shift speed for speed setting %d.",i);
      		SYSTEM_LOG(line_buffer);
      		return FALSE;
    	}

    	sprintf(line_buffer,"\thorizontal shift speed: %d, %f MHz",i,hs_speed_MHz);
    	SYSTEM_LOG(line_buffer);

  	}
  	return TRUE;
}

int Print_Available_HS_Settings() {

  	char line_buffer[255] = {0};
  	int error;
  	int num_ad_channels;
  	int num_amp;
  	int num_pre_amp_gains;
  	int bit_depth;
  	int num_hs_speeds;
  	int pre_amp_gain_available;
  	float hs_speed;
  	float pre_amp_gain;
  	int i, j, k, l;
  	char output[255] = {0};
 	

  	// Get the number of available A/D channles

  	error = Get_Number_AD_Channels(&num_ad_channels);

  	if (error != TRUE) {
    	SYSTEM_LOG("Print_Available_HS_Settings(): Failed to get the number of available A/D channels");
    	return FALSE;
  	}

  	// Get the number of available output amplifiers

  	error = Get_Number_Amp(&num_amp);

  	if (error != TRUE) {
    	SYSTEM_LOG("Print_Available_HS_Settings(): Failed to get the number of available output amplifiers.");
    	return FALSE;
  	}

  	// Get the number of pre-amplifier gains

  	error = Get_Number_Pre_Amp_Gains(&num_pre_amp_gains);
  
  	if (error != TRUE) {
    	SYSTEM_LOG("Print_Available_HS_Settings(): Failed to get the number of available pre-amplifier gains.");
    	return FALSE;
  	}

  	// Go trough all available setting combinations

  	for (i=0; i<num_ad_channels; i++) {
    
    	if (Get_Bit_Depth(i,&bit_depth) != TRUE) {
      		sprintf(line_buffer,"Print_Available_HS_Settings(): Failed to get the bit depth for A/D channel %d.",i);
      		SYSTEM_LOG(line_buffer);
      		return FALSE;
    	}
    
    	sprintf(line_buffer,"A/D channel : %d, bit depth %d.",i,bit_depth);
    	SYSTEM_LOG(line_buffer);

    	for (j=0; j<num_amp; j++) {
      
      		error = Get_Number_HS_Speeds(i,j,&num_hs_speeds);

      		if (error != TRUE) {
				SYSTEM_LOG("Print_Available_HS_Settings(): Failed to get the number of available horizontal shift speeds.");
				return FALSE;
      		}

      		sprintf(line_buffer,"\toutput amplifier: %d,",j);
      		SYSTEM_LOG(line_buffer);

      		for (k=0; k<num_hs_speeds; k++) {
	
				if (Get_HS_Speed(i,j,k,&hs_speed) != TRUE) {
	  				SYSTEM_LOG("Print_Available_HS_Settings(): Failed to get the actual horizontal shift speeds.");
	  				return FALSE;
				}

				sprintf(line_buffer,"\t\thorizontal shift speed: %d, %f MHz",k,hs_speed);
				SYSTEM_LOG(line_buffer);
	
				for (l=0; l<num_pre_amp_gains; l++) {
	
	  				if (Get_Pre_Amp_Gain(l,&pre_amp_gain) != TRUE) {
	    				SYSTEM_LOG("Print_Available_HS_Settings(): Failed to get the pre-amplifier gain factor.");
	    				return FALSE;
	  				}

	  				error = Is_Pre_Amp_Gain_Available(i,j,k,l,&pre_amp_gain_available);

	  				if (error != TRUE) {
	   					SYSTEM_LOG("Print_Available_HS_Settings(): Failed to get the pre-amplifier gain availability.");
	    				return FALSE;
	  				}

	  				if (pre_amp_gain_available == PRE_AMP_GAIN_AVAILABLE) {
						sprintf(line_buffer,"%s\t\t\tpre-amplifier gain: %d, gain factor %f",output,l,pre_amp_gain);
						SYSTEM_LOG(line_buffer);
					}
				}
      		}
    	}
  	}
  	return TRUE;
}

/*
 * Print available vertical clock voltage amplitude to the screen.
 */
int Print_Available_VS_Amplitudes() {

  	char line_buffer[255] = {0};
  	int error;
  	int num_vs_amplitudes;
  	int i;

  	// Get the number of available vertical clock voltage amplitudes

  	error = Get_Number_VS_Amplitudes(&num_vs_amplitudes);
  
  	if (error != TRUE) {
    	SYSTEM_LOG("Print_Available_VS_Amplitudes(): Failed to get the number of available vertical clock voltage amplitudes.");
    	return FALSE;
  	}	
  
  	/* Print the amplitudes to the screen */

  	SYSTEM_LOG("Available vertical clock voltage amplitudes: ");

  	SYSTEM_LOG("\tvertical clock voltage amplitude: 0, NORMAL");

  	for (i=1; i<num_vs_amplitudes; i++) {
    	sprintf(line_buffer,"\tvertical clock voltage amplitude: %d, +%d",i,i);
    	SYSTEM_LOG(line_buffer);
  	}
  	return TRUE;
}

/*
 * Print available pre-amplifier gains to stdout.
 */
int Print_Available_Pre_Amp_Gains(int ad_channel,int output_amp,int hs_speed) {

  	char line_buffer[255] = {0};
  	int num_pre_amp_gains;
  	int pre_amp_gain_available;
  	int error;
  	int pre_amp_gain;
  	float pre_amp_gain_factor;

  	// Get the number of available pre-amp gains

  	error = Get_Number_Pre_Amp_Gains(&num_pre_amp_gains);

  	if (error != TRUE) {
    	SYSTEM_LOG("List_Available_Pre_Amp_Gains(): Failed to get the number of available pre-amplifier gains.");
    	return FALSE;
  	}

  	// For each pre-amp gain check that the A/D channel exists, and that the amplifier, 
    //	speed and gain are available.

  	for (pre_amp_gain=0; pre_amp_gain<num_pre_amp_gains; pre_amp_gain++) { 
  
    	error = Is_Pre_Amp_Gain_Available(ad_channel,output_amp,hs_speed,pre_amp_gain,&pre_amp_gain_available);

    	if (error != TRUE) {
      		sprintf(line_buffer,"List_Available_Pre_Amp_Gains(): Failed to retrieve pre-amplifier gain availability information. Error: %d.",error);
      		SYSTEM_LOG(line_buffer);
      		return FALSE;
    	}
    
    	// If the pre-amp gain is available, get the actual gain factor and print 
       	//   all to stdout.
    
    	if (pre_amp_gain_available == PRE_AMP_GAIN_AVAILABLE) {
        
      		if (Get_Pre_Amp_Gain(pre_amp_gain,&pre_amp_gain_factor) != TRUE) {
				sprintf(line_buffer,"List_Available_Pre_Amp_Gains(): Failed to get the pre-amplifier gain factor for gain %d.",pre_amp_gain);
				SYSTEM_LOG(line_buffer);
				return FALSE;
      		}
	
      		sprintf(line_buffer,"\tpre-amplifier gain: %d, gain factor: %f",pre_amp_gain,pre_amp_gain_factor);
      		SYSTEM_LOG(line_buffer);
    	}
  	}
	return TRUE;
}

/*
 * Print the current cooling status to the screen.
 */
void Print_Cooling_Status() {

    unsigned int error;
    int temperature;
    char str[255] = {0};

    error = GetTemperature(&temperature);

    switch (error) {
    
    case DRV_TEMP_OFF:
      sprintf(str,"Cooler OFF. Detector temperature %d C.",temperature);
      SYSTEM_LOG(str);
      break;
    
    case DRV_TEMP_STABILIZED:
      sprintf(str,"Stabilized. Detector temperature %d C.",temperature);
      SYSTEM_LOG(str);
      break;

    case DRV_TEMP_NOT_REACHED:
      sprintf(str,"Cooling. Detector temperature %d C.",temperature);
      SYSTEM_LOG(str);
      break;
   
    case DRV_NOT_INITIALIZED:
      sprintf(str,"Failed to get cooling status. System not initialized. Detector temperature %d C.",temperature);
      SYSTEM_LOG(str);
      break;

    case DRV_ACQUIRING:
      sprintf(str,"Failed to get cooling status. Acquisition in progress. Detector temperature %d C.",temperature);
      SYSTEM_LOG(str);
      break;

    case DRV_ERROR_ACK:
      sprintf(str,"Failed to get cooling status. Unable to communicate with card. Detector temperature %d C.",temperature);
      SYSTEM_LOG(str);
      break;

    case DRV_TEMP_DRIFT:
      sprintf(str,"Temperature has stabilized but has since drifted. Detector temperature %d C.",temperature);
      SYSTEM_LOG(str);
      break;

    case DRV_TEMP_NOT_STABILIZED:
      sprintf(str,"Temperature reached but not stabilized. Detector temperature %d C.",temperature);
      SYSTEM_LOG(str);
      break;

    default:
      SYSTEM_LOG("What the f?#@!&");
    }
}

/*
 * Start cooler.
 */
int Start_Cooler() {

    unsigned int error;

    error = CoolerON();

    switch (error) {

    case DRV_SUCCESS:
      
      SYSTEM_LOG("Start_Cooler(): Cooler Started.");
        return TRUE;
        break;
      
    case DRV_NOT_INITIALIZED:
    
      SYSTEM_LOG("Start_Cooler(): Failed to start cooling. System not initialized.");
      return FALSE;
      break;
    
    case DRV_ACQUIRING:

      SYSTEM_LOG("Start_Cooler(): Failed to start cooling. Acquisition in progress.");
      return FALSE;
      break;

    case DRV_ERROR_ACK:

      SYSTEM_LOG("Start_Cooler(): Failed to start cooling. Unable to communicate with card.");
      return FALSE;
      break;

    default:

      SYSTEM_LOG("Start_Cooler(): What the f?#@!&");
      return FALSE;
      
    }

    return(TRUE);
}

/*
 * Stop cooler.
 */
int Stop_Cooler() {

    unsigned int error;

    error = CoolerOFF();

    switch (error) {
    
    case DRV_SUCCESS:

      return TRUE;
      break;

    case DRV_NOT_INITIALIZED:
    
      SYSTEM_LOG("Stop_Cooler(): Failed to stop cooling. System not initialized.");
      return FALSE;
      break;

    case DRV_ACQUIRING:

      SYSTEM_LOG("Stop_Cooler(): Failed to stop coolig. Acquisition in progress.");
      return FALSE;
      break;

    case DRV_ERROR_ACK:

      SYSTEM_LOG("Stop_Cooler(): Failed to stop cooling. Unable to communicate with card.");
      return FALSE;
      break;

    case DRV_NOT_SUPPORTED:
      SYSTEM_LOG("Stop_Cooler(): Failed to stop cooling. Camera does not support switching cooler off.");
      return FALSE;
      break;

    default:
    
      SYSTEM_LOG("Stop_Cooler():  What the f?#@!&");
      return FALSE;
    }
    return TRUE;
}

/*
 * Check if the detector has reached the target temperature.
 */
int Is_Target_Temperature_Reached(int target_temperature, int *temperature, int *ok) {

  unsigned int cooling_status;
  int current_temperature;
  
  // Get the cooling status

  if (Get_Cooling_Status(&cooling_status, &current_temperature) != TRUE) {
    SYSTEM_LOG("Is_Target_Temperature_Reached(): Failed to get cooling status.");
    return FALSE;
  }

  *temperature = current_temperature;

  if (cooling_status == DRV_TEMP_STABILIZED) {
    *ok = TRUE;
    return TRUE;
  } else {
    *ok = FALSE;
    return TRUE;
  }

}

/*
 * Check if detector temperature is above the limit.
 */
int Is_Temperature_Above_Limit(int limit, int *temperature, int *ok) {

  int current_temperature;
  int status;
  char str[255] = {0};
  
  // Get the current detector temperature

  if (Get_Detector_Temperature(&current_temperature,&status) != TRUE) {
    SYSTEM_LOG("Is_Temperature_Above_Limit(): Failed to get current detector temperature.");
    return FALSE;
  }

  *temperature = current_temperature;
  
  if (current_temperature < -900) {
    sprintf(str,"Is_Temperature_Above_Limit(): Something wrong. Got temperature %d C????",current_temperature);
    SYSTEM_LOG(str);
    return FALSE;
  }
  
  // Check if temperature is abovet the limit

  if (current_temperature > limit) {
    *ok = TRUE;
    return TRUE;
  } else {
    *ok = FALSE;
    return TRUE;
  }
}


/*
 * Wait for the temperature of the detector to reach the given value.
 */
int Is_Min_Temperature_Reached(int temperature, int *ok) {

  unsigned int error;
  int current_temperature;

 // Get the current detector temperature

  error = GetTemperature(&current_temperature);

  switch (error) {

  case DRV_TEMP_OFF: 
  case DRV_TEMP_STABILIZED: 
  case DRV_TEMP_NOT_REACHED:
    
    if (current_temperature > temperature) {
      *ok = TRUE;
      return TRUE;
    } else {
      *ok = TRUE;
      return FALSE;
    }
    break;
    
  case DRV_NOT_INITIALIZED:

    SYSTEM_LOG("Is_Min_Temperature_Reached(): Failed to get detector temeprature. System not initialized.");
    *ok = FALSE;
    return FALSE;
    break;
    
  case DRV_ACQUIRING:

    SYSTEM_LOG("Is_Min_Temperature_Reached(): Failed to get detector temeprature. Acquisition on progress.");
    *ok = FALSE;
    return FALSE;
    break;
    
  case DRV_ERROR_ACK:

    SYSTEM_LOG("Is_Min_Temperature_Reached(): Failed to get detector temeprature. Unable to communicate with card.");
    *ok = FALSE;
    return FALSE;
    break;
    
  default:

    SYSTEM_LOG("Is_Min_Temperature_Reached(): What the f?#@!&");
    return FALSE;

  }
  return TRUE; 
}

/*
 * Set the exposure time.
 */
int Set_Exposure_Time(float exposure) {
	unsigned int error;

	error = SetExposureTime(exposure);

	switch (error) {

	case DRV_SUCCESS:
    	return TRUE;
    	break;

	case DRV_NOT_INITIALIZED:
    	SYSTEM_LOG("Set_Exposure_Time(): ERROR! System not initalized.");
    	return FALSE;
    	break;

  	case DRV_ACQUIRING:
    	SYSTEM_LOG("Set_Exposure_Time(): ERROR! Acquisition in progress.");
    	return FALSE;
    	break;

  	case DRV_P1INVALID:
    	SYSTEM_LOG("Set_Exposure_Time(): ERROR! Exposure time invalid.");
    	return FALSE;
    	break;

  	default:
    	SYSTEM_LOG("Set_Exposure_Time(): WHAT THE HELL!!!!");
    	return FALSE;
    	break;

  	}

  	return FALSE;

}

/*
 * Set the desired temperature.
 */
int Set_Temperature(int temperature) {

	unsigned int error;

  	error = SetTemperature(temperature);

  	switch (error) {
    
  	case DRV_SUCCESS:
    
    	return TRUE;
    	break;
    
  	case DRV_NOT_INITIALIZED:

    	SYSTEM_LOG("Set_Temperature(): Failed to set temperature. System not initialized.");
    	return FALSE;
    	break;

  	case DRV_ACQUIRING:

    	SYSTEM_LOG("Set_Temperature(): Faield to set temperature. Acquisition in progress.");
    	return FALSE;
    	break;

  	case DRV_ERROR_ACK:

    	SYSTEM_LOG("Set_Temperature(): Failed to set temperature. Unable to comminucate with card.");
    	return FALSE;
    	break;

 	case DRV_P1INVALID:

    	SYSTEM_LOG("Set_Temperature(): Failed to set temperature. Temperature invalid.");
    	return FALSE;
    	break;

  	default:

    /* This should never happen */

    	SYSTEM_LOG("Set_Temperature(): What the f?#!�@&$!");
    	return FALSE;
	} 

  	return TRUE;
}

/*
 * Set the readout mode.
 */
int Set_Read_Mode(int mode) {

	unsigned int error;

  	error = SetReadMode(mode);

	switch (error) {
    
  	case DRV_SUCCESS:
    
    	return TRUE;
    	break;
    
  	case DRV_NOT_INITIALIZED:

    	SYSTEM_LOG("Set_Read_Mode(): Failed to set read mode. System not initialized.");
    	return FALSE;
    	break;

  	case DRV_ACQUIRING:

    	SYSTEM_LOG("Set_Read_Mode(): Faield to set read mode. Acquisition in progress.");
    	return FALSE;
    	break;

  	case DRV_P1INVALID:

    	SYSTEM_LOG("Set_Read_Mode(): Failed to set read mode. Read mode invalid.");
    	return FALSE;
    	break;

  	default:

    /* This should never happen */

    	SYSTEM_LOG("Set_Read_Mode(): What the f?#!�@&$!");
    	return FALSE;
      
  	} 

  	return TRUE;
}

/*
 * Set the acquisition mode.
 */ 
int Set_Acquisition_Mode(int mode) {

  	unsigned int error;

  	error = SetAcquisitionMode(mode);

  	switch (error) {
    
  	case DRV_SUCCESS:

    	/* For kinetic series modes we have to set the number of scans  */

    	if ( (mode == KINETICS)||(mode == FAST_KINETICS) ) {
      		if (Set_Kinetic(ACCUMULATION_NUMBER,KINETICS_NUMBER) != TRUE) {
	  			SYSTEM_LOG("Set_Acquisition_Mode(): Failed to set number of scans for kinetic mode.");
	  			return FALSE;
	  			break;
			}
    	}
    	return TRUE;
    	break;
    
  	case DRV_NOT_INITIALIZED:
    	SYSTEM_LOG("Set_Acquisition_Mode(): Failed to set acquisition mode. System not initialized.");
    	return FALSE;
    	break;

  	case DRV_ACQUIRING:
    	SYSTEM_LOG("Set_Acquisition_Mode(): Faield to set acquisition mode. Acquisition in progress.");
    	return FALSE;
    	break;

  	case DRV_P1INVALID:
    	SYSTEM_LOG("Set_Acquisition_Mode(): Failed to set acquisition mode. Acquisition mode invalid.");
    	return FALSE;
   		break;

  	default:

    /* This should never happen */

    	SYSTEM_LOG("Set_Acquisition_Mode(): What the f?#!�@&$!");
    	return FALSE;
      
  	} 

  	return FALSE;
}

/*
 * Set number of scans for kinetic mode.
 */
int Set_Kinetic(int accs, int scans)
{

  	unsigned int error;

  	error = SetNumberAccumulations(accs);
  
  	switch (error) {
    
    case DRV_SUCCESS: 

      	return TRUE;
      	break;

    case DRV_NOT_INITIALIZED:

      	SYSTEM_LOG("Set_Kinetic(): Failed to set number of accumulations. System not initialized.");
      	return FALSE;
      	break;
    
    case DRV_ACQUIRING:

      	SYSTEM_LOG("Set_Kinetic(): Failed to set number of accumulations.  Acquisition in progress.");
      	return FALSE;
     	 break;

    case DRV_P1INVALID:
      
      	SYSTEM_LOG("Set_Kinetic(): Failed to set number of accumulations. Invalid array pointer.");
      	return FALSE;
      	break;
      
    default:

      	/* This should never happen */
      
      	SYSTEM_LOG("Set_Kinetic(): What the f?#@!&");
      	return FALSE;      
      
    }
  
  	error = SetNumberKinetic(scans);

  	switch (error) {
    
    case DRV_SUCCESS: 

      	return TRUE;
      	break;

    case DRV_NOT_INITIALIZED:

      	SYSTEM_LOG("Set_Kinetic(): Failed to set number of scans. System not initialized.");
      	return FALSE;
      	break;
    
    case DRV_ACQUIRING:

      	SYSTEM_LOG("Set_Kinetic(): Failed to set number of scans.  Acquisition in progress.");
      	return FALSE;
      	break;

    case DRV_P1INVALID:
      
      	SYSTEM_LOG("Set_Kinetic(): Failed to set number of scans. Invalid array pointer.");
      	return FALSE;
      	break;
      
    default:

      	/* This should never happen */
      
      	SYSTEM_LOG("Set_Kinetic(): p*rk*");
      	return FALSE;      
      
    }
}

/*
 * Set the kinetic cycle time.
 */
int Set_Kinetic_Cycle_Time(float time) {

  unsigned int error;

  error = SetKineticCycleTime(time);

  switch (error) {
  
  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Set_Kinetic_Cycle_Time(): Unable to set the kinetic cycle time. System not initalized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    SYSTEM_LOG("Set_Kinetic_Cycle_Time(): Unable to set the kinetic cycle time. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Set_Kinetic_Cycle_Time(): Unable to set the kinetic cycle time. Time invalid.");
    return FALSE;
    break;

  default:
    SYSTEM_LOG("Set_Kinetic_Cycle_Time(): What the f?#!�@&$!");
    return FALSE;
  }

  return FALSE;

}

/*
 * Set the number of scans to be taken during a single kinetic seriers acquisiton sequence.
 */
int Set_Number_Kinetics(int number) {
  
  unsigned int error;
  
  error = SetNumberKinetics(number);

  switch (error) {

  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Set_Number_Kinetics(): Unable to set the number of scans in kinetic series. System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    SYSTEM_LOG("Set_Number_Kinetics(): Unable to set the number of scans in kinetic series. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Set_Number_Kinetics(): Unable to set the number of scans in kinetic series. Number in series invalid.");
    return FALSE;
    break;

  default:
    SYSTEM_LOG("Set_Number_Kinetics(): What the f?#!�@&$!");
    return FALSE;

  }
  
  return FALSE;

}

/*
 * Set cooler mode.
 */
int Set_Cooler_Mode(int mode) {

  unsigned int error;

  error = SetCoolerMode(mode);

  switch (error) {
    
  case DRV_SUCCESS:
    return TRUE;
    break;
    
  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Set_Cooler_Mode(): Failed to set cooler mode. System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    SYSTEM_LOG("Set_Cooler_Mode(): Failed to set cooler mode. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Set_Cooler_Mode(): Failed to set cooler mode. Cooler mode invalid.");
    return FALSE;
    break;

  case DRV_NOT_SUPPORTED:
    SYSTEM_LOG("Set_Cooler_Mode(): Failed to set cooler mode. Camera does not support.");
    return FALSE;
    break;

  default:
    /* This should never happen */
    SYSTEM_LOG("Set_Cooler_Mode(): What the f?#!�@&$!");
    return FALSE;
      
  } 

  return FALSE;

}

/*
 * Turn cooler ON/OFF.
 */
int Set_Cooler_State(int state) {

  switch (state) {

  case COOLING_ON: // Start cooler

    if (Start_Cooler() != TRUE) {
      SYSTEM_LOG("Set_Cooler_State(): Failed to start cooling.");
      return FALSE;
    } else {
      return TRUE;
    }
    break;

  case COOLING_OFF: // Stop cooler

    if (Stop_Cooler() != TRUE) {
      SYSTEM_LOG("Set_Cooler_State(): Failed to stop cooling.");
      return FALSE;
    } else {
      return TRUE;
    }
    break;
    
  default:

    SYSTEM_LOG("Set_Cooler_State(): Invalid cooler state.");
    return FALSE;
  }
}


/*
 * Set the fan mode.
 */
int Set_Fan_Mode(int mode) {

  unsigned int error;

  error = SetFanMode(mode);

  switch (error) {
    
  case DRV_SUCCESS:
    return TRUE;
    break;
    
  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Set_Fan_Mode(): Failed to set fan mode. System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    SYSTEM_LOG("Set_Fan_Mode(): Failed to set fan mode. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_I2CTIMEOUT:
    SYSTEM_LOG("Set_Fan_Mode(): Failed to set fan mode. I2C command timed out.");
    return FALSE;
    break;

  case DRV_I2CDEVNOTFOUND:
    SYSTEM_LOG("Set_Fan_Mode(): Failed to set fan mode. I2C device not present.");
    return FALSE;
    break;

  case DRV_ERROR_ACK:
    SYSTEM_LOG("Set_Fan_Mode(): Failed to set fan mode. Unable to communicate with card.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Set_Fan_Mode(): Failed to set fan mode. Fan mode invalid.");
    return FALSE;
    break;

  default:
    /* This should never happen */
    SYSTEM_LOG("Set_Fan_Mode(): What the f?#!�@&$!");
    return FALSE;
      
  } 

  return FALSE;

}

/*
 * Set the horizontal shift speed.
 */
int Set_HS_Speed(int type, int index) {

  unsigned int error;

  error = SetHSSpeed(type,index);

  switch (error) {
    
  case DRV_SUCCESS:
    return TRUE;
    break;
    
  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Set_HS_Speed(): Failed to set horizontal shift speed. System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    SYSTEM_LOG("Set_HS_Speed(): Failed to set horizontal shift speed. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Set_HS_Speed(): Failed to set horizontal shift speed. Mode invalid.");
    return FALSE;
    break;

  case DRV_P2INVALID:
    SYSTEM_LOG("Set_HS_Speed(): Failed to set horizontal shift speed. Index out of range.");
    return FALSE;
    break;

  default:
    /* This should never happen */
    SYSTEM_LOG("Set_HS_Speed(): What the f?#!�@&$!");
    return FALSE;
      
  } 

  return FALSE;

}

/*
 * Set pre-amplifier gain.
 */
int Set_Pre_Amp_Gain(int index) {

  unsigned int error;

  error = SetPreAmpGain(index);

  switch (error) {
     
  case DRV_SUCCESS:
    
    return TRUE;
    break;
    
  case DRV_NOT_INITIALIZED:

    SYSTEM_LOG("Set_Pre_Amp_Gain(): Failed to set pre-amplifier gain. System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:

    SYSTEM_LOG("Set_Pre_Amp_Gain(): Failed to set pre-amplifier gain. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:

    SYSTEM_LOG("Set_Pre_Amp_Gain(): Failed to set pre-amplifier gain. Index invalid.");
    return FALSE;
    break;

  default:

    /* This should never happen */

    SYSTEM_LOG("Set_Pre_Amp_Gain(): What the f?#!�@&$!");
    return FALSE;
      
  } 

  return TRUE; 

}

/*
 * Set vertical shift speed.
 */
int Set_VS_Speed(int index) {

  unsigned int error;

  error = SetVSSpeed(index);

  switch (error) {
     
  case DRV_SUCCESS:
    
    return TRUE;
    break;
    
  case DRV_NOT_INITIALIZED:

    SYSTEM_LOG("Set_VS_Speed(): Failed to set vertical shift speed. System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:

    SYSTEM_LOG("Set_VS_Speed(): Failed to set vertical shift speed. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:

    SYSTEM_LOG("Set_VS_Speed(): Failed to set vertical shift speed. Index out of range.");
    return FALSE;
    break;

  default:

    /* This should never happen */

    SYSTEM_LOG("Set_VS_Speed(): What the f?#!�@&$!");
    return FALSE;
      
  } 

  return TRUE; 

}

/*
 * Set vertical clock voltage amplitude.
 */
int Set_VS_Amplitude(int amplitude) {

  unsigned int error;

  error = SetVSAmplitude(amplitude);

  switch (error) {
     
  case DRV_SUCCESS:
    
    return TRUE;
    break;
    
  case DRV_NOT_INITIALIZED:

    SYSTEM_LOG("Set_VS_Amplitude(): Failed to set vertical clock voltage amplitude. System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:

    SYSTEM_LOG("Set_VS_Amplitude(): Failed to set vertical clock voltage amplitude. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:

    SYSTEM_LOG("Set_VS_Amplitude(): Failed to set vertical clock voltage amplitude. Amplitude invalid.");
    return FALSE;
    break;

  default:

    /* This should never happen */

    SYSTEM_LOG("Set_VS_Amplitude(): What the f?#!�@&$!");
    return FALSE;
      
  } 

  return TRUE; 

}

/*
 * Set EM gain mode.
 */
int Set_EM_Gain_Mode(int mode) {

  unsigned int error;
  
  error = SetEMGainMode(mode);

  switch (error) {
      
  case DRV_SUCCESS:
    return TRUE;
    break;
    
  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Set_EM_Gain_Mode(): Failed to set EM gain mode. System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    SYSTEM_LOG("Set_EM_Gain_Mode(): Failed to set EM gain mode. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Set_EM_Gain_Mode(): Failed to set EM gain mode. EM gain mode invalid.");
    return FALSE;
    break;

  default:
    /* This should never happen */
    SYSTEM_LOG("Set_EM_Gain_Mode(): What the f?#!�@&$!");
    return FALSE;
      
  } 

  return FALSE;
    
}

/*
 * Set the EMCCD gain. 
 */
int Set_EMCCD_Gain(int gain) {

  unsigned int error;

  error = SetEMCCDGain(gain);

  switch (error) {
    
  case DRV_SUCCESS:
    return TRUE;
    break;
    
  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Set_EMCCD_Gain(): Failed to set EMCCD gain. System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    SYSTEM_LOG("Set_EMCCD_Gain(): Failed to set EMCCD gain. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_I2CTIMEOUT:
    SYSTEM_LOG("Set_EMCCD_Gain(): Failed to set EMCCD gain. I2C command timed out.");
    return FALSE;
    break;

  case DRV_I2CDEVNOTFOUND:
    SYSTEM_LOG("Set_EMCCD_Gain(): Failed to set EMCCD gain. I2C device not present.");
    return FALSE;
    break;

  case DRV_ERROR_ACK:
    SYSTEM_LOG("Set_EMCCD_Gain(): Failed to set EMCCD gain. Unable to communicate with card.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Set_EMCCD_Gain(): Failed to set EMCCD gain. Gain invalid.");
    return FALSE;
    break;

  default:
    /* This should never happen */
    SYSTEM_LOG("Set_EMCCD_Gain(): What the f?#!�@&$!");
    return FALSE;
      
  } 

  return FALSE;

}


/*
 * Set the A/D channel.
 */
int Set_AD_Channel(int channel) {

  unsigned int error;

  error = SetADChannel(channel);

  switch (error) {

  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Set_AD_Channel(): Failed to set A/D channel. Index out of range.");
    return FALSE;
    break;

  default:
    /* This should never happen */
    SYSTEM_LOG("Set_AD_Channel(): What the f#��?@!");
    return FALSE;
  }

  return FALSE;

}

/*
 * Set the output amplifier type.
 */
int Set_Output_Amplifier(int type) {

  unsigned int error;

  error = SetOutputAmplifier(type);

  switch (error) {
    
  case DRV_SUCCESS:
    
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:

    SYSTEM_LOG("Set_Output_Amplifier(): Failed to set output amplifier type. System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:

    SYSTEM_LOG("Set_Output_Amplifier(): Faield to set output amplifier type. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:

    SYSTEM_LOG("Set_Output_Amplifier(): Failed to set output amplifier type. Output amplifier type invalid.");
    return FALSE;
    break;

  default:

    /* This should never happen */

    SYSTEM_LOG("Set_Output_Amplifier(): What the f?#!�@&$!");
    return FALSE;
      
  }

  return TRUE;

}

/*
 * Set high capacity state.
 */
int Set_High_Capacity(int state) {

  unsigned int error;

  if (state != HIGH_CAPACITY_ON && state != HIGH_CAPACITY_OFF) {
    SYSTEM_LOG("Set_High_Capacity(): Invalid state.");
    return FALSE;
  }

  error = SetHighCapacity(state);

  switch (error) {

  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Set_High_Capacity(): Failed to set the high capacity. System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    SYSTEM_LOG("Set_High_Capacity(): Failed to set the high capacity. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Set_High_Capacity(): Faield to set the high capacity. Invalid state parameter.");
    return FALSE;
    break;

  default:
    /* This should never happen */
    SYSTEM_LOG("Set_High_Capacity(): What the f#@!$��");
    return FALSE;

  }

  return FALSE;

}

/* 
 * Set frame transfer state.
 */
int Set_Frame_Transfer(int state) {

  unsigned error;

  if (state != FRAME_TRANSFER_ON && state != FRAME_TRANSFER_OFF) {
    SYSTEM_LOG("Set_Frame_Transfer(): Invalid state.");
    return FALSE;
  }

  error = SetFrameTransferMode(state);

  switch (error) {
   
  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Set_Frame_Transfer(): Failed to set the frame transfer. System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    SYSTEM_LOG("Set_Frame_Transfer(): Failed to set the frame transfer. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Set_Frame_Transfer(): Faield to set the frame transfer. Invalid state parameter.");
    return FALSE;
    break;

  default:
    /* This should never happen */
    SYSTEM_LOG("Set_Frame_Transfer(): What the f#@!$��");
    return FALSE;

  }

  return FALSE;

}

/* 
 * Set Shutter Mode.
 */
int Set_Shutter_Mode(int ttl_mode, int mode, int close_time, int open_time){
  unsigned error;

  error = SetShutter(ttl_mode,mode,close_time,open_time);

  switch (error) {
   
    case DRV_SUCCESS:
      return TRUE;
      break;

    case DRV_NOT_INITIALIZED:
      SYSTEM_LOG("Set_Shutter_Mode(): Failed to set the shutter mode. System not initialized.");
      return FALSE;
      break;

    case DRV_ACQUIRING:
      SYSTEM_LOG("Set_Shutter_Mode(): Failed to set the shutter mode. Acquisition in progress.");
      return FALSE;
      break;

    case DRV_ERROR_ACK:
      SYSTEM_LOG("Set_Shutter_Mode(): Failed to set the shutter mode. Unable to communicate with card.");
      return FALSE;
      break;

    case DRV_NOT_SUPPORTED:
      SYSTEM_LOG("Set_Shutter_Mode(): Failed to set the shutter mode. Camera does not support shutter control.");
      return FALSE;
      break;

    case DRV_P1INVALID:
      SYSTEM_LOG("Set_Shutter_Mode(): Faield to set the frame transfer. Invalid TTL type.");
      return FALSE;
      break;

    case DRV_P2INVALID:
      SYSTEM_LOG("Set_Shutter_Mode(): Faield to set the frame transfer. Invalid mode parameter.");
      return FALSE;
      break;

    case DRV_P3INVALID:
      SYSTEM_LOG("Set_Shutter_Mode(): Faield to set the frame transfer. Invalid time to open paramter.");
      return FALSE;
      break;

    case DRV_P4INVALID:
      SYSTEM_LOG("Set_Shutter_Mode(): Faield to set the frame transfer. Invalid time to close parameter.");
      return FALSE;
      break;

    default:
      /* This should never happen */
      SYSTEM_LOG("Set_Shutter_Mode(): What the f#@!$��");
      return FALSE;
  }

  return FALSE;
}

/*
 * Set the baseline clamp to either enabled or disabled 1 = E 0 = D
 */

int Set_Baseline_Clamp(int state) {
  unsigned error;

  error = SetBaselineClamp(state);

  switch (error) {

    case DRV_SUCCESS:
      return TRUE;
      break;

    case DRV_NOT_INITIALIZED:
      SYSTEM_LOG("Set_Baseline_Camp(): Failed to set baseline clamp. System not initialized.");
      return FALSE;
      break;

    case DRV_ACQUIRING:
      SYSTEM_LOG("Set_Baseline_Camp(): Failed to set baseline clamp. Acquisition in progress.");
      return FALSE;
      break;
   
    case DRV_NOT_SUPPORTED:
      SYSTEM_LOG("Set_Baseline_Camp(): Failed to set baseline clamp. Parameter not supported.");
      return FALSE;
      break;

    case DRV_P1INVALID:
      SYSTEM_LOG("Set_Baseline_Camp(): Failed to set baseline clamp. Invalid state parameter");
      return FALSE;
      break;

    default:
      SYSTEM_LOG("Set_Baseline_Camp(): Failed to set baseline clamp. Invalid state parameter");
      return FALSE;
  }

  return FALSE;
}

/*
 * Get the current status of the drivers.
 */
int Get_Status(int *status) {

  unsigned int error;
  int value;

  error = GetStatus(status);

  switch (error) {

  case DRV_SUCCESS:

    value = Driver_Status(status);

    /*switch (value) {
      
    case SUCCESS:
    SYSTEM_LOG("SUCCESS");
    break;
    
    case FALSE:
    SYSTEM_LOG("FALSE");
    
    default:
    
    SYSTEM_LOG("??????");
    }*/
    
    return value;
    break;

  case DRV_NOT_INITIALIZED:

    SYSTEM_LOG("Get_Status(): Failed to get status. System not initalized.");
    return FALSE;
    break;

  default:

    /* This should never happen */
    
    SYSTEM_LOG("Get_Status(): What the f?#@!&");
    return FALSE;

  }

  return TRUE;

}

/*
 * Get the data from the last acquisition.
 */
int Get_Acquired_Data(int32_t *array, long size) {

  unsigned int error;

  error = GetAcquiredData(array,size);

  switch (error) {

  case DRV_SUCCESS: 

    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:

    SYSTEM_LOG("Get_Acquired_Data(): Failed to get acquired data. System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:

    SYSTEM_LOG("Get_Acquired_Data(): Failed to get acquired data. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_ERROR_ACK:

    SYSTEM_LOG("Get_Acquired_Data(): Failed to get acquired data. Unable to communicate with card.");
    return FALSE;
    break;

  case DRV_P1INVALID:

    SYSTEM_LOG("Get_Acquired_Data(): Failed to get acquired data. Invalid array pointer.");
    return FALSE;
    break;

  case DRV_P2INVALID:

    SYSTEM_LOG("Get_Acquired_Data(): Failed to get acquired data. Array size is too small.");
    return FALSE;
    break;

  case DRV_NO_NEW_DATA:
    SYSTEM_LOG("Get_Acquired_Data(): Failed to get acquired data. No acquisition has taken place.");
    break;

  default:

    /* This should never happen */

    SYSTEM_LOG("Get_Acquired_data(): What the F!@#?");
    break;

  }

  return TRUE;


}

/*
 * Get the actual exposure time.
 */
int Get_Exposure_Time(float *exposure) {

  unsigned int error;
  float accumulate;
  float kinetic;

  error = GetAcquisitionTimings(exposure,&accumulate,&kinetic);

  switch (error) {

  case DRV_SUCCESS:

    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:

    SYSTEM_LOG("Get_Exposure_Time(): Failed to get exposure time. System not initalized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:

    SYSTEM_LOG("Get_Exposure_Time(): Failed to get exposure time. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_INVALID_MODE:

    SYSTEM_LOG("Get_Exposure_Time(): Failed to get exposure time. Acquisition or readout mode is not available.");
    return FALSE;
    break;

  default:

    SYSTEM_LOG("Get_Exposure_Time(): What the f?#@!&");
    return FALSE;

  }
	return TRUE;

}

/*
 * Get the maximum exposure time in seconds.
 */
int Get_Max_Exposure(float *max_exposure) {

  unsigned int error;

  error = GetMaximumExposure(max_exposure);

  switch (error) {
    
  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Get_Max_Exposure(): Unable to get maximum exposure. Invalid MaxExposure value (i.e. NULL)");
    return FALSE;
    break;

  default:
    SYSTEM_LOG("Get_Max_Exposure(): What the f?#@!&");
    return FALSE;

  }

  return FALSE;

}

/*
 * Get the detector temperature.
 */
int Get_Temperature(int *temperature) {

  unsigned int error;

  error = GetTemperature(temperature);

  switch (error) {
    
  /* case DRV_SUCCESS: */
  /*   return SUCCESS; */
  /*   break; */

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Get_Temperature(): Failed to get detector temperature. System not initalized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    SYSTEM_LOG("Get_Temperature(): Failed to get detector temperature. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_ERROR_ACK:
    SYSTEM_LOG("Get_Temperature(): Failed to get detector temperature. Unable to communicate with card.");
    return FALSE;
    break;

  case DRV_TEMP_OFF:
    SYSTEM_LOG("Get_Temperature(): Cooler OFF.");
    return TRUE;
    break;

  case DRV_TEMP_STABILIZED:
    SYSTEM_LOG("Get_Temperature(): Stabilized. Temperature has reached set point.");
    return TRUE;
    break;

  case DRV_TEMP_NOT_REACHED:
    SYSTEM_LOG("Get_Temperature(): Cooling. Temperature has not reached set point.");
    return TRUE;
    break;

  case DRV_TEMP_DRIFT:
    SYSTEM_LOG("Get_Temperature(): Temperature had stabilized but has since drifted.");
    return TRUE;
    break;
    
  case DRV_TEMP_NOT_STABILIZED:
    SYSTEM_LOG("Get_Temperature(): Temperature reached but not stabilized.");
    return TRUE;
    break;
      
  default:

    /* This should never happen */

    SYSTEM_LOG("Get_Temperature(): What the f?#@!&");
    return FALSE;

  }

  return TRUE; 

}

/*
 * Get the allowed temperature range.
 */
int Get_Temperature_Range(int *min, int *max) {

  unsigned int error;

  error = GetTemperatureRange(min,max);

  switch (error) {

  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Get_Temperature_Ranghe(): Failed to get temperature range. System not initalized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    SYSTEM_LOG("Get_Temperature_Range(): Failed to get temperature range. Acquisition in progress.");
    return FALSE;
    break;

  default:

    /* This should never happen */

    SYSTEM_LOG("Get_Temperature_Range(): What the f?#@!&");
    return FALSE;

  }

  return FALSE; 

}

/*
 * Get the current detector temperature.
 */
int Get_Detector_Temperature(int *temperature, int *ok) {

  unsigned int error;

  error = GetTemperature(temperature);

  switch (error) {

  case DRV_NOT_INITIALIZED:
    
    SYSTEM_LOG("Get_Detector_Temperature(): Failed to get detector temperature. System is not initialized.");
    return FALSE;
    break;
    
  case DRV_ACQUIRING:

    SYSTEM_LOG("Get_Detector_Temperature(): Failed to get detector temperature. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_ERROR_ACK:
    
    SYSTEM_LOG("Get_Detector_Temperature(): Failed to get detector temperature. Unable to communicate with card.");
    return FALSE;
    break;

  case DRV_TEMP_OFF:

    SYSTEM_LOG("Get_Detector_Temperature(): Cooling is OFF.");
    *ok = FALSE;
    return TRUE;
    break;
    
  case DRV_TEMP_NOT_REACHED:
    
   	SYSTEM_LOG("Get_Detector_Temperature(): Temperature has not reached set point.");
    *ok = FALSE;
    return TRUE;
    break;

  case DRV_TEMP_DRIFT:
    SYSTEM_LOG("Get_Detector_Temperature(): Temperature had stabilized but has since drifted.");
    return TRUE;

  default:
    
    *ok = TRUE;
    return TRUE;
  
  }
  
  return TRUE;

}

/*
 * Get the cooling status. 
 */
int Get_Cooling_Status(unsigned int *cooling_status, int *temperature) {
  
  unsigned int status;

  status = GetTemperature(temperature);

  switch (status) {
    
  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Get_Cooling_Status(): Failed to get cooling status. System not initialized.");
    return FALSE;

  case DRV_ACQUIRING:
    SYSTEM_LOG("Get_Cooling_Status(): Failed to get cooling status. Acquisiton in progress.");
    return FALSE;

  case DRV_ERROR_ACK:
    SYSTEM_LOG("Get_Cooling_Status(): Failed to get cooling status. Unable to communicate with card.");
    return FALSE;

  default:
    *cooling_status = status;
    return TRUE;
    
  }

  *cooling_status = status;

  return TRUE;

}

/*
 * Get the actual pre-amplifier gain factor.
 */
int Get_Pre_Amp_Gain(int pre_amp_gain, float *pre_amp_gain_factor) {

  unsigned int error;
  float gain;

  error = GetPreAmpGain(pre_amp_gain,&gain);
  
  switch (error) {

  case DRV_SUCCESS: 

    *pre_amp_gain_factor = gain;
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:

    SYSTEM_LOG("Get_Pre_Amp_Gain(): ERROR! System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:

    SYSTEM_LOG("Get_Pre_Amp_Gain(): ERROR! Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:

    SYSTEM_LOG("Get_Pre_Amp_Gain(): ERROR! Invalid pre-amplifier gain index.");
    return FALSE;
    break;

  default:

    SYSTEM_LOG("Get_Pre_Amp_Gain(): What the F!@#?");
    break;

  }

  return TRUE;

}

/*
 * Get the number of available A/D channels.
 */
int Get_Number_AD_Channels(int *number) {

  unsigned int error;

  error = GetNumberADChannels(number);

  switch (error) {

  case DRV_SUCCESS: 
    return TRUE;
    break;

  default:
    SYSTEM_LOG("Get_Number_AD_Channels(): What the F!@#?");
    return FALSE;

  }

  return FALSE;
   
}


/*
 * Get the number of available output amplifiers.
 */
int Get_Number_Amp(int *number) {

  unsigned int error;

  error = GetNumberAmp(number);

  switch (error) {

  case DRV_SUCCESS: 
    return TRUE;
    break;

  default:
    SYSTEM_LOG("Get_Number_Amps(): What the F!@#?");
    return FALSE;

  }

  return FALSE;
    
}

/*
 * Get the maximum available horizontal shift speed for the selected output amplifier.
 */
int Get_Amp_Max_HSpeed(int index, float *speed) {

  unsigned int error;

  error = GetAmpMaxSpeed(index,speed);

  switch (error) {
    
  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Get_Amp_Max_HSpeed(): Unable to get the maximum speed. System not initialized.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Get_Amp_Max_HSpeed(): Unable to get the maximum speed. The amplifier index is invalid.");
    return FALSE;
    break;

  default:
    SYSTEM_LOG("Get_Amp_Max_HSpeed(): What the F!@#?");
    break;
  } 

  return FALSE;

}

/*
 * Get the number of available horizontal shift speeds.
 */
int Get_Number_HS_Speeds(int channel, int type, int *number) {

  unsigned int error;

  error = GetNumberHSSpeeds(channel,type,number);

  switch (error) {

  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Get_Number_HS_Speeds(): Failed to get the number of available horixontal shift speeds. System not initialized.");
    return FALSE;
    break;
  
  case DRV_ACQUIRING:
    SYSTEM_LOG("Get_Number_HS_Speeds(): Failed to get the number of available horixontal shift speeds. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Get_Number_HS_Speeds(): Failed to get the number of available horixontal shift speeds. Invalid channel.");
    return FALSE;
    break;

  case DRV_P2INVALID:
    SYSTEM_LOG("Get_Number_HS_Speeds(): Failed to get the number of available horixontal shift speeds. Invalid horizontal read mode.");
    return FALSE;
    break;
    
  default:
    SYSTEM_LOG("Get_Number_HS_Speeds(): What the F#@!?");
    return FALSE;
    break;
  }

  return FALSE; 

}

/*
 * Get the number of available vertical shift speeds.
 */
int Get_Number_VS_Speeds(int *number) {

  unsigned int error;

  error = GetNumberVSSpeeds(number);

  switch (error) {

  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Get_Number_VS_Speeds(): Failed to get the number of available verical shift speeds. System not initialized.");
    return FALSE;
    break;
  
  case DRV_ACQUIRING:
    SYSTEM_LOG("Get_Number_VS_Speeds(): Failed to get the number of available vertical shift speeds. Acquisition in progress.");
    return FALSE;
    break;
    
  default:
    SYSTEM_LOG("Get_Number_VS_Speeds(): What the F#@!?");
    return FALSE;
    break;
  }

  return FALSE; 

}

/*
 * Get the number of available vertical clock voltage amplitudes.
 */
int Get_Number_VS_Amplitudes(int *number) {

  unsigned int error;

  error = GetNumberVSAmplitudes(number);

  switch (error) {
      
  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Get_Number_VS_Amplitudes(): Failed to get the number of available verical clock voltage amplitudes. System not initialized.");
    return FALSE;
    break;
  
  case DRV_ACQUIRING:
    SYSTEM_LOG("Get_Number_VS_Amplitudes(): Failed to get the number of available vertical clock voltage amplitudes. Acquisition in progress.");
    return FALSE;
    break;
    
  case DRV_NOT_AVAILABLE:
    SYSTEM_LOG("Get_Number_VS_Amplitudes(): Failed to get the number of available vertical clock voltage amplitudes. System does not support this feature.");
    return FALSE;
    break;

  default:
    SYSTEM_LOG("Get_Number_VS_Amplitudes(): What the F#@!?");
    return FALSE;
    break;
  }

  return FALSE; 

}

/*
 * Get the number of available pre-amplifier gains.
 */
int Get_Number_Pre_Amp_Gains(int *number) {

  unsigned int error;

  error = GetNumberPreAmpGains(number);

  switch (error) {

  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Get_Number_Pre_Amp_Gains(): Failed to get the number of available pre-amplifier gains. System not initialized.");
    return FALSE;
    break;
  
  case DRV_ACQUIRING:
    SYSTEM_LOG("Get_Number_Pre_Amp_Gains(): Failed to get the number of available pre-amplifier gains. Acquisition in progress.");
    return FALSE;
    break;

  default:
    SYSTEM_LOG("Get_Number_Pre_Amp_Gains(): What the F#@!?");
    return FALSE;
    break;
  }

  return FALSE;


}

/*
 * Get the fastest recommended vertical shift speed.
 */
int Get_Fastest_Recommended_VS_Speed(int *index, float *speed) {
  
  unsigned int error;
  
  error = GetFastestRecommendedVSSpeed(index,speed);
  
  switch (error) {
    
  case DRV_SUCCESS:
    
    return TRUE;
    break;
    
  case DRV_NOT_INITIALIZED:
     
    SYSTEM_LOG("Get_Fastest_Recommended_VS_Speed(): Failed to get the fastest recommended vertical shift speed. System not initialized.");
    return FALSE;
    break;
    
  case DRV_ACQUIRING:
    
    SYSTEM_LOG("Get_Fastest_Recommended_VS_Speed(): Failed to get the fastest recommended vertical shift speed. Acquisition in progress.");
    return FALSE;
    break;
    
  default:

    SYSTEM_LOG("Get_Fastest_Recommended_VS_Speed(): What the F#@!?");
    return FALSE;
    break;
  }

  return TRUE; 

}

/*
 * Get the A/D channel bit depth.
 */
int Get_Bit_Depth(int ad_channel, int *bit_depth) {

  unsigned int error;
  int depth;

  error = GetBitDepth(ad_channel,&depth);

  switch (error) {

  case DRV_SUCCESS:

    *bit_depth = depth;
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:

    SYSTEM_LOG("Get_Bit_Depth(): Failed to get bit depth. System not initialized.");
    return FALSE;
    break;
  
  case DRV_ACQUIRING:

    SYSTEM_LOG("Get_Bit_Depth(): Failed to get bit depth. Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:

    SYSTEM_LOG("Get_Bit_Depth(): Failed to get bit depth. Invalid A/D channel.");
    return FALSE;
    break;

  default:

    SYSTEM_LOG("Get_Bit_Depth(): What the F#@!?");
    return FALSE;
    break;
  }

  return TRUE;

}

/*
 * Get horizontal shift speed.
 */
int Get_HS_Speed(int ad_channel, int output_amp, int hs_speed_index, float *hs_speed_MHz) {

  unsigned int error;

  error = GetHSSpeed(ad_channel,output_amp,hs_speed_index,hs_speed_MHz);

  switch (error) {

  case DRV_SUCCESS:

    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:

    SYSTEM_LOG("Get_HS_Speed(): ERROR! System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    
    SYSTEM_LOG("Get_HS_Speed(): ERROR! Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:

    SYSTEM_LOG("Get_HS_Speed(): ERROR! Invalid A/D channel.");
    return FALSE;
    break;
    
  case DRV_P2INVALID:

    SYSTEM_LOG("GET_HS_Speed(): ERROR! Invalid horizontal read mode (output amplifier).");
    return FALSE;
    break;

  case DRV_P3INVALID:

    SYSTEM_LOG("Get_HS_Speed(): ERROR! Invalid horizontal speed index.");
    return FALSE;
    break;

  default:

    SYSTEM_LOG("Get_HS_Speed(): What the f?#@!");
    return FALSE;

  }

}

/*
 * Get the actual vertical shift speed.
 */
int Get_VS_Speed(int vs_speed, float *vs_speed_usp) {

  unsigned int error;
  float speed;

  error = GetVSSpeed(vs_speed,&speed);

  switch (error) {

  case DRV_SUCCESS:
    *vs_speed_usp = speed;
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Get_VS_Speed(): ERROR! System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    SYSTEM_LOG("Get_VS_Speed(): ERROR! Acquisition in progress.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Get_VS_Speed(): ERROR! Invalid vertical shift speed index.");
    return FALSE;
    break;

  default:

    SYSTEM_LOG("Get_VS_Speed(): What the h#@!?");
    return FALSE;

  }
  
  return FALSE;

}

int Get_VS_Amplitude(int vs_amplitude, int *vs_amp_value){
	unsigned int error;
  	int value;

  	error = GetVSAmplitudeValue(vs_amplitude,&value);

  	switch (error) {

  	case DRV_SUCCESS:
    	*vs_amp_value = value;
    	return TRUE;
    	break;

  	case DRV_NOT_INITIALIZED:
    	SYSTEM_LOG("Get_VS_Amplitude(): ERROR! System not initialized.");
    	return FALSE;
    	break;

  	case DRV_P2INVALID:
    	SYSTEM_LOG("Get_VS_Amplitude(): ERROR! Invalid value pointer.");
    	return FALSE;
    	break;

  	case DRV_P1INVALID:
    	SYSTEM_LOG("Get_VS_Amplitude(): ERROR! Invalid index.");
    	return FALSE;
    	break;

  	default:

    	SYSTEM_LOG("Get_VS_Amplitude(): What the h#@!?");
    	return FALSE;

  	}
  
  	return FALSE;
}

/*
 * Get the current EM gain setting.
 */ 
int Get_EMCCD_Gain(int *gain) {

  unsigned int error;

  error = GetEMCCDGain(gain);

  switch (error) {

  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Get_EMCCD_Gain(): Unable to get the EM gain. System not initialized.");
    return FALSE;
    break;

  case DRV_ERROR_ACK:
    SYSTEM_LOG("Get_EMCCD_Gain(): Unable to get the EM gain. Unable to communicate with the card.");
    return FALSE;
    break;

  default:
    SYSTEM_LOG("Get_EMCCD_Gain(): What the f#@!?");
    return FALSE;
  }

  return FALSE;

}

/*
 * Get the valid EMCCD gain range.
 */
int Get_EM_Gain_Range(int *low, int *high) {

  unsigned int error;
  int min, max;

  error = GetEMGainRange(&min,&max);

  switch (error) {

  case DRV_SUCCESS:

    *low  = min;
    *high = max;
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:

    SYSTEM_LOG("Get_EM_Gain_Range(): Failed to get EM gain range. System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    
    SYSTEM_LOG("Get_EM_Gain_Range(): Failed to get EM gain range. Acquisition in progress.");
    return FALSE;
    break;
    
  default: 
    
    /* This should never happen */

    SYSTEM_LOG("Get_EM_Gain_Range(): What the h#@!?");
    return FALSE;

  }
  
  return TRUE;

}

/*
 * Get the size of the detector in pixels.
 */
int Get_Detector(int *xpixels, int *ypixels) {

  unsigned int error;
  int x, y;

  error = GetDetector(&x,&y);
  
  switch (error) {

  case DRV_SUCCESS:

    *xpixels = x;
    *ypixels = y;
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:

    SYSTEM_LOG("Get_Detector(): Failed to get detector. System not initialized.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    
    SYSTEM_LOG("Get_Detector(): Failed to get detector. Acquisition in progress.");
    return FALSE;
    break;

  default: // This should never happen

    SYSTEM_LOG("Get_Detector(): What the h#@!?");
    return FALSE;

  }
  
  return TRUE;

}

/*
 * Get the keep clean cycle time.
 */
int Get_Keep_Clean_Time(float *clean_time) {

  unsigned int error;
  
  error = GetKeepCleanTime(clean_time);
  
  switch (error) {
    
  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Get_Keep_Clean_Time(): Unable to get keep clean cycle time. System not initialized.");
    return FALSE;
    break;

  case DRV_ERROR_ACK:
    SYSTEM_LOG("Get_Keep_Clean_Time(): Unable to get keep clean cycle time. Error communicating with camera.");
    return FALSE;
    break;

  default:
    SYSTEM_LOG("Get_Keep_Clean_Time(): What the f#@!?");
    return FALSE;
  }

  return FALSE;

}

/*
 * Get the maximum allowed horizontal binning for the given readout mode.
 */
int Get_Max_H_Binning(int read_mode, int *max_bin) {

  unsigned int error;
  
  error = GetMaximumBinning(read_mode,GET_H_BIN,max_bin);
  
  switch (error) {
    
  case DRV_SUCCESS:
    return TRUE;
    break;
    
  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Get_Max_H_Binning(): Unable to get the maximum binning. System not initialized.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Get_Max_H_Binning(): Unable to get the maximum binning. Invalid read mode.");
    return FALSE;
    break;

  case DRV_P2INVALID:
    SYSTEM_LOG("Get_Max_H_Binning(): Unable to get the maximum binning. GET_H_BIN has an invalid value.");
    return FALSE;
    break;

  case DRV_P3INVALID:
    SYSTEM_LOG("Get_Max_H_Binning(): Unable to get the maximum binning. Invalid maximum binning address (i.e. NULL).");
    return FALSE;
    break;


  default:
    SYSTEM_LOG("Get_Max_H_Binning():  What the f#@!?");
    return FALSE;
    
  }

  return FALSE;

}

/*
 * Get the maximum allowed vertical binning for the given readout mode.
 */
int Get_Max_V_Binning(int read_mode, int *max_bin) {

  unsigned int error;
  
  error = GetMaximumBinning(read_mode,GET_V_BIN,max_bin);
  
  switch (error) {
    
  case DRV_SUCCESS:
    return TRUE;
    break;
    
  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Get_Max_V_Binning(): Unable to get the maximum binning. System not initialized.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Get_Max_V_Binning(): Unable to get the maximum binning. Invalid read mode.");
    return FALSE;
    break;

  case DRV_P2INVALID:
    SYSTEM_LOG("Get_Max_V_Binning(): Unable to get the maximum binning. GET_V_BIN has an invalid value.");
    return FALSE;
    break;

  case DRV_P3INVALID:
    SYSTEM_LOG("Get_Max_V_Binning(): Unable to get the maximum binning. Invalid maximum binning address (i.e. NULL).");
    return FALSE;
    break;


  default:
    SYSTEM_LOG("Get_Max_V_Binning():  What the f#@!?");
    return FALSE;
    
  }

  return FALSE;

}

/*
 * Get the minimum image length.
 */
int Get_Min_Image_Length(int *min_length) {

  unsigned int error;

  error = GetMinimumImageLength(min_length);

  switch (error) {

  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Get_Min_Image_Length(): Unable to get the minimum image length. System not initailized.");
    return FALSE;
    break;

  case DRV_P1INVALID:
    SYSTEM_LOG("Get_Min_Image_Length(): Unable to get the minimum image length. Invalid min_length value (i.e. NULL).");
    return FALSE;
    break;

  default:
    SYSTEM_LOG("Get_Min_Image_Length():  What the f#@!?"); 
    return FALSE;
  }

  return FALSE;

}

/*
 * Get the time to readout data from the sensor.
 */
int Get_Readout_Time(float *readout_time) {

  unsigned int error;

  error = GetReadOutTime(readout_time);

  switch (error) {

  case DRV_SUCCESS:
    return TRUE;
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Get_Readout_Time(): Unable to get readout time. System not initailised.");
    return FALSE;
    break;

  case DRV_ERROR_CODES:
    SYSTEM_LOG("Get_Readout_Time(): Unable to get readout time. Error communicating with the card.");
    return FALSE;
    break;

  default:
    SYSTEM_LOG("Get_Readout_Time():  What the f#@!?");
    return FALSE;
  }

  return FALSE;

}

/*
 * Get the state of the cooler on/off
 */
int Get_Cooler_State(int *cooler_status){
	unsigned int error;

	error = IsCoolerOn(cooler_status);

	switch (error) {

  	case DRV_SUCCESS:
    	return TRUE;
    	break;

  	case DRV_NOT_INITIALIZED:
    	SYSTEM_LOG("GGet_Cooler_State(): Unable to get cooler status. System not initailised.");
    	return FALSE;
    	break;

  	case DRV_P1INVALID:
    	SYSTEM_LOG("Get_Cooler_State(): Unable to get cooler status. Parameter is NULL.");
    	return FALSE;
    	break;

  	default:
    	SYSTEM_LOG("Get_Cooler_State():  What the f#@!?");
    	return FALSE;
  	}

  	return FALSE;
}


/*
 * Evaluate the driver status.
 */
int Driver_Status(int *status) {

  unsigned int error;

  error = GetStatus(status);
  
  switch (error) {
  case DRV_SUCCESS:
    break;

  case DRV_NOT_INITIALIZED:
    SYSTEM_LOG("Driver_Status(): Unable to get driver status. System not initialized.");
    return FALSE;
    break;

  default:
    SYSTEM_LOG("Driver_Status(): What the f#@!? ");
  }

  switch (*status) {
  case DRV_IDLE:
    return TRUE;
    break;
    
  case DRV_TEMPCYCLE:    
    SYSTEM_LOG("Driver_Status(): Executing temperature cycle.");
    return FALSE;
    break;

  case DRV_ACQUIRING:
    return TRUE;
    break;

  case DRV_ACCUM_TIME_NOT_MET:    
    SYSTEM_LOG("Driver_Status(): Unable to meet accumulative cycle time.");
    return FALSE;
    break;

  case DRV_KINETIC_TIME_NOT_MET:
    SYSTEM_LOG("Driver_Status(): Unable to meet kinetic cycle time.");
    return FALSE;
    break;

  case DRV_ERROR_ACK:
    SYSTEM_LOG("Driver_Status(): Unable to communicate with the card.");
    return FALSE;
    break;

  case DRV_ACQ_BUFFER:
    SYSTEM_LOG("Driver_Status(): Computer unable to read the data via the ISA slot at the required rate.");
    return FALSE;
    break;

  case DRV_SPOOLERROR:
    SYSTEM_LOG("Driver_Status(): Overflow of the spool buffer.");
    return FALSE;
    break;

  default:
    /* This should never happen */
    SYSTEM_LOG("Driver_Status(): What the hell!!!!");
    return FALSE;
    
  }

  return FALSE;
}
