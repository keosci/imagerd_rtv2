 /*
 * file_io.c
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Jarrett Little
 *
 */

#include "file_io.h"



/**
 * Read in PI photomax emccd camera configuration settings from the given file.
 * Modifing routine to look for define statements to determine the compile time arguments.
 */
int Read_Camera_Settings(const char *conf_file, struct Camera *camera) {

  FILE *file;
  char line[MAX_LINE_LEN+1] = {0};
  char tmp[MAX_LINE_LEN+1] = {0};
  char buffer[MAX_LINE_LEN] = {0};
  char value[MAX_LINE_LEN] = {0};
  int i;
  int update_flag;

  //Open the configuration file for reading

  file = fopen(conf_file,"r");

  if (file == NULL) {
    perror(conf_file);
    sprintf(buffer,"Read_Camera_Settings(): Failed to open file '%s' for reading.\n",conf_file);
    SYSTEM_LOG(buffer);
    return FALSE;
  }

  //Read in and parse all the lines from the file filling in the camera_settings data structure.

  while (fgets(line,MAX_LINE_LEN,file) != NULL) {

    if(line[0] == '\n') continue;
    if(line[0] == '#') continue;

    if(strstr(line,"TEMPERATURE=") != NULL) {
      strcpy(tmp,line+12);
      sscanf(tmp,"%lf",&camera->Temp_SetPoint);
      //printf("Temperature: %f\n",camera->Temp_SetPoint);
    }else if(strstr(line,"ADCSPEED=") != NULL) {
      strcpy(tmp,line+9);
      sscanf(tmp,"%lf",&camera->ADC_Speed);
      //printf("ADC Readout Speed: %.2f\n",camera->ADC_Speed);

    }else if(strstr(line,"READOUTCONTROLMODE=") != NULL) {
      strcpy(tmp,line+19);
      sscanf(tmp,"%d",&camera->ReadOutCtrlMode);
      //printf("Readout Control Mode: %d\n",camera->ReadOutCtrlMode);

    }else if(strstr(line,"OUTPUTSIGNAL=") != NULL) {
      strcpy(tmp,line+13);
      sscanf(tmp,"%d",&camera->Output_Signal);
      //printf("Output Signal: %d\n",camera->Output_Signal);

    }else if(strstr(line,"SHUTTERTIMINGMODE=") != NULL) {
      strcpy(tmp,line+18);
      sscanf(tmp,"%d",&camera->ShutterTimingMode);
      //printf("Output Signal: %d\n",camera->ShutterTimingMode);
    
    }else if(strstr(line,"INVERTOUTPUTSIG=") != NULL) {
      strcpy(tmp,line+16);
      sscanf(tmp,"%d",&camera->Invert_Output);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);
    }else if(strstr(line, "device ID") != NULL) {
	strcpy(value,line+12);
	value[strlen(value)-1] = NULL;
	value[13] = '\0';
	strcpy(camera->device_id,value);
	printf("Device ID: %s\n",camera->device_id);
    }else if(strstr(line,"IMAGE_TYPE=") != NULL) {
	strcpy(tmp,line+11);
      	sscanf(tmp,"%d",&camera->image_type);
      	//printf("Image type: %d\n",camera->image_type);
#ifdef PI_PROEM
    }else if(strstr(line,"ADCQUALITY=") != NULL) {
      strcpy(tmp,line+11);
      sscanf(tmp,"%d",&camera->ADC_Quality);
      //printf("ADC Quality: %d\n",camera->ADC_Quality);
#endif

#ifdef ANDOR_CAM
    }else if(strstr(line,"ACQUISITIONMODE=") != NULL) {
      strcpy(tmp,line+16);
      sscanf(tmp,"%d",&camera->acquisition_mode);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"IMAGE_TYPE=") != NULL) {
      strcpy(tmp,line+11);
      sscanf(tmp,"%d",&camera->image_type);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"H_SIZE=") != NULL) {
      strcpy(tmp,line+7);
      sscanf(tmp,"%d",&camera->h_size);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"V_SIZE=") != NULL) {
      strcpy(tmp,line+7);
      sscanf(tmp,"%d",&camera->v_size);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"H_START=") != NULL) {
      strcpy(tmp,line+8);
      sscanf(tmp,"%d",&camera->h_start);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"V_START=") != NULL) {
      strcpy(tmp,line+8);
      sscanf(tmp,"%d",&camera->v_start);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"FAN_MODE=") != NULL) {
      strcpy(tmp,line+9);
      sscanf(tmp,"%d",&camera->fan_mode);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"COOLING=") != NULL) {
      strcpy(tmp,line+8);
      sscanf(tmp,"%d",&camera->cooling);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"PRE_AMP_GAIN=") != NULL) {
      strcpy(tmp,line+13);
      sscanf(tmp,"%d",&camera->CCD_Gain);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"AD_CHANNEL=") != NULL) {
      strcpy(tmp,line+11);
      sscanf(tmp,"%d",&camera->ad_channel);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"HS_SPEED=") != NULL) {
      strcpy(tmp,line+9);
      sscanf(tmp,"%d",&camera->hs_speed);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"VS_SPEED=") != NULL) {
      strcpy(tmp,line+9);
      sscanf(tmp,"%d",&camera->vs_speed);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"VS_AMPLITUDE=") != NULL) {
      strcpy(tmp,line+13);
      sscanf(tmp,"%d",&camera->vs_amplitude);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"HIGH_CAPACITY=") != NULL) {
      strcpy(tmp,line+14);
      sscanf(tmp,"%d",&camera->high_capacity);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"FRAME_TRANSFER=") != NULL) {
      strcpy(tmp,line+15);
      sscanf(tmp,"%d",&camera->frame_transfer);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"SHUTTER_CTRL=") != NULL) {
      strcpy(tmp,line+13);
      sscanf(tmp,"%d",&camera->Shutter_Mode);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"SHUTTER_OPEN_DELAY=") != NULL) {
      strcpy(tmp,line+19);
      sscanf(tmp,"%d",&camera->Shutter_Open_Delay);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);

    }else if(strstr(line,"SHUTTER_CLOSE_DELAY=") != NULL) {
      strcpy(tmp,line+20);
      sscanf(tmp,"%d",&camera->Shutter_Close_Delay);
      //printf("Inverted Output Signal: %d\n",camera->Invert_Output);
#endif
    }
  }

  if(fclose(file) == EOF) {
    perror("fclose()");
    SYSTEM_LOG("Read_FW_Slots(): Failed to close file.\n");
    return FALSE;
  }

  return(TRUE);
}



/**
 * Read station location information from the given file.
 */
void Read_Station_Settings(const char *conf_file, struct station_info *station_data) {

	FILE *file;
	//char unique_project_id[MAX_LINE_LEN+1]				 = {0};
	//char unique_site_id[MAX_LINE_LEN+1]					   = {0};
	//char project_name[MAX_LINE_LEN+1] 					   = {0};
	char line[MAX_LINE_LEN+1]           					   = {0};
	char value[MAX_LINE_LEN+1]								       = {0};
	char tmp[MAX_LINE_LEN+1] 								         = {0};
	char buffer[MAX_LINE_LEN]								         = {0};
	//char value[5] = {0};
	int  found = FALSE, i=0;
	float lat, lon, alt;
	int avg_temp, avg_pressure;
	char dir_lat, dir_long;


  /* Open the project file for reading (filename = project.conf */
  file = fopen(conf_file,"r");
	if (file == NULL) {
		sprintf(buffer,"Read_Station_Settings(): Failed to open file '%s' for reading. Using defaults for station information.\n",conf_file);
		SYSTEM_LOG(buffer);
		Use_Station_Info_Defaults(station_data);
		return;
	}

	/* Read in and parse all the lines from the file filling in the station_info data structure. */

	while (fgets(line,MAX_LINE_LEN,file) != NULL) {
		//fprintf(stderr,"Read_Station_Settings(): Line = %s.\n",line);

    	if (line[0] == '\n') continue;
    	if (line[0] == '#') continue;

    	if(strstr(line,"project ID") != NULL) {
          for(i=13; i<17; i++){
            value[i-13] = line[i];
          }
          value[4] = '\0';
      		strcpy(station_data->project_id,value);
      		//fprintf(stderr,"\nRead_Station_Settings(): project id = %s\n",station_data->project_id);
    	} else if(strstr(line,"project name") != NULL) {
      		strcpy(value,line+15);
          value[strlen(value)-1] = NULL;
          strcpy(station_data->project_name,value);
      		//fprintf(stderr,"Read_Station_Settings(): project name = %s\n",station_data->project_name);
    	} else if(strstr(line, "site ID") != NULL) {
			    for(i=10; i<14; i++){
				    value[i-10] = line[i];
			    }
			    value[4] = '\0';
      		strcpy(station_data->site_id,value);
      		//fprintf(stderr,"Read_Station_Settings(): site id = %s\n",station_data->site_id);
    	} else if(strstr(line, "site name") != NULL) {
          //fprintf(stderr,"Read_Station_Settings(): site name = %s\n",line);
    	}else if(strstr(line, "latitude") != NULL) {
        	strcpy(tmp,line+10);
      		sscanf(tmp,"%f%c",&lat,&dir_lat);
		if(dir_lat == 'S'){
			lat = -1*lat;
		}
		station_data->latitude = lat;
		//fprintf(stderr,"The lat is %lf%c\n",lat,dir_lat);
      		//fprintf(stderr,"Read_Station_Settings(): latitude = %f\n",station_data->latitude);
     	} else if(strstr(line, "longitude") != NULL) {
      		strcpy(tmp,line+12);
      		sscanf(tmp,"%f%c",&lon,&dir_long);
			    //Add negative to west longitudes for proper ephem calcs
			    //fprintf(stderr,"The long is %f%c\n",lon,dir_long);
			    if(dir_long=='W'){
				    lon = -1*lon;
			    }
      		station_data->longitude = lon;
      		//fprintf(stderr,"Read_Station_Settings(): longitude = %f\n",station_data->longitude);
    	} else if(strstr(line, "altitude") != NULL) {
      		strncpy(tmp,line+10,5);
      		sscanf(tmp,"%f",&alt);
      		station_data->altitude = alt;
      		//fprintf(stderr,"Read_Station_Settings(): altitude = %f\n",station_data->altitude);
    	} else if(strstr(line, "device ID") != NULL) {
      		strcpy(value,line+12);
          //Remove the carriage return at the end of the string
          value[strlen(value)-1] = NULL;
			    //value[13] = '\0';
			    strcpy(station_data->device_id,value);
          //fprintf(stderr,"Read_Station_Settings(): device_id = %s\n",station_data->device_id);
    	} else if(strstr(line, "site notes") != NULL) {
      //fprintf(stderr,"Read_Station_Settings(): site notes = %s\n",line);
    	} else {
      		//fprintf(stderr,"Read_Station_Settings(): project.conf file not in proper format **using defaults**\n");
      		//Use_Station_Info_Defaults(station_data);
    	}
  	}
  	station_data->avg_temp = (int)DEFAULT_TEMP;
  	station_data->avg_pressure = (int)DEFAULT_PRESSURE;

  	/* Close the configuration file */

  	if(fclose(file) == EOF){
    	SYSTEM_LOG("Read_Station_Settings(): Failed to close file.\n");
  	}
}

/**
 * Fill the station information structure with defaults.
 */
void Use_Station_Info_Defaults(struct station_info *station_data) {

    strncpy(station_data->site_id,DEFAULT_SITE_UNIQUE_ID,MAX_STATION_LEN+1);
  	strncpy(station_data->project_id,DEFAULT_PROJECT_UNIQUE_ID,MAX_ID_LEN+1);
  	station_data->latitude  = (float)DEFAULT_LATITUDE;
  	station_data->longitude = (float)DEFAULT_LONGITUDE;
  	station_data->altitude = (float)DEFAULT_ELEVATION;
  	station_data->avg_temp = (float)DEFAULT_TEMP;
  	station_data->avg_pressure = (float)DEFAULT_PRESSURE;
}

/**
 * Read in number of FW_slots for imager and return this number
 */

int Read_FW_Slots(const char *conf_file){

  	FILE *file;
  	char line[MAX_LINE_LEN+1]	= {0};
  	char tmp[MAX_LINE_LEN+1]	= {0};
    char buffer[MAX_LINE_LEN] = {0};
  	int FW_slots=0;


  	file = fopen(conf_file,"r");

  	if (file == NULL) {
    	sprintf(buffer,"Read_Filter_Settings(): Failed to open file '%s' for reading. Using defaults for filter information.\n",conf_file);
    	SYSTEM_LOG(buffer);
		//Use_Filter_Info_Defaults(station_data);
    	return(FALSE);
  	}

  	//Search file for number of slots to create filter structure
  	while (fgets(line,MAX_LINE_LEN,file) != NULL) {

    	if (line[0] == '\n') continue;
    	if (line[0] == '#') continue;

    	if(strstr(line,"Filterwheel Slots") != NULL){
      		strncpy(tmp,line+19,2);
      		sscanf(tmp,"%d",&FW_slots);
      		//printf("filter wheel slots = %d\n",FW_slots);
      		break;
    	}
  	}

  	if(fclose(file) == EOF) {
    	SYSTEM_LOG("Read_FW_Slots(): Failed to close file.\n");
    	return FALSE;
  	}
  	return(FW_slots);
}

/**
 * Fill in each filters specfic information.
 */
int Read_Filter_Settings(const char *conf_file, struct filter_info *filter_data, int FW_slots) {

  FILE *file;
  char line[MAX_LINE_LEN+1]             = {0};
  char wave_length[MAX_LINE_LEN+1]      = {0};
  char desc[MAX_LINE_LEN+1]             = {0};
  char buffer[MAX_LINE_LEN]             = {0};

  char value[MAX_SEQ_LEN+1]             = {0};
  char tmp[MAX_LINE_LEN+1]              = {0};
  int  i=0, filter_num;
  int c=0;
  /* Open the project file for reading (filename = project.conf */

  file = fopen(conf_file,"r");

  if (file == NULL) {
    perror(conf_file);
    sprintf(buffer,"Read_Filter_Settings(): Failed to open file '%s' for reading. Using defaults for filter information.\n",conf_file);
    SYSTEM_LOG(buffer);
    //Use_Filter_Info_Defaults(station_data);
    return(FALSE);
  }

  while (fgets(line,MAX_LINE_LEN,file) != NULL) {
    
    if (line[0] == '\n') continue;
    if (line[0] == '#') continue;

    if(strstr(line,"Filter Slot Number") != NULL){
      strcpy(tmp,line+21);
      sscanf(tmp,"%d",&filter_num);
      filter_num--;
      filter_data[filter_num].filter_slot_number = filter_num+1;
      //printf("filter number = %d\n",filter_num);
    }else if(strstr(line,"Wavelength") != NULL){
      if(strlen(line+13) > 5){
        for(i=13; i<19; i++){
          value[i-13] = line[i];
        }
        value[6] = '\0';
        strcpy(filter_data[filter_num].wavelength,value);
      }else{
        for(i=13; i<17; i++){
          value[i-13] = line[i];
        }
        value[4] = '\0';
        strcpy(filter_data[filter_num].wavelength,value);
      }
      //printf("Wavelength(%d) = %s\n",filter_num,filter_data[filter_num].wavelength);
    }else if(strstr(line,"Description") != NULL){
      //strcpy(filter_data[filter_num].filt_descr,line+14);
      	while (line[c] != '\0'){
		filter_data[filter_num].filt_descr[c] = line[c+14];
		c++;
	}
	filter_data[filter_num].filt_descr[c] = '\0';
	//printf("Filt_decript(%d) = %s\n",filter_num,(filter_data+filter_num)->filt_descr);
	c=0;
    }
  }

  if (fclose(file) == EOF) {
    perror("fclose()");
    SYSTEM_LOG("Read_Filter_Settings(): Failed to close file.\n");
    return FALSE;
  }

  return(TRUE);
}

/*
 * Determine Number of schedule slots in configuration file
 * return number of slots
 */
int Read_Sch_Slots(const char *conf_file){

  FILE *file;
  char line[MAX_LINE_LEN+1]   = {0};
  char tmp[MAX_LINE_LEN+1]    = {0};
  char buffer[MAX_LINE_LEN]   = {0};
  int Slot_Num1=0, Slot_Num2=0;

  file = fopen(conf_file,"r");
  if (file == NULL) {
    perror(conf_file);
    sprintf(buffer,"Read_Filter_Settings(): Failed to open file '%s' for reading.\n",conf_file);
    SYSTEM_LOG(buffer);
    return(FALSE);
  }

  while (fgets(line,MAX_LINE_LEN,file) != NULL) {

    if (line[0] == '\n') continue;
    if (line[0] == '#') continue;

    if(strstr(line,"<slot>") != NULL){
      Slot_Num1++;
    }else if(strstr(line,"</slot>") != NULL){
      Slot_Num2++;
    }
  }
  if(Slot_Num1 != Slot_Num2){
    SYSTEM_LOG("Miss matching <slot> | </slot> in schedule.conf file\n");
    exit(FALSE);
  }

  return(Slot_Num1);
}

/**
 * Fill in each schedule specfic information.
 *
 */
int Read_Sched_Settings(const char *conf_file,
      struct schedule_slot_info *schedule_slot,
      struct schedule_info *schedule_data,
      struct emphemeris *emph_data,
      int Slot_Num) {

  FILE *file;
  char line[MAX_LINE_LEN+1]   = {0};
  char line_buffer[255]       = {0};
  char value[MAX_SEQ_LEN+1]   = {0};
  char tmp[MAX_LINE_LEN+1]    = {0};
  char buffer[MAX_LINE_LEN]   = {0};
  int i=0,slot_num=0;
  int prep_num=0;
  int mem;
  float system_exposure;
  int emgain_flag = FALSE;
  /* Open the project file for reading (filename = schedule.conf **Change** */

  file = fopen(conf_file,"r");

  if (file == NULL) {
    perror(conf_file);
    sprintf(buffer,"Read_Filter_Settings(): Failed to open file '%s' for reading.\n",conf_file);
    SYSTEM_LOG(buffer);
    //Use_Filter_Info_Defaults(station_data);
    return(FALSE);
  }

  while (fgets(line,MAX_LINE_LEN,file) != NULL) {

    if (line[0] == '\n') continue;
    if (line[0] == '#') continue;

    if(strstr(line,"device ID") != NULL){
      strcpy(schedule_data->device_id,line+12);
      //printf("Sched Device ID = %s\n",schedule_data->device_id);
    }else if(strstr(line,"image_mode") != NULL){
      strcpy(tmp,line+11);
      sscanf(tmp,"%d",&schedule_data->imager_mode);
      //printf("Imager Mode Value = %d\n",schedule_data->imager_mode);
    }else if(strstr(line,"schedule_len") != NULL){
      strcpy(tmp,line+13);
      sscanf(tmp,"%d",&schedule_data->sched_len);
      //printf("Schedule Length [MSec] = %d\n",schedule_data->sched_len);
    }else if(strstr(line,"zenith_angle_start") != NULL){
      strcpy(tmp,line+19);
      sscanf(tmp,"%f\n",&emph_data->start_zenith_angle);
      //printf("%s\n",tmp);
      //printf("Starting Zenith Angle = %f\n",emph_data->start_zenith_angle);
    }else if(strstr(line,"<slot>") != NULL){
      /* allocate memory for a slot structure */
      /*
      mem = slot_num+1;
      void *_tmp = realloc(schedule_slot,mem*(sizeof(struct schedule_slot_info)));
      if (!_tmp){
        fprintf(stderr, "ERROR: Couldn't realloc memory!\n");
        return(FALSE);
      }
      schedule_slot = (struct schedule_slot_info *)_tmp;
      */
      while (fgets(line,MAX_LINE_LEN,file) != NULL) {

        if (line[0] == '\n') continue;
        if (line[0] == '#') continue;

        if(strstr(line,"</slot>") != NULL){
          //Work around for imagers w/o emgain
          //Sets emccd_gain for each slot to -1
          if(emgain_flag !=TRUE){
            schedule_slot[slot_num].emccd_gain = -1;
          }
          slot_num++;
          break;
        }else if(strstr(line,"filter_num") != NULL){
          strcpy(tmp,line+11);
          sscanf(tmp,"%d",&schedule_slot[slot_num].filter_num);
          //printf("Slot:%d  Filter Number = %d\n",slot_num,schedule_slot[slot_num].filter_num);
        }else if(strstr(line,"start_time") != NULL){
          strcpy(tmp,line+11);
          sscanf(tmp,"%d",&schedule_slot[slot_num].start_time);
          //printf("Slot:%d  Start Time[MSec] = %d\n",slot_num,schedule_slot[slot_num].start_time);
        }else if(strstr(line,"exposure_len") != NULL){
          strcpy(tmp,line+13);
          sscanf(tmp,"%d",&schedule_slot[slot_num].exposure_len);
          //printf("Slot:%d  Exposure Length[MSec] = %d\n",slot_num,schedule_slot[slot_num].exposure_len);
          //printf("Slot:%d Converted Exposure Time{Sec} = %f\n",slot_num,((float)schedule_slot[slot_num].exposure_len)/1000);
          //Set_Exposure_Time(((float)schedule_slot[slot_num].exposure_len)/1000);
          //Get_Exposure_Time(&system_exposure);
          //printf("The Actual Set exposure Time is = %f\n",system_exposure);

        }else if(strstr(line,"binning") != NULL){
          strcpy(tmp,line+8);
          sscanf(tmp,"%d",&schedule_slot[slot_num].binning);
          //printf("Slot:%d  Binning = %d\n",slot_num,schedule_slot[slot_num].binning);
        }else if(strstr(line,"prep_time") != NULL){
          strcpy(tmp,line+10);
          sscanf(tmp,"%d",&schedule_slot[slot_num].prep_time);
          //printf("Slot:%d  Prep_time = %d\n",slot_num,schedule_slot[slot_num].prep_time);
        }else if(strstr(line,"emccd_gain") != NULL){
          strcpy(tmp,line+11);
          sscanf(tmp,"%d",&schedule_slot[slot_num].emccd_gain);
          //There is emgain in conf file
          emgain_flag = TRUE;
          //printf("Slot:%d  Pro_EM EMCCD Gain = %d\n",slot_num,schedule_slot[slot_num].emccd_gain);
        }else if(strstr(line,"ccd_gain") != NULL){
          strcpy(tmp,line+9);
          sscanf(tmp,"%d",&schedule_slot[slot_num].ccd_gain);
	  //if(schedule_slot[slot_num].ccd_gain > 0){
	  //	schedule_slot[slot_num].ccd_gain = schedule_slot[slot_num].ccd_gain - 1;
	  //}
	 //printf("Slot:%d  Pro_EM CCD Gain = %d\n",slot_num,schedule_slot[slot_num].ccd_gain);
        }
      }
    }
  }

  if (fclose(file) == EOF) {
    perror("fclose()");
    SYSTEM_LOG("Read_Filter_Settings(): Failed to close file.\n");
    return FALSE;
  }

  return(slot_num);
}

int Get_Seqno() {
	FILE *file;
        int seqno;
        char buffer[MAX_LINE_LEN] = {0};
	
	file = fopen(SEQNO_FILE,"r");

        if (file == NULL) {
                perror("fopen()");
                sprintf(buffer,"Failed to open file '%s'.\n",SEQNO_FILE);
                SYSTEM_LOG(buffer);
                return FALSE;
        }

        // Get the status
        fscanf(file,"%d",&seqno);
	fclose(file);

	//printf("Got SEQNO = %d\n",seqno);

	return(seqno);
}

int Update_Seqno(){

	FILE *file;
  	int return_flag = FALSE;
	int seqno;
  	char buffer[MAX_LINE_LEN] = {0};
	
	file = fopen(SEQNO_FILE, "r");

  	if (file == NULL) {
    		perror("fopen()");
    		sprintf(buffer,"Failed to open file '%s'.\n",SEQNO_FILE);
    		SYSTEM_LOG(buffer);
    		return FALSE;
  	}

  	// Get the status
  	fscanf(file,"%d",&seqno);
	//Add one to the seqno
	//printf("RX SEQNO = %d\n",seqno);	

	seqno = seqno + 1;
	
	//printf("Added Seqno = %d\n",seqno);

	fclose(file);

	file = fopen(SEQNO_FILE, "w");

        if (file == NULL) {
                perror("fopen()");
                sprintf(buffer,"Failed to open file '%s'.\n",SEQNO_FILE);
                SYSTEM_LOG(buffer);
                return FALSE;
        }
	
	
	if(fprintf(file,"%d",seqno) < 0){
    		sprintf(buffer,"Update_Seqno(): Failed to write to the file.\n");
    		SYSTEM_LOG(buffer);
    		return_flag = FALSE;
  	}else{
    		return_flag = TRUE;
  	}
	
	//printf("SEQNO = %d\n",seqno);
  	//Close the file

  	fclose(file);

  	return return_flag;	
}


/**
 * Write imaging stop status to file.
 */
int Write_Imaging_Stop_Status(int status) {

  FILE *file;
  int return_flag = FALSE;
  char buffer[MAX_LINE_LEN] = {0};

  /* Check the status */
  switch (status) {

    case STOP_IMAGING:
      break;

    case START_IMAGING:
      break;

    case CONTINUE_IMAGING:
      break;

    case DISCONTINUE_IMAGING:
      break;

    default:
      status = STOP_IMAGING;
      SYSTEM_LOG("Write_Imaging_Stop_Status(): Unknown imaging status. Using status=STOP_IMAGING.\n");
      break;
  }

  /* Open the file */

  file = fopen(STOP_FILE,"w");

  if (file == NULL) {
    perror("fopen()");
    sprintf(buffer,"Failed to open file '%s'.\n",STOP_FILE);
    SYSTEM_LOG(buffer);
    return FALSE;
  }

  /* Write the status */

  if(fprintf(file,"%d",status) < 0){
    sprintf(buffer,"Write_Imaging_Stop_Status(): Failed to write status to the file.\n");
    SYSTEM_LOG(buffer);
    return_flag = FALSE;
  }else{
    return_flag = TRUE;
  }

  /* Close the file */

  fclose(file);

  return return_flag;
}

/**
 * Read imaging stop status from file.
 */
int Read_Imaging_Stop_Status(int *status) {

  FILE *file;
  int stop;
  char buffer[MAX_LINE_LEN] = {0};

  /* Open the file */

  file = fopen(STOP_FILE,"r");

  if (file == NULL) {
    perror("fopen()");
    sprintf(buffer,"Failed to open file '%s'.\n",STOP_FILE);
    SYSTEM_LOG(buffer);
    return FALSE;
  }

  /* Get the status */
  fscanf(file,"%d",&stop);

  /* Close the file */
  fclose(file);

  switch (stop) {

    case STOP_IMAGING:

      *status = STOP_IMAGING;
      return TRUE;
      break;

    case START_IMAGING:

      *status = START_IMAGING;
      return TRUE;
      break;

    case CONTINUE_IMAGING:

      *status = CONTINUE_IMAGING;
      return TRUE;
      break;

    case DISCONTINUE_IMAGING:

      *status = DISCONTINUE_IMAGING;
      return TRUE;
      break;

    default:

      *status = STOP_IMAGING;
      SYSTEM_LOG("Read_Imaging_Stop_Status(): WARNING! Unknown imaging status.");
      return FALSE;

  }
}
