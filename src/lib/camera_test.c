

#include <stdio.h>
#include "picam.h"
#include "pro_em.h"



int main() {
	    
	// - open the first camera if any or create a demo camera
	struct Camera *camera = NULL;
	camera = malloc(sizeof(struct Camera));

    struct Current_Params *cur_params = NULL;
    cur_params = malloc(sizeof(struct Current_Params));

    int i = 0;
    piint count;
    const PicamParameter* parameters;
    const pichar* string;
    PicamAvailableData data;
    PicamAcquisitionErrorsMask errors;
    piint readoutstride = 0;
    
    //Set static parameters for testing purposes
    camera->Temp_SetPoint   = -80.0;
    camera->ADC_Speed       = 10.0;
    camera->ReadOutCtrlMode = 1;
    camera->Output_Signal   = 8;
    camera->Invert_Output   = TRUE;
    camera->x_bin           = 2;
    camera->y_bin           = 2;
    camera->ExposureTime    = 1.0;
    camera->CCD_Gain        = 1;
    camera->EM_Gain         = 400;
    camera->ADC_Quality     = 3;




    if(Initialize() != TRUE){
    	printf("Could Not Initialize Library\n");
    	return(FALSE);
    }

    if(OpenCamera(camera) != TRUE){
    	printf("Could Not Open Camera\n");
    	return(FALSE);
    }


    Picam_GetEnumerationString( PicamEnumeratedType_Model, camera->id.model, &string );
    printf( "%s", string );
    printf( " (SN:%s) [%s]\n", camera->id.serial_number, camera->id.sensor_name );
    Picam_DestroyString( string );



    Configure_Static(camera);

    //printf("Temperature = %lf\n",camera->Temperature);

    Configure_Dynamic(camera);

    /*
    // - accessing parameters returns an allocated array
    Picam_GetParameters(camera->hcam, &parameters, &count );

    for(i=0;i<count;++i){
        printf("%d\n",parameters[i]);
    }

    // - deallocate the parameter array after usage
    Picam_DestroyParameters( parameters );
    */
    Picam_DestroyRois(camera->region);

    GetParamInfo(camera, cur_params);

    Acquire_Data(camera);




    if(CloseCamera(camera) != TRUE){
    	printf("Could Not Uninitialize Library and Close Camera\n");
    	return(FALSE);
    }



    return(TRUE);
}