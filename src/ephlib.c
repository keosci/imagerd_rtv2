/*
 *
 *  ephlib.c
 * 
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Jarrett Little and Trond Trondsen
 *
 *
 */

#include "ephlib.h"

/*
 * Functions to return sun and moon angles.  
 */

int f_sun_present(struct tm *time_now,struct station_info *site_data,struct emphemeris *eph_data){
  
  double sun_angle;

  /* Return TRUE if the sun angle is > maximum allowed sun dip */

  sun_angle =sun_dip(1900+time_now->tm_year,1+time_now->tm_mon,
		    time_now->tm_mday,time_now->tm_hour,time_now->tm_min,
  		    time_now->tm_sec, site_data);

#ifdef DEBUG
  
  //fprintf(stderr," solar alt = %f\n",sun_angle);
	//fprintf(stderr," start angle = %f\n",eph_data->start_zenith_angle);
  //if (sun_angle > eph_data->start_zenith_angle)
  //   fprintf(stderr,"     => SUN PRESENT = TRUE\n");
  //else   
  //   fprintf(stderr,"     => SUN PRESENT = FALSE\n");
#endif
	/* Is sun angle above the specifed angle from the eph_data? */
  return((sun_angle > eph_data->start_zenith_angle) ? TRUE : FALSE); 

}

int f_moon_present(struct tm *time_now,struct station_info *site_data,struct emphemeris *eph_data){
  
  double moon_angle, moon_phase;
  
  /* Return TRUE if the lunar depression angle is greater than MAX_MOON_DIP
     AND lunar phase fraction is greater than MAX_MOON_PHASE 	*/

  moon_angle =moon_dip(1900+time_now->tm_year,1+time_now->tm_mon,
		    time_now->tm_mday,time_now->tm_hour,time_now->tm_min,
  		    time_now->tm_sec, site_data, &moon_phase);

#ifdef DEBUG
  fprintf(stderr," lunar alt = %f\n",moon_angle);
  fprintf(stderr," lunar ph  = %f\n",moon_phase);
  if (moon_angle>eph_data->start_moon_angle && moon_phase>eph_data->moon_phase)
     fprintf(stderr,"     => MOON PRESENT = TRUE\n");
  else   
     fprintf(stderr,"     => MOON PRESENT = FALSE\n");
#endif

  return(((moon_angle>eph_data->start_moon_angle && moon_phase>eph_data->moon_phase)) ? TRUE : FALSE); 

}

double sun_dip (int yr, int mn, int dy, int hr, int m, int s, struct station_info *site_data){

  /* Based on PoCa code V1.1 August 14, 1992  (C) T. S. Trondsen */
  /* This code integrated into Polaris code Februrary 1999       */

  double lsn, rsn, ehp;
  double deps, dpsi;
  double ra, dec;
  double ha;
  double aha, adec;
  double alt, az;
  double mjd, mjd_day;
  double utc, gst;
  double lst;
  double longitude, latitude, height;
  double temp, pressure;

  //longitude =-94.64;         /* neg = West */
 	//latitude  =56.380;         /* North   */
  //height = 145;              /* meters  */
  //pressure = 1000;		/* mBar    */
  //temp = 10;			/* Celsius */

  longitude =(double)site_data->longitude;
  latitude  =(double)site_data->latitude;
  height    =(double)site_data->altitude;
  temp      =(double)site_data->avg_temp;
  pressure  =(double)site_data->avg_pressure;


	/*FIXED no more ned to neg west long done in file_io */
  //longitude = longitude*(-1);  //Only for atha cause its E longitude

  sex_dec (hr, m, s, &utc);	/* convert UT to decimal repr. */
  cal_mjd (mn, dy, yr, &mjd_day);	/* return modified Julian date */
  mjd = mjd_day + (utc / 24);	/* add UT time in fractions of a day */
  sunpos (mjd, &lsn, &rsn);	/* return true exl long and dist */
  nutation (mjd, &deps, &dpsi);	/* deps: nutat in obl, dpsi: nutat in long */
  lsn += dpsi;			/* correct longitude (lam) for nutation */
  lsn -= degrad (20.4 / 3600);	/* light travel time */
  ecl_eq (mjd, 0.0, lsn, &ra, &dec);	/* convert to equatorial coords */
  utc_gst (mjd_day, utc, &gst);	/* find greenwich mean sidereal time, *gst */
  lst = gst + deghr (longitude);	/* now find local sidereal time */
  range (&lst, 24.0);		/* keep it within 24 h */
  ha = hrrad (lst) - ra;	/* find hour angle */
  latitude = degrad (latitude);	/* convert latitude to radians */
  height /= 6378140.0;		/* heigh as a fraction of earth radius */
  ehp = (2.0 * 6378.0 / 146.0e6) / rsn;
  ta_par (ha, dec, latitude, height, ehp, &aha, &adec);		/* correct for geoc parllx */
  hadec_aa (latitude, aha, adec, &alt, &az);	/* convert to horizon coords */
  refract (pressure, temp, alt, &alt);	/* correct for refraction. */

#ifdef DEBUG
  /*
  printf ("Solar ephemeris:\n\r");
  printf ("-------------------------------------\n\r");
  printf ("julian date %f \n\r", mjd + 2415020.0);
  printf ("ecl long: %f, ecl lat: 0.0 \n\r", lsn);
  printf ("right asc: %f, decl: %f \n\r", radhr (ra), raddeg (dec));
  printf ("local sidereal time: %f\n\r", lst);
  printf ("hour angle: %f\n\r", ha);
  printf ("alt: %f,  az: %f\n\r", raddeg (alt), raddeg (az));
  */
#endif 

  return(raddeg (alt));

}

double moon_dip(int yr, int mn, int dy, int hr, int m, int s, struct station_info *site_data, double *phase){
      
    /* Based on PoCa code V1.1 August 14, 1992  (C) T. S. Trondsen */ 
    /* This code integrated into Polaris code Februrary 1999       */ 
      
      double lam, bet, hp;
      double lsn, rsn;
      double el;
      double deps, dpsi;
      double ra, dec;
      double ha;
      double aha, adec;
      double alt, az;
      double mjd, mjd_day;
      double utc, gst;
      double lst;
      double longitude, latitude, height;
      double temp, pressure;
      
    // longitude =-94.64;         /* neg = West */
    // latitude  =56.380;         /* North   */
    // height = 145;              /* meters  */
    // pressure = 1000;           /* mBar    */
    // temp = 10;                 /* Celsius */
      
      longitude =(double)site_data->longitude;
      latitude  =(double)site_data->latitude;
      height    =(double)site_data->altitude;
      temp      =(double)site_data->avg_temp;
      pressure  =(double)site_data->avg_pressure;
      
      sex_dec (hr, m, s, &utc);	/* convert UT to decimal repr. */
      cal_mjd (mn, dy, yr, &mjd_day);	/* return modified Julian date */
      mjd = mjd_day + (utc / 24);	/* add UT time in fractions of a day */
      moon (mjd, &lam, &bet, &hp);	/* return geocentric ecl long, lat, hor parallax */
      nutation (mjd, &deps, &dpsi);	/* deps: nutat in obl, dpsi: nutat in long */
      lam += dpsi;		/* correct longitude (lam) for nutation */
      range (&lam, 2 * PI);
      ecl_eq (mjd, bet, lam, &ra, &dec);	/* convert to equatorial coords */
      utc_gst (mjd_day, utc, &gst);	/* find greenwich mean sidereal time, *gst */
      lst = gst + deghr (longitude);	/* now find local sidereal time */
      range (&lst, 24.0);	/* keep it within 24 h */
      ha = hrrad (lst) - ra;	/* find hour angle */
      latitude = degrad (latitude);	/* convert latitude to radians */
      height /= 6378140.0;	/* heigh as a fraction of earth radius */
      ta_par (ha, dec, latitude, height, hp, &aha, &adec); /* correct for geoc parllx */
      hadec_aa (latitude, aha, adec, &alt, &az);	/* convert to horizon coords */
      refract (pressure, temp, alt, &alt);	/* correct for refraction. */
    
#ifdef DEBUG
      printf ("Lunar ephemeris:\n\r");
      printf ("-------------------------------------\n\r");
      printf ("julian date %f \n\r", mjd + 2415020.0);
      printf ("ecl long: %f, ecl lat: %f \n\r", lam, bet);
      printf ("right asc: %f, decl: %f \n\r", radhr (ra), raddeg (dec));
      printf ("local sidereal time: %f\n\r", lst);
      printf ("hour angle: %f\n\r", ha);
      printf ("moon angle - alt = %f,  az = %f\n\r", raddeg (alt), raddeg (az));
#endif    

      sunpos (mjd, &lsn, &rsn);	/* lsn: solar ecl long */
      range (&lsn, 2 * PI);
      elongation (lam, bet, lsn, &el);
      *phase = fabs (el) / PI * 100.0;
    
#ifdef DEBUG
      printf ("moon phase = %f percent.\n\r", *phase);
#endif    
      
      return (raddeg (alt));
      
}

/* given the modified Julian date, mjd, find the obliquity of the
 * ecliptic, *eps, in radians.
 */

void obliquity ( double mjd, double *eps )
{
	static double lastmjd = -10000, lasteps;

	if (mjd != lastmjd) {
	    double t;
	    t = mjd/36525.;
	    lasteps = degrad(2.345229444E1
			- ((((-1.81E-3*t)+5.9E-3)*t+4.6845E1)*t)/3600.0);
	    lastmjd = mjd;
	}
	*eps = lasteps;
}

/* given the modified JD, mjd, find the nutation in obliquity, *deps, and
 * the nutation in longitude, *dpsi, each in radians.
 */

void nutation ( double mjd, double *deps, double *dpsi )
{
	static double lastmjd = -10000, lastdeps, lastdpsi;
	double ls, ld;	/* sun's mean longitude, moon's mean longitude */
	double ms, md;	/* sun's mean anomaly, moon's mean anomaly */
	double nm;	/* longitude of moon's ascending node */
	double t, t2;	/* number of Julian centuries of 36525 days since
			 * Jan 0.5 1900.
			 */
	double tls, tnm, tld;	/* twice above */
	double a, b;	/* temps */

	if (mjd == lastmjd) {
	    *deps = lastdeps;
	    *dpsi = lastdpsi;
	    return;
	}
	    
	t = mjd/36525.;
	t2 = t*t;

	a = 100.0021358*t;
	b = 360.*(a-(long)a);
	ls = 279.697+.000303*t2+b;

	a = 1336.855231*t;
	b = 360.*(a-(long)a);
	ld = 270.434-.001133*t2+b;

	a = 99.99736056000026*t;
	b = 360.*(a-(long)a);
	ms = 358.476-.00015*t2+b;

	a = 13255523.59*t;
	b = 360.*(a-(long)a);
	md = 296.105+.009192*t2+b;

	a = 5.372616667*t;
	b = 360.*(a-(long)a);
	nm = 259.183+.002078*t2-b;

	/* convert to radian forms for use with trig functions.
	 */
	tls = 2*degrad(ls);
	nm = degrad(nm);
	tnm = 2*degrad(nm);
	ms = degrad(ms);
	tld = 2*degrad(ld);
	md = degrad(md);

	/* find delta psi and eps, in arcseconds.
	 */
	lastdpsi = (-17.2327-.01737*t)*sin(nm)+(-1.2729-.00013*t)*sin(tls)
		   +.2088*sin(tnm)-.2037*sin(tld)+(.1261-.00031*t)*sin(ms)
		   +.0675*sin(md)-(.0497-.00012*t)*sin(tls+ms)
		   -.0342*sin(tld-nm)-.0261*sin(tld+md)+.0214*sin(tls-ms)
		   -.0149*sin(tls-tld+md)+.0124*sin(tls-nm)+.0114*sin(tld-md);
	lastdeps = (9.21+.00091*t)*cos(nm)+(.5522-.00029*t)*cos(tls)
		   -.0904*cos(tnm)+.0884*cos(tld)+.0216*cos(tls+ms)
		   +.0183*cos(tld-nm)+.0113*cos(tld+md)-.0093*cos(tls-ms)
		   -.0066*cos(tls-nm);

	/* convert to radians.
	 */
	lastdpsi = degrad(lastdpsi/3600);
	lastdeps = degrad(lastdeps/3600);

	lastmjd = mjd;
	*deps = lastdeps;
	*dpsi = lastdpsi;
}

/* given a date in months, mn, days, dy, years, yr,
 * return the modified Julian date (number of days elapsed since 1900 jan 0.5),
 * *mjd.
 */

void cal_mjd ( int mn, double dy, int yr, double *mjd )
{
	int b, d, m, y;
	long c;

	m = mn;
	y = (yr < 0) ? yr + 1 : yr;
	if (mn < 3) {
	    m += 12;
	    y -= 1;
	}

	if (yr < 1582 || yr == 1582 && (mn < 10 || mn == 10 && dy < 15)) 
	    b = 0;
	else {
	    int a;
	    a = y/100;
	    b = 2 - a + a/4;
	}

	if (y < 0)
	    c = (long)((365.25*y) - 0.75) - 694025L;
	else
	    c = (long)(365.25*y) - 694025L;

	d = 30.6001*(m+1);

	*mjd = b + c + d + dy - 0.5;
}

/* given the modified Julian date (number of days elapsed since 1900 jan 0.5,),
 * mjd, return the calendar date in months, *mn, days, *dy, and years, *yr.
 */

void mjd_cal ( double mjd, int *mn, double *dy, int *yr )
{
	double d, f;
	double i, a, b, ce, g;

	d = mjd + 0.5;
	i = floor(d);
	f = d-i;
	if (f == 1) {
	    f = 0;
	    i += 1;
	}

	if (i > -115860.0) {
	    a = floor((i/36524.25)+.9983573)+14;
	    i += 1 + a - floor(a/4.0);
	}

	b = floor((i/365.25)+.802601);
	ce = i - floor((365.25*b)+.750001)+416;
	g = floor(ce/30.6001);
	*mn = g - 1;
	*dy = ce - floor(30.6001*g)+f;
	*yr = b + 1899;

	if (g > 13.5)
	    *mn = g - 13;
	if (*mn < 2.5)
	    *yr = b + (float) 1900;
	if (*yr < 1)
	    *yr -= 1;
}

/* given an mjd, set *dow to 0..6 according to which dayof the week it falls
 * on (0=sunday) or set it to -1 if can't figure it out.
 */

void mjd_dow ( double mjd, double *dow )
{
	/* cal_mjd() uses Gregorian dates on or after Oct 15, 1582.
	 * (Pope Gregory XIII dropped 10 days, Oct 5..14, and improved the leap-
	 * year algorithm). however, Great Britian and the colonies did not
	 * adopt it until Sept 14, 1752 (they dropped 11 days, Sept 3-13,
	 * due to additional accumulated error). leap years before 1752 thus
	 * can not easily be accounted for from the cal_mjd() number...
	 */
	if (mjd < -53798.5) {
	    /* pre sept 14, 1752 too hard to correct */
	    *dow = -1;
	    return;
	}
	*dow = ((long)floor(mjd-.5) + 1) % 7;/* 1/1/1900 (mjd 0.5) is a Monday*/
	if (*dow < 0)
	    *dow += 7;
}

/* given a mjd, return the the number of days in the month.  */

void mjd_dpm ( double mjd, int *ndays )
{
	static short dpm[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	int m, y;
	double d;

	mjd_cal (mjd, &m, &d, &y);
	*ndays = (m==2 && ((y%4==0 && y%100!=0)||y%400==0)) ? 29 : dpm[m-1];
}


/* given a mjd, return the year as a double. */

void mjd_year ( double mjd, double *yr )
{
	int m, y;
	double d;
	double e0, e1;	/* mjd of start of this year, start of next year */

	mjd_cal (mjd, &m, &d, &y);
	cal_mjd (1, 1.0, y, &e0);
	cal_mjd (1, 1.0, y+1, &e1);
	*yr = y + (mjd - e0)/(e1 - e0);
}

/* given a decimal year, return mjd */

void year_mjd ( double y, double *mjd )
{
	double e0, e1;	/* mjd of start of this year, start of next year */
	int yf = (int) floor (y);

	cal_mjd (1, 1.0, yf, &e0);
	cal_mjd (1, 1.0, yf+1, &e1);
	*mjd = e0 + (y - yf)*(e1-e0);
}

/* given the mean anomaly, ma, and the eccentricity, s, of elliptical motion,
 * find the true anomaly, *nu, and the eccentric anomaly, *ea.
 * all angles in radians.
 */

void anomaly ( double ma, double s, double *nu, double *ea )
{
	double m, dla, fea;

	m = ma-TWOPI*(long)(ma/TWOPI);
	fea = m;
	while (1) {
	    dla = fea-(s*sin(fea))-m;
	    if (fabs(dla)<1e-6)
		break;
	    dla /= 1-(s*cos(fea));
	    fea -= dla;
	}

	*nu = 2*atan(sqrt((1+s)/(1-s))*tan(fea/2));
	*ea = fea;
}


/* given the mjd, find the geocentric ecliptic longitude, lam, and latitude,
 * bet, and horizontal parallax, hp for the moon.
 * N.B. series for long and lat are good to about 10 and 3 arcseconds. however,
 *   math errors cause up to 100 and 30 arcseconds error, even if use double.
 *   why?? suspect highly sensitive nature of difference used to get m1..6.
 * N.B. still need to correct for nutation. then for topocentric location
 *   further correct for parallax and refraction.
 */

void moon ( double mjd, double *lam, double *bet, double *hp )
{
	double t, t2;
	double ld;
	double ms;
	double md;
	double de;
	double f;
	double n;
	double a, sa, sn, b, sb, c, sc, e, e2, l, g, w1, w2;
	double m1, m2, m3, m4, m5, m6;

	t = mjd/36525.0;
	t2 = t*t;

	m1 = mjd/27.32158213;
	m1 = 360.0*(m1-(long)m1);
	m2 = mjd/365.2596407;
	m2 = 360.0*(m2-(long)m2);
	m3 = mjd/27.55455094;
	m3 = 360.0*(m3-(long)m3);
	m4 = mjd/29.53058868;
	m4 = 360.0*(m4-(long)m4);
	m5 = mjd/27.21222039;
	m5 = 360.0*(m5-(long)m5);
	m6 = mjd/6798.363307;
	m6 = 360.0*(m6-(long)m6);

	ld = 270.434164+m1-(.001133-.0000019*t)*t2;
	ms = 358.475833+m2-(.00015+.0000033*t)*t2;
	md = 296.104608+m3+(.009192+.0000144*t)*t2;
	de = 350.737486+m4-(.001436-.0000019*t)*t2;
	f = 11.250889+m5-(.003211+.0000003*t)*t2;
	n = 259.183275-m6+(.002078+.000022*t)*t2;

	a = degrad(51.2+20.2*t);
	sa = sin(a);
	sn = sin(degrad(n));
	b = 346.56+(132.87-.0091731*t)*t;
	sb = .003964*sin(degrad(b));
	c = degrad(n+275.05-2.3*t);
	sc = sin(c);
	ld = ld+.000233*sa+sb+.001964*sn;
	ms = ms-.001778*sa;
	md = md+.000817*sa+sb+.002541*sn;
	f = f+sb-.024691*sn-.004328*sc;
	de = de+.002011*sa+sb+.001964*sn;
	e = 1-(.002495+7.52e-06*t)*t;
	e2 = e*e;

	ld = degrad(ld);
	ms = degrad(ms);
	n = degrad(n);
	de = degrad(de);
	f = degrad(f);
	md = degrad(md);

	l = 6.28875*sin(md)+1.27402*sin(2*de-md)+.658309*sin(2*de)+
	    .213616*sin(2*md)-e*.185596*sin(ms)-.114336*sin(2*f)+
	    .058793*sin(2*(de-md))+.057212*e*sin(2*de-ms-md)+
	    .05332*sin(2*de+md)+.045874*e*sin(2*de-ms)+.041024*e*sin(md-ms);
	l = l-.034718*sin(de)-e*.030465*sin(ms+md)+.015326*sin(2*(de-f))-
	    .012528*sin(2*f+md)-.01098*sin(2*f-md)+.010674*sin(4*de-md)+
	    .010034*sin(3*md)+.008548*sin(4*de-2*md)-e*.00791*sin(ms-md+2*de)-
	    e*.006783*sin(2*de+ms);
	l = l+.005162*sin(md-de)+e*.005*sin(ms+de)+.003862*sin(4*de)+
	    e*.004049*sin(md-ms+2*de)+.003996*sin(2*(md+de))+
	    .003665*sin(2*de-3*md)+e*.002695*sin(2*md-ms)+
	    .002602*sin(md-2*(f+de))+e*.002396*sin(2*(de-md)-ms)-
	    .002349*sin(md+de);
	l = l+e2*.002249*sin(2*(de-ms))-e*.002125*sin(2*md+ms)-
	    e2*.002079*sin(2*ms)+e2*.002059*sin(2*(de-ms)-md)-
	    .001773*sin(md+2*(de-f))-.001595*sin(2*(f+de))+
	    e*.00122*sin(4*de-ms-md)-.00111*sin(2*(md+f))+.000892*sin(md-3*de);
	l = l-e*.000811*sin(ms+md+2*de)+e*.000761*sin(4*de-ms-2*md)+
	     e2*.000704*sin(md-2*(ms+de))+e*.000693*sin(ms-2*(md-de))+
	     e*.000598*sin(2*(de-f)-ms)+.00055*sin(md+4*de)+.000538*sin(4*md)+
	     e*.000521*sin(4*de-ms)+.000486*sin(2*md-de);
	l = l+e2*.000717*sin(md-2*ms);
	*lam = ld+degrad(l);
	range (lam, 2*PI);


	g = 5.12819*sin(f)+.280606*sin(md+f)+.277693*sin(md-f)+
	    .173238*sin(2*de-f)+.055413*sin(2*de+f-md)+.046272*sin(2*de-f-md)+
	    .032573*sin(2*de+f)+.017198*sin(2*md+f)+.009267*sin(2*de+md-f)+
	    .008823*sin(2*md-f)+e*.008247*sin(2*de-ms-f);
	g = g+.004323*sin(2*(de-md)-f)+.0042*sin(2*de+f+md)+
	    e*.003372*sin(f-ms-2*de)+e*.002472*sin(2*de+f-ms-md)+
	    e*.002222*sin(2*de+f-ms)+e*.002072*sin(2*de-f-ms-md)+
	    e*.001877*sin(f-ms+md)+.001828*sin(4*de-f-md)-e*.001803*sin(f+ms)-
	    .00175*sin(3*f);
	g = g+e*.00157*sin(md-ms-f)-.001487*sin(f+de)-e*.001481*sin(f+ms+md)+
	     e*.001417*sin(f-ms-md)+e*.00135*sin(f-ms)+.00133*sin(f-de)+
	     .001106*sin(f+3*md)+.00102*sin(4*de-f)+.000833*sin(f+4*de-md)+
	     .000781*sin(md-3*f)+.00067*sin(f+4*de-2*md);
	g = g+.000606*sin(2*de-3*f)+.000597*sin(2*(de+md)-f)+
	    e*.000492*sin(2*de+md-ms-f)+.00045*sin(2*(md-de)-f)+
	    .000439*sin(3*md-f)+.000423*sin(f+2*(de+md))+
	    .000422*sin(2*de-f-3*md)-e*.000367*sin(ms+f+2*de-md)-
	    e*.000353*sin(ms+f+2*de)+.000331*sin(f+4*de);
	g = g+e*.000317*sin(2*de+f-ms+md)+e2*.000306*sin(2*(de-ms)-f)-
	    .000283*sin(md+3*f);
	w1 = .0004664*cos(n);
	w2 = .0000754*cos(c);
	*bet = degrad(g)*(1-w1-w2);

	*hp = .950724+.051818*cos(md)+.009531*cos(2*de-md)+.007843*cos(2*de)+
	      .002824*cos(2*md)+.000857*cos(2*de+md)+e*.000533*cos(2*de-ms)+
	      e*.000401*cos(2*de-md-ms)+e*.00032*cos(md-ms)-.000271*cos(de)-
	      e*.000264*cos(ms+md)-.000198*cos(2*f-md);
	*hp = *hp+.000173*cos(3*md)+.000167*cos(4*de-md)-e*.000111*cos(ms)+
	     .000103*cos(4*de-2*md)-.000084*cos(2*md-2*de)-
	     e*.000083*cos(2*de+ms)+.000079*cos(2*de+2*md)+.000072*cos(4*de)+
	     e*.000064*cos(2*de-ms+md)-e*.000063*cos(2*de+ms-md)+
	     e*.000041*cos(ms+de);
	*hp = *hp+e*.000035*cos(2*md-ms)-.000033*cos(3*md-2*de)-
	     .00003*cos(md+de)-.000029*cos(2*(f-de))-e*.000029*cos(2*md+ms)+
	     e2*.000026*cos(2*(de-ms))-.000023*cos(2*(f-de)+md)+
	     e*.000019*cos(4*de-ms-md);
	*hp = degrad(*hp);
}

/* given the modified Julian date, mjd, and an equitorial ra and dec, each in
 * radians, find the corresponding geocentric ecliptic latitude, *lat, and
 * longititude, *lng, also each in radians.
 * correction for the effect on the angle of the obliquity due to nutation is
 * included.
 */

void eq_ecl ( double mjd, double ra, double dec, double *lat, double *lng )
{
	ecleq_aux (EQtoECL, mjd, ra, dec, lng, lat);
}

/* given the modified Julian date, mjd, and a geocentric ecliptic latitude,
 * *lat, and longititude, *lng, each in radians, find the corresponding
 * equitorial ra and dec, also each in radians.
 * correction for the effect on the angle of the obliquity due to nutation is
 * included.
 */

void ecl_eq ( double mjd, double lat, double lng, double *ra, double *dec )
{
	ecleq_aux (ECLtoEQ, mjd, lng, lat, ra, dec);
}

void static
ecleq_aux ( int sw, double mjd, double x, double y, double *p, double *q )
/* sw = +1 for eq to ecliptic, -1 for vv. */
/* sw==1: x==ra, y==dec.  sw==-1: x==lng, y==lat. */
/* sw==1: p==lng, q==lat. sw==-1: p==ra, q==dec. */
{
	static double lastmjd = -10000;	/* last mjd calculated */
	static double seps, ceps;	/* sin and cos of mean obliquity */
	double sx, cx, sy, cy, ty;

	if (mjd != lastmjd) {
	    double eps;
	    double deps, dpsi;
	    obliquity (mjd, &eps);		/* mean obliquity for date */
	    nutation (mjd, &deps, &dpsi);
	    eps += deps;
    	    seps = sin(eps);
	    ceps = cos(eps);
	    lastmjd = mjd;
	}

	sy = sin(y);
	cy = cos(y);				/* always non-negative */
        if (fabs(cy)<1e-20) cy = 1e-20;		/* insure > 0 */
        ty = sy/cy;
	cx = cos(x);
	sx = sin(x);
        *q = asin((sy*ceps)-(cy*seps*sx*sw));
        *p = atan(((sx*ceps)+(ty*seps*sw))/cx);
        if (cx<0) *p += PI;		/* account for atan quad ambiguity */
	range (p, 2*PI);
}

/* given latitude (n+, radians), lat, altitude (up+, radians), alt, and
 * azimuth (angle round to the east from north+, radians),
 * return hour angle (radians), ha, and declination (radians), dec.
 */

void aa_hadec ( double lat, double alt, double az, double *ha, double *dec )
{
	aaha_aux (lat, az, alt, ha, dec);
}

/* given latitude (n+, radians), lat, hour angle (radians), ha, and declination
 * (radians), dec,
 * return altitude (up+, radians), alt, and
 * azimuth (angle round to the east from north+, radians),
 */

void hadec_aa ( double lat, double ha, double dec, double *alt, double *az )
{
	aaha_aux (lat, ha, dec, az, alt);
}

/* the actual formula is the same for both transformation directions so
 * do it here once for each way.
 * N.B. all arguments are in radians.
 */

void static
aaha_aux ( double lat, double x, double y, double *p, double *q )
{
	static double lastlat = -1000.;
	static double sinlastlat, coslastlat;
	double sy, cy;
	double sx, cx;
	double sq, cq;
	double a;
	double cp;

	/* latitude doesn't change much, so try to reuse the sin and cos evals.
	 */
	if (lat != lastlat) {
	    sinlastlat = sin (lat);
	    coslastlat = cos (lat);
	    lastlat = lat;
	}

	sy = sin (y);
	cy = cos (y);
	sx = sin (x);
	cx = cos (x);

/* define GOODATAN2 if atan2 returns full range -PI through +PI.
 */
#ifdef GOODATAN2
	*q = asin ((sy*sinlastlat) + (cy*coslastlat*cx));
	*p = atan2 (-cy*sx, -cy*cx*sinlastlat + sy*coslastlat);
#else
#define	EPS	(1e-20)
	sq = (sy*sinlastlat) + (cy*coslastlat*cx);
	*q = asin (sq);
	cq = cos (*q);
	a = coslastlat*cq;
	if (a > -EPS && a < EPS)
	    a = a < 0 ? -EPS : EPS; /* avoid / 0 */
	cp = (sy - (sinlastlat*sq))/a;
	if (cp >= 1.0)	/* the /a can be slightly > 1 */
	    *p = 0.0;
	else if (cp <= -1.0)
	    *p = PI;
	else
	    *p = acos ((sy - (sinlastlat*sq))/a);
	if (sx>0) *p = 2.0*PI - *p;
#endif
}

/* given a modified julian date, mjd, and a universally coordinated time, utc,
 * return greenwich mean siderial time, *gst.
 */

void utc_gst ( double mjd, double utc, double *gst ) 
{
	double tnaught();
	static double lastmjd = -10000;
	static double t0;

	if (mjd != lastmjd) {
	    t0 = tnaught (mjd);
	    lastmjd = mjd;
	}
	*gst = (1.0/SIDRATE)*utc + t0;
	range (gst, 24.0);
}

/* given a modified julian date, mjd, and a greenwich mean siderial time, gst,
 * return universally coordinated time, *utc.
 */

void gst_utc ( double mjd, double gst, double *utc )
{
	double tnaught();
	static double lastmjd = -10000;
	static double t0;

	if (mjd != lastmjd) {
	    t0 = tnaught (mjd);
	    range (&t0, 24.0);
	    lastmjd = mjd;
	}
	*utc = gst - t0;
	range (utc, 24.0);
	*utc *= SIDRATE;
}

static double
tnaught ( double mjd )
/* mjd = julian days since 1900 jan 0.5 */
{
	double dmjd;
	int m, y;
	double d;
	double t, t0;

	mjd_cal (mjd, &m, &d, &y);
	cal_mjd (1, 0., y, &dmjd);
	t = dmjd/36525;
	t0 = 6.57098e-2 * (mjd - dmjd) - 
	     (24 - (6.6460656 + (5.1262e-2 + (t * 2.581e-5))*t) -
		   (2400 * (t - (((double)y - 1900)/100))));
	return (t0);
}

/* given true ha and dec, tha and tdec, the geographical latitude, phi, the
 * height above sea-level (as a fraction of the earths radius, 6378.16km),
 * ht, and the equatorial horizontal parallax, ehp, find the apparent
 * ha and dec, aha and adec allowing for parallax.
 * all angles in radians. ehp is the angle subtended at the body by the
 * earth's equator.
 */

void ta_par ( double tha, double tdec, double phi, double ht, double ehp, 
		double *aha, double *adec )
{
	static double last_phi, last_ht, rsp, rcp;
	double rp;	/* distance to object in Earth radii */
	double ctha;
	double stdec, ctdec;
	double tdtha, dtha;
	double caha;

	/* avoid calcs involving the same phi and ht */
	if (phi != last_phi || ht != last_ht) {
	    double cphi, sphi, u;
	    cphi = cos(phi);
	    sphi = sin(phi);
	    u = atan(9.96647e-1*sphi/cphi);
	    rsp = (9.96647e-1*sin(u))+(ht*sphi);
	    rcp = cos(u)+(ht*cphi);
	    last_phi  =  phi;
	    last_ht  =  ht;
	}

        rp = 1/sin(ehp);

        ctha = cos(tha);
	stdec = sin(tdec);
	ctdec = cos(tdec);
        tdtha = (rcp*sin(tha))/((rp*ctdec)-(rcp*ctha));
        dtha = atan(tdtha);
	*aha = tha+dtha;
	caha = cos(*aha);
	range (aha, 2*PI);
        *adec = atan(caha*(rp*stdec-rsp)/(rp*ctdec*ctha-rcp));
}

/* given the apparent ha and dec, aha and adec, the geographical latitude, phi,
 * the height above sea-level (as a fraction of the earths radius, 6378.16km),
 * ht, and the equatorial horizontal parallax, ehp, find the true ha and dec,
 * tha and tdec allowing for parallax.
 * all angles in radians. ehp is the angle subtended at the body by the
 * earth's equator.
 * uses ta_par() iteratively: find a set of true ha/dec that converts back
  *  to the given apparent ha/dec.
 */

void at_par ( double aha, double adec, double phi, double ht, double ehp, 
		double *tha, double *tdec )
{
	double nha, ndec;	/* ha/dec corres. to current true guesses */
	double eha, edec;	/* error in ha/dec */

	/* first guess for true is just the apparent */
	*tha = aha;
	*tdec = adec;

	while (1) {
	    ta_par (*tha, *tdec, phi, ht, ehp, &nha, &ndec);
	    eha = aha - nha;
	    edec = adec - ndec;
	    if (fabs(eha)<1e-6 && fabs(edec)<1e-6)
		break;
	    *tha += eha;
	    *tdec += edec;
	}
}


/* given hours (or degrees), hd, minutes, m, and seconds, s, 
 * return decimal hours (or degrees), *d.
 * in the case of hours (angles) < 0, only the first non-zero element should
 *   be negative.
 */

void sex_dec ( int hd, int m, int s, double *d )
{
	int sign = 1;

	if (hd < 0) {
	    sign = -1;
	    hd = -hd;
	} else if (m < 0) {
	    sign = -1;
	    m = -m;
	} else if (s < 0) {
	    sign = -1;
	    s = -s;
	}

	*d = (((double)s/60.0 + (double)m)/60.0 + (double)hd) * sign;
}

/* given decimal hours (or degrees), d.
 * return nearest hours (or degrees), *hd, minutes, *m, and seconds, *s, 
 * each always non-negative; *isneg is set to 1 if d is < 0, else to 0.
 */

void dec_sex ( double d, int *hd, int *m, int *s, int *isneg )
{
	double min;

	if (d < 0) {
	    *isneg = 1;
	    d = -d;
	} else
	    *isneg = 0;

	*hd = (int)d;
	min = (d - *hd)*60.;
	*m = (int)min;
	*s = (int)((min - *m)*60. + 0.5);

	if (*s == 60) {
	    if ((*m += 1) == 60) {
		*hd += 1;
		*m = 0;
	    }
	    *s = 0;
	}
	/* no  negative 0's */
	if (*hd == 0 && *m == 0 && *s == 0)
	    *isneg = 0;
}

/* insure 0 <= *v < r.
 */

void range ( double *v, double r )
{
	while (*v <  0) *v += r;
	while (*v >= r) *v -= r;
}

/* correct the true altitude, ta, for refraction to the apparent altitude, aa,
 * each in radians, given the local atmospheric pressure, pr, in mbars, and
 * the temperature, tr, in degrees C.
 */

void refract ( double pr, double tr, double ta, double *aa )
{
	double r;	/* refraction correction*/

        if (ta >= degrad(15.)) {
	    /* model for altitudes at least 15 degrees above horizon */
            r = 7.888888e-5*pr/((273+tr)*tan(ta));
	} else if (ta > degrad(-5.)) {
	    /* hairier model for altitudes at least -5 and below 15 degrees */
	    double a, b, tadeg = raddeg(ta);
	    a = ((2e-5*tadeg+1.96e-2)*tadeg+1.594e-1)*pr;
	    b = (273+tr)*((8.45e-2*tadeg+5.05e-1)*tadeg+1);
	    r = degrad(a/b);
	} else {
	    /* do nothing if more than 5 degrees below horizon.
	     */
	    r = 0;
	}

	*aa  =  ta + r;
}

/* correct the apparent altitude, aa, for refraction to the true altitude, ta,
 * each in radians, given the local atmospheric pressure, pr, in mbars, and
 * the temperature, tr, in degrees C.
 */

void unrefract ( double pr, double tr, double aa, double *ta )
{
	double err;
	double appar;
	double true;

	/* iterative solution: search for the true that refracts to the
	 *   given apparent.
	 * since refract() is discontinuous at -5 degrees, there is a range
	 *   of apparent altitudes between about -4.5 and -5 degrees that are
	 *   not invertable (the graph of ap vs. true has a vertical step at
	 *   true = -5). thus, the iteration just oscillates if it gets into
	 *   this region. if this happens the iteration is forced to abort.
	 *   of course, this makes unrefract() discontinuous too.
	 */
	true = aa;
	do {
	    refract (pr, tr, true, &appar);
	    err = appar - aa;
	    true -= err;
	} while (fabs(err) >= 1e-6 && true > degrad(-5));

	*ta = true;
}

/* given geocentric ecliptic longitude and latitude, lam and bet, of some object
 * and the longitude of the sun, lsn, find the elongation, el. this is the
 * actual angular separation of the object from the sun, not just the difference
 * in the longitude. the sign, however, IS set simply as a test on longitude
 * such that el will be >0 for an evening object <0 for a morning object.
 * to understand the test for el sign, draw a graph with lam going from 0-2*PI
 *   down the vertical axis, lsn going from 0-2*PI across the hor axis. then
 *   define the diagonal regions bounded by the lines lam=lsn+PI, lam=lsn and
 *   lam=lsn-PI. the "morning" regions are any values to the lower left of the
 *   first line and bounded within the second pair of lines.
 * all angles in radians.
 */

void elongation ( double lam, double bet, double lsn, double *el )
{
	*el = acos(cos(bet)*cos(lam-lsn));
	if (lam>lsn+PI || lam>lsn-PI && lam<lsn) *el = - *el;
}


/* given the modified JD, mjd, return the true geocentric ecliptic longitude
 *   of the sun for the mean equinox of the date, *lsn, in radians, and the
 *   sun-earth distance, *rsn, in AU. (the true ecliptic latitude is never more
 *   than 1.2 arc seconds and so may be taken to be a constant 0.)
 * if the APPARENT ecliptic longitude is required, correct the longitude for
 *   nutation to the true equinox of date and for aberration (light travel time,
 *   approximately  -9.27e7/186000/(3600*24*365)*2*pi = -9.93e-5 radians).
 */

void sunpos ( double mjd, double *lsn, double *rsn )
{
	double t, t2;
	double ls, ms;    /* mean longitude and mean anomoay */
	double s, nu, ea; /* eccentricity, true anomaly, eccentric anomaly */
	double a, b, a1, b1, c1, d1, e1, h1, dl, dr;

	t = mjd/36525.;
	t2 = t*t;
	a = 100.0021359*t;
	b = 360.*(a-(long)a);
	ls = 279.69668+.0003025*t2+b;
	a = 99.99736042000039*t;
	b = 360*(a-(long)a);
	ms = 358.47583-(.00015+.0000033*t)*t2+b;
	s = .016751-.0000418*t-1.26e-07*t2;
	anomaly (degrad(ms), s, &nu, &ea);
	a = 62.55209472000015*t;
	b = 360*(a-(long)a);
	a1 = degrad(153.23+b);
	a = 125.1041894*t;
	b = 360*(a-(long)a);
	b1 = degrad(216.57+b);
	a = 91.56766028*t;
	b = 360*(a-(long)a);
	c1 = degrad(312.69+b);
	a = 1236.853095*t;
	b = 360*(a-(long)a);
	d1 = degrad(350.74-.00144*t2+b);
	e1 = degrad(231.19+20.2*t);
	a = 183.1353208*t;
	b = 360*(a-(long)a);
	h1 = degrad(353.4+b);
	dl = .00134*cos(a1)+.00154*cos(b1)+.002*cos(c1)+.00179*sin(d1)+
								.00178*sin(e1);
	dr = 5.43e-06*sin(a1)+1.575e-05*sin(b1)+1.627e-05*sin(c1)+
					    3.076e-05*cos(d1)+9.27e-06*sin(h1);
	*lsn = nu+degrad(ls-ms+dl);
	*rsn = 1.0000002*(1-s*cos(ea))+dr;
	range (lsn, 2*PI);
}

/* corrects ra and dec, both in radians, for precession from epoch 1 to epoch 2.
 * the epochs are given by their modified JDs, mjd1 and mjd2, respectively.
 * N.B. ra and dec are modifed IN PLACE.
 * TODO: find a better algorithm; this one is not even symmetric.
 */

void precess (double mjd1, double mjd2, double *ra, double *dec)
/* initial and final epoch modified JDs */
/* ra/dec for mjd1 in, for mjd2 out */
{
	static double lastmjd1 = -10000, lastmjd2 = -10000;
	static double m, n, nyrs;
	double dra, ddec;	/* ra and dec corrections */

	if (mjd1 != lastmjd1 || mjd2 != lastmjd2) {
	    double t1, t2; /* Julian centuries of 36525 days since Jan .5 1900*/
	    double m1, n1; /* "constants" for t1 */
	    double m2, n2; /* "constants" for t2 */
	    t1 = mjd1/36525.;
	    t2 = mjd2/36525.;
	    m1 = 3.07234+(1.86e-3*t1);
	    n1 = 20.0468-(8.5e-3*t1);
	    m2 = 3.07234+(1.86e-3*t2);
	    n2 = 20.0468-(8.5e-3*t2);
	    m = (m1+m2)/2;	/* average m for the two epochs */
	    n = (n1+n2)/2;	/* average n for the two epochs */
	    nyrs = (mjd2-mjd1)/365.2425;
	    lastmjd1 = mjd1;
	    lastmjd2 = mjd2;
	}

	dra = (m+(n*sin(*ra)*tan(*dec)/15))*7.272205e-5*nyrs;
	ddec = n*cos(*ra)*4.848137e-6*nyrs;
	*ra += dra;
	*dec += ddec;
	/* added by ECD */
	if (*dec > PI/2) {
	    *dec = PI - *dec;
	    *ra += PI;
	} else if (*dec < -PI/2) {
	    *dec = -PI - *dec;
	    *ra += PI;
	}
	range (ra, 2*PI);
}









