/*
 *  pro_em.c
 *
 *	Camera Library Plugin for PI Pro_EM Camera
 *
 *  Copyright 2013 - Jarrett Little
 *
 */


//Custom Library Includes
#include "pro_em.h"

/*
 *  Initialize the picam library
 */
int Init(void) {

 	//Perform initialization of the picam library (must be done before any picam function calls).
 	Picam_InitializeLibrary();

 	//Log that the picam init was completed.
 	SYSTEM_LOG("Initialized PICAM Library");

 	return(TRUE);

}

/*
 *  Open camera and return the camera handle.
 *  If no physical camera is attached, generate a demo camera and return its handle.
 *  PicamModel_ProEMPlus1024B
 */
int OpenCamera(struct Camera *camera){

	if( Picam_OpenFirstCamera( &(camera->hcam) ) == PicamError_None )
		Picam_GetCameraID( camera->hcam, &(camera->id) );
	else {
		Picam_ConnectDemoCamera(
			PicamModel_ProEMPlus1024B,
			"0008675309",
			&(camera->id));
		Picam_OpenCamera( &(camera->id), &(camera->hcam) );
		SYSTEM_LOG("No Camera Detected, Creating Demo Camera");
		SYSTEM_LOG("Demo Camera: ProEMSeries");

		return(TRUE);
	}
	SYSTEM_LOG("Opened ProEM Camera");
	return(TRUE);
}

/*
 *
 */
int CloseCamera(struct Camera *camera){
	//Closing camera attached to camera handle and uninitialize library.
	Picam_CloseCamera( &(camera->hcam) );
    Picam_UninitializeLibrary();
    SYSTEM_LOG("Closed camera and uninitialized library");
    return(TRUE);
}

/*
 * Safe Exit when PICAM Errors are detected.
 */
void SafeExit(struct Camera *camera, piint error){
	char error_str[MAX_STRING_LEN]  = {0};
	sprintf(error_str,"Error Code Detected: %d -> Safe Exit\n",error);
	SYSTEM_LOG(error_str);
	CloseCamera(camera);
}


/*
 *  Grab the current camera parameters and store them in the Current
 *  Params structure. This structure can then be printed to system log.
 */
int GetParamInfo(struct Camera *camera, struct Current_Params *params) {
	piint xbin, ybin, window_height;

	//Allocate Memory for the nine important set able parameters
	PicamParameter parameters[12];
	//Set the parameter value into the array.
	parameters[0] = PicamParameter_ExposureTime;
	parameters[1] = PicamParameter_AdcSpeed;
	parameters[2] = PicamParameter_ReadoutControlMode;
	parameters[3] = PicamParameter_AdcAnalogGain;
	parameters[4] = PicamParameter_AdcEMGain;
	parameters[5] = PicamParameter_SensorTemperatureSetPoint;
	parameters[6] = PicamParameter_OutputSignal;
	parameters[7] = PicamParameter_InvertOutputSignal;
	parameters[8] = PicamParameter_SensorTemperatureReading;
	parameters[9] = PicamParameter_PixelBitDepth;
	parameters[10] = PicamParameter_KineticsWindowHeight;
	parameters[11] = PicamParameter_AdcQuality;

	//Get and store parameters values into the current parameter structure
	params->ExposureTime 	= Get_Flt_Param(camera,parameters[0]);
	params->Temperature 	= Get_Flt_Param(camera,parameters[8]);
	params->ADC_Speed		= Get_Flt_Param(camera,parameters[1]);
	params->Temp_SetPoint	= Get_Flt_Param(camera,parameters[5]);
	params->Invert_Output	= Get_Int_Param(camera,parameters[7]);
	params->EM_Gain			= Get_Int_Param(camera,parameters[4]);
	params->ReadOutCtrlMode = Get_Int_Param(camera,parameters[2]);
	params->CCD_Gain		= Get_Int_Param(camera,parameters[3]);
	params->Output_Signal	= Get_Int_Param(camera,parameters[6]);
	window_height 			= Get_Int_Param(camera,parameters[10]);
	params->ADC_Quality		= Get_Int_Param(camera,parameters[11]);

	//printf("Window Height = %d\n",window_height);

	camera->bit_depth 		= Get_Int_Param(camera,parameters[9]);
	params->bit_depth = camera->bit_depth;

	Get_ROIS_Param(camera,&xbin,&ybin);

	params->x_bin = xbin;
	params->y_bin = ybin;

	//Function to print out current params structure
	print_params(params);

	//Remove pointer to parameters stucture
	Picam_DestroyParameters(parameters);

	return(TRUE);
}


/*
 * Input parameter type and return the floating point value.
 */
double Get_Flt_Param(struct Camera *camera, PicamParameter parameter){
	piflt value;
	Picam_GetParameterFloatingPointValue(camera->hcam, parameter, &value );
	return(value);
}

/*
 * Input parameter type and returns the integer value.
 */
int Get_Int_Param(struct Camera *camera, PicamParameter parameter){
	piint value;
    Picam_GetParameterIntegerValue(camera->hcam, parameter, &value );
	return(value);
}

/*
 * Get ROIS bin for both Y and X axis.
 */
void Get_ROIS_Param(struct Camera *camera, piint* xbin, piint* ybin) {
	PicamError error;

	/* Get the orinal ROI */
	error = Picam_GetParameterRoisValue(camera->hcam,
							PicamParameter_Rois,
							&(camera->region));
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		if (camera->region->roi_count == 1){

			*xbin = camera->region->roi_array[0].x_binning;
			*ybin = camera->region->roi_array[0].y_binning;

		}else{
			SYSTEM_LOG("No Region of Interest Found");
			SafeExit(camera,error);
			return(FALSE);
		}
	}
	return;
}

/*
 * Outputs the current params sturcture to cmd prompt or system log.
 */
int print_params(struct Current_Params *params) {
	char str[MAX_STRING_LEN]  = {0};

	SYSTEM_LOG("==================================");
	SYSTEM_LOG("Current Parameter Readings");
	SYSTEM_LOG("==================================");
	//Print out standard value parameters - no enumeration types
	sprintf(str,"Current Exposure Time: %.2f [Seconds]",params->ExposureTime);
	SYSTEM_LOG(str);
	sprintf(str,"Current Temperature:  %.2f [Celsius]",params->Temperature);
	SYSTEM_LOG(str);
	sprintf(str,"Temperature Setpoint: %.2f [Celsius]",params->Temp_SetPoint);
	SYSTEM_LOG(str);
	sprintf(str,"ADC Read-out Speed:    %.2f [MHz]",params->ADC_Speed);
	SYSTEM_LOG(str);
	sprintf(str,"Current EM Gain:       %d",params->EM_Gain);
	SYSTEM_LOG(str);
	sprintf(str,"X Binning:             %d",params->x_bin);
	SYSTEM_LOG(str);
	sprintf(str,"Y Binning:             %d",params->y_bin);
	SYSTEM_LOG(str);
	sprintf(str,"Pixel Bit Depth:       %d",params->bit_depth);
	SYSTEM_LOG(str);

	//Print out enumeration types

	//Inverted Output Status
	if(params->Invert_Output == 0){
		SYSTEM_LOG("Inverted Output:       False");
	}else{
		SYSTEM_LOG("Inverted Output:       True");
	}
	//params->ReadOutCtrlMode
	switch(params->ReadOutCtrlMode){
		case 1: SYSTEM_LOG("Read-out Control Mode: FullFrame"); break;
		case 2: SYSTEM_LOG("Read-out Control Mode: FrameTransfer"); break;
		case 3: SYSTEM_LOG("Read-out Control Mode: Kinetics"); break;
		case 4: SYSTEM_LOG("Read-out Control Mode: SpectraKinetics"); break;
		case 5: SYSTEM_LOG("Read-out Control Mode: Interline"); break;
		case 6: SYSTEM_LOG("Read-out Control Mode: Dif"); break;
		default: SYSTEM_LOG("Read-out Control Mode: Error No Valid Readout Mode Found"); break;
	}
	//params->Output_Signal
	switch(params->Output_Signal){
		case 1: SYSTEM_LOG("Output Signal:         NotReadingOut"); break;
		case 2: SYSTEM_LOG("Output Signal:         ShutterOpen"); break;
		case 3: SYSTEM_LOG("Output Signal:         Busy"); break;
		case 4: SYSTEM_LOG("Output Signal:         AlwaysLow"); break;
		case 5: SYSTEM_LOG("Output Signal:         AlwaysHigh"); break;
		case 6: SYSTEM_LOG("Output Signal:         Acquiring"); break;
		case 7: SYSTEM_LOG("Output Signal:         ShiftingUnderMask"); break;
		case 8: SYSTEM_LOG("Output Signal:         Exposing"); break;
		case 9: SYSTEM_LOG("Output Signal:         EffectivelyExposing"); break;
		case 10: SYSTEM_LOG("Output Signal:         ReadingOut"); break;
		case 11: SYSTEM_LOG("Output Signal:         WaitingForTrigger"); break;
		default: SYSTEM_LOG("Output Signal:         Error No Valid Output Signal Found"); break;
	}
	//params->CCD_Gain
	switch(params->CCD_Gain){
		case 1: SYSTEM_LOG("CCD Gain:              Low"); break;
		case 2: SYSTEM_LOG("CCD Gain:              Medium"); break;
		case 3: SYSTEM_LOG("CCD Gain:              High"); break;
		default: SYSTEM_LOG("CCD Gain:              Error No Valid CCD Gain"); break;
	}
	//params->ADC_Quality
	switch(params->ADC_Quality){
		case 1: SYSTEM_LOG("ADC Quality:           Low Noise"); break;
		case 2: SYSTEM_LOG("ADC Quality:           High Capacity"); break;
		case 3: SYSTEM_LOG("ADC Quality:           Electron Multiplied"); break;
		default: SYSTEM_LOG("ADC Quality:           Error No Valid ADC Quality"); break;

	}

	SYSTEM_LOG("\n");

	return(TRUE);
}

/*
 * Configure Static Parameters on Camera Initialization
 * Parameters:
 * Temperature Setpoint
 * Readout Speed
 * Readout Control Mode
 * Output Signal
 * Invert Output Signal
 * ROIS Static Parameters
 *
 */
int Configure_Static(struct Camera *camera){
	PicamError error;
	char str[MAX_STRING_LEN]  = {0};
	pibln committed;
	const PicamParameter* failed_parameters;
	piint failed_parameters_count;
	const PicamRoisConstraint *constraint;
	/* Variables to compute central region in image */
	piint totalWidth, totalHeight;


	SYSTEM_LOG("==================================");
	SYSTEM_LOG("Configuring Static Parameters");
	SYSTEM_LOG("==================================");

	//Set Temperature SetPoint
	error = Picam_SetParameterFloatingPointValue(camera->hcam,
					PicamParameter_SensorTemperatureSetPoint,
					camera->Temp_SetPoint);
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		sprintf(str,"Temperature Set Point = %f [Degrees C]",camera->Temp_SetPoint);
		SYSTEM_LOG(str);
	}

	//Set ADC Quality
	error = Picam_SetParameterIntegerValue(camera->hcam,
					PicamParameter_AdcQuality,
					camera->ADC_Quality);
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		sprintf(str,"ADC Quality = %d",camera->ADC_Quality);
		SYSTEM_LOG(str);
	}

	//Check to see if the ADC speed setting is with range for the given quality.
	//Low noise = 1.0 or .1 MHz
	if(camera->ADC_Quality == 1){
		//Check ADC Read out Speed
		if(camera->ADC_Speed > 1.0){
			camera->ADC_Speed = 1.0;
			sprintf(str,"Warning: ADC selected readout speed not availible for ADC quality.ADC readout speed set to default for ADC quality.");
			SYSTEM_LOG(str);
		}
	}
	//Set ADC Read Out Speed
	error = Picam_SetParameterFloatingPointValue(camera->hcam,
					PicamParameter_AdcSpeed,
					camera->ADC_Speed);
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		sprintf(str,"ADC Read Out Speed = %.2f [MHz]",camera->ADC_Speed);
		SYSTEM_LOG(str);
	}

	//Set Read Out Control Mode
	error = Picam_SetParameterIntegerValue(camera->hcam,
					PicamParameter_ReadoutControlMode,
					camera->ReadOutCtrlMode);
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		sprintf(str,"Read Out Control Mode = %d",camera->ReadOutCtrlMode);
		SYSTEM_LOG(str);
	}


	//Set Shutter Timing Mode
	error = Picam_SetParameterIntegerValue(camera->hcam,
                                        PicamParameter_ShutterTimingMode,
                                        camera->ShutterTimingMode);
        if(error != PicamError_None){
                SafeExit(camera,error);
                return(FALSE);
        }else{
                sprintf(str,"Shutter Timing Mode = %d",camera->ShutterTimingMode);
                SYSTEM_LOG(str);
        }

	//Set Output Signal
	error = Picam_SetParameterIntegerValue(camera->hcam,
					PicamParameter_OutputSignal,
					camera->Output_Signal);
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		sprintf(str,"Output Signal = %d",camera->Output_Signal);
		SYSTEM_LOG(str);
	}

	//Set Invert Output Signal
	error = Picam_SetParameterIntegerValue(camera->hcam,
					PicamParameter_InvertOutputSignal,
					camera->Invert_Output);
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		sprintf(str,"Invert Output Signal = %d",camera->Invert_Output);
		SYSTEM_LOG(str);
	}

	//Set Kinetic Window Height
	error = Picam_SetParameterIntegerValue(camera->hcam,
					PicamParameter_KineticsWindowHeight,
					50);
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		sprintf(str,"Kinetic Window Height = %d",50);
		SYSTEM_LOG(str);
	}


	/*Princeton Instruments Region of Interest (ROI) */
	/* Get dimensional constraints */
	error = Picam_GetParameterRoisConstraint(camera->hcam,
							PicamParameter_Rois,
							PicamConstraintCategory_Required,
							&constraint);
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		//Get width and height from constraints
		totalWidth  = (piint)constraint->width_constraint.maximum;
		totalHeight = (piint)constraint->height_constraint.maximum;

		//sprintf(str,"Total Width = %d and Total Height = %d\n",totalWidth,totalHeight);
		//SYSTEM_LOG(str);
		//Clean up constraints after using constraints
		Picam_DestroyRoisConstraints(constraint);
	}

	/* Get the orinal ROI */
	error = Picam_GetParameterRoisValue(camera->hcam,
							PicamParameter_Rois,
							&(camera->region));
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		if (camera->region->roi_count == 1){

			//printf("Original Height = %d\n",camera->region->roi_array[0].height);
			//printf("Original Width  = %d\n",camera->region->roi_array[0].width);

			camera->region->roi_array[0].height	= totalHeight;
			camera->region->roi_array[0].width	= totalWidth;

			camera->ccd_height = totalHeight;
			camera->ccd_width = totalWidth;

			sprintf(str,"Region Height = %d and Region Width = %d",camera->region->roi_array[0].height,
													camera->region->roi_array[0].width);
			/*
			SYSTEM_LOG(str);
			sprintf(str,"Column Cord = %d",camera->region->roi_array[0].x);
			SYSTEM_LOG(str);
			sprintf(str,"Row Cord = %d",camera->region->roi_array[0].y);
			SYSTEM_LOG(str);
			*/

		}else{
			SYSTEM_LOG("No Region of Interest Found");
			SafeExit(camera,error);
			return(FALSE);
		}
	}
	/* Set the region of interest */
	error = Picam_SetParameterRoisValue(camera->hcam,
							PicamParameter_Rois,
							camera->region);
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		sprintf(str,"Region of Interest Set");
		SYSTEM_LOG(str);
	}

	// - show that the modified parameters need to be applied to hardware
    Picam_AreParametersCommitted(camera->hcam,&committed);
    if( committed ){
        SYSTEM_LOG("Parameters have not changed");
    }else{
        SYSTEM_LOG("Parameters have been modified");
    }

    // - apply the changes to hardware
    SYSTEM_LOG("Commit to hardware...  ");
    error = Picam_CommitParameters(camera->hcam,
            	&failed_parameters,
            	&failed_parameters_count );
    if( failed_parameters_count > 0 ) {
        SYSTEM_LOG("The following parameters are invalid:");
        for( piint i = 0; i < failed_parameters_count; ++i ) {
        	sprintf(str,"Failed Parameters = %d\n",failed_parameters[i]);
        	SYSTEM_LOG(str);
        }
    }

    // - free picam-allocated resources
    Picam_DestroyParameters( failed_parameters );
    //Picam_DestroyRois( camera->region );

	return(TRUE);
}

/*
 * Configure Dynamic Paremeters For Each Filter Slot
 * Parameters:
 * Exposure Time
 * EM Gain
 * CCD Gain
 * ROIS Binning
 */
int Configure_Dynamic(struct Camera *camera){
	PicamError error;
	char str[MAX_STRING_LEN]  = {0};
	pibln committed;
	const PicamParameter* failed_parameters;
	piint failed_parameters_count;
	const PicamRoisConstraint *constraint;

	SYSTEM_LOG("==================================");
	SYSTEM_LOG("Configuring Dynamic Parameters");
	SYSTEM_LOG("==================================");

	//Set Exposure Time
	error = Picam_SetParameterFloatingPointValue(camera->hcam,
					PicamParameter_ExposureTime,
					camera->ExposureTime);
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		sprintf(str,"Set Exposure Time = %.2f [Seconds]",camera->ExposureTime);
		SYSTEM_LOG(str);
	}

	//Set ADC Analog Gain
	error = Picam_SetParameterIntegerValue(camera->hcam,
					PicamParameter_AdcAnalogGain,
					camera->CCD_Gain);
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		sprintf(str,"Set CCD Gain = %d",camera->CCD_Gain);
		SYSTEM_LOG(str);
	}

	//Set EM Gain
	error = Picam_SetParameterIntegerValue(camera->hcam,
					PicamParameter_AdcEMGain,
					camera->EM_Gain);
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		sprintf(str,"Set EM Gain = %d",camera->EM_Gain);
		SYSTEM_LOG(str);
	}


	/* The vertical and horizontal binning */
	error = Picam_GetParameterRoisValue(camera->hcam,
							PicamParameter_Rois,
							&(camera->region));
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		if (camera->region->roi_count == 1){

			camera->region->roi_array[0].x_binning	= camera->x_bin;
			camera->region->roi_array[0].y_binning	= camera->y_bin;

			sprintf(str,"X Bin = %d and Y Bin = %d",camera->region->roi_array[0].x_binning,
													camera->region->roi_array[0].y_binning);
			SYSTEM_LOG(str);

		}else{
			SYSTEM_LOG("No Region of Interest Found");
			SafeExit(camera,error);
			return(FALSE);
		}
	}

	/* Set the region of interest */
	error = Picam_SetParameterRoisValue(camera->hcam,
							PicamParameter_Rois,
							camera->region);
	if(error != PicamError_None){
		SafeExit(camera,error);
		return(FALSE);
	}else{
		sprintf(str,"ROIS Set");
		SYSTEM_LOG(str);
	}



	// - show that the modified parameters need to be applied to hardware
    Picam_AreParametersCommitted(camera->hcam,&committed);
    if( committed ){
        SYSTEM_LOG("Parameters have not changed");
    }else{
        SYSTEM_LOG("Parameters have been modified");
    }

    // - apply the changes to hardware
    SYSTEM_LOG("Commit to hardware...  ");
    error = Picam_CommitParameters(camera->hcam,
            	&failed_parameters,
            	&failed_parameters_count );
    if( failed_parameters_count > 0 ) {
        SYSTEM_LOG("The following parameters are invalid:");
        for( piint i = 0; i < failed_parameters_count; ++i ) {
        	sprintf(str,"Failed Parameters = %d\n",failed_parameters[i]);
        	SYSTEM_LOG(str);
        }
    }

    // - free picam-allocated resources
    Picam_DestroyParameters( failed_parameters );
    //Picam_DestroyRois( camera->region );


	return(TRUE);
}

/*
 * Acquire Data
 */
int Acquire_Data(struct Camera *camera){
	PicamError error_str;
	pibln error;
	piint num_frames = 1;
	PicamAcquisitionErrorsMask errors;
	char str[MAX_STRING_LEN]  = {0};
	const pichar* string;

	//piint readoutstride = 0;
	//piint frame_size;


    for( piint i = 0; i < num_frames; i++ ) {
    	Picam_GetParameterIntegerValue(camera->hcam, PicamParameter_ReadoutStride, &(camera->readoutstride));
    	Picam_GetParameterIntegerValue(camera->hcam, PicamParameter_FrameSize, &(camera->frame_size));
        error_str = Picam_Acquire(camera->hcam, num_frames, NO_TIMEOUT, &(camera->data), &errors);
        if(error_str){
        	sprintf(str,"Error: Picam_Acquire return value: %d",error_str);
        	SYSTEM_LOG(str);
        	Picam_GetEnumerationString(PicamEnumeratedType_Error, error_str, &string);
        	sprintf(str,"Error: Camera Picam_Acquire Return String: %s",string);
        	SYSTEM_LOG(str);
        	Picam_DestroyString(string);
            sprintf(str,"Error: Camera only collected %d frames\n",(piint)camera->data.readout_count);
			SYSTEM_LOG(str);
			Picam_GetEnumerationString(PicamEnumeratedType_Error, errors, &string);
			sprintf(str,"Error: Camera Acquire error = %s",string);
			SYSTEM_LOG(str);
			Picam_DestroyString(string);
			Picam_DestroyHandles(camera->hcam);
			SYSTEM_LOG("Picam_DestroyHandles: Destroyed Camera Memory");
			return(FALSE);
        }
	}

	Picam_GetEnumerationString(PicamEnumeratedType_Error, errors, &string);
	sprintf(str,"Camera Acquire Error = %s",string);
	SYSTEM_LOG(str);
	Picam_DestroyString(string);

	//Save raw data to a static file which will be converted over to a readable image file later
	error = SaveRaw((pibyte*)camera->data.initial_readout,1,camera->readoutstride,camera->frame_size);
	if(error != TRUE){
		SYSTEM_LOG("SaveRaw(): Did not execute save raw image function. ERROR");
		return(FALSE);
	}

	return(TRUE);
}





/*
 * Save data to raw format.
 */
int SaveRaw( pibyte* buf, piint numframes, piint framelength, piint frame_size) {
    FILE *pFile;

    pFile = fopen(RAW_FILE,"wb");
	if( pFile ){
		if( !fwrite(buf, 1, (numframes*framelength), pFile ) ){
			SYSTEM_LOG("SaveRaw(): Data file not saved");
			return(FALSE);
		}
		fclose( pFile );
	}else{
		SYSTEM_LOG("SaveRaw(): Data file not saved");
		return(FALSE);
	}

	return(TRUE);
}


/*
 *
 */
void ReadTemperature(struct Camera *camera) {
    PicamError error;
    const pichar* string;


    // - read temperature
    printf("Read sensor temperature: ");
    error = Picam_ReadParameterFloatingPointValue(camera->hcam,PicamParameter_SensorTemperatureReading,&(camera->Temperature));
    //PrintError( error );
    if( error == PicamError_None )
    {
        printf("Temperature is %lf degrees C\n",camera->Temperature);
    }

}
