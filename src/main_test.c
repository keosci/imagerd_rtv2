

/* System Library Includes */
#include <errno.h>
#include <malloc.h>
#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>


#include "definitions.h"
#include "file_io.h"
#include "picam.h"
#include "pro_em.h"
#include "log.h"
#include "lib_sched.h"

int to_seconds(int time);
int to_nanoseconds(int time);
void Delete_Containers(struct Camera *camera,
            struct station_info *station_data,
            struct filter_info *filter_data,
            struct schedule_info *schedule_data,
            struct emphemeris *emph_data,
            struct schedule_slot_info *schedule_slot,
            struct image_info *image_data,
            struct detector_info *detector_data);


/*
 * Main
 */

int main(int argc, char *argv[]) {
	char str[MAX_STRING_LEN]  = {0};
	char *camera_type[MAX_LINE_LEN+1] = {0};
  int i=0, ret=0,FW_slots=0, Slot_Num=0;
  int first_instant=1;
  int sun_present, moon_present;
  int old_sun_status, old_moon_status;
  int sun_below_horizon = FALSE;
  int moon_below_horizon = 0;
  int camera_init = FALSE;
  char buffer[255] = {0};
  int j = 0, imagerd_pid=0;
	int dark_flag=FALSE;

  Print_Log_Header();


	//Container for station information
  struct station_info *station_data = NULL;
  station_data = malloc(sizeof(struct station_info));
  if (station_data == NULL) {
    SYSTEM_LOG("Failed to allocate memory\n");
    exit(FALSE);
  }

  //Read in station info 
  Read_Station_Settings(STATIONS_CONF_FILE, station_data);

  //Print Station Settings
  Print_Log_Station_Settings(station_data);

  //Container for generic camera structure
  struct Camera *camera = NULL;
  camera = malloc(sizeof(struct Camera));
  if (camera == NULL) {
    SYSTEM_LOG("Failed to allocate memory\n");
    exit(FALSE);
  }

  if(Read_Camera_Settings(IMAGER_CONF_FILE, camera)!=TRUE){
    SYSTEM_LOG("Read_Camera_Settings(): Failed to read in imager conf file -- Exiting");
    exit(FALSE);
  }

  Print_Camera_Settings(camera);

  struct filter_info *filter_data = NULL;

  FW_slots = Read_FW_Slots(IMAGER_CONF_FILE);
  if(FW_slots == 0){
    SYSTEM_LOG("Failed to extract filter information ***EXITING***\n");
    exit(FALSE);
  }
  //sprintf(str,"Number of Filter Slots: %d",FW_slots);
  //SYSTEM_LOG(str);


  //Create memory space for filter struct
  filter_data = malloc(FW_slots*(sizeof(struct filter_info)));
  if (filter_data == NULL) {
    SYSTEM_LOG("Failed to allocate memory for filter data\n");
    exit(FALSE);
  }

  //Read and store filter setting and print to log
  if(Read_Filter_Settings(IMAGER_CONF_FILE, filter_data, FW_slots)!=TRUE){
		SYSTEM_LOG("Read_Filter_Settings():Failed to read filter settings -- Exiting");
    exit(FALSE);
	}

  Print_Filter_Settings(filter_data, FW_slots);

	//Schedule and emphemeris information storage section
  //Create memory space for static schedule info
  struct schedule_info *schedule_data = NULL;
  schedule_data = malloc(sizeof(struct schedule_info));
  if (schedule_data == NULL) {
    SYSTEM_LOG("Failed to allocate memory for schedule_info\n");
    exit(FALSE);
  }

  //Create memory for emphemeris info structure
  struct emphemeris *emph_data = NULL;
  emph_data = malloc(sizeof(struct emphemeris));
  if (emph_data == NULL) {
    SYSTEM_LOG("Failed to allocate memory for ephemeris\n");
    exit(FALSE);
  }

  Slot_Num = Read_Sch_Slots(SCH_CONF_FILE);
	if(Slot_Num == 0 || Slot_Num == FALSE){
		SYSTEM_LOG("Read_Sch_Slots(): Failed to read number of schedule slots --Exiting");
		exit(FALSE);
	}

  //sprintf(str,"Number of Schedule Slots: %d",Slot_Num);
  //SYSTEM_LOG(str);

  //Create pointer to schedule_slot info but fill memory while reading in file
  struct schedule_slot_info *schedule_slot = NULL;
  schedule_slot = malloc(Slot_Num*(sizeof(struct schedule_slot_info)));
  if (schedule_slot == NULL){
    SYSTEM_LOG("Failed to allocate memory for schedule slot info --Exiting");
    exit(FALSE);
  }
  //Set the number of slots into the schedule data holder */
  schedule_data->num_slots = Slot_Num;

  if(Read_Sched_Settings(SCH_CONF_FILE,schedule_slot,schedule_data,emph_data,Slot_Num)!=Slot_Num){
    SYSTEM_LOG("Read_Sched_Settings(): Failed to read in all schedule slots --Exiting");
    exit(FALSE);
  }

  Print_Sch_Settings(schedule_slot, schedule_data, emph_data, Slot_Num, camera_type);

  
  if(!Check_Schedule_Settings(schedule_slot,schedule_data,emph_data,Slot_Num)){
    SYSTEM_LOG("Schedule Settings Invalid **Exiting**\n");
    exit(FALSE);
  }
  

  //Create pointer to image_info struct and generate proper memory storage
  struct image_info *image_data = NULL;
  image_data = malloc(sizeof(struct image_info));
  if (image_data == NULL) {
    SYSTEM_LOG("Failed to allocate memory for image info.");
    exit(FALSE);
  }
  //Create pointer to detector info struct and generate proper memory storage
  struct detector_info *detector_data = NULL;
  detector_data = malloc(sizeof(struct detector_info));
  if (detector_data == NULL) {
    SYSTEM_LOG("Failed to allocate memory for detector info.");
    exit(FALSE);
  }

  //Initialize and open camera
  SYSTEM_LOG("Initializing Pro-EM Camera.\n");
  if(Initialize() != TRUE){
    SYSTEM_LOG("Initialize Camera: Failed to initialize Camera -- EXITING");
    if(CloseCamera(camera) != TRUE){
      SYSTEM_LOG("Could Not Uninitialize Library and Close Camera -- EXITING");
    }
    Delete_Containers(camera,station_data,filter_data,schedule_data,
          emph_data,schedule_slot,image_data,detector_data);
    exit(FALSE);
  }
  
  if(OpenCamera(camera) != TRUE){
    SYSTEM_LOG("Open Camera: Failed to Open Camera -- EXITING.\n");
    if(CloseCamera(camera) != TRUE){
      SYSTEM_LOG("Could Not Uninitialize Library and Close Camera -- EXITING");
    }
    Delete_Containers(camera,station_data,filter_data,schedule_data,
          emph_data,schedule_slot,image_data,detector_data);
    exit(FALSE);
  }

  if(Configure_Static(camera) != TRUE){
    SYSTEM_LOG("Configure Static Parameters: Failed to configure static parameters -- EXITING");
    if(CloseCamera(camera) != TRUE){
        SYSTEM_LOG("Could Not Uninitialize Library and Close Camera -- EXITING");
      }
    Delete_Containers(camera,station_data,filter_data,schedule_data,
          emph_data,schedule_slot,image_data,detector_data);
    exit(FALSE);
  }

  Capture_Image(station_data,camera,filter_data,schedule_data,emph_data,
                schedule_slot, detector_data, image_data,0,0,schedule_data->num_slots);



}

/*
 * Function to take milliseconds and return seconds
 */
int to_seconds(int time){
  div_t divresult;
  divresult = div (time,1000);
  return(divresult.quot);
}

/*
 * Function to take milliseocnds and return nanoseconds
 */
int to_nanoseconds(int time){
  div_t divresult;
  divresult = div (time,1000);
  return(divresult.rem*1000000);
}

/*
 * Free memory.
 */
void Delete_Containers(struct Camera *camera,
            struct station_info *station_data,
            struct filter_info *filter_data,
            struct schedule_info *schedule_data,
            struct emphemeris *emph_data,
            struct schedule_slot_info *schedule_slot,
            struct image_info *image_data,
            struct detector_info *detector_data){
  
  //TODO Add free ProEM mem structure
  //if (camera_data_pmax)
  if(camera)
    free(camera);
  if (station_data)
    free(station_data);
  if (filter_data)
    free(filter_data);
  if (schedule_data)
    free(schedule_data);
  if (emph_data)
    free(emph_data);
  if (schedule_slot)
    free(schedule_slot);
  if (image_data)
    free(image_data);
  if (detector_data)
    free(detector_data);
  
  SYSTEM_LOG("Freed Structure Memory"); 
  
}