/*
 *
 *  imagerd_rt_start.c
 * 	
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "definitions.h"
#include "file_io.h"
#include "imagerd_rt.h"

/**
 * Main
 */
int main(int argc, char *argv[]) {

 	/* Update the imaging stop status file with start flag. */
	if (Write_Imaging_Stop_Status(START_IMAGING) != TRUE) {
    	fprintf(stderr,"Failed to update the imaging stop status. Try again.\n");
    	return(FALSE);
	}
	return(TRUE);
}
