CC=gcc
INCLUDE=include/.
INSTDIR=.
LIBS=-lm -lrt -lpng -lz -ldl -lpicam
CFLAGS=-O -w -D_GNU_SOURCE -pthread -std=gnu99


all: main_test


main_test: main_test.o file_io.o pro_em.o log.o lib_sched.o lib_capture.o
	$(CC) -o main_test main_test.o file_io.o pro_em.o log.o lib_sched.o lib_capture.o -g -DLINUX $(LIBS)

main_test.o: main_test.c
	$(CC) -I$(INCLUDE) $(CFLAGS) -c main_test.c 

file_io.o: file_io.c
	$(CC) -I$(INCLUDE) $(CFLAGS) -c file_io.c

pro_em.o: pro_em.c
	$(CC) -I$(INCLUDE) $(CFLAGS) -c pro_em.c

log.o: log.c
	$(CC) -I$(INCLUDE) $(CFLAGS) -c log.c

lib_sched.o: lib_sched.c
	$(CC) -I$(INCLUDE) $(CFLAGS) -c lib_sched.c

lib_capture.o: lib_capture.c
	$(CC) -I$(INCLUDE) $(CFLAGS) -c lib_capture.c

clean:
	-rm *.o
	-rm main_test

run:
	-./main_test