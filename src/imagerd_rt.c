/*
 *  imagerd_rt.c
 *
 *	This file is part of imagerd_rt.
 *
 *  imagerd_rt is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  imagerd_rt is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with imagerd_rt.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Auther: Jarrett Little
 *
 */

#include "imagerd_rt.h"
#define FW_Shutter_Ctrl
/* Global Variables */
volatile int imaging_should_stop = FALSE;
volatile int imaging_should_cont = TRUE;
volatile int camera_connected = FALSE;
volatile sig_atomic_t imaging_bound = FALSE;

/*
 * Main
 */

int main(int argc, char *argv[]) {
  /* Signal Handling Structure */
  struct sigaction act,old_act,sa;
  struct sigevent sev;
  sigset_t mask;

  /* Absolute timer structures */
  timer_t timerid;
  struct itimerspec its;
  int clock_id = CLOCK_REALTIME;
  int abs = TIMER_ABSTIME;

  /* Absolute timing structures */
  struct tm *time_now;
  time_t mytimenow;
  struct timeval tv;

  char *camera_type[MAX_LINE_LEN+1] = {0};
  int i=0, ret=0,FW_slots=0, Slot_Num=0;
  int first_instant=1;
  int sun_present, moon_present;
  int old_sun_status, old_moon_status;
  int sun_below_horizon = FALSE;
  int moon_below_horizon = 0;
  int camera_init = FALSE;
  //Flag for sensor head cooled or not
  int cooled = FALSE;
  char buffer[255] = {0};
  int j = 0, imagerd_pid=0;
	int dark_flag=FALSE;

	/* Timing File */
  struct stat sb;

  /** Section 1  -- Filling data structures and printing to system log */

  /* Container for station information */
  struct station_info *station_data = NULL;
  station_data = malloc(sizeof(struct station_info));
  if (station_data == NULL) {
    SYSTEM_LOG("Failed to allocate memory\n");
    exit(FALSE);
  }

  /* Write header information to the program log */
  Print_Log_Header();

  // Create symbolic link directory /dev/shm/imagerd_rt
  if(Check_Directory_And_Create("/dev/shm/imagerd_rt/") != TRUE){
    SYSTEM_LOG("Failed to create directory /dev/shm/imager_rt for symbolic link");
      return(FALSE);
  }else{
    SYSTEM_LOG("Created /dev/shm/imagerd_rt/");
  }

	/* First get the pid of imagerd currently running then write it to a file */

  imagerd_pid = getpid();
  FILE *pid_file;
  pid_file = fopen(PID_FILE,"w+");
  if(pid_file == NULL){
     SYSTEM_LOG("Could not open pid file for writing. --EXITING--");
     return(FALSE);
  }
  fprintf(pid_file,"%d",imagerd_pid);
  fclose(pid_file);
  fprintf(stderr,"Imagerd PID = %d\n",imagerd_pid);

	//Write start_imaging = 11 to the stop file
	if(Write_Imaging_Stop_Status(START_IMAGING) != TRUE) {
		SYSTEM_LOG("Failed to update the imaging stop status. EXITING\n");
		return(FALSE);
	}

	SYSTEM_LOG("Wrote START_IMAGING to mode file");


  // Read in station info
  Read_Station_Settings(STATIONS_CONF_FILE, station_data);
  // Print Station Settings to Log
  Print_Log_Station_Settings(station_data);

  //Container for generic camera structure
  struct Camera *camera = NULL;
  camera = malloc(sizeof(struct Camera));
  if (camera == NULL) {
    SYSTEM_LOG("Failed to allocate memory\n");
    exit(FALSE);
  }

  if(Read_Camera_Settings(IMAGER_CONF_FILE, camera)!=TRUE){
    SYSTEM_LOG("Read_Camera_Settings(): Failed to read in imager conf file -- Exiting");
    exit(FALSE);
  }

  Print_Camera_Settings(camera);

  //Structure for holding filter data
  struct filter_info *filter_data = NULL;
	//Determine how many filter slots are
  FW_slots = Read_FW_Slots(IMAGER_CONF_FILE);
  if(FW_slots == 0){
    SYSTEM_LOG("Failed to extract filter information ***EXITING***\n");
    exit(FALSE);
  }

  //Create memory space for filter struct
  filter_data = malloc(FW_slots*(sizeof(struct filter_info)));
  if (filter_data == NULL) {
    SYSTEM_LOG("Failed to allocate memory for filter data\n");
    exit(FALSE);
  }

  //Read and store filter setting and print to log
  if(Read_Filter_Settings(IMAGER_CONF_FILE, filter_data, FW_slots)!=TRUE){
		SYSTEM_LOG("Read_Filter_Settings():Failed to read filter settings -- Exiting");
    exit(FALSE);
	}
 	Print_Filter_Settings(filter_data, FW_slots);


 	//Schedule and emphemeris information storage section

  //Create memory space for static schedule info
  struct schedule_info *schedule_data = NULL;
  schedule_data = malloc(sizeof(struct schedule_info));
  if (schedule_data == NULL) {
    SYSTEM_LOG("Failed to allocate memory for schedule_info\n");
    exit(FALSE);
  }


  //Create memory for emphemeris info structure
  struct emphemeris *emph_data = NULL;
  emph_data = malloc(sizeof(struct emphemeris));
  if (emph_data == NULL) {
    SYSTEM_LOG("Failed to allocate memory for ephemeris\n");
    exit(FALSE);
  }

  Slot_Num = Read_Sch_Slots(SCH_CONF_FILE);
	if(Slot_Num == 0 || Slot_Num == FALSE){
		SYSTEM_LOG("Read_Sch_Slots(): Failed to read number of schedule slots --Exiting");
		exit(FALSE);
	}

  //Create pointer to schedule_slot info but fill memory while reading in file
  struct schedule_slot_info *schedule_slot = NULL;
  schedule_slot = malloc(Slot_Num*(sizeof(struct schedule_slot_info)));
  if (schedule_slot == NULL){
    SYSTEM_LOG("Failed to allocate memory for schedule slot info --Exiting");
    exit(FALSE);
	 }
	//Set the number of slots into the schedule data holder
	schedule_data->num_slots = Slot_Num;

	if(Read_Sched_Settings(SCH_CONF_FILE,schedule_slot,schedule_data,emph_data,Slot_Num)!=Slot_Num){
		SYSTEM_LOG("Read_Sched_Settings(): Failed to read in all schedule slots --Exiting");
		exit(FALSE);
	}
	Print_Sch_Settings(schedule_slot, schedule_data, emph_data, Slot_Num, camera_type);
  if(!Check_Schedule_Settings(schedule_slot,schedule_data,emph_data,Slot_Num)){
    SYSTEM_LOG("Schedule Settings Invalid **Exiting**\n");
    exit(FALSE);
  }

	//Create pointer to image_info struct and generate proper memory storage
 	struct image_info *image_data = NULL;
  image_data = malloc(sizeof(struct image_info));
  if (image_data == NULL) {
    SYSTEM_LOG("Failed to allocate memory for image info.");
    exit(FALSE);
  }
	//Create pointer to detector info struct and generate proper memory storage
	struct detector_info *detector_data = NULL;
	detector_data = malloc(sizeof(struct detector_info));
	if (detector_data == NULL) {
		SYSTEM_LOG("Failed to allocate memory for detector info.");
		exit(FALSE);
	}

  /** End of Section 1 -- all data from config files have been read in and stored */


  /**
  *================================================================================
  *	Section 2 -- Initialize Camera Head and input camera settings
  *  	This section also checks that all camera parameters are correct and inbound
  *================================================================================
  */

  //Initialize and open camera
  SYSTEM_LOG("Initializing Camera.\n");
	if(Init() != TRUE){
    SYSTEM_LOG("Initialize Camera: Failed to initialize Camera -- EXITING");
	  if(CloseCamera(camera) != TRUE){
    	SYSTEM_LOG("Could Not Uninitialize Library and Close Camera -- EXITING");
    }
		Delete_Containers(camera,station_data,filter_data,schedule_data,
					emph_data,schedule_slot,image_data,detector_data);
		exit(FALSE);
  }

  if(OpenCamera(camera) != TRUE){
    SYSTEM_LOG("Open Camera: Failed to Open Camera -- EXITING.\n");
		if(CloseCamera(camera) != TRUE){
    	SYSTEM_LOG("Could Not Uninitialize Library and Close Camera -- EXITING");
    }
		Delete_Containers(camera,station_data,filter_data,schedule_data,
					emph_data,schedule_slot,image_data,detector_data);
		exit(FALSE);
  }

	camera_init = TRUE;

  //Check and print out current camera parameters
  struct Current_Params *cur_params = NULL;
  cur_params = malloc(sizeof(struct Current_Params));
  if(GetParamInfo(camera, cur_params) != TRUE){
    SYSTEM_LOG("GetParamInfo(): Error getting current camera parameters");
    if(CloseCamera(camera) != TRUE){
      SYSTEM_LOG("Could Not Uninitialize Library and Close Camera -- EXITING");
    }
    Delete_Containers(camera,station_data,filter_data,schedule_data,
          emph_data,schedule_slot,image_data,detector_data);
    exit(FALSE);
  }
  free(cur_params);

  //Configure Camera with static parameters
	if(Configure_Static(camera) != TRUE){
		SYSTEM_LOG("Configure Static Parameters: Failed to configure static parameters -- EXITING");
		if(CloseCamera(camera) != TRUE){
    		SYSTEM_LOG("Could Not Uninitialize Library and Close Camera -- EXITING");
    	}
		Delete_Containers(camera,station_data,filter_data,schedule_data,
					emph_data,schedule_slot,image_data,detector_data);
		exit(FALSE);
	}
	PrintParameters(camera);

  //Establish comm with ASC and sent filter wheel to home position
	/*
  if(!Go_Home()){
   	SYSTEM_LOG("ASC ERROR -Could not find home- **Exiting**\n");
		Delete_Containers(camera_data_ixon,camera_data_pmax,camera_data_iccd,station_data,
						filter_data,schedule_data,emph_data,schedule_slot,image_data,detector_data);
    exit(FALSE);
  }else{
    SYSTEM_LOG("ASC successfully found home position\n");
  }
  */

  //===============================================================================
  //Close shutter and sun shade for initial operation and if theres a filter wheel Move
  //to position 1
#ifndef DEBUG_SERIAL

#ifndef FW_Shutter_Ctrl
  //Manually Close Shutter
  if(!Close_Shutter()){
    SYSTEM_LOG("Close_Shutter() - Could not close shutter - **Exiting**");
		Delete_Containers(camera,station_data,filter_data,schedule_data,
					emph_data,schedule_slot,image_data,detector_data);
    exit(FALSE);
  }else{
    SYSTEM_LOG("Close_Shutter(): Status: CLOSED");
  }
  //Set CCD External Shutter Control to Perm Closed
  if(!Set_Shutter(SHUTTER_MODE_CLOSE,camera->Shutter_Close_Delay, camera->Shutter_Open_Delay)){
    SYSTEM_LOG("Set_Shutter() - Could not set CCD external shutter function - **Exiting**");
    Delete_Containers(camera,station_data,filter_data,schedule_data,
          emph_data,schedule_slot,image_data,detector_data);
    exit(FALSE);
  }else{
    SYSTEM_LOG("Set_Shutter(): Status: CLOSED");
  }
  #ifdef SUN_SHADE
  //Initaize and then close the sun shade
  //TODO -> Set sunshade to manual mode
  if(!Close_SunShade()){
    SYSTEM_LOG("Close_SunShade() - Could not close sun shade - **Exiting**");
    Delete_Containers(camera,station_data,filter_data,schedule_data,
          emph_data,schedule_slot,image_data,detector_data);
    exit(FALSE);
  }else{
    SYSTEM_LOG("Close_SunShade(): Status: CLOSED");
  }
  #endif
#endif
//Shutter is controlled via the filterwheel smartmotor
#ifdef FW_Shutter_Ctrl
  //Move the filterwheel to position 1
  if(!FW_Control(FWCTRL,1)){
    SYSTEM_LOG("FW_Control() - Could not move filterwheel to position 1  - **Exiting**");
		Delete_Containers(camera,station_data,filter_data,schedule_data,
					emph_data,schedule_slot,image_data,detector_data);
    exit(FALSE);
  }else{
    SYSTEM_LOG("FW_Control(FWCTRL,1): Status: Position 1");
  }
  //Make sure the shutter is CLOSED
  if(!FW_Control(SHUTCTRL,CLOSE)) {
    SYSTEM_LOG("FW_Control() - Could not close shutter  - **Exiting**");
		Delete_Containers(camera,station_data,filter_data,schedule_data,
					emph_data,schedule_slot,image_data,detector_data);
    exit(FALSE);
  }else{
    SYSTEM_LOG("FW_Control(SHUTCTRL,CLOSE): Status: CLOSED");
  }
#endif
#endif
  //===============================================================================

 	//End of Section 3 -- Communication to ASC has been successfull


  /**
  *================================================================================
  *	Section 4 -- Set up signals and fork off as per daemon specifications
  *================================================================================
  */

  //Prepare a new signal handler
  act.sa_handler = signal_stop_imaging;
  sigemptyset(&act.sa_mask);
  act.sa_flags = 0;
  if(sigaction(SIGTERM,&act,&old_act) != 0){
    SYSTEM_LOG("Failed to prepare signal handler. Aborting...");
		Delete_Containers(camera,station_data,filter_data,schedule_data,
					emph_data,schedule_slot,image_data,detector_data);
    exit(FALSE);
  }
 	// End of Section 4 -- Signals have been established


  /**
  *================================================================================
  *	Section 5 -- Initialize ephemeris with current imaging time
  *================================================================================
  */
  mytimenow =time(NULL);
  time_now =gmtime(&mytimenow);
  old_sun_status = f_sun_present(time_now,station_data,emph_data);
  old_moon_status = f_moon_present(time_now,station_data,emph_data);
 	// End of Section 5 -- Ephemeris values obtained


  /**
  *================================================================================
  *	Section 6 -- Set up timer and signal for schedule length bounds
  *================================================================================
  */

  SYSTEM_LOG("Establishing timer and handler for signal");
  // Signal set up
  sa.sa_flags = SA_SIGINFO;
  sigemptyset(&mask);
  sa.sa_sigaction = handler;
  sigemptyset(&sa.sa_mask);
  if (sigaction(SIGRTMAX-1, &sa, NULL) == -1){
		Delete_Containers(camera,station_data,filter_data,schedule_data,
					emph_data,schedule_slot,image_data,detector_data);
		errExit("sigaction");
	}
  // Timer set up
  sev.sigev_notify = SIGEV_SIGNAL;
  sev.sigev_signo = SIGRTMAX-1;
  sev.sigev_value.sival_ptr = &timerid;
  if (timer_create(clock_id, &sev, &timerid) == -1){
		Delete_Containers(camera,station_data,filter_data,schedule_data,
					emph_data,schedule_slot,image_data,detector_data);
		errExit("timer_create");
	}


 	/**
  *================================================================================
  *							Enter Main Loop
  *================================================================================
  */

  while(imaging_should_stop==FALSE) {
    gettimeofday(&tv, NULL);
    mytimenow = tv.tv_sec;
    time_now = gmtime(&mytimenow);
    strftime(buffer,sizeof(buffer),"%m-%d-%Y  %T.",time_now);

    //fprintf(stderr,"%s%ld Sec= %ld   MSec = %ld\n",buffer,tv.tv_usec,tv.tv_sec,tv.tv_usec/1000);
    //fiprintf(stderr,"Seconds = %d\n",time_now->tm_sec);

    Check_Imaging_Should_Stop_Status();
    //Check sun ephemeris and proceed with outcome
    sun_present = f_sun_present(time_now,station_data,emph_data);
		//fprintf(stderr,"Sun Angle = %d\n",sun_present);

		//Performed at the end of an imaging session. Take Dark images, shutdown camera, close shutter etc.
    if(sun_present == TRUE) {
	   	if(sun_present == TRUE) {
				if(sun_below_horizon == TRUE) {
					SYSTEM_LOG("Local Sunrise Detected");
					// 'Close' the system for the day
					SYSTEM_LOG("Closing Shutter and Sun Shade");
//====================================================================================
                #ifndef DEBUG_SERIAL
                  #ifndef FW_Shutter_Ctrl
                    //Close shutter
                    if(!Close_Shutter()){
                        SYSTEM_LOG("Close_Shutter() - Could not close shutter - **Exiting**");
                        Delete_Containers(camera,station_data,filter_data,schedule_data,
                                    emph_data,schedule_slot,image_data,detector_data);
                        Exit_Procedure(camera);
                        exit(FALSE);
                    }else{
                        SYSTEM_LOG("Close_Shutter(): Status: CLOSED");
                    }
                    //Set CCD External Shutter Control to Perm Closed
                    if(!Set_Shutter(SHUTTER_MODE_CLOSE,camera->Shutter_Close_Delay, camera->Shutter_Open_Delay)){
                      SYSTEM_LOG("Set_Shutter() - Could not set CCD external shutter function - **Exiting**");
                      Delete_Containers(camera,station_data,filter_data,schedule_data,
                                    emph_data,schedule_slot,image_data,detector_data);
                      exit(FALSE);
                    }else{
                      SYSTEM_LOG("Set_Shutter(): Status: CLOSED");
                    }
                    #ifdef SUN_SHADE
                    //Close sun shade
                    if(!Close_SunShade()){
                        SYSTEM_LOG("Close_SunShade() - Could not close sun shade - **Exiting**");
                        Delete_Containers(camera,station_data,filter_data,schedule_data,
                                    emph_data,schedule_slot,image_data,detector_data);
                        Exit_Procedure(camera);
                        exit(FALSE);
                    }else{
                        SYSTEM_LOG("Close_SunShade(): Status: CLOSED");
                    }
                    #endif
                  #endif
                  #ifdef FW_Shutter_Ctrl
                  //Make sure the shutter is CLOSED
                  if(!FW_Control(SHUTCTRL,CLOSE)) {
                    SYSTEM_LOG("FW_Control() - Could not close shutter  - **Exiting**");
                		Delete_Containers(camera,station_data,filter_data,schedule_data,
                					emph_data,schedule_slot,image_data,detector_data);
                    exit(FALSE);
                  }else{
                    SYSTEM_LOG("FW_Control(SHUTCTRL,CLOSE): Status: CLOSED");
                  }
		  //Set CCD External Shutter Control to Perm Closed
                  //Performing configure static function on camera, this will reset output signal to 4
                  if(Configure_Static(camera) != TRUE){
			              SYSTEM_LOG("Configure Static Parameters: Failed to configure static parameters -- EXITING");
			              if(CloseCamera(camera) != TRUE){
				              SYSTEM_LOG("Could Not Uninitialize Library and Close Camera -- EXITING");
			              }
			              Delete_Containers(camera,station_data,filter_data,schedule_data,
							                        emph_data,schedule_slot,image_data,detector_data);
			              exit(FALSE);
		              }  

		#endif
                #endif
//====================================================================================
					while(sun_below_horizon == TRUE) {
						gettimeofday(&tv, NULL);
						mytimenow = tv.tv_sec;
						time_now = gmtime(&mytimenow);
						if(time_now->tm_sec == 0 && fabs((tv.tv_usec/1000) - 5) < 5){
							//Take dark frames in the morning */
							//Caputer Dark Images
							SYSTEM_LOG("Capturing Dark Counts (Morning)");
							//fprintf(stderr,"Sec = %d and MSec = %ld\n",time_now->tm_sec,tv.tv_usec/1000);

                            //Capturing Dark Frames in morning
              if(Capture_Schedule_Slots(station_data,camera,filter_data,
                                        schedule_data,emph_data,schedule_slot,detector_data,
                                        image_data,Slot_Num,1) == FALSE) {
                SYSTEM_LOG("IMAGERD: Capture Slots Failed to Image Dark Frames-- Exiting\n");
                Delete_Containers(camera,station_data,filter_data,schedule_data,
                                  emph_data,schedule_slot,image_data,detector_data);
                //Exit Procedure aka close shutter and sunshade. Also closes camera.
                Exit_Procedure(camera);
                exit(FALSE);
              }

							/* Stop timers from activating during day */
							SYSTEM_LOG("Deactivating Timers");
							its.it_value.tv_sec = 0;
							its.it_value.tv_nsec = 0;
							its.it_interval.tv_sec = 0;
							its.it_interval.tv_nsec = 0;
							if (timer_settime(timerid, 0, &its, NULL) == -1) errExit("timer_settime");

							sun_below_horizon = FALSE;
						}
					}
					Exit_Procedure(camera);
          //Camera has been disallocated
          camera_init = FALSE;
          //CCD has been warmed back up.
          cooled = FALSE;
          SYSTEM_LOG("Closed Shutter, Captured Dark Frames, Reset FW, and Shutdown Camera\n");
				}
				/* Check imaging should stop status */
				Check_Imaging_Should_Stop_Status();
				/** Remove this sleep */
				/* Sleep for 10 seconds for power saving reasons */
				sleep(10);
				SYSTEM_LOG("TICK --  SYSTEM HEART BEAT");
			}
		} else if(imaging_should_stop==FALSE) {
			//TODO Check moon??
			if(sun_below_horizon == FALSE && imaging_should_stop==FALSE) {
				SYSTEM_LOG("LOCAL SUNSET DECTECTED");
				// If camera not open initialize or connect the camera head
				// Set camera flag to true
				if(camera_init != TRUE) {
					// Camera initialization and perform initial setup
          //Initialize and open camera
          SYSTEM_LOG("Reinitializing Camera.\n");
          if(Init() != TRUE){
            SYSTEM_LOG("Initialize Camera: Failed to initialize Camera -- EXITING");
              if(CloseCamera(camera) != TRUE){
                SYSTEM_LOG("Could Not Uninitialize Library and Close Camera -- EXITING");
              }
              Delete_Containers(camera,station_data,filter_data,schedule_data,
					                       emph_data,schedule_slot,image_data,detector_data);
              exit(FALSE);
          }

          if(OpenCamera(camera) != TRUE){
            SYSTEM_LOG("Open Camera: Failed to Open Camera -- EXITING.\n");
            if(CloseCamera(camera) != TRUE){
              SYSTEM_LOG("Could Not Uninitialize Library and Close Camera -- EXITING");
            }
		        Delete_Containers(camera,station_data,filter_data,schedule_data,
					                   emph_data,schedule_slot,image_data,detector_data);
		        exit(FALSE);
          }

          camera_init = TRUE;

          //Print out current camera parameters
          struct Current_Params *cur_params = NULL;
          cur_params = malloc(sizeof(struct Current_Params));
          if(GetParamInfo(camera, cur_params) != TRUE){
            SYSTEM_LOG("GetParamInfo(): Error getting current camera parameters");
            if(CloseCamera(camera) != TRUE){
              SYSTEM_LOG("Could Not Uninitialize Library and Close Camera -- EXITING");
            }
            Delete_Containers(camera,station_data,filter_data,schedule_data,
                              emph_data,schedule_slot,image_data,detector_data);
            exit(FALSE);
          }
          free(cur_params);

          //Configure Camera with static parameters
          if(Configure_Static(camera) != TRUE){
		        SYSTEM_LOG("Configure Static Parameters: Failed to configure static parameters -- EXITING");
		        if(CloseCamera(camera) != TRUE){
    		      SYSTEM_LOG("Could Not Uninitialize Library and Close Camera -- EXITING");
    	      }
		        Delete_Containers(camera,station_data,filter_data,schedule_data,
					                     emph_data,schedule_slot,image_data,detector_data);
		        exit(FALSE);
          }
        }

        //Check to see if the camera head is cooled or not
        //Start Cooler and wait to hit proper temperature.
        #ifndef DEBUG_NO_COOL
        if(cooled == FALSE && camera_init == TRUE){
          if(camera->cooling == COOLING_ON){
            SYSTEM_LOG("Starting camera cooling.");
            if(Set_Cooler_State(camera->cooling) != TRUE){
              SYSTEM_LOG("Failed to turn on the cooling.");
              imaging_should_stop = TRUE;
            }
            SYSTEM_LOG("Camera cooling has been initiated.");
            // Set the camera temperature
            if (Set_Temperature((int)camera->Temp_SetPoint) != TRUE) {
              SYSTEM_LOG("Failed to set the detector temperature.");
              imaging_should_stop = TRUE;
            }
            SYSTEM_LOG("CCD temperature set --> beginning to cool");
            //Wait for the camera to reach the target temperature
            if (Wait_Camera_Temperature((int)camera->Temp_SetPoint,FALSE) != TRUE) {
              SYSTEM_LOG("Failed to get camera temperature or the cooling is OFF.\n");
              imaging_should_stop = TRUE;
            }
            SYSTEM_LOG("Camera has reached  operating temperature");
          } else {
            SYSTEM_LOG("Camera cooling parameter set to OFF");
          }
        }
        cooled = TRUE;
        #endif

      #ifndef DEBUG_SERIAL
				//Move FW to starting filter position -> this will be from the schedule slot structure
				sprintf(buffer,"Setting filter wheel to starting position: Filter Number = %d",schedule_slot[0].filter_num);
				SYSTEM_LOG(buffer);
				if(FW_Control(FWCTRL,schedule_slot[0].filter_num)!=TRUE) {
					SYSTEM_LOG("Set_Filter(): Failed to set next filterwheel position.\n");
					// Exit Procedure aka close shutter etc
          Delete_Containers(camera,station_data,filter_data,schedule_data,
                             emph_data,schedule_slot,image_data,detector_data);
          Exit_Procedure(camera);
          exit(FALSE);
				}
				sprintf(buffer,"Setting Starting Filter Number = %d\n",schedule_slot[0].filter_num);
				SYSTEM_LOG(buffer);
      #endif
				//wait for a zero second boundary and capture dark counts
				while(sun_below_horizon == FALSE) {
					gettimeofday(&tv, NULL);
					mytimenow = tv.tv_sec;
					time_now = gmtime(&mytimenow);
					if(time_now->tm_sec == 0 && fabs((tv.tv_usec/1000) - 5) < 5 && dark_flag==FALSE){
						// Set timer for continues operations
						// Capture Dark Images -> Capture Dark images at beginning of night
						SYSTEM_LOG("Capturing Dark Counts (Night)");
            //Capturing Dark Frames in morning
            if(Capture_Schedule_Slots(station_data,camera,filter_data,
                                      schedule_data,emph_data,schedule_slot,detector_data,
                                      image_data,Slot_Num,1) == FALSE){
              SYSTEM_LOG("IMAGERD: Capture Slots Failed to Image Dark Frames-- Exiting\n");
              Delete_Containers(camera,station_data,filter_data,schedule_data,
                                emph_data,schedule_slot,image_data,detector_data);
              //Exit Procedure aka close shutter etc
              Exit_Procedure(camera);
              exit(FALSE);
            }

//===================================================================================
						//If successful open shutter and set sun_below_horizon to be equal to true
						//Open Shutter and Sun Shade
                    #ifndef DEBUG_SERIAL
                      #ifndef FW_Shutter_Ctrl

                        if(!Open_Shutter()) {
		                      SYSTEM_LOG("Open_Shutter(): ERROR - Could not open shutter - **Exiting**");
		                      Delete_Containers(camera,station_data,filter_data,schedule_data,
                                  emph_data,schedule_slot,image_data,detector_data);
		                      //Exit Procedure aka close shutter etc
		                      Exit_Procedure(camera);
		                      exit(FALSE);
	                      }else{
		                      SYSTEM_LOG("Open_Shutter(): Status: OPEN");
	                      }

                        //Set CCD External Shutter Mode
                        if(!Set_Shutter(camera->Shutter_Mode,camera->Shutter_Close_Delay,camera->Shutter_Open_Delay)){
                          SYSTEM_LOG("Set_Shutter() - Could not set CCD external shutter function - **Exiting**");
                          Delete_Containers(camera,station_data,filter_data,schedule_data,
                                        emph_data,schedule_slot,image_data,detector_data);
                          exit(FALSE);
                        }else{
                          //Shutter Mode
                          switch(camera->Shutter_Mode){
                            case 0: SYSTEM_LOG("Shutter Mode:               AUTO"); break;
                            case 1: SYSTEM_LOG("Shutter Mode:               OPEN"); break;
                            case 2: SYSTEM_LOG("Shutter Mode:               CLOSE"); break;
                            default: SYSTEM_LOG("Shutter Mode:               No Valid Shutter Mode Found"); break;
                          }
                        }
                        #ifdef SUN_SHADE
                        //Open Sun Shade
                        if(!Open_SunShade()) {
              	          SYSTEM_LOG("Open_SunShade(): ERROR - Could not open sun shade - **Exiting**");
              	          Delete_Containers(camera,station_data,filter_data,schedule_data,
                                  emph_data,schedule_slot,image_data,detector_data);
		                    //Exit Procedure aka close shutter etc
              	          Exit_Procedure(camera);
	      	                exit(FALSE);
                        }else{
              	            SYSTEM_LOG("Open_SunShade(): Status: OPEN");
                        }
                        #endif
                      #endif
                      #ifdef FW_Shutter_Ctrl
                      //Check to see if the camera->outputsignal is set to 2. If so leave shutter alone.
		      if(camera->Output_Signal !=2 && camera->Output_Signal !=8){		
                      	if(!FW_Control(SHUTCTRL,OPEN)) {
                        	SYSTEM_LOG("FW_Control() - Could not open shutter  - **Exiting**");
                    		Delete_Containers(camera,station_data,filter_data,schedule_data,
                    					emph_data,schedule_slot,image_data,detector_data);
                       		Exit_Procedure(camera);
                        	exit(FALSE);
                      	}else{
                        	SYSTEM_LOG("FW_Control(SHUTCTRL,OPEN): Status: OPEN");
                      	}
		      }else{
			//set outputmode
			SetOutputSignal(camera);
			sprintf(buffer,"Leaving shutter up to camera control via Output Signal: %d",camera->Output_Signal);
			SYSTEM_LOG(buffer);
		      }
                      #endif
                    #endif
//===================================================================================
						dark_flag = TRUE;
					}
					// Now wait for another zero boundary and set timers
					if(time_now->tm_sec == 0 && fabs((tv.tv_usec/1000) - 5) < 5 && dark_flag==TRUE) {
					// Set timer for continues operation
						SYSTEM_LOG("Setting RT Timer for Main Loop Scheduling");

						its.it_value.tv_sec = to_seconds(schedule_data->sched_len);
						its.it_value.tv_nsec = to_nanoseconds(schedule_data->sched_len);
						its.it_interval.tv_sec = to_seconds(schedule_data->sched_len);
						its.it_interval.tv_nsec = to_nanoseconds(schedule_data->sched_len);

						if (timer_settime(timerid, 0, &its, NULL) == -1) errExit("timer_settime");
						dark_flag = FALSE;
						sun_below_horizon = TRUE;
					}
				}
			}

			//Regular imaging starts here -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

			if(imaging_bound == TRUE && imaging_should_stop==FALSE && imaging_should_cont==TRUE){
				// Call capture image function or create new thread to allow image capture
				imaging_bound = FALSE;
				gettimeofday(&tv, NULL);
				mytimenow = tv.tv_sec;
				time_now = gmtime(&mytimenow);
				strftime(buffer,sizeof(buffer),"%m-%d-%Y  %T.",time_now);

				//fprintf(stderr,"*****************Capturing Image Slots Schedlue************************\n");
				//fprintf(stderr,"%s%ld Sec= %ld   MSec = %ld\n",buffer,tv.tv_usec,tv.tv_sec,tv.tv_usec/1000);

				// Capture images of complete schedule length depending on which camera being used
        if(Capture_Schedule_Slots(station_data,camera,filter_data,
                                  schedule_data,emph_data,schedule_slot,detector_data,
                                  image_data,Slot_Num,0) == FALSE){
          SYSTEM_LOG("Capture Schedule Slots Failed to Image Dark Frames-- Exiting\n");
          Delete_Containers(camera,station_data,filter_data,schedule_data,
                            emph_data,schedule_slot,image_data,detector_data);
          //Exit Procedure aka close shutter etc
          Exit_Procedure(camera);
          exit(FALSE);
        }
			}
		}
		//Check imaging should stop status
		//Check_Imaging_Should_Stop_Status();
		old_sun_status = sun_present;
	}
	// Free all dynamically allocated memory
	Delete_Containers(camera,station_data,filter_data,schedule_data,
                    emph_data,schedule_slot,image_data,detector_data);

	// Deleting PID file
	remove(PID_FILE);

	if(Exit_Procedure(camera)!=TRUE){
		SYSTEM_LOG("Failed to perform proper exit prcedure");
	}

	SYSTEM_LOG("IMAGERD_RT: Memory successfully unallocated -- safely exiting imagerd_rt -- BYE\n");

	exit(0);
}


/**
 * Signal catching function.
 */
void signal_stop_imaging() {
  	SYSTEM_LOG("Imaging will stop soon");
  	imaging_should_stop = TRUE;
}

void handler(int signum) {
  	// Aet image_bound flag to true
  	imaging_bound = TRUE;
}


/**
 * Close Shutter and Camera Before Exiting
 */
int Exit_Procedure(struct Camera *camera){
	//Close Shutter and Close the Sun Shade
  #ifndef DEBUG_SERIAL
  #ifndef FW_Shutter_Ctrl

  if(!Close_Shutter()){
    SYSTEM_LOG("Close_Shutter() - Could not close shutter - **Exiting**");
    exit(FALSE);
  }else{
    SYSTEM_LOG("Close_Shutter(): Status: CLOSED");
  }
  #ifdef SUN_SHADE
  //Close Sun Shade
  if(!Close_SunShade()){
    SYSTEM_LOG("Close_SunShade() - Could not close sun shade - **Exiting**");
    exit(FALSE);
  }else{
    SYSTEM_LOG("Close_SunShade(): Status: CLOSED");
  }
  #endif
  #endif
  #ifdef FW_Shutter_Ctrl
  //Make sure the shutter is CLOSED
  if(!FW_Control(SHUTCTRL,CLOSE)) {
    SYSTEM_LOG("FW_Control() - Could not close shutter  - **Exiting**");
    exit(FALSE);
  }else{
    SYSTEM_LOG("FW_Control(SHUTCTRL,CLOSE): Status: CLOSED");
  }
  #endif
  #endif
  #ifndef DEBUG_NO_COOL
  //Wait for safe exit temperature
  SYSTEM_LOG("Exit_Procedure(): Waiting for safe camera temperature before exiting.");
  Wait_Safe_Exit_Temperature();
  #endif


  SYSTEM_LOG("Closing Camera");
  if(CloseCamera(camera) != TRUE){
    SYSTEM_LOG("Could Not Uninitialize Library and Close Camera");
    return(FALSE);
  }
	return(TRUE);
}


/**
 * Check imaging should stop status.
 */
void Check_Imaging_Should_Stop_Status() {

  	int status;

  	//Get the imaging stop status */

  	if (Read_Imaging_Stop_Status(&status) != TRUE) {
    	SYSTEM_LOG("Failed to get imaging stop status from file. Stopping imaging.\n");
    	imaging_should_stop = TRUE;
    	return;
  	}

  	/* Check the status */

  	switch (status) {

    	case STOP_IMAGING:
      		imaging_should_stop = TRUE;
      		imaging_should_cont = FALSE;
      		SYSTEM_LOG("Imaging will stop soon");
      		return;
      		break;

    	case START_IMAGING:
      		imaging_should_stop = FALSE;
      		imaging_should_cont = TRUE;
      		return;
      		break;

    	case CONTINUE_IMAGING:
      		imaging_should_stop = FALSE;
      		imaging_should_cont = TRUE;
      		return;
      		break;

    	case DISCONTINUE_IMAGING:
      		imaging_should_stop = FALSE;
      		imaging_should_cont = FALSE;
			    imaging_bound = FALSE;
			    SYSTEM_LOG("Imaging will stop soon but cooler will be kept on");
      		return;
      		break;

    	default:
    		// This should never happen
      		SYSTEM_LOG("Got unknown imaging stop status. Stopping imaging.\n");
      		imaging_should_stop = TRUE;
      		return;
  	}
}

/*
 * Function to take milliseconds and return seconds
 */
int to_seconds(int time){
	div_t divresult;
	divresult = div (time,1000);
	return(divresult.quot);
}

/*
 * Function to take milliseocnds and return nanoseconds
 */
int to_nanoseconds(int time){
	div_t divresult;
	divresult = div (time,1000);
	return(divresult.rem*1000000);
}


/*
 * Free memory.
 */
void Delete_Containers(struct Camera *camera,
						struct station_info *station_data,
						struct filter_info *filter_data,
						struct schedule_info *schedule_data,
						struct emphemeris *emph_data,
						struct schedule_slot_info *schedule_slot,
						struct image_info *image_data,
						struct detector_info *detector_data){

  //TODO Add free ProEM mem structure
  //if (camera_data_pmax)
    if(camera)
    	free(camera);
	if (station_data)
    	free(station_data);
  	if (filter_data)
    	free(filter_data);
  	if (schedule_data)
    	free(schedule_data);
  	if (emph_data)
    	free(emph_data);
  	if (schedule_slot)
    	free(schedule_slot);
	if (image_data)
		free(image_data);
	if (detector_data)
		free(detector_data);

	SYSTEM_LOG("Freed Structure Memory");

}
