/*
 *
 *  definitions.h
 * 
 *  Copyright (C) 2013 Jarrett Little
 * 
 *
 */


#ifndef _DEFINITIONS_H_
#define _DEFINITIONS_H_


/*
 * Flags
 */

#define TRUE		1
#define FALSE		0
#define UNDEF		-1


/*
 * Configuration file 
 */

#define IMAGER_CONF_FILE				       "/usr/local/imagerd_rt/conf/imager.conf"	         /**< Camera configuration file. */
#define STATIONS_CONF_FILE			       "/usr/local/imagerd_rt/conf/project.conf"	       /**< Station location configuration file. */
#define STOP_FILE						           "/usr/local/imagerd_rt/aux/stop_file"             /**< File for the imaging stop signal.*/       
#define SCH_CONF_FILE					         "/usr/local/imagerd_rt/conf/schedule.conf"
#define PID_FILE						           "/dev/shm/imagerd_rt/imagerd_rt.pid"              /* File to hold imagerd pid number */
#define SEQ_FILE						           "/usr/local/imagerd_rt/aux/seq_file"
#define RAW_FILE                       "/usr/local/imagerd_rt/aux/image.raw"
#define FITS_FILE			"/usr/local/imagerd_rt/aux/image.fits"
#define PAL_FILE                       "/usr/local/imagerd_rt/aux/Pal/GREY.PAL"
#define META_FILE		       "/usr/local/imagerd_rt/aux/meta-data.txt"
#define SEQNO_FILE			"/usr/local/imagerd_rt/aux/seqno.txt"

#define PNG_MAX_VALUE					         65535	/**< Maximum value for 16-bit PNG image pixel. */

#define MAX_LINE_LEN       			       250  	/**< Maximum line lenght for the configuration file. */
#define MAX_FILENAME_LEN   			    	 255  	/**< Maximum filename length. */
#define MAX_STATION_LEN    				     5    	/**< Maximum station name length. */
#define MAX_ID_LEN						         10		
#define MAX_TIME_LEN        			     16   	/**< Maximum timestamp length. */
#define MAX_STR_LEN						         25		/**< Maximum string length  */
#define MAX_SEQ_LEN						         20  	/**< Maximum number */
#define MAX_STRING_LEN                 80

#define STOP_IMAGING      				     10  	/**< Imaging stop flag. */
#define START_IMAGING     				     11  	/**< Imaging start flag. */
#define CONTINUE_IMAGING  				     12  	/**< Imaging continue flag */
#define DISCONTINUE_IMAGING				     13  	/**< Imaging discontinue flag */

/*Default Values*/
#define DEFAULT_IMAGE_FILENAME  		  "image.png"    	/**< Default image filename. */
#define DEFAULT_SITE_UNIQUE_ID      	"____"          /**< Default station name. */
#define DEFAULT_PROJECT_UNIQUE_ID		  "______"		/**< Default project ID  */
#define DEFAILT_PROJECT_NAME			    "______"		/**< Default project name  */
#define DEFAULT_FILTER          		  5577       	/**< Default filter. */
#define DEFAULT_LATITUDE        		  90.0           	/**< Default latitude. */
#define DEFAULT_LONGITUDE       		  90.0           	/**< Default longitude. */
#define DEFAULT_ELEVATION       		  100            	/**< Default elevation. */
#define DEFAULT_TEMP					        -20				/**< Default temperature */
#define DEFAULT_PRESSURE				      1010.0			/**< Defualt pressure  */
#define DEFAULT_X_OFFSET        		  0              	/**< Default x-offset. */
#define DEFAULT_Y_OFFSET        		  0              	/**< Default y-offset. */


/*
 * Structures 
 */

struct station_info {
  char site_id[MAX_STATION_LEN+1];
  char project_id[MAX_ID_LEN+1];
  char project_name[MAX_ID_LEN+1];
  char device_id[MAX_STR_LEN+1];
  float altitude;
  float latitude;
  float longitude;
  float avg_temp;
  float avg_pressure;
};


struct filter_info {
  int filter_slot_number;
  char wavelength[10];
  char filt_descr[256];
};

struct detector_info {
  int xpixels;
  int ypixels;
  int temperature;
};

struct image_info {
  float exposure;
  int width;
  int height;
  int h_bin;
  int v_bin;
  int h_start;
  int h_end;
  int v_start;
  int v_end;
  int p_bin;
  int s_bin;
};

struct schedule_slot_info {
  int filter_num;
  int binning;
  int exposure_len;
  int prep_time;
  int start_time;
  int ccd_gain;
  int emccd_gain;
  float fw_temperature;
};

struct schedule_info {
  int num_slots;
  int imager_mode;
  char device_id[MAX_STR_LEN+1];
  int sched_len;
};

struct emphemeris {
  float start_zenith_angle;
  float sun_angle;
  float start_moon_angle;
  float moon_phase;
};

#endif /* _DEFINITIONS_H_ */



