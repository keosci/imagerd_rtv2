/*
 * file_io.h
 * 
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Jarrett Little
 *
 */


#ifndef _FILE_IO_H_
#define _FILE_IO_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <math.h>
#include <time.h>
#include <ctype.h>
#include <regex.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>

#include "definitions.h"
#include "imagerd_rt.h"

//#include "picam.h"
//#include "pro_em.h"

/*
 * Functions
 */
int Read_Camera_Settings(const char *conf_file, struct Camera *camera);
void Read_Station_Settings(const char *conf_file, struct station_info *station_data);
void Use_Station_Info_Defaults(struct station_info *station_data);
int Read_FW_Slots(const char *conf_file);
int Read_Filter_Settings(const char *conf_file, struct filter_info *filter_data, int FW_slots);
int Read_Sch_Slots(const char *conf_file);
int Read_Sched_Settings(const char *conf_file, 
      struct schedule_slot_info *schedule_slot, 
      struct schedule_info *schedule_data, 
      struct emphemeris *emph_data,
      int Slot_Num);
int Get_Seqno();
int Update_Seqno();
int Write_Imaging_Stop_Status(int status);
int Read_Imaging_Stop_Status(int *status);



#endif /* _FILE_IO_H_ */




