/*
 *  lib_andor.h
 * 
 *	Low-Level Camera Library for Andor Camera
 * 
 *  Copyright 2015 - Jarrett Little
 *
 */

#ifndef _LIB_ANDOR_H_
#define _LIB_ANDOR_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <math.h>
#include <time.h>
#include <ctype.h>
#include <regex.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>

#include "imagerd_rt.h"
#include "definitions.h"


/*
 * Flags
 */
#define TRUE    1
#define FALSE   0
#define UNDEF  -1
#define MAX_STRING_LEN 255
#define NO_TIMEOUT -1


/*
 * Low-Level ANDOR Camera Functions
 */
int Initialize_CCD(char *directory);
int Start_Acquisition();
int Set_Image(int hbin, int vbin, int hstart, int hend, int vstart, int vend);
int Save_To_TIFF(char *filename, char *pal, int pos, int imageBits);
int Save_To_BMP(char* filename, char* pal, long ymin, long ymax);

int Check_Temperature(int temperature);
int Check_AD_Channel(int ad_channel);
int Check_Output_Amp(int amp);
int Check_HS_Speed(int hs_speed,int ad_channel,int output_amp);
int Check_VS_Speed(int vs_speed);
int Check_VS_Amplitude(int vs_amplitude,int vs_speed);
int Check_Pre_Amp_Gain(int pre_amp_gain,int ad_channel,int output_amp,int hs_speed,int *available_pre_amp_gain_index);

int Find_Lowest_Available_Pre_Amp_Gain(int num_pre_amp_gains,int ad_channel,int output_amp,int hs_speed,int *available_pre_amp_gain);
int Is_Pre_Amp_Gain_Available(int channel, int amplifier, int index, int gain, int *status);

int Print_AD_Channels();
int Print_Available_VS_Speeds();
int Print_Available_HS_Speeds(int ad_channel, int output_amp);
int Print_Available_HS_Settings();
int Print_Available_VS_Amplitudes();
int Print_Available_Pre_Amp_Gains(int ad_channel,int output_amp,int hs_speed);

void Print_Cooling_Status();
int Start_Cooler();
int Stop_Cooler();
int Is_Target_Temperature_Reached(int target_temperature, int *temperature, int *ok);
int Is_Temperature_Above_Limit(int limit, int *temperature, int *ok);
int Is_Min_Temperature_Reached(int temperature, int *ok);

int Set_Exposure_Time(float exposure);
int Set_Read_Mode(int mode);
int Set_Acquisition_Mode(int mode);
int Set_Kinetic(int accs, int scans);
int Set_Kinetic_Cycle_Time(float time);
int Set_Number_Kinetics(int number);
int Set_Cooler_Mode(int mode);
int Set_Cooler_State(int state);
int Set_Fan_Mode(int mode);
int Set_HS_Speed(int type, int index);
int Set_Pre_Amp_Gain(int index);
int Set_VS_Speed(int index);
int Set_VS_Amplitude(int amplitude);
int Set_EM_Gain_Mode(int mode);
int Set_EMCCD_Gain(int gain);
int Set_Temperature(int temperature);
int Set_AD_Channel(int channel);
int Set_Output_Amplifier(int type);
int Set_High_Capacity(int state);
int Set_Frame_Transfer(int state);
int Set_Shutter_Mode(int ttl_mode, int mode, int close_time, int open_time);
int Set_Baseline_Clamp(int state);
int Get_Status(int *status);
int Get_Acquired_Data(int32_t *array, long size);
int Get_Exposure_Time(float *exposure);
int Get_Max_Exposure(float *max_exposure);
int Get_Temperature(int *temperature);
int Get_Temperature_Range(int *min, int *max);
int Get_Cooling_Status(unsigned int *cooling_status, int *temperature);
int Get_Detector_Temperature(int *temperature, int *ok);
int Get_Pre_Amp_Gain(int pre_amp_gain, float *pre_amp_gain_factor);
int Get_Number_AD_Channels(int *number);
int Get_Number_Amp(int *number);
int Get_Amp_Max_HSpeed(int index, float *speed);
int Get_Number_HS_Speeds(int channel, int type, int *number);
int Get_Number_VS_Speeds(int *number);
int Get_Number_VS_Amplitudes(int *number);
int Get_Number_Pre_Amp_Gains(int *number);
int Get_Fastest_Recommended_VS_Speed(int *index, float *speed);
int Get_Bit_Depth(int ad_channel, int *bit_depth);
int Get_HS_Speed(int ad_channel, int output_amp, int hs_speed_index, float *hs_speed_MHz);
int Get_VS_Speed(int vs_speed, float *vs_speed_usp);
int Get_EMCCD_Gain(int *gain);
int Get_EM_Gain_Range(int *low, int *high);
int Get_Detector(int *hpixels, int *vpixels);
int Get_Keep_Clean_Time(float *clean_time);
int Get_Max_H_Binning(int read_mode, int *max_bin);
int Get_Max_V_Binning(int read_mode, int *max_bin);
int Get_Min_Image_Length(int *min_length);
int Get_Readout_Time(float *readout_time);
int Get_Cooler_State(int *cooler_status);
int Get_VS_Amplitude(int vs_amplitude, int *vs_amp_value);

int Driver_Status(int *status);
int Wait_Camera_Temperature(int temperature, int safe);
void Wait_Safe_Exit_Temperature();


#endif /* _LIB_ANDOR_H_ */
