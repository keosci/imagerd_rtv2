/*
 *
 *  ephlib.h
 * 
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Jarrett Little
 *
 *
 */

#ifndef _EPHLIB_H_
#define _EPHLIB_H_

#include <stdio.h>
#include <math.h>
#include "definitions.h"
#include "imagerd_rt.h"


#define	degrad(x)	((x)*PI/180.)
#define	raddeg(x)	((x)*180./PI)
#define	hrdeg(x)	((x)*15.)
#define	deghr(x)	((x)/15.)
#define	hrrad(x)	degrad(hrdeg(x))
#define	radhr(x)	deghr(raddeg(x))
#define	SIDRATE		.9972695677
#define NUMBER_OF_STARS 14
#define	EQtoECL	1
#define	ECLtoEQ	(-1)

#ifndef PI
#define	PI		3.141592653589793
#endif
#define	TWOPI	(2*PI)

/*
 * Functions
 */

/* Functions to return sun and moon angles */
int f_sun_present(struct tm *time_now,struct station_info *site_data,struct emphemeris *eph_data);
int f_moon_present(struct tm *time_now,struct station_info *site_data,struct emphemeris *eph_data);
double sun_dip (int yr, int mn, int dy, int hr, int m, int s, struct station_info *site_data);
double moon_dip(int yr, int mn, int dy, int hr, int m, int s, struct station_info *site_data, double *phase);

/* Function prototypes for EPHLIB functions. */
void obliquity ( double, double * );
void nutation ( double, double *, double * );
void cal_mjd ( int, double, int, double * );
void mjd_cal ( double, int *, double *, int * );
void mjd_dow ( double, double * );
void mjd_dpm ( double, int * );
void mjd_year ( double, double * );
void year_mjd ( double, double * );
void anomaly ( double, double, double *, double * );
void moon ( double, double *, double *, double * );
void eq_ecl ( double, double, double, double *, double * );
void ecl_eq ( double, double, double, double *, double * );
void static ecleq_aux ( int, double, double, double, double *, double * );
void aa_hadec ( double, double, double, double *, double * );
void hadec_aa ( double, double, double, double *, double * );
void static aaha_aux ( double lat, double x, double y, double *p, double *q );
void utc_gst ( double, double, double * );
void gst_utc ( double, double, double * );
static double tnaught ( double );
void ta_par ( double, double, double, double, double, double *, double * );
void at_par ( double, double, double, double, double, double *, double * );
void sex_dec ( int, int, int, double * );
void dec_sex ( double, int *, int *, int *, int * );
void range ( double *, double );
void refract ( double, double, double, double * );
void unrefract ( double, double, double, double * );
void elongation ( double, double, double, double * );
void sunpos ( double, double *, double * );
void precess (double mjd1, double mjd2, double *ra, double *dec);
void do_starpos( void );

/*
 * Structures
 */

typedef struct {
  char Name[13];
  double Magnitude;
  char PopName[11];
  double RA;
  double DEC;
  double Epoch;
  double AZ;
  double ZenithAng;
} Starstruc;


#endif /* _EPHLIB_H_ */