/*
 *  imagerd_rt.h
 *
 *	This file is part of imagerd_rt.
 *
 *  imagerd_rt is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  imagerd_rt is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with imagerd_rt.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Auther: Jarrett Little
 *
 */


#ifndef _IMAGERD_RT_H_
#define _IMAGERD_RT_H_

/* System Library Includes */
#include <errno.h>
#include <malloc.h>
#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <stdint.h>

/* Imagerd_rt Library Includes */
#include "file_io.h"
#include "log.h"
#include "ephlib.h"
#include "lib_capture.h"
#include "lib_sched.h"
#include "imager_ctrl.h"
#include "definitions.h"


/* Compiler Flag to compiler and include/link for each type of Driver/SDK */
//#define ANDOR_CAM
//or
//#define PI_PROEM
//or
#define PI_PIXIS


/* Camera header files TODO FIX this for SDK2.0*/
#ifdef PI_PROEM
#include "picam.h"
#include "pro_em.h"
#endif
#ifdef PI_PIXIS
#include "picam.h"
#include "pixis.h"
#endif
#ifdef ANDOR_CAM
#include "atmcdLXd.h"
#include "lib_andor.h"
#include "andor.h"
#endif


//DEBUG_STDERR is defined then all error messages are printed to std err
//#define DEBUG_STDERR
//DEBUG_SERIAL is flag to skip over serial command calls.
//#define DEBUG_SERIAL
//DEBUG for extra error statements
//#define DEBUG
//Debugging without cooling camera head
#define DEBUG_NO_COOL
//SunShade Flag.  Remove commented define to enable sunshade.
//#define SUN_SHADE
//Uncomment to enable filter wheel and filterwheel controlled shutters
#define FW_Shutter_Ctrl
//Uncomment to enable Omega temperature control
#define Omega_Temp_Ctrl


#define SOFTWARE_VERSION "3.0"

#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \
                        } while (0)


/*
 * Log information.
 */

#ifdef DEBUG_STDERR
#define SYSTEM_LOG(x) fprintf(stderr,"%s\n",x)
#else
#define SYSTEM_LOG(x) syslog(LOG_LOCAL4 | LOG_INFO, x)
#endif



/*
 * Functions
 */
int Exit_Procedure(struct Camera *camera);
void signal_stop_imaging();
void handler(int signum);
void Check_Imaging_Should_Stop_Status();
int to_seconds(int time);
int to_nanoseconds(int time);
void Delete_Containers(struct Camera *camera,
						struct station_info *station_data,
						struct filter_info *filter_data,
						struct schedule_info *schedule_data,
						struct emphemeris *emph_data,
						struct schedule_slot_info *schedule_slot,
						struct image_info *image_data,
						struct detector_info *detector_data);


#endif /* _IMAGERD_RT_H_ */
