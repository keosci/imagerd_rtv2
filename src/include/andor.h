/*
 *  andor.h
 * 
 *	Camera Plugin for Andor Camera
 * 
 *  Copyright 2015 - Jarrett Little
 *
 */

#ifndef _ANDOR_H_
#define _ANDOR_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <math.h>
#include <time.h>
#include <ctype.h>
#include <regex.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>

#include "imagerd_rt.h"
#include "definitions.h"
//Low Level Andor Functions
#include "lib_andor.h"
//iKon Spec
#include "iKon.h"

/*
 *  Andor SDK 2.0 Specfic Defines
 */

//Andor directory
#define ANDOR_PATH "/usr/local/etc/andor"  //< Directory containing the Andor initialization file

/*
 * Flags
 */
#define TRUE    1
#define FALSE   0
#define UNDEF  -1
#define MAX_STRING_LEN 255
#define NO_TIMEOUT -1

#define PNG_COMPRESSION_LEVEL  0

/*
 * Generic Camera Structure -> Holds camera specific parameters. To be changed depending on the camera 
 * SDK or library. 
 */
struct Camera {
	/*
	 *	ANDOR iKon Specific Parameters
	 */
	int ExposureTime;   	//->SLOT
	int Temperature;		//->STATIC
	float Temp_SetPoint;		//->STATIC
	int fan_mode;			//->STATIC
	int cooling;			//->STATIC
	int CCD_Gain;			//->SLOT -> Actaully pre-amp-gain
	int hs_speed;			//->STATIC
	float hs_speed_MHz;		//->Readable
	float vs_speed_usp;		//->Readable
	int ad_channel;			//->STATIC
	int vs_speed;			//->STATIC
	int vs_amplitude;		//->STATIC
	int vs_amp_value;		//->Readable
	int output_amp;			//->STATIC
	int high_capacity;		//->STATIC
	int frame_transfer;		//->STATIC
	int ReadOutCtrlMode;	//->STATIC
	int acquisition_mode;	//->STATIC
	int bit_depth;			//->STATIC
	//iKon does not have an EMCCD
	float emccd_gain_modifier_current;
	int EM_Gain;
	int em_gain_mode;
	int ADC_Quality;
	int ADC_Speed;
	int Invert_Output;
	int Output_Signal;
	int Shutter_Mode;
	int Shutter_Open_Delay;
	int Shutter_Close_Delay;


	//ROIS Values for CCD Readout (BINNING -> SLOT) -> DONE!
	int h_size;
	int v_size;
	int h_start;
	int v_start;
	int h_end;
	int v_end;
	int x_bin;
	int y_bin;
	int height;
	int width;
	int ccd_height;
	int ccd_width;
	int image_type;

	int32_t *pixels;

	/*
	 * PICAM Specific Parameters
	 */
	
	/*
	PicamHandle hcam;
	PicamAvailableData data;
	PicamCameraID id;
	PicamRois *region;

	piflt Temperature;
	piflt ExposureTime;
	piflt ADC_Speed;
	pibln Invert_Output;
	piint EM_Gain;
	piflt Temp_SetPoint;
	piint ADC_Quality;
	piint ReadOutCtrlMode;
	piint CCD_Gain;
	piint Output_Signal;
	piint x_bin;
	piint y_bin;
	piint bit_depth;
	piint readoutstride;
	piint frame_size;
	piint ccd_height;
	piint ccd_width;
	*/
};

struct Current_Params{
	/*
	 *	ANDOR iKon Specific Parameters
	 */
	int ExposureTime;   	//->SLOT 
	int Temperature;		//->STATIC
	float Temp_SetPoint;	//->STATIC !
	int fan_mode;			//->STATIC !
	int cooling;			//->STATIC !
	int CCD_Gain;			//->SLOT -> Actaully pre-amp-gain !
	int hs_speed;			//->STATIC !
	float hs_speed_MHz;		//->Readable !
	float vs_speed_usp;		//->Readable !
	int ad_channel;			//->STATIC !
	int vs_speed;			//->STATIC !
	int vs_amplitude;		//->STATIC !
	int vs_amp_value;		//->Readable !
	int output_amp;			//->STATIC !
	int high_capacity;		//->STATIC !
	int frame_transfer;		//->STATIC !
	int ReadOutCtrlMode;	//->STATIC !
	int acquisition_mode;	//->STATIC !
	int bit_depth;			//->STATIC
	//iKon does not have an EMCCD
	float emccd_gain_modifier_current;
	int EM_Gain;
	int em_gain_mode;
	int ADC_Quality;
	int ADC_Speed;
	int Invert_Output;
	int Output_Signal;
	int Shutter_Mode;
	int Shutter_Open_Delay;
	int Shutter_Close_Delay;

	//ROIS Values for CCD Readout (BINNING -> SLOT) -> DONE!
	int h_size;
	int v_size;
	int h_start;
	int v_start;
	int h_end;
	int v_end;
	int x_bin;
	int y_bin;
	int height;
	int width;
	int ccd_height;
	int ccd_width;
	int image_type;

	int32_t *pixels;
};



/*
 * Function declarations
 */
//Change Initialization() to Init to avoid conflict with Andor SDK2.0
int Init(void);
//Andor SDK has no open camera function therefore to keep with consistency it will just return true
int OpenCamera(struct Camera *camera);
int CloseCamera(struct Camera *camera);
int Initialize_Image_Pixels(struct Camera *camera);
void Delete_Image_Pixels(struct Camera *camera);
int PrintParameters(struct Camera *camera);
int PrintConfigParameters(struct Camera *camera);
int GetParamInfo(struct Camera *camera, struct Current_Params *params);
void Check_Settings_Against_AD_Channel(struct Camera *camera);
void Check_Settings_Against_Output_Amplifier(struct Camera *camera);
int PrintCurParams(struct Current_Params *params);
int Configure_Static(struct Camera *camera);
int Configure_Dynamic(struct Camera *camera);
//void Get_ROIS_Param(struct Camera *camera, piint* ybin, piint* xbin);
int Acquire_Data(struct Camera *camera);
int SaveToFile(char *filename, struct Camera *camera,struct image_info *image_data, struct station_info *station_data, int type);
void ReadTemperature(struct Camera *camera);
int Wait_Camera_Temperature(int temperature, int safe);
int Set_Shutter(int mode, int close_delay, int open_delay);





#endif /* _ANDOR_H_ */
