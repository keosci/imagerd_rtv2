/*
 *  log.h
 * 
 *	This file is part of imagerd_rt.
 *
 *  imagerd_rt is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  imagerd_rt is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with imagerd_rt.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Auther: Jarrett Little
 *
 */


#ifndef LOG_H_
#define LOG_H_

#include <stdio.h>
#include <syslog.h>
#include "imagerd_rt.h"
#include "definitions.h"


//#include "picam.h"
//#include "pro_em.h"



/*
 * Functions
 */

void Print_Log_Header();
void Print_Log_Station_Settings(struct station_info *station_data);
void Print_Camera_Settings(struct Camera *camera);
void Print_Filter_Settings(struct filter_info *filter_data, int FW_slots);
void Print_Sch_Settings(struct schedule_slot_info *schedule_slot, 
			struct schedule_info *schedule_data, 
			struct emphemeris *emph_data,
			int Slot_Num,char *camera_type);
#endif /* LOG_H_ */
