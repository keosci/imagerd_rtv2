/*
 *  pro_em.h
 *
 *	Camera Library Plugin for PI Pro_EM Camera
 *
 *  Copyright 2013 - Jarrett Little
 *
 */

#ifndef _PRO_EM_H_
#define _PRO_EM_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <math.h>
#include <time.h>
#include <ctype.h>
#include <regex.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>

#include "imagerd_rt.h"
#include "definitions.h"
#include "picam.h"


/*
 * Flags
 */

#define TRUE    1
#define FALSE   0
#define UNDEF  -1
#define MAX_STRING_LEN 255
#define NO_TIMEOUT -1
//#define SYSTEM_LOG(x) fprintf(stderr,"%s\n",x)

/*
 * Generic Camera Structure -> Holds camera specific parameters. To be changed depending on the camera
 * SDK or library.
 */
struct Camera {
	/*
	 *	PICAM Specific Parameters
	 */
	PicamHandle hcam;
	PicamAvailableData data;
	PicamCameraID id;
	PicamRois *region;

	piflt Temperature;
	piflt ExposureTime;
	piflt ADC_Speed;
	pibln Invert_Output;
	piint EM_Gain;
	piflt Temp_SetPoint;
	piint ADC_Quality;
	piint ReadOutCtrlMode;
	piint CCD_Gain;
	piint Output_Signal;
	piint x_bin;
	piint y_bin;
	piint bit_depth;
	piint readoutstride;
	piint frame_size;
	piint ccd_height;
	piint ccd_width;

};

struct Current_Params {
	piflt Temperature;
	piflt ExposureTime;
	piflt ADC_Speed;
	pibln Invert_Output;
	piint EM_Gain;
	piflt Temp_SetPoint;
	piint ADC_Quality;
	piint ReadOutCtrlMode;
	piint CCD_Gain;
	piint Output_Signal;
	piint x_bin;
	piint y_bin;
	piint bit_depth;
};



/*
 * Function declarations
 */
int Init(void);
int OpenCamera(struct Camera *camera);
int CloseCamera(struct Camera *camera);
int GetParamInfo(struct Camera *camera, struct Current_Params *params);
double Get_Flt_Param(struct Camera *camera, PicamParameter parameter);
int print_params(struct Current_Params *params);
int Configure_Static(struct Camera *camera);
int Configure_Dynamic(struct Camera *camera);
void Get_ROIS_Param(struct Camera *camera, piint* ybin, piint* xbin);
int Acquire_Data(struct Camera *camera);
int SaveRaw( pibyte* buf, piint numframes, piint framelength, piint frame_size);
void ReadTemperature(struct Camera *camera);

#endif /* _PRO_EM_H_ */
