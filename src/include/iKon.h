/*
 *  iKon.h
 * 
 *	Andor iKon Camera Parameters
 * 
 *  Copyright 2015 - Jarrett Little
 *
 */

#ifndef _IKON_H_
#define _IKON_H_


/*
 * Available readout modes 
 */

#define FULL_VERTICAL_BINNING 0   
#define MULTI_TRACK           1
#define RANDOM_TRACK          2
#define SINGLE_TRACK          3
#define IMAGE                 4

/*
 * Available acquisition modes
 */

#define SINGLE_SCAN              1
#define ACCUMULATE               2
#define KINETICS                 3
#define FAST_KINETICS            4
#define RUN_TILL_ABORT           5

/*
 * iKon camera operation modes
 */

#define READOUT_MODE       	IMAGE        /**< Readout mode. */ 
#define ACQUISITION_MODE	SINGLE_SCAN    /**< Acquisition mode. */

/*
 * Accumulation
 */

#define ACCUMULATION_NUMBER 1
 
/*
 * Kinetic series
 */

#define KINETICS_NUMBER  1

/*
 * Image binning
 */

#define MIN_H_BIN   1
#define MIN_V_BIN   1
#define GET_H_BIN   0
#define GET_V_BIN   1

/*
 * Image offsets (1 -> )
 */

#define MIN_H_START 1
#define MIN_V_START 1

/*
 * Image size
 */

#define X_SIZE_MIN   1
#define Y_SIZE_MIN   1 
#define X_SIZE_MAX   2048
#define Y_SIZE_MAX   2048
#define X_SIZE       X_SIZE_MAX 
#define Y_SIZE       Y_SIZE_MAX

/*
 * Image Bit Depth
 */
#define BIT_8 	0
#define BIT_16	1
#define COLOUR	2

/*
 * Pre Amp Gain Flags
 */
#define PRE_AMP_GAIN_AVAILABLE     	1  	//< Pre-amplifier gain is available flag.
#define PRE_AMP_GAIN_NOT_AVAILABLE 	0  	//< Pre-amplifier gain is not available flag.


/* 
 * Output amplifier 
 */

#define OUTPUT_AMP_CCD     0  /**< Output amplifier type conventional CCD. */

/*
 * Cooling
 */

#define COOLING_ON         0   /**< Cooling ON flag. */
#define COOLING_OFF        1   /**< Cooling OFF flag. */

/*
 * Fan modes
 */

#define FAN_MODE_MIN       0   /**< Minimum allowed fan mode value. */
#define FAN_MODE_MAX       2   /**< Maximum allowed fan mode value. */

/*
 * Frame transfer 
 */

#define FRAME_TRANSFER_ON  1   /**< Frame transfer ON flag. */
#define FRAME_TRANSFER_OFF 0   /**< Frame transfer OFF flag. */

/*
 * High capacity
 */

#define HIGH_CAPACITY_ON   1   /**< High capacity ON flag. */
#define HIGH_CAPACITY_OFF  0   /**< High capacity OFF flag. */

/*
 * Shutter operation
 */

#define SHUTTER_TTL_HIGH   1
#define SHUTTER_TTL_LOW    0

#define SHUTTER_MODE_AUTO  0
#define SHUTTER_MODE_OPEN  1
#define SHUTTER_MODE_CLOSE 2

#define SHUTTER_TYPE       SHUTTER_TTL_LOW
#define SHUTTER_MODE       SHUTTER_MODE_AUTO

#define SHUTTER_OPEN_TIME  60
#define SHUTTER_CLOSE_TIME 60

/*
 * Trigger operation
 */

#define TRIGGER_INTERNAL        0
#define TRIGGER_EXTERNAL        1
#define TRIGGER_EXTERNAL_START  6

#define TRIGGER_MODE            TRIGGER_INTERNAL 

/*
 * Cooler operation 
 */

#define SHUTDOWN_TEMP_ON        1
#define SHUTDOWN_TEMP_OFF       0

#define COOLER_MODE             SHUTDOWN_TEMP_OFF

#define START_TEMPERATURE	18
#define ALLOWED_TEMP_DRIFT	3     /**< Allowed temperature drift from the set target temperature (+/-). */
#define SHUTDOWN_TEMPERATURE	13   	/**< The minimum detector temperature for shutdown. */




#define SAFE_TEMPERATURE   5    /**< Detector temperature when it is safe to shutdown the camera. */

/*
 * Default values
 */
#define DEFAULT_EXPOSURE            	2.000  		/**< Default exposure time in seconds. */
#define DEFAULT_TEMPERATURE       	-40  		/**< Default desired detector temperature. */
#define DEFAULT_READOUT_MODE		IMAGE 		/**< Default readout mode */
#define DEFAULT_ACQUISITION_MODE	SINGLE_SCAN
#define DEFAULT_FAN_MODE            	0  			/**< Default fan mode. */
#define DEFAULT_PRE_AMP_GAIN        	2  			/**< Default pre-amp gain. */
#define DEFAULT_HS_SPEED            	0  			/**< Default horizontal shift speed. */
#define DEFAULT_AD_CHANNEL          	0  			/**< Default A/D channel. */
#define DEFAULT_VS_SPEED            	1  			/**< Default vertical shift (readout) speed. */
#define DEFAULT_VS_AMPLITUDE        	0  			/**< Default vertical clock voltage amplitude. */
#define DEFAULT_OUTPUT_AMPLIFIER    	OUTPUT_AMP_CCD   /**< Default output amplifier type. */    
#define DEFAULT_COOLING             	COOLING_ON         /**< Default cooling setting. */
#define DEFAULT_HIGH_CAPACITY       	HIGH_CAPACITY_OFF  /**< Default high capacity functionality. */
#define DEFAULT_EMCCD_GAIN          	-1                 /**< Default EMCCD gain value. */
#define DEFAULT_EM_GAIN_MODE        	-1       	/**< Default EM gain mode. */
#define DEFAULT_FRAME_TRANSFER      	FRAME_TRANSFER_OFF  /**< Default frame transfer mode. */
#define DEFAULT_V_BIN               	1  			/**< Default vertical binning. */
#define DEFAULT_H_BIN               	1  			/**< Default horizontal binning. */
#define DEFAULT_H_SIZE            	2048  		/**< Default horizontal image size. */
#define DEFAULT_V_SIZE            	2048  		/**< Default vertical image size. */
#define DEFAULT_H_START             	1  			/**< Default start column. */
#define DEFAULT_V_START             	1  			/**< Default start row. */



#endif /* _IKON_H_ */
