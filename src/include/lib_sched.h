/*
 * lib_sched.h
 * 
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Original code Copyright (C) 2011 Jarrett Little
 * 2013-11-23 Modified by Jarrett Little.
 */

#ifndef _LIB_SCHED_H_
#define _LIB_SCHED_H_


#include <errno.h>
#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>

#include "imagerd_rt.h"
#include "definitions.h"


//#include "picam.h"
//#include "pro_em.h"


int Check_Schedule_Settings(struct schedule_slot_info *schedule_slot, 
			struct schedule_info *schedule_data, 
			struct emphemeris *emph_data,
			int Slot_Num);
								   
int Capture_Schedule_Slots(struct station_info *station_data,
							struct Camera *camera,
							struct filter_info *filter_data,
							struct schedule_info *schedule_data,
							struct emphemeris *emph_data,
							struct schedule_slot_info *schedule_slot,
							struct detector_info *detector_data,
							struct image_info *image_data,
							int Slot_Num, int dark);
								
void slot_handler(int signum);


struct slot_timers
{
  int type;
  struct sigevent evp;
  struct itimerspec its_slot;
};



#endif /*_LIB_SCHED_H_ */