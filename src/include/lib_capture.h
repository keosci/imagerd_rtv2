/*
 *
 * lib_capture.h
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Jarrett Little
 *
 */

#ifndef _LIB_CAPTURE_H_
#define _LIB_CAPTURE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <syslog.h>


#include "imagerd_rt.h"
#include "definitions.h"
#include "file_io.h"
#include "log.h"
#include "lib_sched.h"


//TODO Not sure if we need this?
//#include "picam.h"
//#include "pro_em.h"


#define PNG_COMPRESSION_LEVEL  3

/*
 * Functions
 */

int Capture_Image(struct station_info *station_data,
					struct Camera *camera,
					struct filter_info *filter_data,
					struct schedule_info *schedule_data,
					struct emphemeris *emph_data,
					struct schedule_slot_info *schedule_slot,
					struct image_info *image_data,
					struct detector_info *detector_data,
					int Slot_Num, int dark, int num_sch_slots);

int Create_Filepath(time_t time_now,
					char *newfilename,
					struct Camera *camera,
					struct filter_info *filter_data,
					struct schedule_slot_info *schedule_slot,
					struct station_info *station_data,
					int Slot_Num, int dark);

int Check_Directory_And_Create(const char *directory);


int Build_Image(char *filename,
				struct Camera *camera,
				struct image_info *image_data,
				struct station_info *station_data,
				struct schedule_slot_info *schedule_slot,
				struct emphemeris *emph_data,
				struct filter_info *filter_data,
				int Slot_Num,
				time_t time_now);


#endif /* _LIB_CAPTURE_H_ */
