/*
 * lib_sched.c
 * 
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Original code Copyright (C) 2011 Jarrett Little
 *
 */

#include "lib_sched.h"

volatile sig_atomic_t imaging_slot = FALSE;
volatile sig_atomic_t exit_capture = FALSE;


int Check_Schedule_Settings(struct schedule_slot_info *schedule_slot, 
			struct schedule_info *schedule_data, 
			struct emphemeris *emph_data,
			int Slot_Num){
  
  	char line_buffer[255] = {0};
  	int total_len=0;
  	int i=0;
  	int slot_len=0;
  
  	// Check that the schedule length divides into 60 evenly
  	if(schedule_data->sched_len % 60000 == 1 | 60000 % schedule_data->sched_len == 1){
    	SYSTEM_LOG("The entered schedule length does not divide evenly into 60. Please change this value\n");
    	return(FALSE);
  	}
  
  	//Check all slot exposure + prep is less then schedule length
  	for(i=0;i<Slot_Num;i++){
    	total_len = total_len + schedule_slot[i].exposure_len + schedule_slot[i].prep_time;
  	}
  	if(total_len > schedule_data->sched_len){
    	SYSTEM_LOG("The combined schedule slot times are greater then schedule length. Please change this value\n");
    	return(FALSE);
  	}
  
  	//Check that the start times are sequential and not over lapping with other slots
  	for(i=0;i<(Slot_Num-1);i++){
    	slot_len = schedule_slot[i].start_time + schedule_slot[i].exposure_len + schedule_slot[i].prep_time;
    	if(slot_len > schedule_slot[i+1].start_time){
      		sprintf(line_buffer,"Slot:%d  will over lap Slot:%d by %d [ms]. Please revise schedule.",i,i+1,slot_len-schedule_slot[i+1].start_time);
      		SYSTEM_LOG(line_buffer);
      		return(FALSE);
    	}
  	}
  	//Check if last slot in schedule fits within schedule length
  	slot_len = schedule_slot[Slot_Num-1].start_time + schedule_slot[Slot_Num-1].exposure_len + schedule_slot[Slot_Num-1].prep_time;
  	if(slot_len > schedule_data->sched_len){
    	sprintf(line_buffer,"Slot:%d  will over lap schedule by %d [ms]. Please revise schedule.",Slot_Num-1,slot_len-schedule_data->sched_len);
    	SYSTEM_LOG(line_buffer);
    	return(FALSE);
  	}
  
  	SYSTEM_LOG("Schedule has passed all checks");
  
  	return(TRUE);
}


int Capture_Schedule_Slots(struct station_info *station_data,
							struct Camera *camera, 
							struct filter_info *filter_data,
							struct schedule_info *schedule_data, 
							struct emphemeris *emph_data,
							struct schedule_slot_info *schedule_slot, 
							struct detector_info *detector_data,
							struct image_info *image_data,
							int Slot_Num, int dark) {

	int slot_number=0,i=0,image_RA=TRUE;
  	char buffer[255] = {0};
  	exit_capture = FALSE;
  	// Signal Handling for slots
  	struct sigaction sa;
  	sigset_t mask;
  	// Absolute timer structures
  	int clock_id = CLOCK_REALTIME;
  	int abs = TIMER_ABSTIME, rel = 0;
  	timer_t timer_id[2];
  	//Absolute timing structures
  	struct tm *time_now;
  	time_t mytimenow;
  	struct timeval tv;
  	//type, { sigev_value, sigev_signo }, { it_iteration, it_value }
  	//First timer is for slot to slot timing
  	//Second timer is to exit capture function safely and return for next imaging cycle
  	struct slot_timers timers[2] = {
		{rel, {0,SIGUSR1}, {0,0,0,0} },
		{rel, {0,SIGUSR2}, {0,0,(schedule_data->sched_len/1000),0} }
  	};

  	if(schedule_slot[slot_number].start_time != 0){
		//printf("Sch slot number = %d and start time = %d\n",slot_number,(schedule_slot[slot_number].start_time/1000));
		timers[0].its_slot.it_value.tv_sec = to_seconds(schedule_slot[slot_number].start_time);
		timers[0].its_slot.it_value.tv_nsec = to_nanoseconds(schedule_slot[slot_number].start_time);
		image_RA = FALSE;
  	} else { 
		//printf("Sch slot number = %d and start time = %d\n",slot_number+1,(schedule_slot[slot_number+1].start_time/1000));
		timers[0].its_slot.it_value.tv_sec = labs(to_seconds((schedule_slot[slot_number+1].start_time-schedule_slot[slot_number].start_time)));
		timers[0].its_slot.it_value.tv_nsec = labs(to_nanoseconds((schedule_slot[slot_number+1].start_time-schedule_slot[slot_number].start_time)));
		image_RA = TRUE;
	}

	SYSTEM_LOG("Establishing timer and handler for slot signals");
   	//Signal set up
  	sa.sa_flags = 0;
  	sigemptyset(&mask);
  	sa.sa_sigaction = slot_handler;
  	sigemptyset(&sa.sa_mask);
  	for(i=0;i<2;i++){
		if(timer_create(clock_id, &timers[i].evp, &timer_id[i]) == -1){
			perror("timer_create");
			SYSTEM_LOG("Schedule_Routine: Timer Create Failed ** Exiting\n");
			return(FALSE);
		}
		if(sigaction(timers[i].evp.sigev_signo, &sa, NULL) == -1){
			SYSTEM_LOG("Schedule_Routine: Sigaction Failed ** Exiting\n");
			perror("sigaction");
			return(FALSE);
		}
  	}

  	SYSTEM_LOG("CAMERA CAPTURE: Setting timers");
	if(timer_settime(timer_id[0], timers[0].type, &timers[0].its_slot, NULL) == -1){
		SYSTEM_LOG("Schedule_Routine: Timer set time 0 Failed ** Exiting\n");
		perror("timer_settime");
		return(FALSE);
	}
	if(timer_settime(timer_id[1], timers[1].type, &timers[1].its_slot, NULL) == -1){
		SYSTEM_LOG("Schedule_Routine: Timer set time 1 Failed ** Exiting\n");
		perror("timer_settime");
		return(FALSE);
	}

	while(exit_capture == FALSE){
		if(imaging_slot == TRUE || image_RA == TRUE){
			//reset slot flag 
			imaging_slot = FALSE;
			//Increment slot number if not first image
			if(image_RA == FALSE){
				slot_number++;
				// if slot number + 1 is greater then the total slot number don't reset timer
				if((slot_number+1) < Slot_Num){
					//reset timer for next imaging start time 
					timers[0].its_slot.it_value.tv_sec = labs(to_seconds((schedule_slot[slot_number+1].start_time-schedule_slot[slot_number].start_time)));
					timers[0].its_slot.it_value.tv_nsec = labs(to_nanoseconds((schedule_slot[slot_number+1].start_time-schedule_slot[slot_number].start_time)));
					if(timer_settime(timer_id[0], timers[0].type, &timers[0].its_slot, NULL) == -1){
						SYSTEM_LOG("Schedule_Routine: Timer Reset  0 Failed ** Exiting\n");						
						perror("timer_settime");
						return(FALSE);
					}
				}
			}
			image_RA = FALSE;
			gettimeofday(&tv, NULL);
			mytimenow = tv.tv_sec;
			time_now = gmtime(&mytimenow);

			SYSTEM_LOG("<*************************************>");
			sprintf(buffer,"Acquiring Image Data ---------> Slot: %d",slot_number);
			SYSTEM_LOG(buffer);
			SYSTEM_LOG("<*************************************>");
			
			Capture_Image(station_data,
						camera,
						filter_data,
						schedule_data,
						emph_data,
						schedule_slot,
						image_data,
						detector_data,
						slot_number,dark,schedule_data->num_slots);
			
		}
	}

	for(i=0;i<2;i++) {
		if(timer_delete(timer_id[i]) == -1) {
			sprintf(buffer,"Timer Delete[%d]: %s\n",i,strerror(errno));
      		SYSTEM_LOG(buffer);
      		SYSTEM_LOG("Schedule_Routine: Timer Delete Failed ** Exiting\n");
			return(FALSE);
		}
			
	}
	return(TRUE);
}


/**
 * Handler function for timers slot imaging
 */
void slot_handler(int signum){
  /* set image_slot flag to true */
  switch(signum){
	case SIGUSR1:
	  imaging_slot = TRUE;
	  break;
	case SIGUSR2:
	  imaging_slot = FALSE;
	  exit_capture = TRUE;
	  break;
	default:
	  fprintf(stderr,"Didn't catch a signal being watched\n");
  }
}






