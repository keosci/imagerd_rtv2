/*
 *  log.c
 *
 *	This file is part of imagerd_rt.
 *
 *  imagerd_rt is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  imagerd_rt is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with imagerd_rt.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Auther: Jarrett Little
 *
 */

#include "log.h"

/**
 * Write the header field to the log file.
 */
void Print_Log_Header() {

  SYSTEM_LOG("\n=================================================");
  SYSTEM_LOG("|   Real Time Imaging Daemon -> imagerd_rt      |");
  SYSTEM_LOG("=================================================");

}


/**
 * Write the station location information to the program log.
 */
void Print_Log_Station_Settings(struct station_info *station_data) {

  char line_buffer[255] = {0};

  /* Write the settings to the log */

  SYSTEM_LOG("\n");
  SYSTEM_LOG("Current Station Settings");
  SYSTEM_LOG("========================");
  //SYSTEM_LOG("\n");

  sprintf(line_buffer,"Project ID: %s",station_data->project_id);
  SYSTEM_LOG(line_buffer);

  sprintf(line_buffer,"Project Name: %s",station_data->project_name);
  SYSTEM_LOG(line_buffer);

  sprintf(line_buffer,"Site ID: %s",station_data->site_id);
  SYSTEM_LOG(line_buffer);

  //sprintf(line_buffer,"Device ID: %s",station_data->device_id);
  //SYSTEM_LOG(line_buffer);

  sprintf(line_buffer,"Latitude: %f   \nLongitude: %f",station_data->latitude,station_data->longitude);
  SYSTEM_LOG(line_buffer);

  sprintf(line_buffer,"Altitude: %.2f",station_data->altitude);
  SYSTEM_LOG(line_buffer);

  sprintf(line_buffer,"Avg. Temperature: %.2f",station_data->avg_temp);
  SYSTEM_LOG(line_buffer);

  sprintf(line_buffer,"Avg. Pressure: %.2f",station_data->avg_pressure);
  SYSTEM_LOG(line_buffer);

}


/**
 * Write Camera setting information to the program log
 */
void Print_Camera_Settings(struct Camera *camera) {

  char str[255] = {0};
  int error;

  //Write the settings to the log

  //SYSTEM_LOG("\n");
  //SYSTEM_LOG("Imager Configuration Settings ");
  //SYSTEM_LOG("=============================");

  error = PrintParameters(camera);
  if(error != TRUE){
    SYSTEM_LOG("Print_Camera_Settings(): Error could not print out full camera parameters. Check system log for specific error.");
  }

  sprintf(str,"Device ID: %s",camera->device_id);
  SYSTEM_LOG(str);

  //SYSTEM_LOG("\n");
  //SYSTEM_LOG("End Of Configuration Settings ");
  //SYSTEM_LOG("=============================");

  /*
  //Print out standard value parameters - no enumeration types
  sprintf(str,"Temperature Setpoint: %.2f [Celsius]",camera->Temp_SetPoint);
  SYSTEM_LOG(str);
  sprintf(str,"ADC Read-out Speed:    %.2f [MHz]",camera->ADC_Speed);
  SYSTEM_LOG(str);
  //Print out enumeration types

  //Inverted Output Status
  if(camera->Invert_Output == 0){
    SYSTEM_LOG("Inverted Output:       False");
  }else{
    SYSTEM_LOG("Inverted Output:       True");
  }
  //params->ReadOutCtrlMode
  switch(camera->ReadOutCtrlMode){
    case 1: SYSTEM_LOG("Read-out Control Mode: FullFrame"); break;
    case 2: SYSTEM_LOG("Read-out Control Mode: FrameTransfer"); break;
    case 3: SYSTEM_LOG("Read-out Control Mode: Kinetics"); break;
    case 4: SYSTEM_LOG("Read-out Control Mode: SpectraKinetics"); break;
    case 5: SYSTEM_LOG("Read-out Control Mode: Interline"); break;
    case 6: SYSTEM_LOG("Read-out Control Mode: Dif"); break;
    default: SYSTEM_LOG("Read-out Control Mode: Error No Valid Readout Mode Found"); break;
  }
  //params->Output_Signal
  switch(camera->Output_Signal){
    case 1: SYSTEM_LOG("Output Signal:         NotReadingOut"); break;
    case 2: SYSTEM_LOG("Output Signal:         ShutterOpen"); break;
    case 3: SYSTEM_LOG("Output Signal:         Busy"); break;
    case 4: SYSTEM_LOG("Output Signal:         AlwaysLow"); break;
    case 5: SYSTEM_LOG("Output Signal:         AlwaysHigh"); break;
    case 6: SYSTEM_LOG("Output Signal:         Acquiring"); break;
    case 7: SYSTEM_LOG("Output Signal:         ShiftingUnderMask"); break;
    case 8: SYSTEM_LOG("Output Signal:         Exposing"); break;
    case 9: SYSTEM_LOG("Output Signal:         EffectivelyExposing"); break;
    case 10: SYSTEM_LOG("Output Signal:         ReadingOut"); break;
    case 11: SYSTEM_LOG("Output Signal:         WaitingForTrigger"); break;
    default: SYSTEM_LOG("Output Signal:         Error No Valid Output Signal Found"); break;
  }

  //params->ADC_Quality
  switch(camera->ADC_Quality){
    case 1: SYSTEM_LOG("ADC Quality:           Low Noise"); break;
    case 2: SYSTEM_LOG("ADC Quality:           High Capacity"); break;
    case 3: SYSTEM_LOG("ADC Quality:           Electron Multiplied"); break;
    default: SYSTEM_LOG("ADC Quality:           Error No Valid ADC Quality"); break;

  }
  */

}




/**
 * Write the filter information to the program log.
 */

void Print_Filter_Settings(struct filter_info *filter_data, int FW_slots) {

  char line_buffer[255] = {0};
  int i=0;
  /* Write the settings to the log */

  SYSTEM_LOG("\n");
  SYSTEM_LOG("Current Filter Settings");
  SYSTEM_LOG("=======================");


  for(i=0;i<FW_slots;i++){
    sprintf(line_buffer,"Filter(%d) Wavelength: %s",i+1,(filter_data+i)->wavelength);
    SYSTEM_LOG(line_buffer);

    sprintf(line_buffer,"Filter(%d) Description: %s",i+1,(filter_data+i)->filt_descr);
    SYSTEM_LOG(line_buffer);

  }
}

/**
 * Write the schedule information to the program log.
 */

void Print_Sch_Settings(struct schedule_slot_info *schedule_slot,
			struct schedule_info *schedule_data,
			struct emphemeris *emph_data,
			int Slot_Num, char *camera_type){

  char line_buffer[255] = {0};
  int i=0;

  SYSTEM_LOG("\n");
  SYSTEM_LOG("Current Schedule Settings");
  SYSTEM_LOG("=========================");

  sprintf(line_buffer,"Sched Device ID:           %s",schedule_data->device_id);
  SYSTEM_LOG(line_buffer);

  sprintf(line_buffer,"Imager Mode Value:         %d",schedule_data->imager_mode);
  SYSTEM_LOG(line_buffer);

  sprintf(line_buffer,"Schedule Length:           %d [ms]",schedule_data->sched_len);
  SYSTEM_LOG(line_buffer);

  sprintf(line_buffer,"Starting Zenith Angle:     %.2f [deg]",emph_data->start_zenith_angle);
  SYSTEM_LOG(line_buffer);

  for(i=0;i<Slot_Num;i++){
    sprintf(line_buffer,"Slot:%d  Filter Number:     %d",i,schedule_slot[i].filter_num);
    SYSTEM_LOG(line_buffer);

    sprintf(line_buffer,"Slot:%d  Start Time:        %d [ms]",i,schedule_slot[i].start_time);
    SYSTEM_LOG(line_buffer);

    sprintf(line_buffer,"Slot:%d  Exposure Length:   %d [ms]",i,schedule_slot[i].exposure_len);
    SYSTEM_LOG(line_buffer);

    sprintf(line_buffer,"Slot:%d  Binning:           %d",i,schedule_slot[i].binning);
    SYSTEM_LOG(line_buffer);

    sprintf(line_buffer,"Slot:%d  Prep_time:         %d",i,schedule_slot[i].prep_time);
    SYSTEM_LOG(line_buffer);

    sprintf(line_buffer,"Slot:%d  Pro-EM EMCCD Gain: %d",i,schedule_slot[i].emccd_gain);
    SYSTEM_LOG(line_buffer);

    sprintf(line_buffer,"Slot:%d  CCD Gain:   %d",i,schedule_slot[i].ccd_gain);
    SYSTEM_LOG(line_buffer);

  }

}
