/*
 *
 * lib_capture.c
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Jarrett Little
 *
 */

/*******************************************************************\
 *                                                                 *
 *                 Image Capture Functions                   	   *
 *                                                                 *
\******************************************************************/

#include "lib_capture.h"

int Capture_Image(struct station_info *station_data,
				struct Camera *camera,
				struct filter_info *filter_data,
				struct schedule_info *schedule_data,
				struct emphemeris *emph_data,
				struct schedule_slot_info *schedule_slot,
				struct image_info *image_data,
				struct detector_info *detector_data,
				int Slot_Num, int dark, int num_sch_slots) {

	char new_filename[MAX_FILENAME_LEN] = {0};
	struct tm *time_now;
	time_t mytimenow;
	struct timeval tv;
	char buffer[255] = {0};
	char str[255] = {0};
	float fw_temp;

	//Capture current time
	gettimeofday(&tv, NULL);
	mytimenow = tv.tv_sec;
	time_now = gmtime(&mytimenow);

	//struct Current_Params *cur_params = NULL;
    	//cur_params = malloc(sizeof(struct Current_Params));


	//Set camera structure with current slot parameters
	camera->ExposureTime    = (float) schedule_slot[Slot_Num].exposure_len;
  	camera->CCD_Gain        = (int) schedule_slot[Slot_Num].ccd_gain;

	sprintf(str,"Exposure Time -> %f [ms]  | CCD Gain -> %d",camera->ExposureTime,camera->CCD_Gain);
	SYSTEM_LOG(str);

	sprintf(str,"Schedule Exposure Time -> %d [ms]  | Schedule CCD Gain -> %d",schedule_slot[Slot_Num].exposure_len,schedule_slot[Slot_Num].ccd_gain);
	SYSTEM_LOG(str);
    	//Check to see if emccd_gain is not equal to -1
    	/*
    	if(schedule_slot[Slot_Num].emccd_gain != -1){
    		camera->EM_Gain 	= schedule_slot[Slot_Num].emccd_gain;
    	}
    	*/
    	camera->x_bin           = schedule_slot[Slot_Num].binning;
    	camera->y_bin           = schedule_slot[Slot_Num].binning;

	sprintf(str,"X Bin -> %d  |  Y Bin -> %d",camera->x_bin,camera->y_bin);
	SYSTEM_LOG(str);



	SYSTEM_LOG("Updating Dynamic Camera Parameters");

	//Set Dynamic Camera Parameter
	if(Configure_Dynamic(camera) != TRUE){
		SYSTEM_LOG("Configure_Dynamic(): Error setting dynamic camera parameters");
		return(FALSE);
	}

	/*
	if(GetParamInfo(camera, cur_params) != TRUE){
		SYSTEM_LOG("GetParamInfo(): Error getting current camera parameters");
		return(FALSE);
	}
	*/

	//Free Memory of Current Params Structure
	//free(cur_params);

	if(Create_Filepath(time_now,new_filename,camera,filter_data,schedule_slot,station_data,Slot_Num,dark)!=TRUE){
		SYSTEM_LOG("Create_Filepath(): Failed to create filename and or filepath.\n");
    	return(FALSE);
	}


	//fprintf(stderr,"\nStarting To Capture Single Image\n");

#ifdef ANDOR_CAM
	if(Initialize_Image_Pixels(camera) != TRUE) {
		SYSTEM_LOG("Initialize_Image_Pixels(): Could not allocate memory for datas");
		return(FALSE);
  }
#endif

	//Acquire Image
	if(Acquire_Data(camera) != TRUE){
		SYSTEM_LOG("Acquire_Data(): Failed to aquire image data");
    		return(FALSE);
	}
	SYSTEM_LOG("Acquired Image Data -> camera->pixels");

	//Update SEQNO file
	if(Update_Seqno() != TRUE){
		SYSTEM_LOG("Update_Seqno(): Failed to update SEQNO FILE");
                return(FALSE);
	}

#ifdef PI_PIXIS
	//Build PNG and PGM image from acquired data
	if(Build_Image(new_filename, camera, image_data, station_data, schedule_slot, emph_data, filter_data, Slot_Num,time_now)!=TRUE){
		SYSTEM_LOG("Build_Image(): Failed to build image - Exiting\n");
		return(FALSE);
	}

#endif

#ifdef PI_PROEM
	//Build PNG and PGM image from acquired data
	if(Build_Image(new_filename, camera, image_data, station_data, schedule_slot, emph_data, filter_data, Slot_Num,time_now)!=TRUE) {
		SYSTEM_LOG("Build_Image(): Failed to build image - Exiting\n");
		return(FALSE);
	}

#endif


	//Moving the build image function into the camera source library.
	//Each camera has a differnet way to build an image.
	//Build PNG and PGM image from acquired data
#ifdef ANDOR_CAM
	if(SaveToFile(new_filename, camera, image_data, station_data, camera->image_type) != TRUE){
		SYSTEM_LOG("SaveToFile(): Failed to build image - Exiting");
		return(FALSE);
	}

	//Delete pixel data (Don't need too)
	Delete_Image_Pixels(camera);
#endif

	SYSTEM_LOG("Built Image File");
	SYSTEM_LOG(new_filename);

	//Log CCD Temperature at the end of every imaging slot
	//ReadTemperature(camera);



	//Move filter wheel to next filter position not zero based but starts and 1 to FW_slots
	SYSTEM_LOG("Moving to next filter slot");
	sprintf(buffer,"The number of sch slots = %d",num_sch_slots);
	SYSTEM_LOG(buffer);
  	if((Slot_Num+1) < num_sch_slots){
  	if(FW_Control(FWCTRL,schedule_slot[Slot_Num+1].filter_num)!=TRUE){
			SYSTEM_LOG("FW_Control(): Failed to set next filterwheel position.");
			return FALSE;
		}
		sprintf(buffer,"Moved to Filter number = %d and slot_num = %d",schedule_slot[Slot_Num+1].filter_num,Slot_Num+1);
  		SYSTEM_LOG(buffer);
	}else{
		if(FW_Control(FWCTRL,schedule_slot[0].filter_num)!=TRUE){
			SYSTEM_LOG("FW_Control(): Failed to set next filterwheel position.");
			return FALSE;
		}
		sprintf(buffer,"Moved to Filter number = %d and slot_num = %d",schedule_slot[0].filter_num,0);
  		SYSTEM_LOG(buffer);
	}

	//Get the CCD Temperature
	ReadTemperature(camera);	

#ifdef Omega_Temp_Ctrl
	//Log current filterwheel temperature for each image acquired
	if(Omega_Control(RDTEMP,0,&fw_temp)!=TRUE){
		SYSTEM_LOG("Omega_Control(): Failed to log filterwheel temperature");
                return FALSE;
	}
	schedule_slot->fw_temperature = fw_temp;
	sprintf(buffer,"Filterwheel Temperature = %.2f",schedule_slot->fw_temperature);
	SYSTEM_LOG(buffer);
#endif


	SYSTEM_LOG("<*************************************>");
	SYSTEM_LOG("End of Slot Capture");
	SYSTEM_LOG("<*************************************>");

	return(TRUE);
}


/**
 * Create image filename and directory path.
 */
int Create_Filepath(time_t time_now,
					char *newfilename,
					struct Camera *camera,
					struct filter_info *filter_data,
					struct schedule_slot_info *schedule_slot,
					struct station_info *station_data,
					int Slot_Num, int dark){

	char new_filepath[256] 			= {0};
	char new_filename[256] 			= {0};
	char destination_top_level		="/data";
	char filename_time_holder[]		="20001231_060000_ ";
	char year_time_holder[]			="2011/";
	char month_time_holder[]		="12/";
	char day_time_holder[]			="31/";
	char hr_time_holder[]			="ut24/";
	char dark_suffix[]        		="_DARK";
	char filter[]              		="______";
	char exp_pad3[]					="000";
	char exp_pad2[]					="00";
	char exp_pad1[]					="0";
	char buff[20]					={0};
	size_t dirlen;

	/* -- Build Filename -- */
	/* 20110409_024045_PINA_nascam-iccd01_6300bg_002000ms.png */
	/* YYYYMMDD_hhmmss_site_device_XXXXXX_EEEEEEms.pnm */

	strftime(filename_time_holder,sizeof(filename_time_holder), "%Y%m%d_%H%M%S_",time_now);
	strcpy(new_filename, filename_time_holder);
	strcat(new_filename, station_data->site_id);
	strcat(new_filename, "_");
	strcat(new_filename, camera->device_id);
	strcat(new_filename, "_");
	strcpy(filter, filter_data[schedule_slot[Slot_Num].filter_num-1].wavelength);
	strcat(new_filename, filter);
	strcat(new_filename, "_");
	snprintf(buff,sizeof(buff), "%d", schedule_slot[Slot_Num].exposure_len);

	if(schedule_slot[Slot_Num].exposure_len < 1000){
		strcat(new_filename,exp_pad3);
		strcat(new_filename,buff);
	}else if(schedule_slot[Slot_Num].exposure_len < 10000){
		strcat(new_filename,exp_pad2);
		strcat(new_filename,buff);
	}else if(schedule_slot[Slot_Num].exposure_len < 100000){
		strcat(new_filename,exp_pad1);
		strcat(new_filename,buff);
	}
	strcat(new_filename, "ms");
	if(dark==1){
		strcat(new_filename, dark_suffix);
	}

	//fprintf(stderr, "Fully qualified filename = %s\n",new_filename);


	/* -- Build and Create Directory -- */
	strcpy(new_filepath, "/data/");
	//fprintf(stderr, "%s\n",new_filepath);
	if(Check_Directory_And_Create(new_filepath) != TRUE){
    	return(FALSE);
	}


	/* YYYY */
	dirlen = strlen(year_time_holder)+1;
	strftime(year_time_holder,dirlen,"%Y/",time_now);
	strcat(new_filepath,year_time_holder);
	/* build path for regular data path */
	if(Check_Directory_And_Create(new_filepath) != TRUE){
    	return(FALSE);
	}


	/* MM */
	dirlen = strlen(month_time_holder)+1;
	strftime(month_time_holder,dirlen,"%m/",time_now);
	strcat(new_filepath,month_time_holder);
	if(Check_Directory_And_Create(new_filepath) != TRUE){
    	return(FALSE);
	}

  	/* DD */
  	dirlen =strlen(day_time_holder)+1;
  	strftime(day_time_holder,dirlen,"%d/",time_now);
  	strcat(new_filepath,day_time_holder);
  	if(Check_Directory_And_Create(new_filepath) != TRUE){
    	return(FALSE);
	}

	/* site and device id */
	strcat(new_filepath, station_data->site_id);
	strcat(new_filepath, "_");
	strcat(new_filepath, camera->device_id);
	strcat(new_filepath, "/");
	if(Check_Directory_And_Create(new_filepath) != TRUE){
    	return(FALSE);
	}

	/* UTHH */
  	dirlen =strlen(hr_time_holder)+1;
  	strftime(hr_time_holder,dirlen,"ut%H/",time_now);
  	strcat(new_filepath,hr_time_holder);
	if(Check_Directory_And_Create(new_filepath) != TRUE){
    	return(FALSE);
	}

	//fprintf(stderr, "Fully qualified data filepath = %s\n",new_filepath);
	//fprintf(stderr, "Fully qualified pgm filepath = %s\n",filepath_pgm);


	strcat(newfilename, new_filepath);
	strcat(newfilename, new_filename);

	//fprintf(stderr, "Final filename = %s\n",newfilename);

	return(TRUE);
}

/**
 * Check the existence of the given directory. If the directory
 * does not exist, it is created.
 */
int Check_Directory_And_Create(const char *directory) {

	struct stat directory_stat;
	char buffer[255] = {0};

 	//Check the existance of the directory

	if (stat(directory,&directory_stat) != 0){
		if(errno == ENOENT){
      		//Directory is not found. Create it.
			sprintf(buffer,"Creating directory '%s'",directory);
    		SYSTEM_LOG(buffer);
			if (mkdir(directory,S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH) != 0){
				perror("Failed to create directory.");
				SYSTEM_LOG("Failed to create direcotry.");
				return FALSE;
    		}
		}else{
			perror("Directory stat failed.");
    		SYSTEM_LOG("Directory stat failed.");
    		return FALSE;
		}
	}
	return TRUE;
}

/*
int Build_Image_From_Raw(char *filename,
				struct Camera *camera,
				struct image_info *image_data,
				struct station_info *station_data) {

	char build_cmd[255];
	char buffer[255];
	int error;
	//Example Image Command
	//rawtopgm -bpp 2 -littleendian 1024 1024 sample.raw | pnmtopng > sample.png

	//Attach suffix to filename relevant to the file format
	(void)strcat(filename,".png");

	//Start building cmd string
	strcpy(build_cmd, "rawtopgm -bpp 2 -littleendian ");

	sprintf(buffer,"%d",camera->ccd_width/camera->x_bin);
	strcat(build_cmd, buffer);
	strcat(build_cmd," ");
	sprintf(buffer,"%d",camera->ccd_height/camera->y_bin);
	strcat(build_cmd, buffer);
	strcat(build_cmd," ");

	strcat(build_cmd, RAW_FILE);

	strcat(build_cmd," | pnmtopng > ");
	strcat(build_cmd, filename);

	//SYSTEM_LOG(build_cmd);

	error = system(build_cmd);
	if(error == -1){
		SYSTEM_LOG("system(): Command Error");
		return(FALSE);
	}

	return(TRUE);
}
*/

/**
 * Write image to PNG file.
 */
int Build_Image(char *filename,
				struct Camera *camera,
				struct image_info *image_data,
				struct station_info *station_data,
				struct schedule_slot_info *schedule_slot,
                                struct emphemeris *emph_data,
                                struct filter_info *filter_data,
                                int Slot_Num,
				time_t time_now) {

	FILE *output;
	char pnmtopng_cmd[255];
	char write_metadata_fits[255];
	int sys_status = 0;
	int  i,errno_sys;
	char buffer[255]={0};
	char time_str[] = "20181118 032915";
	char filter[255] = {0};
	char filt_desc[255] = {0};
	char meta_str[MAX_LINE_LEN] = {0};
	char fw_temp_str[] = "25.00";
	char seqno[255] = {0};
	//Create file to attach metadata in
	FILE *meta_data;
	//Open file with "w"
	meta_data = fopen(META_FILE, "w");
	if (meta_data == NULL) {
    		perror("fopen()");
    		sprintf(buffer,"Failed to open file '%s'.\n",META_FILE);
    		SYSTEM_LOG(buffer);
    		return FALSE;
  	}
	//Build Meta Data in one large string and write to file
	//Insert Time Metadata
	strftime(time_str,sizeof(time_str), "%Y%m%d %H%M%S",time_now);
	strcpy(meta_str,"ExposureStart\t");
	strcat(meta_str,time_str);
	strcat(meta_str,"\n");
	//First Camera Parameters
	//Binning
	sprintf(buffer,"%d",camera->x_bin);
	strcat(meta_str,"Binning	");
	strcat(meta_str,buffer);
	strcat(meta_str,"\n");
	//Bit Depth
	strcat(meta_str,"BitDepth\t");
	sprintf(buffer,"%d",camera->bit_depth);
	strcat(meta_str,buffer);
	strcat(meta_str,"\n");
	//CCD Gain
	strcat(meta_str,"CCDGain\t");
        sprintf(buffer,"%d",camera->CCD_Gain);
        strcat(meta_str,buffer);
        strcat(meta_str,"\n");
	//CCD Temperature
	strcat(meta_str,"CCDTemp\t");
        sprintf(buffer,"%.2f",camera->Temperature);
        strcat(meta_str,buffer);
        strcat(meta_str,"\n");
	//Exposure
	strcat(meta_str,"Exposure\t");
        sprintf(buffer,"%.2f",camera->ExposureTime);
        strcat(meta_str,buffer);
        strcat(meta_str," ms\n");
	//Readout Speed
	strcat(meta_str,"ReadoutSpeed\t");
        if(camera->ADC_Speed == 2.0){
        	strcat(meta_str,"2 MHz\n");
        } else if(camera->ExposureTime == 0.1){
		strcat(meta_str,"100 KHz\n");
	}
	strcat(meta_str,"SEQNO\t");
        sprintf(seqno,"%d",Get_Seqno());
        strcat(meta_str,seqno);
        strcat(meta_str,"\n");
	//Station data
	//Site ID
	strcat(meta_str,"SiteID\t");
        strcat(meta_str,station_data->site_id);
        strcat(meta_str,"\n");
	//Device ID
	strcat(meta_str,"DeviceID\t");
        strcat(meta_str,camera->device_id);
        strcat(meta_str,"\n");
	//Latitude
	strcat(meta_str,"Latitude\t");
        sprintf(buffer,"%.2f",station_data->latitude);
        strcat(meta_str,buffer);
        strcat(meta_str,"\n");
	//Longitude
	strcat(meta_str,"Longitude\t");
        sprintf(buffer,"%.2f",station_data->longitude);
        strcat(meta_str,buffer);
        strcat(meta_str,"\n");
	//Filter Informatin
	//WaveLegnth
	strcat(meta_str,"FilterWavelength\t");
	strcpy(filter, filter_data[schedule_slot[Slot_Num].filter_num-1].wavelength);
	strcat(meta_str,filter);
	strcat(meta_str,"\n");
	//Filter Position
	strcat(meta_str,"FilterPosition\t");
        sprintf(buffer,"%d",filter_data[schedule_slot[Slot_Num].filter_num-1].filter_slot_number);
        strcat(meta_str,buffer);
        strcat(meta_str,"\n");
	//Filter Desicription
	strcat(meta_str,"FilterDescription\t");
        strcpy(filt_desc,(filter_data[schedule_slot[Slot_Num].filter_num-1].filt_descr));
        //printf("Filt Description = %s\n",filter_data[schedule_slot[Slot_Num].filter_num-1].filt_descr);
	strcat(meta_str,filt_desc);
        //strcat(meta_str,"\n");
	//Filter Wheel Temperature
	strcat(meta_str,"FWTemp\t");
        sprintf(fw_temp_str,"%.2f",schedule_slot->fw_temperature);
	strcat(meta_str,fw_temp_str);
        strcat(meta_str,"\n");
	//Version (Hard Coded)
	strcat(meta_str,"Version\t");
        strcat(meta_str,SOFTWARE_VERSION);
        strcat(meta_str,"\n");

	//printf("Meta Data String:\n %s\n",meta_str);

	//Write meta data to file
	//fputs(meta_str,meta_data);
	fprintf(meta_data,"%s",meta_str);
	//Close File
	fclose(meta_data);
	
	//Only allows for png image creation.
	camera->image_type = 1;

	//Switch between image types
	switch(camera->image_type){
		case 0:{
			//Attach suffix to filename relevant to the file format
			(void)strcat(filename,".fits");
			strcpy(pnmtopng_cmd, "rawtopgm -bpp 2 -littleendian ");
                        sprintf(buffer,"%d",camera->ccd_width/camera->x_bin);
                        strcat(pnmtopng_cmd, buffer);
                        strcat(pnmtopng_cmd," ");
                        sprintf(buffer,"%d",camera->ccd_height/camera->y_bin);
                        strcat(pnmtopng_cmd, buffer);
			strcat(pnmtopng_cmd," | pnmtopng | pngtopam | pamtofits");
			strcat(pnmtopng_cmd," > ");
                        strcat(pnmtopng_cmd, FITS_FILE);
			
			//Write png image to image.raw for temp purposes
			output = popen(pnmtopng_cmd, "w");
                        if (!output){
                                SYSTEM_LOG("Build_Image(): Failed to open pipe to pnmtopng.");
                                return FALSE;
                        }

			fwrite(camera->data.initial_readout, 1, camera->readoutstride, output);
			
			if (pclose (output) != 0){
                                SYSTEM_LOG("Image_To_PNG(): Failed to close pipe to pnmtopng.");
                                return FALSE;
                        }

			//Build command to add metadata into fits image.
			strcpy(write_metadata_fits, "/usr/local/funtools/bin/funhead ");
			strcat(write_metadata_fits, FITS_FILE);
			strcat(write_metadata_fits, " ");
			strcat(write_metadata_fits, filename);
			strcat(write_metadata_fits, " ");
			strcat(write_metadata_fits, "/usr/local/imagerd_rt/aux/meta-data.txt"); 
			sys_status = system(write_metadata_fits);
			if(sys_status != 0){
				SYSTEM_LOG("Build_Image(): Failed to add metadata into fits image");
				return FALSE;
			}
			//printf("Return Code from system call: %d\n",sys_status);
			break;
		}
		case 1:{

			//Attach suffix to filename relevant to the file format
			(void)strcat(filename,".png");


			strcpy(pnmtopng_cmd, "rawtopgm -bpp 2 -littleendian ");
			sprintf(buffer,"%d",camera->ccd_width/camera->x_bin);
			strcat(pnmtopng_cmd, buffer);
			strcat(pnmtopng_cmd," ");
			sprintf(buffer,"%d",camera->ccd_height/camera->y_bin);
			strcat(pnmtopng_cmd, buffer);
			//strcat(pnmtopng_cmd," | pnmtopng");
			strcat(pnmtopng_cmd," | pnmtopng -text=/usr/local/imagerd_rt/aux/meta-data.txt");


			//strcat(pnmtopng_cmd, buffer);
			strcat(pnmtopng_cmd," > ");
			strcat(pnmtopng_cmd, filename);

			//fprintf(stderr,"command %s\n",pnmtopng_cmd);

			output = popen(pnmtopng_cmd, "w");
			if (!output){
				SYSTEM_LOG("Build_Image(): Failed to open pipe to pnmtopng.");
				return FALSE;
			}

			//Write Image data to pipe and pgm file
			fwrite(camera->data.initial_readout, 1, camera->readoutstride, output);

			//Close pipe
			if (pclose (output) != 0){
				SYSTEM_LOG("Image_To_PNG(): Failed to close pipe to pnmtopng.");
				return FALSE;
			}
			break;
		}
		default:
			SYSTEM_LOG("ERROR: Bad Image Type. Change in imager.conf");
			break;
	}
	return(TRUE);
}
