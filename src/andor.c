/*
 *  andor.c
 * 
 *	Camera Plugin for Andor 2.0 SDK Camera
 * 
 *  Copyright 2015 - Jarrett Little
 *
 */

 //Custom Library Includes
#include "andor.h"


int Init(void){
	char str[MAX_STRING_LEN]  = {0};
	
	
	int error = Initialize_CCD(ANDOR_PATH);
  
  	if (error != TRUE) {
    	sprintf(str,"Init(): Failed to initialize the SDK Library: %s",error);
		SYSTEM_LOG(str);
		return(FALSE);
  	}
  
	//Log that the ANDOR Library init is completed.
 	SYSTEM_LOG("Initialized ANDOR Library");
	return(TRUE);
}


int OpenCamera(struct Camera *camera){

	//Open Camera -> Doesn't exist in the SDK 2.0 -> Returns True for compatiblity.
 	SYSTEM_LOG("Opened ANDOR Camera");
	return(TRUE);
}


int CloseCamera(struct Camera *camera){

	int error = ShutDown();
	if(error != DRV_SUCCESS){
		SYSTEM_LOG("CloseCamera(): Failed to close camera:");
		return(FALSE);
	}

	SYSTEM_LOG("Shutdown and closed ANDOR camera");
	return(TRUE);
}

/*
 * Safe Exit when ANDOR Errors are detected.
 */
void SafeExit(struct Camera *camera, int error){
	char error_str[MAX_STRING_LEN]  = {0};
	sprintf(error_str,"Error Code Detected: %d -> Safe Exit\n",error);
	SYSTEM_LOG(error_str);
	CloseCamera(camera);
}


int Initialize_Image_Pixels(struct Camera *camera){

	camera->pixels = malloc(((camera->ccd_width/camera->x_bin) * (camera->ccd_height/camera->y_bin)) * sizeof(int32_t));
	if(camera->pixels == NULL){
		SYSTEM_LOG("Initialize_Image_Pixels(): Failed to allocate memory.");
		return(FALSE);
	}
	SYSTEM_LOG("Initialize_Image_Pixels(): Generated memory to hold acquired data");
	return(TRUE);
}

void Delete_Image_Pixels(struct Camera *camera){
	if(camera->pixels){
		free(camera->pixels);
		SYSTEM_LOG("Delete_Image_Pixels(): Freed memory for acquired data");
	}
}

/*
 * Prints out the current configuration parameters
 */
int PrintParameters(struct Camera *camera){
	char str[255] = {0};

	
	SYSTEM_LOG("\n");
  	SYSTEM_LOG("Imager Configuration Settings ");
  	SYSTEM_LOG("=============================");

	//Print out standard value parameters - no enumeration types
  	sprintf(str,"Temperature Setpoint:       %.2f [Celsius]",camera->Temp_SetPoint);
  	SYSTEM_LOG(str);
  	
  	//Image Type
  	switch(camera->image_type){
    	case 0: SYSTEM_LOG("Image Output Type:          TIFF"); break;
    	case 1: SYSTEM_LOG("Image Output Type: 		BMP"); break;
    	case 2: SYSTEM_LOG("Image Output Type: 		PNG"); break;
    	case 3: SYSTEM_LOG("Image Output Type: 		JPG"); break;
    	default: SYSTEM_LOG("Image Output Type: 	 Error No Valid Image Type Found"); break;
  	}

  	//ReadOutCtrlMode
  	switch(camera->ReadOutCtrlMode){
    	case 0: SYSTEM_LOG("Read-out Control Mode:      Full Verticle Binning"); break;
    	case 1: SYSTEM_LOG("Read-out Control Mode:      Multi Track"); break;
    	case 2: SYSTEM_LOG("Read-out Control Mode:      Random Track"); break;
    	case 3: SYSTEM_LOG("Read-out Control Mode:      Single Track"); break;
    	case 4: SYSTEM_LOG("Read-out Control Mode:      Image"); break;
    	default: SYSTEM_LOG("Read-out Control Mode:      Error No Valid Readout Mode Found"); break;
  	}
  	//acquisition_mode
  	switch(camera->acquisition_mode){
    	case 1: SYSTEM_LOG("Acquisition Mode:           Single Scan"); break;
    	case 2: SYSTEM_LOG("Acquisition Mode:           Accumulate"); break;
    	case 3: SYSTEM_LOG("Acquisition Mode:           Kinetics"); break;
    	case 4: SYSTEM_LOG("Acquisition Mode:           Fast Kinetics"); break;
    	case 5: SYSTEM_LOG("Acquisition Mode:           Run Till Abort"); break;
    	default: SYSTEM_LOG("Acquisition Mode:           Error No Valid Acquisition Mode Found"); break;
  	}

  	//fan_mode
  	switch(camera->fan_mode){
    	case 0: SYSTEM_LOG("Fan Mode:                   Fan On Full"); break;
    	case 1: SYSTEM_LOG("Fan Mode:                   Fan On Low"); break;
    	case 2: SYSTEM_LOG("Fan Mode:                   Fan Off"); break;
    	default: SYSTEM_LOG("Fan Mode:                   Error No Valid Fan Mode Found"); break;
  	}

  	//cooling on/off
  	switch(camera->cooling){
    	case 0: SYSTEM_LOG("Cooling:                    ON"); break;
    	case 1: SYSTEM_LOG("Cooling:                    OFF"); break;
    	default: SYSTEM_LOG("Fan Mode:                   Error No Valid Cooling State Found"); break;
  	}

  	//AD Channel
  	sprintf(str,"AD Channel:                 %d",camera->ad_channel);
  	SYSTEM_LOG(str);

  	//HS Speed
  	sprintf(str,"HS Speed:                   %d",camera->hs_speed);
  	SYSTEM_LOG(str);

  	//Pre Amp Gain
  	sprintf(str,"Pre-Amp Gain:               %d",camera->CCD_Gain);
  	SYSTEM_LOG(str);

  	//VS Speed
  	sprintf(str,"VS Speed:                   %d",camera->vs_speed);
  	SYSTEM_LOG(str);

  	//Vertical Clock Voltage Amplitude 
  	switch(camera->vs_amplitude){
    	case 0: SYSTEM_LOG("VS Amplitude:               Normal"); break;
    	case 1: SYSTEM_LOG("VS Amplitude:               +1"); break;
    	case 2: SYSTEM_LOG("VS Amplitude:               +2"); break;
    	case 3: SYSTEM_LOG("VS Amplitude:               +3"); break;
    	case 4: SYSTEM_LOG("VS Amplitude:               +4"); break;
    	default: SYSTEM_LOG("VS Amplitude:               Error No Valid VS Amplitude Found"); break;
  	}

  	//High Capacity on/off
  	switch(camera->high_capacity){
    	case 0: SYSTEM_LOG("High Capacity:              DISABLED"); break;
    	case 1: SYSTEM_LOG("High Capacity:              ENABLED"); break;
    	default: SYSTEM_LOG("High Capacity:              Error No Valid High Capacity State Found"); break;
  	}

	  //Frame Transfer on/off
  	switch(camera->frame_transfer){
    	case 0: SYSTEM_LOG("Frame Transfer:             DISABLED"); break;
    	case 1: SYSTEM_LOG("Frame Transfer:             ENABLED"); break;
    	default: SYSTEM_LOG("Frame Transfer:             Error No Valid Frame Transfer State Found"); break;
  	}

    //Shutter Mode
    switch(camera->Shutter_Mode){
      case 0: SYSTEM_LOG("Shutter Mode:               AUTO"); break;
      case 1: SYSTEM_LOG("Shutter Mode:               OPEN"); break;
      case 2: SYSTEM_LOG("Shutter Mode:               CLOSE"); break;
      default: SYSTEM_LOG("Shutter Mode:               No Valid Shutter Mode Found"); break;
    }

  	SYSTEM_LOG("\n");
  	SYSTEM_LOG("Imager Region of Interest Settings ");
  	SYSTEM_LOG("==================================");

  	//Horizontal Size
  	sprintf(str,"Horizontal Size: %d -> %d [Pixels]",camera->h_start,camera->h_size);
  	SYSTEM_LOG(str);
 
  	//Vertical Size
  	sprintf(str,"Vertical Size:   %d -> %d [Pixels]",camera->v_start,camera->v_size);
  	SYSTEM_LOG(str);

  	SYSTEM_LOG("\n");
  	SYSTEM_LOG("End Of Configuration Settings ");
  	SYSTEM_LOG("=============================");

	return(TRUE);
}

/*
 * Prints out the current camera parameters after the intial check
 */
int PrintCurParams(struct Current_Params *params){
	char str[255] = {0};

	
	SYSTEM_LOG("\n");
  	SYSTEM_LOG("Imager Current Configuration Settings ");
  	SYSTEM_LOG("=====================================");


		//Print out standard value parameters - no enumeration types
  	sprintf(str,"Temperature Setpoint:       %.2f [Celsius]",params->Temp_SetPoint);
  	SYSTEM_LOG(str);
  	
  	//Image Type
  	switch(params->image_type){
    	case 0: SYSTEM_LOG("Image Output Type:          TIFF"); break;
    	case 1: SYSTEM_LOG("Image Output Type: 	        BMP"); break;
    	case 2: SYSTEM_LOG("Image Output Type:          PNG"); break;
    	case 3: SYSTEM_LOG("Image Output Type:          JPG"); break;
    	default: SYSTEM_LOG("Image Output Type:          Error No Valid Image Type Found"); break;
  	}

  	//ReadOutCtrlMode
  	switch(params->ReadOutCtrlMode){
    	case 0: SYSTEM_LOG("Read-out Control Mode:      Full Verticle Binning"); break;
    	case 1: SYSTEM_LOG("Read-out Control Mode:      Multi Track"); break;
    	case 2: SYSTEM_LOG("Read-out Control Mode:      Random Track"); break;
    	case 3: SYSTEM_LOG("Read-out Control Mode:      Single Track"); break;
    	case 4: SYSTEM_LOG("Read-out Control Mode:      Image"); break;
    	default: SYSTEM_LOG("Read-out Control Mode:      Error No Valid Readout Mode Found"); break;
  	}
  	//acquisition_mode
  	switch(params->acquisition_mode){
    	case 1: SYSTEM_LOG("Acquisition Mode:           Single Scan"); break;
    	case 2: SYSTEM_LOG("Acquisition Mode:           Accumulate"); break;
    	case 3: SYSTEM_LOG("Acquisition Mode:           Kinetics"); break;
    	case 4: SYSTEM_LOG("Acquisition Mode:           Fast Kinetics"); break;
    	case 5: SYSTEM_LOG("Acquisition Mode:           Run Till Abort"); break;
    	default: SYSTEM_LOG("Acquisition Mode:           Error No Valid Acquisition Mode Found"); break;
  	}

  	//fan_mode
  	switch(params->fan_mode){
    	case 0: SYSTEM_LOG("Fan Mode:                   Fan On Full"); break;
    	case 1: SYSTEM_LOG("Fan Mode:                   Fan On Low"); break;
    	case 2: SYSTEM_LOG("Fan Mode:                   Fan Off"); break;
    	default: SYSTEM_LOG("Fan Mode:                   Error No Valid Fan Mode Found"); break;
  	}

  	//cooling on/off
  	switch(params->cooling){
    	case 0: SYSTEM_LOG("Cooling:                    ON"); break;
    	case 1: SYSTEM_LOG("Cooling:                    OFF"); break;
    	default: SYSTEM_LOG("Fan Mode:                   Error No Valid Cooling State Found"); break;
  	}

  	//AD Channel
  	sprintf(str,"AD Channel:                 %d",params->ad_channel);
  	SYSTEM_LOG(str);

  	//HS Speed
  	sprintf(str,"HS Speed:                   %d",params->hs_speed);
  	SYSTEM_LOG(str);

  	//Pre Amp Gain
  	sprintf(str,"Pre-Amp Gain:               %d",params->CCD_Gain);
  	SYSTEM_LOG(str);

  	//VS Speed
  	sprintf(str,"VS Speed:                   %d",params->vs_speed);
  	SYSTEM_LOG(str);

  	//Vertical Clock Voltage Amplitude 
  	switch(params->vs_amplitude){
    	case 0: SYSTEM_LOG("VS Amplitude:               Normal"); break;
    	case 1: SYSTEM_LOG("VS Amplitude:               +1"); break;
    	case 2: SYSTEM_LOG("VS Amplitude:               +2"); break;
    	case 3: SYSTEM_LOG("VS Amplitude:               +3"); break;
    	case 4: SYSTEM_LOG("VS Amplitude:               +4"); break;
    	default: SYSTEM_LOG("VS Amplitude:               Error No Valid VS Amplitude Found"); break;
  	}

  	//High Capacity on/off
  	switch(params->high_capacity){
    	case 0: SYSTEM_LOG("High Capacity:              DISABLED"); break;
    	case 1: SYSTEM_LOG("High Capacity:              ENABLED"); break;
    	default: SYSTEM_LOG("High Capacity:              Error No Valid High Capacity State Found"); break;
  	}

	//Frame Transfer on/off
  	switch(params->frame_transfer){
    	case 0: SYSTEM_LOG("Frame Transfer:             DISABLED"); break;
    	case 1: SYSTEM_LOG("Frame Transfer:             ENABLED"); break;
    	default: SYSTEM_LOG("Frame Transfer:             Error No Valid Frame Transfer State Found"); break;
  	}

  	SYSTEM_LOG("\n");
  	SYSTEM_LOG("Imager Region of Interest Settings ");
  	SYSTEM_LOG("==================================");

  	//Horizontal Size
  	sprintf(str,"Horizontal Size: %d -> %d [Pixels]",params->h_start,params->h_size);
  	SYSTEM_LOG(str);
 
  	//Vertical Size
  	sprintf(str,"Vertical Size:   %d -> %d [Pixels]",params->v_start,params->v_size);
  	SYSTEM_LOG(str);

	SYSTEM_LOG("\n");
  	SYSTEM_LOG("End Of Current Configuration Settings ");
  	SYSTEM_LOG("=============================");

	return(TRUE);
}

int PrintConfigParameters(struct Camera *camera){

	//Print out all possible settings for the camera head attached.
	SYSTEM_LOG("\n");
	SYSTEM_LOG("========================================");
  	SYSTEM_LOG("Possible Imager Configuration Parameters ");
  	SYSTEM_LOG("========================================");

	
	Print_AD_Channels();
	
	Print_Available_VS_Speeds();
	
	Print_Available_HS_Settings();
	
	Print_Available_VS_Amplitudes();
	
	//Print_Available_Pre_Amp_Gains(int ad_channel,int output_amp,int hs_speed);

	SYSTEM_LOG("\n");
	SYSTEM_LOG("===============================================");
  	SYSTEM_LOG("End of Possible Imager Configuration Parameters ");
  	SYSTEM_LOG("===============================================");


	return TRUE;
}


/*
 * Retrieves current configuration from camera and checks parameters that where loaded into 
 * the configration file.
 */
int GetParamInfo(struct Camera *camera, struct Current_Params *params){

	int available_pre_amp_gain_index;
  	int hpixels, vpixels;
  	int bin_ok, valid_bin;
  	int size_ok, valid_size;
  	int silent = TRUE;
  	char line_buffer[255] = {0}; 
  
	//Print possible configuration values 
  	if(PrintConfigParameters(camera) != TRUE){
  		SYSTEM_LOG("GetParamInfo(): Error retrieving possible camera values... Continuing with parameter check.");
  	}
  	//Print header for checking parameters
  	SYSTEM_LOG("\n");
	SYSTEM_LOG("===============================================");
  	SYSTEM_LOG("Starting Imager Configuration Parameters Checks");
  	SYSTEM_LOG("===============================================");

  	//Fan mode
  	if (camera->fan_mode < FAN_MODE_MIN || camera->fan_mode > FAN_MODE_MAX) {
    	sprintf(line_buffer,"Check_Camera_Settings(): Invalid fan mode %d. Using default mode %d.\n",camera->fan_mode,DEFAULT_FAN_MODE);
    	SYSTEM_LOG(line_buffer);
    	camera->fan_mode = DEFAULT_FAN_MODE;
  	}else{
		SYSTEM_LOG("Fan Mode ----------------- Success");
	}
	//Get fan mode from camera and set to current_params structure:
  	params->fan_mode = camera->fan_mode;
  
  	//Cooling
  	if (camera->cooling != COOLING_ON && camera->cooling != COOLING_OFF) {
    	sprintf(line_buffer,"Check_Camera_Settings(): Invalid cooling mode %d. ",camera->cooling);
    	SYSTEM_LOG(line_buffer);
    	if (DEFAULT_COOLING == COOLING_ON) {
      		SYSTEM_LOG("Using default mode cooling ON.\n");
    	} else {
      		SYSTEM_LOG("Using default mode cooling OFF.\n");
    	}
    	camera->cooling = DEFAULT_COOLING;
  	}else{
		SYSTEM_LOG("Cooling ------------------ Success");
	}
	//Get cooling status
  	//if(Get_Cooler_State(params->cooling) != TRUE){
  	//	SYSTEM_LOG("Could not get cooling status.");
  	//}
  
  	//Frame transfer
  	if (camera->frame_transfer != FRAME_TRANSFER_ON && camera->frame_transfer != FRAME_TRANSFER_OFF) {
    	sprintf(line_buffer,"Check_Camera_Settings(): Invalid frame transfer mode %d. ",camera->frame_transfer);
    	SYSTEM_LOG(line_buffer);
    	if (DEFAULT_FRAME_TRANSFER == FRAME_TRANSFER_ON) {
      		SYSTEM_LOG("Using default mode frame transfer ENABLED.\n");
    	}else{
      		SYSTEM_LOG("Using default mode frame transfer DISABLED.\n");
    	}
    	camera->frame_transfer = DEFAULT_FRAME_TRANSFER;
  	}else{
		SYSTEM_LOG("Frame Transfer ----------- Success");
	}
	params->frame_transfer = camera->frame_transfer;

  	//High capacity
  	if (camera->high_capacity != HIGH_CAPACITY_ON && camera->high_capacity != HIGH_CAPACITY_OFF) {
    	sprintf(line_buffer,"Check_Camera_Settings(): Invalid high capacity mode %d. ",camera->high_capacity);
    	SYSTEM_LOG(line_buffer);
    	if (DEFAULT_HIGH_CAPACITY == HIGH_CAPACITY_ON) {
      		SYSTEM_LOG("Using default mode high_capacity ENABLED.\n");
    	} else {
      		SYSTEM_LOG("Using default mode high_capacity DISABLED.\n");
    	}
    	camera->high_capacity = DEFAULT_HIGH_CAPACITY;
  	}else{
		SYSTEM_LOG("High Capacity ------------ Success");
	}
	params->high_capacity = camera->high_capacity;

	// Temperature
  	if (Check_Temperature(camera->Temp_SetPoint) != TRUE) {
    	sprintf(line_buffer,"Check_Camera_Settings(): Using default temeprature %d.",DEFAULT_TEMPERATURE);
    	SYSTEM_LOG(line_buffer);
    	camera->Temp_SetPoint = DEFAULT_TEMPERATURE;
  	}else{
		SYSTEM_LOG("Camera Temperature ------- Success");
	}
	params->Temp_SetPoint = camera->Temp_SetPoint;

	// A/D channel
  	if (Check_AD_Channel(camera->ad_channel) != TRUE){
    	sprintf(line_buffer,"Check_Camera_Settings(): Using default AD channel %d.",DEFAULT_AD_CHANNEL);
    	SYSTEM_LOG(line_buffer);
    	camera->ad_channel = DEFAULT_AD_CHANNEL;
  	}else{
		SYSTEM_LOG("AD Channel --------------- Success");
	}
	params->ad_channel = camera->ad_channel;

  	//Output amplifier
  	if (Check_Output_Amp(camera->output_amp) != TRUE) {
    	sprintf(line_buffer,"Check_Camera_Settings(): Using default output amplifier %d.",DEFAULT_OUTPUT_AMPLIFIER);
    	SYSTEM_LOG(line_buffer);
    	camera->output_amp = DEFAULT_OUTPUT_AMPLIFIER;
  	}else{
		SYSTEM_LOG("Output Amp --------------- Success");
	}
	params->output_amp = camera->output_amp;

	//Horizontal shift speed
  	if (Check_HS_Speed(camera->hs_speed,camera->ad_channel,camera->output_amp) != TRUE){
    	sprintf(line_buffer,"Check_Camera_Settings(): Using default horizontal shift speed %d.",DEFAULT_HS_SPEED);
    	SYSTEM_LOG(line_buffer);
    	camera->hs_speed = DEFAULT_HS_SPEED;
  	}else{
		SYSTEM_LOG("HS Speed ----------------- Success");
	}
	if(Get_HS_Speed(camera->ad_channel, camera->output_amp, camera->hs_speed, &camera->hs_speed_MHz) != TRUE){
		SYSTEM_LOG("Could not obtain hs speed in MHz.");
	}
	params->hs_speed = camera->hs_speed;
	//Speed in MHz	
	params->hs_speed_MHz = camera->hs_speed_MHz;

  	//Pre-amplifier gain (CCD_Gain)
  	if (Check_Pre_Amp_Gain(camera->CCD_Gain,camera->ad_channel,camera->output_amp,camera->hs_speed,&available_pre_amp_gain_index) != TRUE){
    	sprintf(line_buffer,"Check_Camera_Settings(): Using pre-amplifier gain %d.",available_pre_amp_gain_index);
    	SYSTEM_LOG(line_buffer);
    	camera->CCD_Gain = available_pre_amp_gain_index;
  	}
  	params->CCD_Gain = camera->CCD_Gain;
  
  	//Vertical shift speed
  	if (Check_VS_Speed(camera->vs_speed) != TRUE) {
    	sprintf(line_buffer,"Check_Camera_Settings(): Using default vertical shift speed %d.",DEFAULT_VS_SPEED);
    	SYSTEM_LOG(line_buffer);
    	camera->vs_speed = DEFAULT_VS_SPEED;
  	}else{
		SYSTEM_LOG("VS Speed ----------------- Success");
	}
	//Speed in microseconds per pixel shift
	if(Get_VS_Speed(camera->vs_speed, &camera->vs_speed_usp) != TRUE){
		SYSTEM_LOG("Could not obtain vs speed in microseconds/pixel shift.");
	}
	params->vs_speed = camera->vs_speed;
	params->vs_speed_usp = camera->vs_speed_usp;

  	// Vertical clock voltage amplitude
  	if (Check_VS_Amplitude(camera->vs_amplitude,camera->vs_speed) != TRUE) {
    	sprintf(line_buffer,"Check_Camera_Settings(): Using default vertical shift speed %d.",DEFAULT_VS_SPEED);
    	SYSTEM_LOG(line_buffer);
    	sprintf(line_buffer,"Check_Camera_Setting(): Using default vertical clock voltage amplitude %d.",DEFAULT_VS_AMPLITUDE);
    	SYSTEM_LOG(line_buffer);
    	camera->vs_speed = DEFAULT_VS_SPEED;
    	camera->vs_amplitude = DEFAULT_VS_AMPLITUDE;
  	}else{
		SYSTEM_LOG("VS Amplitude ------------- Success");
	}
	
	params->vs_amplitude = camera->vs_amplitude;
	params->vs_amp_value = camera->vs_amp_value;

	params->ReadOutCtrlMode = camera->ReadOutCtrlMode;
	params->acquisition_mode = camera->acquisition_mode;

	params->h_size = camera->h_size;
	params->v_size = camera->v_size;
	params->h_start = camera->h_start;
	params->v_start = camera->v_start;


	params->image_type = camera->image_type;	

	//Check all setting against the ad_channel
	Check_Settings_Against_AD_Channel(camera);
	//Check all settings against the output amp
	Check_Settings_Against_Output_Amplifier(camera);

  	//Get the detector size
  	if (Get_Detector(&hpixels,&vpixels) != TRUE) {
    	SYSTEM_LOG("Check_Camera_Settings(): Failed to get the detetctor size. Aborting\n");
    	return FALSE;
  	}

  	//Eventually need to check the ROI for the image!
  	//Print header for checking parameters
  	SYSTEM_LOG("\n");
	SYSTEM_LOG("===============================================");
  	SYSTEM_LOG("Finished Imager Configuration Parameters Checks");
  	SYSTEM_LOG("===============================================");


  	//Print out all the current parameters to log
  	if(PrintParameters(camera) != TRUE){
  		SYSTEM_LOG("Check_Camera_Settings(): Failed to print out parameter values... Aborting\n");
    	return FALSE;
  	}

  	if(PrintCurParams(params) != TRUE){
  		SYSTEM_LOG("Check_Camera_Settings(): Failed to print out current parameter values... Aborting\n");
    	return FALSE;
  	}

  	return(TRUE);
}

/*
 * Check camera settings against A/D channel.
 */
void Check_Settings_Against_AD_Channel(struct Camera *camera) {
	char str[MAX_STRING_LEN]  = {0};
 	int available_pre_amp_gain_index;

  	// Check the availability of the output amplifier against the new A/D channel

  	if (Check_Output_Amp(camera->output_amp) != TRUE) {
    	sprintf(str,"Check_Settings_Against_AD_Channel(): Using default amplifier type.");
    	SYSTEM_LOG(str);
    	camera->output_amp = DEFAULT_OUTPUT_AMPLIFIER;
  	}

  	// Check the horizontal shift speed against the A/D channel

  	if (Check_HS_Speed(camera->hs_speed, camera->ad_channel, camera->output_amp) != TRUE){
    	sprintf(str,"Check_Settings_Against_AD_Channel(): Using default horizontal shift speed %d.",DEFAULT_HS_SPEED);
    	SYSTEM_LOG(str);
    	camera->hs_speed = DEFAULT_HS_SPEED;
  	}

  	// Check the pre-amplifier gain against the A/D channel

  	if (Check_Pre_Amp_Gain(camera->CCD_Gain,camera->ad_channel,camera->output_amp,camera->hs_speed,&available_pre_amp_gain_index) != TRUE) {
    	sprintf(str,"Check_Settings_Against_AD_Channel(): Using pre-amplifier gain %d.",available_pre_amp_gain_index);
    	SYSTEM_LOG(str);
    	camera->CCD_Gain = available_pre_amp_gain_index;
  	}
}

/*
 * Check camera settings against output amplifier type.
 */
void Check_Settings_Against_Output_Amplifier(struct Camera *camera) {
	char str[MAX_STRING_LEN]  = {0};
  	int available_pre_amp_gain_index;

  	// Check the horizontal shift speed against the new output amplifier

  	if (Check_HS_Speed(camera->hs_speed,camera->ad_channel,camera->output_amp) != TRUE){
    	sprintf(str,"Check_Settings_Against_Output_Amplifier(): Using default horizontal shift speed %d.",DEFAULT_HS_SPEED);
    	SYSTEM_LOG(str);
    	camera->hs_speed = DEFAULT_HS_SPEED;
  	}

  	// Check the pre-amplifier gain against the new output amplifier

  	if (Check_Pre_Amp_Gain(camera->CCD_Gain,camera->ad_channel,camera->output_amp,camera->hs_speed,&available_pre_amp_gain_index) != TRUE) {
    	sprintf(str,"Check_Settings_Against_Output_Amplifier(): Using pre-amplifier gain %d.",available_pre_amp_gain_index);
    	SYSTEM_LOG(str);
    	camera->CCD_Gain = available_pre_amp_gain_index;
  	}
}


int Configure_Static(struct Camera *camera){

	char str[MAX_STRING_LEN]  = {0};
	int error = FALSE;
	int occured_error = FALSE;

	SYSTEM_LOG("==================================");
	SYSTEM_LOG("Configuring Static Parameters");
	SYSTEM_LOG("==================================");

	//Set the Default for output amplifier
	camera->output_amp = DEFAULT_OUTPUT_AMPLIFIER;
	//camera->output_amp = 0;
	//Set static camera parameters with this specific order 
	error = Set_Temperature(camera->Temp_SetPoint);
	if(error != TRUE){
		error = Set_Temperature(DEFAULT_TEMPERATURE);
		SYSTEM_LOG("Set_Temperature(): Error -> Using default value");
		occured_error = TRUE;
	}

	error = Set_Read_Mode(camera->ReadOutCtrlMode);
	if(error != TRUE){
		error = Set_Read_Mode(DEFAULT_READOUT_MODE);
		SYSTEM_LOG("Set_Read_Mode(): Error -> Using default value");
		occured_error = TRUE;
	}
	
	error = Set_Acquisition_Mode(camera->acquisition_mode);
	if(error != TRUE){
		error = Set_Acquisition_Mode(DEFAULT_ACQUISITION_MODE);
		SYSTEM_LOG("Set_Acquisition_Mode(): Error -> Using default value");
		occured_error = TRUE;
	}
	
	error = Set_Frame_Transfer(camera->frame_transfer);
  	if(error != TRUE){
		error = Set_Frame_Transfer(DEFAULT_FRAME_TRANSFER);
		SYSTEM_LOG("Set_Frame_Transfer(): Error -> Using default value");
		occured_error = TRUE;
	}
	
	
	error = Set_Cooler_Mode(camera->cooling);
	if(error != TRUE){
		error = Set_Cooler_Mode(DEFAULT_COOLING);
		SYSTEM_LOG("Set_Cooler_Mode(): Error -> Using default value");
		occured_error = TRUE;
	}
	

	error = Set_Fan_Mode(camera->fan_mode);
	if(error != TRUE){
		error = Set_Fan_Mode(DEFAULT_FAN_MODE);
		SYSTEM_LOG("Set_Fan_Mode(): Error -> Using default value");
		occured_error = TRUE;
	}

	error = Set_High_Capacity(camera->high_capacity);
	if(error != TRUE){
		error = Set_High_Capacity(DEFAULT_HIGH_CAPACITY);
		SYSTEM_LOG("Set_High_Capacity(): Error -> Using default value");
		occured_error = TRUE;
	}

	error = Set_AD_Channel(camera->ad_channel);
	if(error != TRUE){
		error = Set_AD_Channel(DEFAULT_AD_CHANNEL);
		SYSTEM_LOG("Set_AD_Channel(): Error -> Using default value");
		occured_error = TRUE;
	}

	error = Set_Output_Amplifier(camera->output_amp);
	if(error != TRUE){
		error = Set_Output_Amplifier(DEFAULT_OUTPUT_AMPLIFIER);
		SYSTEM_LOG("Set_Output_Amplifier(): Error -> Using default value");
		occured_error = TRUE;
	}
  
	error = Set_HS_Speed(camera->output_amp,camera->hs_speed);
	if(error != TRUE){
		error = Set_HS_Speed(camera->output_amp,DEFAULT_HS_SPEED);
		SYSTEM_LOG("Set_HS_Speed(): Error -> Using default value");
		occured_error = TRUE;
	}

	error = Set_VS_Speed(camera->vs_speed);
	if(error != TRUE){
		error = Set_VS_Speed(DEFAULT_VS_SPEED);
		SYSTEM_LOG("Set_VS_Speed(): Error -> Using default value");
		occured_error = TRUE;
	}

	error = Set_VS_Amplitude(camera->vs_amplitude);
	if(error != TRUE){
		error = Set_VS_Amplitude(DEFAULT_VS_AMPLITUDE);
		SYSTEM_LOG("Set_VS_Amplitude(): Error -> Using default value");
		occured_error = TRUE;
	}
	
	error = Set_Baseline_Clamp(0);
	if(error != TRUE){
		SYSTEM_LOG("Set_Baseline_Clamp(): Error -> Could not set the baseline clamp");
                occured_error = TRUE;
        }

	if(occured_error == TRUE){
		SYSTEM_LOG("Configure_Static(): Error setting one or more of the static parameters...");
		SYSTEM_LOG("Configure_Static(): Continuing with default parameters -> Check log file for paremeter error");
	}

	//Set up the ROI -> First get the actual ccd ROI
	//int* xpixels: number of horizontal pixels. int* ypixels: number of vertical pixels.
	error = Get_Detector(&camera->ccd_width,&camera->ccd_height);

	//Place sensor data into camera structure
	camera->width = camera->ccd_width;
	camera->height = camera->ccd_height;
	
  //Default Value for Binning
	camera->x_bin = camera->y_bin = 1;
	camera->h_end = camera->h_start - 1 + (camera->width  * camera->x_bin);
  	camera->v_end = camera->v_start - 1 + (camera->height * camera->y_bin);

	//Call malloc to initialize data memory
	//if(Initialize_Image_Pixels(camera) != TRUE){
	//	SYSTEM_LOG("Initialize_Image_Pixels(): Could not allocate memory for datas");
	//	return(FALSE);		
	//}

	/*	
		int hbin: number of pixels to bin horizontally. 
		int vbin: number of pixels to bin vertically. 
		int hstart: Start column (inclusive).
		int hend: End column (inclusive).
		int vstart: Start row (inclusive). 
		int vend: End row (inclusive).
	*/
	error = Set_Image(camera->x_bin,camera->y_bin,camera->h_start,camera->h_end,camera->v_start,camera->v_end);

	if(error == TRUE){
		//Setting the image in the static section fills the camera structure.  Get_Detector will
		//not have to be called again.
		sprintf(str,"x_bin=%d y_bin=%d h_start=%d v_start=%d width=%d height=%d h_end=%d v_end=%d\n",camera->x_bin,
					camera->y_bin,camera->h_start,camera->v_start,camera->width,camera->height,camera->h_end,camera->v_end);
		SYSTEM_LOG(str);
	}else{
		sprintf(str,"Configure_Static(): Failed to Set_Image(). Exiting Safely: ERROR: %d",error);
		SYSTEM_LOG(str);
		//SafeExit(camera, error);
		return(FALSE);
	}
	
	SYSTEM_LOG("Configure_Static(): Complete...");

	return(TRUE);
}


int Configure_Dynamic(struct Camera *camera){

	char str[MAX_STRING_LEN]  = {0};
	int error = FALSE;
	int occured_error = FALSE;
	float system_exposure;


	SYSTEM_LOG("==================================");
	SYSTEM_LOG("Configuring Dynamic Parameters");
	SYSTEM_LOG("==================================");

	//Set Exposure Time(Float Seconds)
	error = Set_Exposure_Time(((float)camera->ExposureTime)/1000);
	if(error != TRUE){
		error = Set_Exposure_Time(DEFAULT_EXPOSURE);
		SYSTEM_LOG("Set_Exposure_Time(): Error -> Using default value");
		occured_error = TRUE;
	}

	/*
	error = Set_Kinetic_Cycle_Time(((float)camera->ExposureTime)/1000);
	if(error != TRUE){
		error = Set_Kinetic_Cycle_Time(DEFAULT_EXPOSURE);
		SYSTEM_LOG("Set_Kinetic_Cycle_Time(): Error -> Using default value");
		occured_error = TRUE;
	}
	*/
	error = Set_Pre_Amp_Gain(camera->CCD_Gain);
	if(error != TRUE){
		error = Set_Pre_Amp_Gain(DEFAULT_PRE_AMP_GAIN);
		SYSTEM_LOG("Set_Pre_Amp_Gain(): Error -> Using default value");
		occured_error = TRUE;
	}

	if(occured_error == TRUE){
		SYSTEM_LOG("Configure_Dynamic(): Error setting one or more of the dynamic parameters...");
		SYSTEM_LOG("Configure_Dynamic(): Continuing with default parameters -> Check log file for paremeter error");
	}

	//Get the corrected exposure time. System will adjust depending on the above parameters
	error = Get_Exposure_Time(&system_exposure);



	//Set ROI for image slot
	
	camera->h_end = camera->h_start - 1 + (camera->width);
  camera->v_end = camera->v_start - 1 + (camera->height);

  	error = Set_Image(camera->x_bin,camera->y_bin,camera->h_start,camera->h_end,camera->v_start,camera->v_end);
	
	//SetImage(2,2,1,2048,1,2048);

	if(error == TRUE){
		//Setting the image in the static section fills the camera structure.  Get_Detector will
		//not have to be called again.
		sprintf(str,"x_bin=%d y_bin=%d h_start=%d v_start=%d width=%d height=%d h_end=%d v_end=%d\n",camera->x_bin,
					camera->y_bin,camera->h_start,camera->v_start,camera->width,camera->height,camera->h_end,camera->v_end);
		SYSTEM_LOG(str);
	}else{
		sprintf(str,"Configure_Dynamic(): Failed to Set_Image(). Exiting Safely: ERROR: %d",error);
		SYSTEM_LOG(str);
		SafeExit(camera, error);
		return(FALSE);
	}

	SYSTEM_LOG("Configure_Dynamic(): Complete...");

	return(TRUE);
}

int Acquire_Data(struct Camera *camera){

	int error;
	int status;
	char str[255] = {0};
  
	// Check driver status
	error = Get_Status(&status);
	if (error != TRUE) {
		SYSTEM_LOG("Acquire_Image(): Unable to start acquisition. Unable to get driver status or driver busy");
		return FALSE;
	}
	
	if (status != DRV_IDLE) {
    	SYSTEM_LOG("Acquire_Image(): Unable to start acquisition. Driver busy");
    	return FALSE;
  	}

#ifdef DEBUG
	SYSTEM_LOG("Start Acquisition");
#endif

  	// Start acquisition
  	error = Start_Acquisition();
  	if (error != TRUE) {
    	SYSTEM_LOG("Acquire_Image(): Failed to start acquisition");
   		return FALSE;
  	}
  
  	error = Get_Status(&status);
  	if (error != TRUE) {
    	SYSTEM_LOG("Acquire_Image(): Failed to get driver status.\n");
    	return FALSE;
  	}

#ifdef DEBUG
	SYSTEM_LOG("Entering Acquisition while loop DRV_ACQ");
#endif   
  	
  	// Loop until acquisition finished
  	while (status == DRV_ACQUIRING) {
    		error = Get_Status(&status);
    		//sprintf(str,"Inside While Loop. Status = %d",status);
		//SYSTEM_LOG(str);
		if (error != TRUE) {
      			SYSTEM_LOG("Acquire_Image(): Failed to get driver status.\n");
      			return FALSE;
    		}
  	}

#ifdef DEBUG
	SYSTEM_LOG("Acquiring data to camera->pixels\n");
#endif
  
  	// Retreave the data
  	error = Get_Acquired_Data(camera->pixels,(camera->width/camera->x_bin) * (camera->height/camera->y_bin));
  	if (error != TRUE) {
    	SYSTEM_LOG("Acquire_Image(): Failed to get acquired data.");
    	return FALSE;
  	}
  
#ifdef DEBUG
	SYSTEM_LOG("Finished Acquiring data to camera->pixels -- return to build image\n");
#endif 

  	return(TRUE); 
}


int SaveToFile(char *filename,struct Camera *camera,struct image_info *image_data,struct station_info *station_data, int type){
	int error,i;
	int status, size;
  	char str[255] = {0};
	FILE *output,*f;

	char pnmtopng_cmd[255];
	char buffer[255];

	//Switch statement to determine what type of image
	//0=TIFF, 1=BMP, 2=PNG, 3=JPEG
	switch(type){

		case 0:
			//Build TIFF image
			//Attach suffix to filename relevant to the file format
			f = fopen("/usr/local/imagerd_rt/image_raw_tiff.txt","w");	
			for (i = 0; i < ((camera->width/camera->x_bin) * (camera->height/camera->y_bin)); i++) {
				fprintf(f,"%u ",((camera->pixels)[i]));	
			}
			
			(void)strcat(filename,".tiff");
			error = Save_To_TIFF(filename,PAL_FILE,KINETICS_NUMBER,BIT_16);
			if(error !=TRUE){
				SYSTEM_LOG("SaveToFile(): Failed to create TIFF image.");
    			return FALSE;
			}
			(void)strcat(filename,".raw");
			SaveAsRaw(filename, 1);
			SYSTEM_LOG("SaveToFile(): Successfully Created TIFF image.");
			break;

		case 1:
			//Build Bmp Image
			//Attach suffix to filename relevant to the file format
			(void)strcat(filename,".bmp");
			error = Save_To_BMP(filename,PAL_FILE,0,0);
			if(error !=TRUE){
				SYSTEM_LOG("SaveToFile(): Failed to create Bitmap image.");
    			return FALSE;
			}
			SYSTEM_LOG("SaveToFile(): Successfully C.reated Bitmap image.");
			break;

		case 2:
      			//Build PNG image from raw data using netpgm and a pipe

      			//Attach suffix to filename relevant to the file format
      			(void)strcat(filename,".png");
			       //Compression Level for PNG image
      			sprintf(buffer,"%d",PNG_COMPRESSION_LEVEL);
      			//Start Building netPBM command
      			strcpy(pnmtopng_cmd, "pamflip -tb | pnmtopng -compression ");
      			strcat(pnmtopng_cmd, buffer);
      			strcat(pnmtopng_cmd," > ");
      			strcat(pnmtopng_cmd, filename);
		#ifdef DEBUG
      			sprintf(str,"PNG Command: %s\n",pnmtopng_cmd);
      			SYSTEM_LOG(str);
		#endif
      			f = fopen("/usr/local/imagerd_rt/image_raw_png.txt","w");
			//Open Pipe!
      			output = popen(pnmtopng_cmd, "w");
      			if (!output){
        			SYSTEM_LOG("SaveToFile(): Failed to open pipe for pnmtopng.");
        			return FALSE;
      			}
      			//NetPBM Top of image file for creation of PNM image
      			fprintf(output,"P2\n%d %d\n65535\n",(camera->width/camera->x_bin),(camera->height/camera->y_bin));
      
     			// Write image data to png image file
      			for (i = 0; i < ((camera->width/camera->x_bin) * (camera->height/camera->y_bin)); i++) {
        			fprintf(output, "%i ",((camera->pixels)[i]));
      				fprintf(f,"%i ",((camera->pixels)[i]));
			}	
  
      			if (pclose(output) != 0){
        			SYSTEM_LOG("SaveToFile(): Failed to close pipe to pnmtopng.");
        			return FALSE;
      			}
			fclose(f);
			

     			/*	 
			strcpy(pnmtopng_cmd, "rawtopgm -bpp 2 -littleendian ");
			sprintf(buffer,"%d",camera->ccd_width/camera->x_bin);
			strcat(pnmtopng_cmd, buffer);
			strcat(pnmtopng_cmd," ");
			sprintf(buffer,"%d",camera->ccd_height/camera->y_bin);
			strcat(pnmtopng_cmd, buffer);
			strcat(pnmtopng_cmd," | pnmtopng");
			strcat(pnmtopng_cmd," > ");
			strcat(pnmtopng_cmd, filename);

			output = popen(pnmtopng_cmd, "w");
			if (!output){
				SYSTEM_LOG("Build_Image(): Failed to open pipe to pnmtopng.");
				return FALSE;
			}

			size = (camera->ccd_height/camera->y_bin)*(camera->ccd_width/camera->x_bin);


			//Write Image data to pipe and pgm file 
			fwrite(camera->pixels, sizeof(long), size, output);

			//Close pipe
			if (pclose (output) != 0){
				SYSTEM_LOG("Image_To_PNG(): Failed to close pipe to pnmtopng.");
				return FALSE;
			}
			*/
			break;

		/*
    case 3:
			//Build jpeg image
			//Attach suffix to filename relevant to the file format
			(void)strcat(filename,".jpg");
			strcpy(pnmtopng_cmd, "rawtopgm -bpp 2 -littleendian ");
			sprintf(buffer,"%d",camera->ccd_width/camera->x_bin);
			strcat(pnmtopng_cmd, buffer);
			strcat(pnmtopng_cmd," ");
			sprintf(buffer,"%d",camera->ccd_height/camera->y_bin);
			strcat(pnmtopng_cmd, buffer);
			strcat(pnmtopng_cmd," | pnmtojpeg");
			strcat(pnmtopng_cmd," > ");
			strcat(pnmtopng_cmd, filename);

			output = popen(pnmtopng_cmd, "w");
			if (!output){
				SYSTEM_LOG("Build_Image(): Failed to open pipe to pnmtojpeg.");
				return FALSE;
			}

			size = (camera->ccd_height/camera->y_bin)*(camera->ccd_width/camera->x_bin);


			//Write Image data to pipe and pgm file 
			fwrite(camera->pixels, sizeof(long), size, output);

			//Close pipe
			if (pclose (output) != 0){
				SYSTEM_LOG("Image_To_PNG(): Failed to close pipe to pnmtojpeg.");
				return FALSE;
			}
			break;
    */
		default:
			SYSTEM_LOG("SaveToFile()): Not a valid type of image.");
    		return FALSE;
	}

	return(TRUE);
}


void ReadTemperature(struct Camera *camera){
	char str[255] = {0};
	int error;
	int temperature;

	SYSTEM_LOG("Get_Temperature(): Checking CCD temperature....");	

	error = Get_Temperature(&temperature);
	if(error != TRUE){
		SYSTEM_LOG("Get_Temperature(): Error -> Could not get temperature");
		return(FALSE);
	}else{
		sprintf(str,"Current CCD Temperature: %d C",temperature);
		SYSTEM_LOG(str);
	}

	return(TRUE);
}


/*
 * Wait until the camera temperature has reached target temperature.
 */
int Wait_Camera_Temperature(int temperature, int safe) {

  int stop = FALSE;
  int current_temperature;
  int ok;
  char line_buffer[255] = {0};
  
  // Loop checking the temperature until it is ok

  while(stop == FALSE){
    
    if(safe == FALSE){

      if (Is_Target_Temperature_Reached(temperature,&current_temperature,&ok) != TRUE) {
        SYSTEM_LOG("Failed to check if the target temperature has been reached.");
        return FALSE;
      }

    }else{
      
      if(Is_Temperature_Above_Limit(temperature,&current_temperature,&ok) != TRUE) {
        SYSTEM_LOG("Failed to check if the target temperature has been reached.\n");
        return FALSE;
      }

    }

    if(ok == TRUE){

      if(safe == FALSE){

        // Temperature has to be ok twice in a row

        sleep(5);

        if(Is_Target_Temperature_Reached(temperature,&current_temperature,&ok) != TRUE) {
          SYSTEM_LOG("Failed to check if the target temperature has been reached.\n");
          return FALSE;
        }
      
        if(ok == TRUE){
          sprintf(line_buffer,"Target temperature %d C reached. Current temperature %d C (allowed drift +/-%d).\n",temperature,current_temperature,ALLOWED_TEMP_DRIFT);
          SYSTEM_LOG(line_buffer);
          stop = TRUE;
        }
 
      } else {
          sprintf(line_buffer,"Target temperature %d C reached. Current temperature %d C.\n",temperature,current_temperature);
          SYSTEM_LOG(line_buffer);
          stop = TRUE;
      }

    } else {
      sprintf(line_buffer,"Current temperature %d C. Target %d C (allowed drift +/-%d C).\n",current_temperature,temperature,ALLOWED_TEMP_DRIFT);
      SYSTEM_LOG(line_buffer);
      // Sleep for a while before new check
      sleep(10); 
    }
  } 
  return(TRUE);
}

/*
 * Wait until the camera temperature has reached safe level before shutdown.
 */
void Wait_Safe_Exit_Temperature() {

  int current_temperature;
  int ok;
  char buffer[255] = {0};

  /* Shutdown the cooling */

  SYSTEM_LOG("Turning cooling OFF.");

  Stop_Cooler();

  // Set the detector temperature to safe level

  if (Set_Temperature(SHUTDOWN_TEMPERATURE) != TRUE) {
    SYSTEM_LOG("Failed to set the detector temperature.");
  }
  
  // Check the current detectot temperature

  if (Get_Detector_Temperature(&current_temperature,&ok) != TRUE) {

    SYSTEM_LOG("Failed to get detector temperature.");
    SYSTEM_LOG("Waiting for the temperature to raise.");
    sleep(90); // Give the temperature time to raise

  } else {

    // If temperature is already ok, we are done.

    if (current_temperature >= SHUTDOWN_TEMPERATURE) {

      sprintf(buffer,"Current temperature %d C. Safe temperature %d C. OK!\n",current_temperature,SHUTDOWN_TEMPERATURE);
      SYSTEM_LOG(buffer);
      
    } else {
      
      //Wait for the temperature to raise
      
      if (Wait_Camera_Temperature(SHUTDOWN_TEMPERATURE,TRUE) != TRUE) {
        SYSTEM_LOG("Failed to get detector temperature.");
        SYSTEM_LOG("Waiting for the temperature to raise.");
        sleep(120); // Give the temperature time to raise
      }
      
    }
  }
}

/*
 * Wrapper around Set Shutter -> pre configures the set shutter function
 */
int Set_Shutter(int mode, int close_delay, int open_delay){
  int status;

  //Check Shutter Input Mode
  if(mode == SHUTTER_MODE_AUTO){
    mode = SHUTTER_MODE_AUTO;
    SYSTEM_LOG("Set_Shutter(): Setting Shutter Mode: AUTO");
  }else if(mode == SHUTTER_MODE_OPEN){
    mode = SHUTTER_MODE_OPEN;
    SYSTEM_LOG("Set_Shutter(): Setting Shutter Mode: OPEN");
  }else if(mode == SHUTTER_MODE_CLOSE){
    mode = SHUTTER_MODE_CLOSE;
    SYSTEM_LOG("Set_Shutter(): Setting Shutter Mode: CLOSE");
  }else{
    SYSTEM_LOG("Set_Shutter(): Invalid Input Mode -> Could not set up shutter -> Exitiing");
    return FALSE;
  }

  status = Set_Shutter_Mode(SHUTTER_TTL_HIGH, mode, close_delay, open_delay);
  if(status != TRUE){
    SYSTEM_LOG("Set_Shutter(): Error Setting Shutter Mode -> Exiting");
    return FALSE;
  }

  SYSTEM_LOG("Set_Shutter(): SUCCESS -> Set Shutter Mode");
  return TRUE;
}











