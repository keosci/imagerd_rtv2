/*
 *
 * imager_ctrl.c
 *
 * 2011-04-08 Modified by Jarrett Little.
 * 2013-11-23 Modified by Jarrett Little.
 * 2018-10-27 Modified by Jarrett Little.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 *
 */


#include "imager_ctrl.h"

/**
 * Initialize the auxilary device connection to shutter control box
 */
int Connect_AUX(void) {
  char str[MAX_STRING_LEN]  = {0};

  aux_fd=open(AUX_PORT, O_RDWR | O_NOCTTY);
  if(aux_fd<0){
    perror(AUX_PORT);
    SYSTEM_LOG("Connect_AUX(): open() failed.");
    return(FALSE);
  }
  sprintf(str,"Successfully opened port: %s",AUX_PORT);
  SYSTEM_LOG(str);
  if(Set_AUX_Port() != TRUE){
    SYSTEM_LOG("Connect_AUX(): Failed to Set_AUX_Port()");
    return(FALSE);
  }
  return(TRUE);
}

/**
 * Initialize the sun shade device connection
 */
int Connect_SUN(void) {
  char str[MAX_STRING_LEN]  = {0};

  sun_fd=open(SUNSHADE_PORT, O_RDWR | O_NOCTTY);
  if(sun_fd<0){
    perror(SUNSHADE_PORT);
    SYSTEM_LOG("Connect_SUN(): open() failed.");
    return(FALSE);
  }
  sprintf(str,"Successfully opened port: %s",SUNSHADE_PORT);
  SYSTEM_LOG(str);
  if(Set_SUN_Port() != TRUE){
    SYSTEM_LOG("Connect_SUN(): Failed to Set_SUN_Port()");
    return(FALSE);
  }
  return(TRUE);
}

/**
 * Initialize the filterwheel device connection
 */
int Connect_FW(void){
  char str[MAX_STRING_LEN]  = {0};

  fw_fd=open(FW_PORT, O_RDWR | O_NOCTTY);
  if(sun_fd<0){
    perror(FW_PORT);
    SYSTEM_LOG("Connect_FW(): open() failed.");
    return(FALSE);
  }
  sprintf(str,"Successfully opened port: %s",FW_PORT);
  SYSTEM_LOG(str);
  if(Set_FW_Port() != TRUE){
    SYSTEM_LOG("Connect_FW(): Failed to Set_FW_Port()");
    return(FALSE);
  }
  return(TRUE);
}

int Connect_OMG(void){
  char str[MAX_STRING_LEN]  = {0};

  omega_fd=open(OMEGA_PORT, O_RDWR | O_NOCTTY);
  if(omega_fd<0){
    perror(OMEGA_PORT);
    SYSTEM_LOG("Connect_OMG(): open() failed.");
    return(FALSE);
  }
  sprintf(str,"Successfully opened port: %s",OMEGA_PORT);
  SYSTEM_LOG(str);
  if(Set_OMG_Port() != TRUE){
    SYSTEM_LOG("Connect_OMG(): Failed to Set_OMG_Port()");
    return(FALSE);
  }
  return(TRUE);
}

/**
 * Release the AUX device
 */
int Release_AUX_Connection(void)
{
  int retval;
  retval=close(aux_fd);
  if(retval<0){
    perror(AUX_PORT);
    SYSTEM_LOG("Release_AUX_Connection(): close() failed");
    return(FALSE);
  }

  return TRUE;
}

/**
 * Release the SUN device
 */
int Release_SUN_Connection(void)
{
  int retval;
  retval=close(sun_fd);
  if(retval<0){
    perror(SUNSHADE_PORT);
    SYSTEM_LOG("Release_SUN_Connection(): close() failed");
    return(FALSE);
  }

  return TRUE;
}

/**
 * Release the Filterwheel device
 */
int Release_FW_Connection(void)
{
  int retval;
  retval=close(fw_fd);
  if(retval<0){
    perror(FW_PORT);
    SYSTEM_LOG("Release_FW_Connection(): close() failed");
    return(FALSE);
  }

  return TRUE;
}

/**
 * Release the omega temp controller device
 */
int Release_OMG_Connection(void)
{
  int retval;
  retval=close(omega_fd);
  if(retval<0){
    perror(OMEGA_PORT);
    SYSTEM_LOG("Release_OMG_Connection(): close() failed");
    return(FALSE);
  }

  return TRUE;
}



/**
 * Set the serial port settings for shutter box
 * 9600N18
 */
int Set_AUX_Port(void) {
  struct termios oldtio,newtio;

  tcgetattr(aux_fd,&oldtio);
  bzero(&newtio,sizeof(newtio)); /* Clear the new settings */

  newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNBRK | IGNPAR;
  newtio.c_oflag = ONLCR;
  newtio.c_lflag = 0; //ICANON; /* ??????????? */

  newtio.c_cc[VINTR]   =0;  /* Ctrl-C */
  newtio.c_cc[VQUIT]   =0;  /* Ctrl-\ */
  newtio.c_cc[VERASE]  =0;
  newtio.c_cc[VKILL]   =0;
  newtio.c_cc[VEOF]    =4;   /* Ctrl-d */
  //newtio.c_cc[VTIME]   =5;  /* inter-char timer unused */
  //newtio.c_cc[VMIN]    =5;
  newtio.c_cc[VTIME]   =2;  /* timeout in deciseconds  */
  newtio.c_cc[VMIN]    =20;
  newtio.c_cc[VSWTC]   =0;
  newtio.c_cc[VSTART]  =0;
  newtio.c_cc[VSTOP]   =0;
  newtio.c_cc[VSUSP]   =0;
  newtio.c_cc[VEOL]    =0;
  newtio.c_cc[VREPRINT]=0;
  newtio.c_cc[VDISCARD]=0;
  newtio.c_cc[VWERASE] =0;
  newtio.c_cc[VLNEXT]  =0;
  newtio.c_cc[VEOL2]   =0;


  tcflush(aux_fd,TCIFLUSH);  //Flush both input and output
  if(tcsetattr(aux_fd,TCSANOW,&newtio) != 0) {
    perror(AUX_PORT);
    SYSTEM_LOG("Set_AUX_Port(): Failed tcflush");
    if(Release_AUX_Connection() != TRUE){
      SYSTEM_LOG("Set_AUX_Port(): Failed to release serial port connection.\n");
    }
    return(FALSE);
  }

  return(TRUE);
}


/**
 * Set the serial port settings for shutter box
 * 9600 7 data byte 1 stop byte Odd parity
 */
int Set_SUN_Port(void) {
  struct termios oldtio,newtio;


  tcgetattr(sun_fd,&oldtio);
  bzero(&newtio,sizeof(newtio)); /* Clear the new settings */

  newtio.c_cflag = BAUDRATE | CS7 | PARENB | PARODD | CLOCAL | CREAD;
  newtio.c_iflag = IGNBRK | IGNPAR;
  newtio.c_oflag = ONLCR;
  newtio.c_lflag = 0; //ICANON;

  newtio.c_cc[VINTR]   =0;  /* Ctrl-C */
  newtio.c_cc[VQUIT]   =0;  /* Ctrl-\ */
  newtio.c_cc[VERASE]  =0;
  newtio.c_cc[VKILL]   =0;
  newtio.c_cc[VEOF]    =4;   /* Ctrl-d */
  //newtio.c_cc[VTIME]   =5;  /* inter-char timer unused */
  //newtio.c_cc[VMIN]    =5;
  newtio.c_cc[VTIME]   =2;  /* timeout in deciseconds  */
  newtio.c_cc[VMIN]    =20;
  newtio.c_cc[VSWTC]   =0;
  newtio.c_cc[VSTART]  =0;
  newtio.c_cc[VSTOP]   =0;
  newtio.c_cc[VSUSP]   =0;
  newtio.c_cc[VEOL]    =0;
  newtio.c_cc[VREPRINT]=0;
  newtio.c_cc[VDISCARD]=0;
  newtio.c_cc[VWERASE] =0;
  newtio.c_cc[VLNEXT]  =0;
  newtio.c_cc[VEOL2]   =0;

  tcflush(sun_fd,TCIOFLUSH); /* Flush both input and output */
  if(tcsetattr(sun_fd,TCSANOW,&newtio)<0){
    perror(SUNSHADE_PORT);
    SYSTEM_LOG("Set_SUN_Port(): Failed tcflush");
    if(Release_SUN_Connection() != TRUE){
      SYSTEM_LOG("Set_SUN_Port(): Failed to release serial port connection.\n");
    }
    return(FALSE);
  }

  return(TRUE);
}

/**
 * Set the serial port settings for filterwheel
 * 9600N18
 */
int Set_FW_Port(void) {
  struct termios oldtio,newtio;

  tcgetattr(fw_fd,&oldtio);
  bzero(&newtio,sizeof(newtio)); /* Clear the new settings */

  newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNBRK | IGNPAR;
  newtio.c_oflag = ONLCR;
  newtio.c_lflag = 0; //ICANON; /* ??????????? */

  newtio.c_cc[VINTR]   =0;  /* Ctrl-C */
  newtio.c_cc[VQUIT]   =0;  /* Ctrl-\ */
  newtio.c_cc[VERASE]  =0;
  newtio.c_cc[VKILL]   =0;
  newtio.c_cc[VEOF]    =4;   /* Ctrl-d */
  //newtio.c_cc[VTIME]   =5;  /* inter-char timer unused */
  //newtio.c_cc[VMIN]    =5;
  newtio.c_cc[VTIME]   =2;  /* timeout in deciseconds  */
  newtio.c_cc[VMIN]    =20;
  newtio.c_cc[VSWTC]   =0;
  newtio.c_cc[VSTART]  =0;
  newtio.c_cc[VSTOP]   =0;
  newtio.c_cc[VSUSP]   =0;
  newtio.c_cc[VEOL]    =0;
  newtio.c_cc[VREPRINT]=0;
  newtio.c_cc[VDISCARD]=0;
  newtio.c_cc[VWERASE] =0;
  newtio.c_cc[VLNEXT]  =0;
  newtio.c_cc[VEOL2]   =0;


  tcflush(fw_fd,TCIFLUSH);  //Flush both input and output
  if(tcsetattr(fw_fd,TCSANOW,&newtio) != 0) {
    perror(FW_PORT);
    SYSTEM_LOG("Set_FW_Port(): Failed tcflush");
    if(Release_FW_Connection() != TRUE){
      SYSTEM_LOG("Set_FW_Port(): Failed to release serial port connection.\n");
    }
    return(FALSE);
  }

  return(TRUE);
}

/**
 * Set the serial port settings for omega controller
 * 9600N18
 */
int Set_OMG_Port(void) {
  struct termios oldtio,newtio;

  tcgetattr(omega_fd,&oldtio);
  bzero(&newtio,sizeof(newtio)); /* Clear the new settings */

  newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNBRK | IGNPAR;
  newtio.c_oflag = ONLCR;
  newtio.c_lflag = 0; //ICANON; /* ??????????? */

  newtio.c_cc[VINTR]   =0;  /* Ctrl-C */
  newtio.c_cc[VQUIT]   =0;  /* Ctrl-\ */
  newtio.c_cc[VERASE]  =0;
  newtio.c_cc[VKILL]   =0;
  newtio.c_cc[VEOF]    =4;   /* Ctrl-d */
  //newtio.c_cc[VTIME]   =5;  /* inter-char timer unused */
  //newtio.c_cc[VMIN]    =5;
  newtio.c_cc[VTIME]   =2;  /* timeout in deciseconds  */
  newtio.c_cc[VMIN]    =20;
  newtio.c_cc[VSWTC]   =0;
  newtio.c_cc[VSTART]  =0;
  newtio.c_cc[VSTOP]   =0;
  newtio.c_cc[VSUSP]   =0;
  newtio.c_cc[VEOL]    =0;
  newtio.c_cc[VREPRINT]=0;
  newtio.c_cc[VDISCARD]=0;
  newtio.c_cc[VWERASE] =0;
  newtio.c_cc[VLNEXT]  =0;
  newtio.c_cc[VEOL2]   =0;


  tcflush(fw_fd,TCIFLUSH);  //Flush both input and output
  if(tcsetattr(omega_fd,TCSANOW,&newtio) != 0) {
    perror(OMEGA_PORT);
    SYSTEM_LOG("Set_OMG_Port(): Failed tcflush");
    if(Release_FW_Connection() != TRUE){
      SYSTEM_LOG("Set_OMG_Port(): Failed to release serial port connection.\n");
    }
    return(FALSE);
  }

  return(TRUE);
}

/**
 *  Send auxilary command to the device
 */
int Send_AUX_Command(char *command) {
  char str[MAX_STRING_LEN]  = {0};
  char mybuf[]={129};
  char d = '\n';  // CR character
  int i=0;
  tcflush(aux_fd,TCIOFLUSH); /* Flush both input and output */

  //sprintf(str,"Send_AUX_Command: %s\n",command);
  //SYSTEM_LOG(str);

  for(i;i<strlen(command);i++) {
    //sprintf(str,"-->%c",*(command+i));
    //SYSTEM_LOG(str);
    write(aux_fd,command+i,1);
  }

  return TRUE;
}

/**
 *  Send sun shade command to the device
 */
int Send_SUN_Command(char *command) {
  char str[MAX_STRING_LEN]  = {0};
  char mybuf[]={129};
  char d = '\n';  // CR character
  int i=0;
  tcflush(sun_fd,TCIOFLUSH); /* Flush both input and output */

  fprintf(stderr,"Send_SUN_Command(): %s\n",command);

  for(i;i<strlen(command);i++) {
    //sprintf(str,"-->%c\n",*(command+i));
    //SYSTEM_LOG(str);
    write(sun_fd,command+i,1);
  }

  return TRUE;
}

/**
 *  Send FW command to the device
 */
int Send_FW_Command(char *command) {
  char str[MAX_STRING_LEN]  = {0};
  char mybuf[]={129};
  char d = '\n';  // CR character
  int i=0;
  tcflush(fw_fd,TCIOFLUSH); /* Flush both input and output */

  //sprintf(str,"Send_AUX_Command: %s\n",command);
  //SYSTEM_LOG(str);

  for(i;i<strlen(command);i++) {
    //sprintf(str,"-->%c",*(command+i));
    //SYSTEM_LOG(str);
    write(fw_fd,command+i,1);
  }

  return TRUE;
}

/**
 *  Send omega command to the device
 */
int Send_OMG_Command(char *command) {
  char str[MAX_STRING_LEN]  = {0};
  char mybuf[]={129};
  char d = '\n';  // CR character
  int i=0;
  tcflush(omega_fd,TCIOFLUSH); /* Flush both input and output */

  //sprintf(str,"Send_AUX_Command: %s\n",command);
  //SYSTEM_LOG(str);

  for(i;i<strlen(command);i++) {
    //sprintf(str,"-->%c",*(command+i));
    //SYSTEM_LOG(str);
    write(omega_fd,command+i,1);
  }

  return TRUE;
}


/**
 * Open/Close Shutter
 *
 * Command to Shutter Box:
 * controlvalue= -1=inquire, 0=close, 1=open)
 *
 * Open Shutter Command     = "!0S1"
 * Close Shutter Command    = "!0S0"
 * Query Shutter Command    = "!0R"
 *
 * Response: 0 = closed 1 = open
 */
int Shutter_Control(int controlvalue) {
  int res;
  fd_set rfds;
  struct timeval tv;
  char str[MAX_STRING_LEN]  = {0};
  char buf[255];
  int state;


  // Prepare the timeout mechanism
  tv.tv_sec=ASC_TIMEOUT;
  tv.tv_usec=0;
  FD_ZERO(&rfds);
  FD_SET(aux_fd,&rfds);

  if(controlvalue == OPEN || controlvalue == CLOSE) {

    //Place shutter command into the buf ->  this will be sent to the send command.
    sprintf(buf,"!0S%d",controlvalue);

    sprintf(str,"Control String = %s",buf);
    SYSTEM_LOG(str);

    if (Send_AUX_Command(buf) != TRUE){
      return FALSE;
    }

  }else if(controlvalue == INQUIRE){

    //Place shutter command into the buf ->  this will be sent to the send command.
    sprintf(buf,"!0R");

    sprintf(str,"Control String = %s",buf);
    SYSTEM_LOG(str);

    if (Send_AUX_Command(buf) != TRUE) {
      return FALSE;
    }

    if(!select(aux_fd+1,&rfds,NULL,NULL,&tv)){
      SYSTEM_LOG("Shutter_Control() Timeout");
      //return ASC_TIMEOUT_ERR;
      return FALSE;
    }

    //Transfer the stored characters from the serial port to the variable buffer
    res=read(aux_fd,buf,255);
    if(res<0){
      perror(AUX_PORT);
      SYSTEM_LOG("Shutter_Control(): read() failed");
      return FALSE;
    }

    //Cast char to int for comparision
    state = buf[0] - '0';
    //Check to see if value returned is 0 or 1
    if(state == 0) {
      sprintf(str,"Shutter : Closed -> Returned Value: %d",state);
      SYSTEM_LOG(str);
      //Return closed shutter value
      return(IS_CLOSED);

    }else if(state == 1) {
      sprintf(str,"Shutter : Open -> Returned Value: %d",state);
      SYSTEM_LOG(str);
      //Return closed shutter value
      return(IS_OPEN);

    }else{
      sprintf(str,"Shutter : Undefined -> Returned Value: %d",state);
      SYSTEM_LOG(str);
      return FALSE;
    }

  }

  return TRUE;
}

/**
 * Open/Close Sun Shade
 *
 * Command to Sun Shade Controller:
 * controlvalue= -1=inquire, 0=close, 1=open)
 * First set the SunShield to manual control mode: “VU1*”
 * Close SunShield: “VX1*”
 * Open SunShield: “VX0*”
 *
 * Response:
 */
int SunShade_Control(int controlvalue) {
  int res, state;
  int manual_mode = FALSE;
  fd_set rfds;
  struct timeval tv;
  char str[MAX_STRING_LEN]  = {0};
  char buf[255];
  char send_char;


  // Prepare the timeout mechanism
  tv.tv_sec=ASC_TIMEOUT;
  tv.tv_usec=0;
  FD_ZERO(&rfds);
  FD_SET(sun_fd,&rfds);

  //First check is controller in manual mode
  if(Send_SUN_Command("TU*") != TRUE) {
    return FALSE;
  }

  if(!select(sun_fd+1,&rfds,NULL,NULL,&tv)){
      SYSTEM_LOG("SunShade_Control() Timeout");
      return FALSE;
  }

  //Transfer the stored characters from the serial port to the variable buffer
  res=read(sun_fd,buf,255);
  if(res<0){
    perror(SUNSHADE_PORT);
    SYSTEM_LOG("SunShade_Control(): read() failed");
    return FALSE;
  }

  //TODO -> Test this
  //Test returned buffer for the return code
  //If the controller is not in manual mode set that
  //Once in manual mode set the flag to true
  // The character we're looking for is at positin 14 -> either 1 or 0
  //Cast char to int for comparision

  //printf("Buffer Pos 14 = %c\n",buf[14]);

  state = buf[14] - '0';
  if(state == 0) {
    SYSTEM_LOG("SunShade_Control(): Not in manual mode. Setting controller to manual");
    if(Send_SUN_Command("VU1*") != TRUE) {
      SYSTEM_LOG("SunShade_Control(): Could not set controller to manual mode. Exiting");
      return FALSE;
    }
    manual_mode = TRUE;
    SYSTEM_LOG("Sun Shade Controller: Manual Mode");
  }

  //Now
  if(controlvalue == OPEN || controlvalue == CLOSE){
    //set the send_char variable for X to transmits open or close
    sprintf(buf,"VX%d*", controlvalue);

    sprintf(str,"Control String = %s\n",buf);
    SYSTEM_LOG(str);

    if (Send_SUN_Command(buf) != TRUE){
        return FALSE;
    }

  }else if(controlvalue == INQUIRE_SHADE){

    if(Send_SUN_Command("TX*") != TRUE) {
      return FALSE;
    }

    if(!select(sun_fd+1,&rfds,NULL,NULL,&tv)){
      SYSTEM_LOG("SunShade_Control() Timeout");
      //return ASC_TIMEOUT_ERR;
      return FALSE;
    }

    //Transfer the stored characters from the serial port to the variable buffer
    res=read(sun_fd,buf,255);
    if(res<0){
      perror(SUNSHADE_PORT);
      SYSTEM_LOG("SunShade_Control(): read() failed");
      return FALSE;
    }

    //sprintf(str,"Returned from INQUIRE_SHADE: %s",buf);
    //SYSTEM_LOG(str);

    //TODO -> Test
    //Add return logic for open / close
    state = buf[14] - '0';
    //Check to see if value returned is 0 or 1
    if(state == 0) {
      sprintf(str,"SunShield : Closed -> Returned Value: %d",state);
      SYSTEM_LOG(str);
      //Return closed shutter value
      return(IS_CLOSED);

    }else if(state == 1) {
      sprintf(str,"SunShield : Open -> Returned Value: %d",state);
      SYSTEM_LOG(str);
      //Return closed shutter value
      return(IS_OPEN);

    }else{
      sprintf(str,"SunShield : Undefined -> Returned Value: %d",state);
      SYSTEM_LOG(str);
      return FALSE;
    }

  }else if(controlvalue == INQUIRE_CTRL) {
    if(Send_SUN_Command("TU*") != TRUE) {
      return FALSE;
    }

    if(!select(sun_fd+1,&rfds,NULL,NULL,&tv)){
      SYSTEM_LOG("SunShade_Control() Timeout");
      //return ASC_TIMEOUT_ERR;
      return FALSE;
    }

    //Transfer the stored characters from the serial port to the variable buffer
    res=read(sun_fd,buf,255);
    if(res<0){
      perror(SUNSHADE_PORT);
      SYSTEM_LOG("SunShade_Control(): read() failed");
      return FALSE;
    }

    //sprintf(str,"Returned from INQUIRE_CTRL: %s",buf);
    //SYSTEM_LOG(str);

    //TODO
    //Add return logic for control register.
    state = buf[14] - '0';
    //Check to see if value returned is 0 or 1
    if(state == 0) {
      sprintf(str,"SunShield : Auto Mode -> Returned Value: %d",state);
      SYSTEM_LOG(str);
      //Return closed shutter value
      return(IS_CLOSED);

    }else if(state == 1) {
      sprintf(str,"SunShield : Manual Mode -> Returned Value: %d",state);
      SYSTEM_LOG(str);
      //Return closed shutter value
      return(IS_OPEN);

    }else{
      sprintf(str,"SunShield : Undefined -> Returned Value: %d",state);
      SYSTEM_LOG(str);
      return FALSE;
    }
  }

  return TRUE;
}


/**
 * Open the shutter
 */
int Open_Shutter(void){
  int retval = 0;
  char str[MAX_STRING_LEN]  = {0};

  //Initialize the connection
  if(Connect_AUX() != TRUE) {
    SYSTEM_LOG("Open_Shutter(): Connect_AUX() failed.");
    if(Release_AUX_Connection() != TRUE){
      SYSTEM_LOG("Open_Shutter(): Failed to release serial port connection.");
    }
    return FALSE;
  }

  //Send Open Command to the shutter
  if (Shutter_Control(OPEN) != TRUE) {
    SYSTEM_LOG("Open_Shutter(): Shutter_Control(OPEN) failed.");
    if(Release_AUX_Connection() != TRUE){
      SYSTEM_LOG("Open_Shutter(): Failed to release serial port connection.");
    }
    return FALSE;
  }

  usleep(100000);

  //Check current state of shutter
  retval = Shutter_Control(INQUIRE);
  if(retval == FALSE) {
    sprintf(str,"Open_Shutter(): Shutter_Control(INQUIRE) failed -> retval = %d.",retval);
    SYSTEM_LOG(str);
    if(Release_AUX_Connection() != TRUE){
      SYSTEM_LOG("Open_Shutter(): Failed to release serial port connection.\n");
    }
    return FALSE;
  } else if(retval == IS_OPEN) {

    SYSTEM_LOG("Open_Shutter(): Status: OPEN");

  } else {
    sprintf(str,"Open_Shutter(): Shutter_Control(INQUIRE) failed -> retval = %d.",retval);
    SYSTEM_LOG(str);
    if(Release_AUX_Connection() != TRUE){
      SYSTEM_LOG("Open_Shutter(): Failed to release serial port connection.\n");
    }
    return FALSE;
  }

  if(Release_AUX_Connection() != TRUE){
      SYSTEM_LOG("Open_Shutter(): Failed to release serial port connection.\n");
      return FALSE;
  }

  return TRUE;
}

/**
 * Close the shutter
 */
int Close_Shutter(void){
  int retval = 0;
  char str[MAX_STRING_LEN]  = {0};

  //Initialize the connection
  if(Connect_AUX() != TRUE) {
    SYSTEM_LOG("Close_Shutter(): Connect_AUX() failed.");
    if(Release_AUX_Connection() != TRUE){
      SYSTEM_LOG("Close_Shutter(): Failed to release serial port connection.");
    }
    return FALSE;
  }

  //Send Open Command to the shutter
  if (Shutter_Control(CLOSE) != TRUE) {
    SYSTEM_LOG("Close_Shutter(): Shutter_Control(CLOSE) failed.");
    if(Release_AUX_Connection() != TRUE){
      SYSTEM_LOG("Close_Shutter(): Failed to release serial port connection.");
    }
    return FALSE;
  }

  usleep(100000);

  //Check current state of shutter
  retval = Shutter_Control(INQUIRE);
  if(retval == FALSE) {
    sprintf(str,"Close_Shutter(): Shutter_Control(INQUIRE) failed -> retval = %d.",retval);
    SYSTEM_LOG(str);
    if(Release_AUX_Connection() != TRUE){
      SYSTEM_LOG("Close_Shutter(): Failed to release serial port connection.\n");
    }
    return FALSE;
  } else if(retval == IS_CLOSED) {

    SYSTEM_LOG("Close_Shutter(): Status: CLOSED");

  } else {
    sprintf(str,"Close_Shutter(): Shutter_Control(INQUIRE) failed -> retval = %d.",retval);
    SYSTEM_LOG(str);
    if(Release_AUX_Connection() != TRUE){
      SYSTEM_LOG("Close_Shutter(): Failed to release serial port connection.\n");
    }
    return FALSE;
  }

  if(Release_AUX_Connection() != TRUE){
    SYSTEM_LOG("Close_Shutter(): Failed to release serial port connection.\n");
    return FALSE;
  }

  return TRUE;
}


/**
 * Gets the shutter state
 * Prints the shutter state to SYSTEM_LOG
 */
int Get_Shutter(void) {
  int retval = 0;
  char str[MAX_STRING_LEN]  = {0};

  //Initialize the connection
  if(Connect_AUX() != TRUE) {
    SYSTEM_LOG("Get_Shutter(): Connect_AUX() failed.");
    if(Release_AUX_Connection() != TRUE){
      SYSTEM_LOG("Get_Shutter(): Failed to release serial port connection.");
    }
    return FALSE;
  }

  //Check current state of shutter
  retval = Shutter_Control(INQUIRE);
  if(retval == FALSE) {
    sprintf(str,"Open_Shutter(): Shutter_Control(INQUIRE) failed -> retval = %d.",retval);
    SYSTEM_LOG(str);
    if(Release_AUX_Connection() != TRUE){
      SYSTEM_LOG("Open_Shutter(): Failed to release serial port connection.\n");
    }
    return FALSE;
  } else if(retval == IS_OPEN) {

    SYSTEM_LOG("Get_Shutter: Shutter is Open");

  } else if(retval == IS_CLOSED) {

    SYSTEM_LOG("Get_Shutter: Shutter is Closed");

  } else {
    sprintf(str,"Get_Shutter(): Shutter_Control(INQUIRE) failed -> retval = %d.",retval);
    SYSTEM_LOG(str);
    if(Release_AUX_Connection() != TRUE){
      SYSTEM_LOG("Get_Shutter(): Failed to release serial port connection.\n");
    }
    return FALSE;
  }

  // Release the connection
  if(Release_AUX_Connection() != TRUE){
    SYSTEM_LOG("Get_Shutter(): Failed to release serial port connection.\n");
    return FALSE;
  }

  return(TRUE);
}


int Open_SunShade(void) {
  int retval = 0;
  char str[MAX_STRING_LEN]  = {0};

  //Initialize the connection
  if(Connect_SUN() != TRUE) {
    SYSTEM_LOG("Manual_Mode(): Connect_SUN() failed.");
    if(Release_SUN_Connection() != TRUE){
      SYSTEM_LOG("Manual_Mode(): Failed to release serial port connection.");
    }
    return FALSE;
  }

  //Send Open Command to the shutter
  if (SunShade_Control(OPEN_SHADE) != TRUE) {
    SYSTEM_LOG("Open_SunShade(): SunShade_Control(OPEN_SHADE) failed.");
    if(Release_SUN_Connection() != TRUE){
      SYSTEM_LOG("Open_SunShade(): Failed to release serial port connection.");
    }
    return FALSE;
  }

  usleep(100000);

  //Check current state of sun shield
  retval = SunShade_Control(INQUIRE_SHADE);
  if(retval == FALSE) {
    sprintf(str,"Open_SunShade(): SunShade_Control(INQUIRE_SHADE) failed -> retval = %d.",retval);
    SYSTEM_LOG(str);
    if(Release_SUN_Connection() != TRUE){
      SYSTEM_LOG("Open_SunShade(): Failed to release serial port connection.\n");
    }
    return FALSE;
  } else if(retval == IS_OPEN) {

    SYSTEM_LOG("Open_SunShade(): Status: OPEN");

  } else {
    sprintf(str,"Open_SunShade(): SunShade_Control(INQUIRE_SHADE) failed -> retval = %d.",retval);
    SYSTEM_LOG(str);
    if(Release_SUN_Connection() != TRUE){
      SYSTEM_LOG("Open_SunShade(): Failed to release serial port connection.\n");
    }
    return FALSE;
  }

  if(Release_SUN_Connection() != TRUE){
    SYSTEM_LOG("Open_SunShade(): Failed to release serial port connection.\n");
    return FALSE;
  }

  return TRUE;
}


int Close_SunShade(void) {
  int retval = 0;
  char str[MAX_STRING_LEN]  = {0};

  //Initialize the connection
  if(Connect_SUN() != TRUE) {
    SYSTEM_LOG("Close_SunShade(): Connect_SUN() failed.");
    if(Release_SUN_Connection() != TRUE){
      SYSTEM_LOG("Close_SunShade(): Failed to release serial port connection.");
    }
    return FALSE;
  }

  //Send Open Command to the shutter
  if (SunShade_Control(CLOSE_SHADE) != TRUE) {
    SYSTEM_LOG("Close_SunShade(): SunShade_Control(CLOSE_SHADE) failed.");
    if(Release_SUN_Connection() != TRUE){
      SYSTEM_LOG("Close_SunShade(): Failed to release serial port connection.");
    }
    return FALSE;
  }

  usleep(100000);

  //Check current state of sun shield
  retval = SunShade_Control(INQUIRE_SHADE);
  if(retval == FALSE) {
    sprintf(str,"Close_SunShade(): SunShade_Control(INQUIRE_SHADE) failed -> retval = %d.",retval);
    SYSTEM_LOG(str);
    if(Release_SUN_Connection() != TRUE){
      SYSTEM_LOG("Close_SunShade(): Failed to release serial port connection.\n");
    }
    return FALSE;
  } else if(retval == IS_CLOSED) {

    SYSTEM_LOG("Close_SunShade(): Status: CLOSED");

  } else {
    sprintf(str,"Close_SunShade(): SunShade_Control(INQUIRE_SHADE) failed -> retval = %d.",retval);
    SYSTEM_LOG(str);
    if(Release_SUN_Connection() != TRUE){
      SYSTEM_LOG("Close_SunShade(): Failed to release serial port connection.\n");
    }
    return FALSE;
  }

  if(Release_SUN_Connection() != TRUE){
    SYSTEM_LOG("Close_SunShade(): Failed to release serial port connection.\n");
    return FALSE;
  }

  return TRUE;
}

int Get_SunShade(void) {
  int retval = 0;
  char str[MAX_STRING_LEN]  = {0};

  //Initialize the connection
  if(Connect_SUN() != TRUE) {
    SYSTEM_LOG("Get_SunShade(): Connect_SUN() failed.");
    if(Release_SUN_Connection() != TRUE){
      SYSTEM_LOG("Get_SunShade(): Failed to release serial port connection.");
    }
    return FALSE;
  }

  //Check current state of sun shield
  retval = SunShade_Control(INQUIRE_SHADE);
  if(retval == FALSE) {
    sprintf(str,"Get_SunShade(): SunShade_Control(INQUIRE_SHADE) failed -> retval = %d.",retval);
    SYSTEM_LOG(str);
    if(Release_SUN_Connection() != TRUE){
      SYSTEM_LOG("Get_SunShade(): Failed to release serial port connection.\n");
    }
    return FALSE;
  } else if(retval == IS_OPEN) {

    SYSTEM_LOG("Get_SunShade: SunShield is Open");

  } else if(retval == IS_CLOSED) {

    SYSTEM_LOG("Get_SunShade: SunShield is Closed");

  } else {
    sprintf(str,"Get_SunShade(): SunShade_Control(INQUIRE_SHADE) failed -> retval = %d.",retval);
    SYSTEM_LOG(str);
    if(Release_AUX_Connection() != TRUE){
      SYSTEM_LOG("Get_SunShade(): Failed to release serial port connection.\n");
    }
    return FALSE;
  }

  return TRUE;
}

//Device Control Functions for Multi Filter imagers
//FW_Contro(): func         -> shutter_ctrl
//             controlvalue -> open || close || status
//             func         -> fw_ctrl
//             controlvalue -> filter_num || home || status
int FW_Control(int func,int controlvalue) {
  int retval = 0;
  int res;
  fd_set rfds;
  struct timeval tv;
  char str[MAX_STRING_LEN]  = {0};
  char buf[255];

  //Initialize the connection
  if(Connect_FW() != TRUE) {
    SYSTEM_LOG("FW_Control(): Connect_FW() failed.");
    if(Release_FW_Connection() != TRUE){
      SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
    }
    return FALSE;
  }

  // Prepare the timeout mechanism
  tv.tv_sec=ASC_TIMEOUT;
  tv.tv_usec=0;
  FD_ZERO(&rfds);
  FD_SET(fw_fd,&rfds);

  switch(func){
  case FWCTRL:
      if(controlvalue >=0){
        //Move Filterwheel to position from control value
        if(controlvalue == HOME){
          //Move the Filterwheel to home position
          sprintf(buf,"GOSUB5\r");
          if (Send_FW_Command(buf) != TRUE) {
            SYSTEM_LOG("FW_Control(): Send_FW_Command() failed.");
            if(Release_FW_Connection() != TRUE){
              SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
            }
            return FALSE;
          }
          if(!select(fw_fd+1,&rfds,NULL,NULL,&tv)){
            SYSTEM_LOG("FW_Control() Timeout");
            if(Release_FW_Connection() != TRUE){
              SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
            }
            return FALSE;
          }
          //Transfer the stored characters from the serial port to the variable buffer
          res=read(fw_fd,buf,255);
          if(res<0){
            perror(FW_PORT);
            SYSTEM_LOG("FW_Control(): read() failed");
            if(Release_FW_Connection() != TRUE){
              SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
            }
            return FALSE;
          }
          buf[res]=0;
          if(strncmp("HOME:1",buf,6) == 0){
            sprintf(str,"FilterWheel @ Home Pos: HOMED -> Returned Value: %s",buf);
            SYSTEM_LOG(str);
            break;
          }else{
            sprintf(str,"FilterWheel did not reach Home Pos: Returned Value: %s",buf);
            SYSTEM_LOG(str);
            if(Release_FW_Connection() != TRUE){
              SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
            }
            return FALSE;
          }
        }else if(controlvalue >= FILTER_MIN || controlvalue <= FILTER_MAX){
          sprintf(buf,"g=%d\rGOSUB4\r",controlvalue);
          if (Send_FW_Command(buf) != TRUE) {
            SYSTEM_LOG("FW_Control(): Send_FW_Command() failed.");
            if(Release_FW_Connection() != TRUE){
              SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
            }
            return FALSE;
          }
          if(!select(fw_fd+1,&rfds,NULL,NULL,&tv)){
            SYSTEM_LOG("FW_Control() Timeout");
            if(Release_FW_Connection() != TRUE){
              SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
            }
            return FALSE;
          }
          //Transfer the stored characters from the serial port to the variable buffer
          res=read(fw_fd,buf,255);
          if(res<0){
            perror(FW_PORT);
            SYSTEM_LOG("FW_Control(): read() failed");
            if(Release_FW_Connection() != TRUE){
              SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
            }
            return FALSE;
          }
          buf[res]=0;
          if(strncmp("FILT:",buf,5) == 0){
            sprintf(str,"FilterWheel @ FILT Pos: Returned Value: %s",buf);
            SYSTEM_LOG(str);
            break;
          }else{
            sprintf(str,"FilterWheel did not reach Desired Pos: Returned Value: %s",buf);
            SYSTEM_LOG(str);
            if(Release_FW_Connection() != TRUE){
              SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
            }
            return FALSE;
          }
        }
      }else{
        //Log the status of the filterwheel ie. What position
        sprintf(str,"FW_Control: Invalid controlvalue --> %d",controlvalue);
        SYSTEM_LOG(str);
        if(Release_FW_Connection() != TRUE){
          SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
        }
        return FALSE;
      }
      break;
  case SHUTCTRL:
      if(controlvalue == 0 || controlvalue == 1){
        //Close or open the shutter
        sprintf(buf,"d=%d\rGOSUB6\r",controlvalue);
        if (Send_FW_Command(buf) != TRUE) {
          SYSTEM_LOG("FW_Control(): Send_FW_Command() failed.");
          if(Release_FW_Connection() != TRUE){
            SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
          }
          return FALSE;
        }
        if(!select(fw_fd+1,&rfds,NULL,NULL,&tv)){
          SYSTEM_LOG("FW_Control() Timeout");
          if(Release_FW_Connection() != TRUE){
            SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
          }
          return FALSE;
        }
        //Transfer the stored characters from the serial port to the variable buffer
        res=read(fw_fd,buf,255);
        if(res<0){
          perror(FW_PORT);
          SYSTEM_LOG("FW_Control(): read() failed");
          if(Release_FW_Connection() != TRUE){
            SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
          }
          return FALSE;
        }
        buf[res]=0;
        if(controlvalue == 0 && strncmp("SHTR:0",buf,6) == 0){
          sprintf(str,"Shutter Closed: Returned Value: %s",buf);
          SYSTEM_LOG(str);
          break;
        }else if(controlvalue == 1 && strncmp("SHTR:1",buf,6) == 0){
	  sprintf(str,"Shutter Opened: Returned Value: %s",buf);
          SYSTEM_LOG(str);
          break;
	}else{
          sprintf(str,"Shutter did not open or close from command: Returned Value: %s",buf);
          SYSTEM_LOG(str);
          if(Release_FW_Connection() != TRUE){
            SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
          }
          return FALSE;
        }

      }else if(controlvalue == -1){
        //Inquire the shutter
        sprintf(buf,"d=%d\rGOSUB6\r",controlvalue);
        if (Send_FW_Command(buf) != TRUE) {
          SYSTEM_LOG("FW_Control(): Send_FW_Command() failed.");
          if(Release_FW_Connection() != TRUE){
            SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
          }
          return FALSE;
        }
        if(!select(fw_fd+1,&rfds,NULL,NULL,&tv)){
          SYSTEM_LOG("FW_Control() Timeout");
          if(Release_FW_Connection() != TRUE){
            SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
          }
          return FALSE;
        }
        //Transfer the stored characters from the serial port to the variable buffer
        res=read(fw_fd,buf,255);
        if(res<0){
          perror(FW_PORT);
          SYSTEM_LOG("FW_Control(): read() failed");
          if(Release_FW_Connection() != TRUE){
            SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
          }
          return FALSE;
        }
        buf[res]=0;
        if(strncmp("SHTR:0",buf,6) == 0){
          sprintf(str,"Shutter Closed: Returned Value: %s",buf);
          SYSTEM_LOG(str);
          break;
        }else{
          sprintf(str,"Shutter in unknown state: Returned Value: %s",buf);
          SYSTEM_LOG(str);
          if(Release_FW_Connection() != TRUE){
            SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
          }
          return FALSE;
        }
	if(strncmp("SHTR:1",buf,6) == 0){
          sprintf(str,"Shutter Opened: Returned Value: %s",buf);
          SYSTEM_LOG(str);
          break;
        }else{
          sprintf(str,"Shutter in unknown state: Returned Value: %s",buf);
          SYSTEM_LOG(str);
          if(Release_FW_Connection() != TRUE){
            SYSTEM_LOG("FW_Control(): Failed to release serial port connection.");
          }
          return FALSE;
        }
        break;
      }
      break;
  default:
    SYSTEM_LOG("FW_Control(): Invalid function parameter.");
  }

  if(Release_FW_Connection() != TRUE){
    SYSTEM_LOG("FW_Control(): Failed to release serial port connection.\n");
    return FALSE;
  }

  return TRUE;
}

//Device Control Function of the Omega Temperature controller
//func:
int Omega_Control(int func, float controlvalue, float *fw_temp){
  int retval = 0;
  int res;
  int dec=0;
  fd_set rfds;
  struct timeval tv;
  char str[MAX_STRING_LEN]  = {0};
  char tmp[MAX_STRING_LEN]  = {0};
  char num[25] = {0};
  char buf[255];
  char s;

  //Initialize the connection
  if(Connect_OMG() != TRUE) {
    SYSTEM_LOG("Omega_Control(): Connect_OMG() failed.");
    if(Release_OMG_Connection() != TRUE){
      SYSTEM_LOG("Omega_Control(): Failed to release serial port connection.");
    }
    return FALSE;
  }

  // Prepare the timeout mechanism
  tv.tv_sec=ASC_TIMEOUT;
  tv.tv_usec=0;
  FD_ZERO(&rfds);
  FD_SET(omega_fd,&rfds);

  switch(func){
    case RDTEMP:
      //Read the current temperature of the filterwheel housing
      //Command = *X01\r
      sprintf(buf,"*X01\r");
      if (Send_OMG_Command(buf) != TRUE) {
        SYSTEM_LOG("Omega_Control(): Send_OMG_Command() failed.");
        if(Release_OMG_Connection() != TRUE){
          SYSTEM_LOG("Omega_Control(): Failed to release serial port connection.");
        }
        return FALSE;
      }
      if(!select(omega_fd+1,&rfds,NULL,NULL,&tv)){
        SYSTEM_LOG("Omega_Control() Timeout");
        if(Release_OMG_Connection() != TRUE){
          SYSTEM_LOG("Omega_Control(): Failed to release serial port connection.");
        }
        return FALSE;
      }
      //Transfer the stored characters from the serial port to the variable buffer
      res=read(omega_fd,buf,255);
      if(res<0){
        perror(OMEGA_PORT);
        SYSTEM_LOG("Omega_Control(): read() failed");
        if(Release_OMG_Connection() != TRUE){
          SYSTEM_LOG("Omega_Control(): Failed to release serial port connection.");
        }
        return FALSE;
      }
      buf[res]=0;
      sprintf(str,"Omega Temperature Controller Current Temp [C]: %s",buf);
      SYSTEM_LOG(str);
      //Transfer Str to float
      *fw_temp = atof(buf);
      break;

    case SETTEMP:
      //First check bounds of the controlvalue
      dec = (int) (controlvalue * 10);
      if(dec >= 16 && dec < 256){
        //Value is within range -> convert to weird hex reprsentive
        strcpy(tmp,"*W012000");
        //Convert Decimal Value to hex
        dec = (int) (controlvalue * 10);
        sprintf(num,"%X",dec);
        //Join the tmp string with the num string
        strcat(tmp,num);
        strcat(tmp,"\r");
        //Now tmp has the correct command
      }else if(dec > 0 && dec < 16){
        //Value is within range -> convert to weird hex reprsentive
        strcpy(tmp,"*W0120000");
        //Convert Decimal Value to hex
        sprintf(num,"%X",dec);
        //Join the tmp string with the num string
        strcat(tmp,num);
        strcat(tmp,"\r");
        //Now tmp has the correct command
      }else if(dec >=256 && dec < 4096){
        //Value is within range -> convert to weird hex reprsentive
        strcpy(tmp,"*W01200");
        //Convert Decimal Value to hex
        sprintf(num,"%X",dec);
        //Join the tmp string with the num string
        strcat(tmp,num);
        strcat(tmp,"\r");
        //Now tmp has the correct command
      }else{
        SYSTEM_LOG("Omega_Control(): controlvalue out of range");
        if(Release_OMG_Connection() != TRUE){
          SYSTEM_LOG("Omega_Control(): Failed to release serial port connection.");
        }
        return FALSE;
      }
      strcpy(buf,tmp);
      sprintf(str,"Omega Command: %s",buf);
      SYSTEM_LOG(str);
      if (Send_OMG_Command(buf) != TRUE) {
        SYSTEM_LOG("Omega_Control(): Send_OMG_Command() failed.");
        if(Release_OMG_Connection() != TRUE){
          SYSTEM_LOG("Omega_Control(): Failed to release serial port connection.");
        }
        return FALSE;
      }
      if(Release_OMG_Connection() != TRUE){
    	SYSTEM_LOG("Omega_Control(): Failed to release serial port connection.\n");
    	return FALSE;
      }
      SYSTEM_LOG("Closed Serial Port");
      //Make sure the setpoint took
      //Pause for a second
      sleep(1);
      //Initialize the connection
      if(Connect_OMG() != TRUE) {
	SYSTEM_LOG("Omega_Control(): Connect_OMG() failed.");
        if(Release_OMG_Connection() != TRUE){
      	   SYSTEM_LOG("Omega_Control(): Failed to release serial port connection.");
        }
        return FALSE;
      }

      // Prepare the timeout mechanism
      tv.tv_sec=ASC_TIMEOUT;
      tv.tv_usec=0;
      FD_ZERO(&rfds);
      FD_SET(omega_fd,&rfds);

      strcpy(buf,"*R01\r");
      sprintf(str,"Omega Read Setpoint Command: %s",buf);
      SYSTEM_LOG(str);
      if (Send_OMG_Command(buf) != TRUE) {
        SYSTEM_LOG("Omega_Control(): Send_OMG_Command() failed.");
        if(Release_OMG_Connection() != TRUE){
          SYSTEM_LOG("Omega_Control(): Failed to release serial port connection.");
        }
        return FALSE;
      }
      if(!select(omega_fd+1,&rfds,NULL,NULL,&tv)){
        SYSTEM_LOG("Omega_Control() Timeout");
        if(Release_OMG_Connection() != TRUE){
          SYSTEM_LOG("Omega_Control(): Failed to release serial port connection.");
        }
        return FALSE;
      }
      //SYSTEM_LOG("Waiting for return from serial port");
      //Transfer the stored characters from the serial port to the variable buffer
      res=read(omega_fd,buf,255);
      if(res<0){
        perror(OMEGA_PORT);
        SYSTEM_LOG("Omega_Control(): read() failed");
        if(Release_OMG_Connection() != TRUE){
          SYSTEM_LOG("Omega_Control(): Failed to release serial port connection.");
        }
        return FALSE;
      }
      buf[res]=0;

      char *cmp  = tmp + 4;
      strcpy(tmp,cmp);

      //printf("Size of Compare = %d and the tmp string: %s\n",sizeof(tmp),tmp);
      //printf("Size of buf = %d and the buf string: %s\n",sizeof(buf),buf);

      if(strncmp(buf,tmp,sizeof(tmp)) == 0){
        sprintf(str,"Omega Setpoint Successful: Returned Value: %s",buf);
        SYSTEM_LOG(str);
        break;
      }else{
        sprintf(str,"Omega Setpoint Not Set: Returned Value: %s",buf);
        SYSTEM_LOG(str);
        if(Release_OMG_Connection() != TRUE){
          SYSTEM_LOG("Omega_Control(): Failed to release serial port connection.");
        }
        return FALSE;
      }
      break;
    default:
      SYSTEM_LOG("Omega_Control(): Invalid function parameter");
  }
  if(Release_OMG_Connection() != TRUE){
    SYSTEM_LOG("Omega_Control(): Failed to release serial port connection.\n");
    return FALSE;
  }
  return TRUE;
}
