#!/bin/sh

# This service only runs as root
[ "$(id -u)" = 0 ] && {
    PreCmd="/bin/su - ${USER} -c"
} || {
      echo $0 must run as root.
    exit 1
}

logger -t imagerd_rt -p local6.notice "init script $0 called with $# argument(s): $*"

# Source function library & LSB routines
. /etc/rc.d/init.d/functions

log_success_msg () {
    echo -n $*; success "$*"; echo
}
log_failure_msg () {
    echo -n $*; failure "$*"; echo
}
log_warning_msg () {
    echo -n $*; warning "$*"; echo
}


# Autoconfigure according to script name
PROGNAME="imagerd_rt"

ROOTDIR=/usr/local/imagerd_rt/
LOCKFILE=/dev/shm/imagerd_rt/imagerd_rt.pid
STATFILE=/var/log/${PROGNAME}.log
PROGFILE=${ROOTDIR}${PROGNAME}
MODEFILE=${ROOTDIR}aux/stop_file

source /opt/pleora/ebus_sdk/x86_64/bin/set_puregev_env


if [ ! -e $PROGFILE ]; then
  logger -s -t imager -p local6.error "Error- executable not found: $PROGFILE"
  exit 1
fi

USERNAME=`id -un`
umask 002


start_service() {
  if [ -e ${LOCKFILE} ]; then
    log_failure_msg "Lockfile exists, not starting"
    return 1
  fi

  log_success_msg  "Starting $PROGNAME"
  #start-stop-daemon --start --chuid root:root --nicelevel 2 --pidfile $LOCKFILE --startas $PROGFILE &
  daemon -2 --user=root --pidfile $LOCKFILE $PROGFILE &
  #echo -e $?

  return $?
}


stop_service() {

  log_success_msg "Stopping $PROGNAME"

  if [ ! -e ${LOCKFILE} ]; then
    echo -e "Cannot find lockfile: ${PROGNAME}"
    echo -e "${PROGNAME} stopped"
    #echo -e $?
    return 1
  fi

  pid=$(/bin/cat ${LOCKFILE})
  PSINFO=$(/bin/ps -p $pid -o pid,user,group,args)
  if [ $(echo $PSINFO | grep -c ${PROGNAME}) -ne 1 ]; then
    echo -e "${PROGNAME} is stopped but lockfile exists: ${LOCKFILE}"
    log_success_msg "Removing lockfile: ${LOCKFILE}"
    rm -f ${LOCKFILE}
    log_success_msg "${PROGNAME} already stopped"
    echo -e $?
    return 1
  fi

  # TERM first, then KILL if not dead
  echo "10" > ${MODEFILE}

	echo -e "${PROGNAME} will stop after it completes
  its imaging schedule, this can take some time.
  Use /etc/init.d/${PROGNAME} status to check if ${PROGNAME}
  has stopped and if it does not stop use /etc/init.d/${PROGNAME}
  nasty to kill it."

  log_success_msg ""

  return 0
}

status_service() {

  if [ -e ${LOCKFILE} ]; then
    PID=$(/bin/cat ${LOCKFILE})
    PSINFO=$(/bin/ps -p $PID -o pid,user,group,args)
    if [ $(echo $PSINFO | grep -c ${1}) -eq 1 ]; then
      echo -e "${1} is running as process ${PID}"
    else
      echo -e "${1} is stopped but lockfile exists: ${LOCKFILE} - run unlock or stop again to remove pid file"
    fi
  else
    log_success_msg "${1} is stopped"
  fi

	if [ -e ${MODEFILE} ]; then
		MODE=$(/bin/cat ${MODEFILE})
		case $MODE in
			"10" )
				echo -e "${1} mode is ${MODE}: imagerd is scheduled to terminate. Run start to execute imagerd and change mode to 11-image/resume.\n\n 10 - terminate execution \n 11 -	image/resume\n";;

			"11" )
				echo -e "${1} mode is ${MODE}: imagerd is in imaging mode.\n\n 10 - terminate execution \n 11 -	image/resume \n" ;;
		esac
	else
			echo -e "${1}: mode file does not exist, cannot report mode imager is in" ;
  fi

	return 0
}

RETVAL=0			;# 0 for success, 1 for failure

case "$1" in
 start) start_service;;
 stop)  stop_service;;
 restart) stop_service; start_service;;
 status) status_service $PROGNAME;;
 unlock) /bin/rm -f ${LOCKFILE};;
 monitor)
   if [ -n ${STATFILE} ]; then
     watch tail -n 20 ${STATFILE}
   else
     echo "Error- could not find status file"
     RETVAL=1
   fi
   ;;
 *)
   echo "Usage: $0
	 {start|stop|restart|status}"
   RETVAL=1
   ;;
esac

exit $RETVAL
